// OS.c
// Runs on TM4C123 
// Create and manage threads.
// Brandon Boesch
// Ce Wei
// January 25, 2016

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Interrupts.h"
#include "OS.h"

#define PF1  (*((volatile uint32_t *)0x40025008))
#define EIGHTY_MHZ 80000000

void (*PeriodicTask)(void);   // user function

volatile uint32_t Count = 0;  // debug interrupt counter

// ***************** OS_AddPeriodicThread ******************
// Activate TIMER1 interrupts to run user task periodically.
// Inputs:  task - A pointer to a user function.
//          freq - Frequency of user task
//          pri  - Priority of timer [0:7]. 0 = highest priority.
// Outputs: none
void OS_AddPeriodicThread(void(*task)(void), uint32_t freq, uint8_t pri){
	uint32_t period = EIGHTY_MHZ/freq;
	long sr = StartCritical();
  SYSCTL_RCGCTIMER_R |= 0x02;   // 0) activate TIMER1
  PeriodicTask = task;          // user function
  TIMER1_CTL_R = 0x00000000;    // 1) disable TIMER1A during setup
  TIMER1_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER1_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER1_TAILR_R = period-1;    // 4) reload value
  TIMER1_TAPR_R = 0;            // 5) bus clock resolution
  TIMER1_ICR_R = 0x00000001;    // 6) clear TIMER1A timeout flag
  TIMER1_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|(pri << 13); // 8) priority
  NVIC_EN0_R = 1<<21;           // 9) enable IRQ 21 in NVIC
  TIMER1_CTL_R = 0x00000001;    // 10) enable TIMER1A
	EndCritical(sr);
}

// ***************** Timer1A_Handler ******************
void Timer1A_Handler(void){
	// PF1 = 0x02;                     // turn on debugging LED
  TIMER1_ICR_R = TIMER_ICR_TATOCINT; // acknowledge TIMER1A timeout
	long sr = StartCritical();
	Count = Count + 1;                 // debug interrupt counter
	EndCritical(sr);
  (*PeriodicTask)();                 // execute user task
	// PF1 = 0x00;	                   // turn off debugging LED
}

// ***************** Timer1A_Enable ******************
void Timer1A_Enable(void){
	NVIC_EN0_R = 1<<21;                // enable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000001;         // enable TIMER1A
}

// ***************** Timer1A_Disable ******************
void Timer1A_Disable(void){
	NVIC_DIS0_R = 1<<21;               // disable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000000;         // disable TIMER1A
}

// ***************** OS_ReadPeriodicTime ******************
unsigned long OS_ReadPeriodicTime(void){
	return Count;
}

// ***************** OS_ClearPeriodicTime ******************
void OS_ClearPeriodicTime(void){
	long sr = StartCritical();
	Count = 0;
	EndCritical(sr);
}
