// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// January 30, 2016

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "Interpreter.h"
#include "UART.h"
#include "ST7735.h"
#include "EdgeInterrupt.h"
#include "ADC.h"
#include "LED.h"
#include "OS.h"

// *************Global Variables and Stucts*****************************
typedef struct{
  const char *name;
  void (*func)(void);
	const char *tag;
}cmdTable;

cmdTable Commands[] = {        // CMD_LEN defines max command name length
	{"help",     cmdHelp,        "Displays all available commands."},
	{"clear",    cmdClear,       "Clears the LCD screen."},
  {"lcd",      cmdLCD,         "Tests the LCD display."},
  {"adc",      cmdADC,         "Reads one sample from ADC."},
	{"collect",  cmdCollect,     "Collects multiple samples from ADC and plots results."},
	{"add",      cmdAddThread,   "Adds a periodic thread."},
	{"read",     cmdRead,        "Reads the global variable 'Count'."}, 
  {"reset",    cmdReset,       "Resets the global variable 'Count' to zero."},
	{"enable",   cmdEnableTask,  "Re-enables periodic task. Task must be initalized first."},
	{"disable",  cmdDisableTask, "Disables periodic task. Task must be initalized first."},
};

uint16_t DataBuffer[SIZE_OF_BUFF];     // buffer used in ADC_Collect() function
// *********************************************************************

//------------parse-----------------
// Compare user's input with table of 
// available commands.
void parse(const char *input){
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(input,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
	UART_OutStringNL("Command not recognized");
	return;
}

//------------dummy-------------------------
// Function that does nothing.  
// Used to measure ISR run time.
void dummy(void){};

	
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
	UART_OutStringNL("Below is a list of availble commands:");
	for(int i = 0; i < NUM_OF(Commands); i++){
		if(i+1 <10) UART_OutString("    ");              // ****************
		else if(i+1 < 100) UART_OutString("   ");       // output formating
		UART_OutUDec(i+1);                            // 
		UART_OutString(") ");                         // ****************
		UART_OutString((char*)Commands[i].name);      // display command name
		for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
		  UART_OutString("-");                        // output formating
		}
    UART_OutStringNL((char*)Commands[i].tag);     // display command tag
	}
}


//------------cmdClear-----------------
// Clears the LCD screen.
void cmdClear(void){
	ST7735_FillScreen(ST7735_BLACK);
	UART_OutStringNL("Clear LCD screen complete"); 
}


//------------cmdLCD-----------------
// Tests the LCD screen. Progress through test by pressing SW1.
void cmdLCD(void){
	UART_OutStringNL("Press SW1 to cycle through test.");  
  ST7735_Message(0, 0, "LCD Test ", 42);
	for(int i = 1; i <= 7; i++){               // tests top portion of screen
		while(!Edge_SW1){}                       // waits for input from SW1
		Edge_SW1 = false;                        // clear interrupt semaphore
		ST7735_Message(0, i, "Number = ", i);
	}
	for(int i = 7; i >= 0; i--){               // tests bottom portion of screen            
		while(!Edge_SW1){}                       // waits for input from SW1
		Edge_SW1 = false;                        // clear interrupt semaphore
		ST7735_Message(1, i, "Woo-Hoo!!!", 42);
	}
	UART_OutStringNL("LCD test complete.");
}


//------------cmdADC-----------------
// Tests the ADC.
void cmdADC(void){
	UART_OutStringNL("Which ADC port do you wish to sample from? [0:11]");
	UART_OutString("--Enter Port #--> ");
	uint32_t port = UART_InUDec();
	UART_OutCRLF();
	ADC_InitSWTrigger(port);       // initialize choosen ADC channel
  int16_t sample = ADC_In();     // read from port
	UART_OutString("ADC Port_");
	UART_OutUDec(port);
	UART_OutString(" = ");
	UART_OutUDecNL(sample);
	UART_OutStringNL("ADC test complete.");
}


//------------cmdCollect-----------------
// Collects multiple samples on the ADC and
// displays the results on a Voltage vs Time plot.
void cmdCollect(void){
	UART_OutStringNL("Which ADC port do you wish to sample from? [0:11]");
	UART_OutString("--Enter Port #--> ");
	uint32_t port = UART_InUDec();
	UART_OutCRLF();
	UART_OutStringNL("What frequency[Hz] would you like to sample at? [100:10000]");
	UART_OutString("--Enter Frequency--> ");
	uint32_t freq = UART_InUDec();
	UART_OutCRLF();
	UART_OutString("How many samples would you like to store? [1:");
  UART_OutUDec(SIZE_OF_BUFF);
	UART_OutStringNL("]");
	UART_OutString("--Enter Samples--> ");
	uint32_t samples = UART_InUDec();
	UART_OutCRLF();
	ADC_Collect(port, freq, DataBuffer, samples);
	ST7735_SetCursor(0,0);
	ST7735_OutString("ADC Voltage Vs. Time");
	ST7735_PlotClear(0,63);     // graph range from 0 to 63
	for(int i = 0; i < samples; i++){
	  ST7735_PlotLine(DataBuffer[i]>>6);   // divide by 64 to fit screen
		ST7735_PlotNextErase();
	}
	UART_OutStringNL("Collect samples complete.");
}


//------------cmdAddThread-----------------
// Adds a periodic thread to the system
void cmdAddThread(void){
	void (*task)(void);
	UART_OutStringNL("Which task would you like to add: ");
	UART_OutStringNL("    (1) LED_RedToggle");
	UART_OutStringNL("    (2) LED_GreenToggle");
  UART_OutStringNL("    (3) LED_BlueToggle");
	UART_OutStringNL("    (4) LED_AllOff");
	UART_OutStringNL("    (5) dummy");
	UART_OutString("--Enter Task #--> ");
	uint32_t num = UART_InUDec();
	UART_OutCRLF();
	switch (num){
		case(1): task = &LED_RedToggle; break;
		case(2): task = &LED_GreenToggle; break;
		case(3): task = &LED_BlueToggle; break;
		case(4): task = &LED_AllOff; break;
		case(5): task = &dummy; break;
		default: UART_OutStringNL("Improper selection"); return;
	}
  UART_OutStringNL("What frequency[Hz] should the task to run at? [0:80,000,000");
	UART_OutString("--Enter Frequency--> ");
	uint32_t freq = UART_InUDec();
	UART_OutCRLF();
	UART_OutStringNL("What priority should the task to run at?");
	UART_OutString("--Enter Priority--> ");
	uint8_t pri = UART_InUDec();
	UART_OutCRLF();
	OS_AddPeriodicThread(task, freq, pri);
  UART_OutStringNL("Add thread complete.");
}


//------------cmdAddThread-------------------------
// Reads the current value of global variable Count
void cmdRead(void){
	UART_OutString("Current value of Count = ");
	UART_OutUDecNL(OS_ReadPeriodicTime());
	UART_OutStringNL("Read command complete.");
}


//------------cmdReset-------------------------
// Resets the value of global variable Count
void cmdReset(void){
	OS_ClearPeriodicTime();
	UART_OutStringNL("Reset of global 'Count' complete.");
}


//------------cmdEnableTask-------------------------
// Re-enables the task's Timer. Task must be initialized first.
void cmdEnableTask(void){
	Timer1A_Enable();
	UART_OutStringNL("Task enabled complete.");
}


//------------cmdDisableTask-------------------------
// Disables the task's Timer. Task must be initialized first.
void cmdDisableTask(void){
  Timer1A_Disable();
	UART_OutStringNL("Task disabled complete.");
}
