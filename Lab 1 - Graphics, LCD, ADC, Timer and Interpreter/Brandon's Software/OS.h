// OS.h
// Runs on TM4C123 
// Create and manage threads.
// Brandon Boesch
// Ce Wei
// January 25, 2016

// ***************** OS_AddPeriodicThread ******************
// Activate TIMER1 interrupts to run user task periodically.
// Inputs:  task - A pointer to a user function.
//          freq - Frequency of user task
//          pri  - Priority of timer [0:7]. 0 = highest priority.
// Outputs: none
void OS_AddPeriodicThread(void(*task)(void), uint32_t period, uint8_t pri);

void Timer1A_Enable(void);

void Timer1A_Disable(void);

unsigned long OS_ReadPeriodicTime(void);

void OS_ClearPeriodicTime(void);
