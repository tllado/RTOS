// Lab1.c
// Runs on TM4C123 
// Implements an interpreter using the serial port and interrupting I/O.  We also
// created test systems for the LCD driver, ADC driver, and a 32-bit timer interrupt.
// Brandon Boesch
// Ce Wei
// January 22, 2016

#include <stdint.h>


#include "PLL.h"
#include "UART.h"
#include "ST7735.h"
#include "Interrupts.h"
#include "EdgeInterrupt.h"
#include "LED.h"
#include "Interpreter.h"


int main(void){
  PLL_Init(Bus80MHz);                  // set system clock to 80 MHz
	LED_Init();
	UART_Init();                         // initialize UART
	Edge_Init();                         // initialize GPIO Port F interrupt
  ST7735_InitR(INITR_REDTAB);          // initialize ST7735 LCD
	EnableInterrupts();
	
	UART_OutCRLF();
	UART_OutCRLF();
	UART_OutStringNL("******************************************************");
	UART_OutStringNL("******************************************************");
	UART_OutStringNL("Welcome to your OS.");
	UART_OutStringNL("Type \"help\" for a list of commands.");
	char cmd[CMD_LEN+1];                 // string to store command line inputs. +1 for null.
  while(1){
		UART_OutCRLF();
		UART_OutString("--Enter Command--> ");
		UART_InStringNL(cmd, CMD_LEN);  
		parse(cmd);
	}
}	









