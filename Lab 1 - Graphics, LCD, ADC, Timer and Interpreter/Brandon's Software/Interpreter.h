// Interpreter.h
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// January 30, 2016

#define CMD_LEN 10                           // maximum number of letters allowed for command names.
#define SIZE_OF_BUFF 128                     // size of data buffer used in ADC_Collect().
#define NUM_OF(x) (sizeof(x)/sizeof(x[0]))   // counts the # of available commands.


//------------parse-----------------
// Compare user's input with table of 
// available commands.
void parse(const char *input);

//------------dummy-------------------------
// Function that does nothing.  
// Used to measure ISR run time.
void dummy(void);

//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void);

//------------cmdClear-----------------
// Clears the LCD screen.
void cmdClear(void);
	
//------------cmdLCD-----------------
// Tests the LCD screen. Progress through test by pressing SW1.
void cmdLCD(void);

//------------cmdADC-----------------
// Tests the ADC.
void cmdADC(void);

//------------cmdCollect-----------------
// Collects multiple samples on the ADC and
// displays the results on a Voltage vs Time plot.
void cmdCollect(void);

//------------cmdAddThread-----------------
// Adds a periodic thread to the system
void cmdAddThread(void);

//------------cmdAddThread-------------------------
// Reads the current value of global variable Count
void cmdRead(void);

//------------cmdReset-------------------------
// Resets the value of global variable Count
void cmdReset(void);

//------------cmdEnableTask-------------------------
// Re-enables the task's Timer. Task must be initialized first.
void cmdEnableTask(void);

//------------cmdDisableTask-------------------------
// Disables the task's Timer. Task must be initialized first.
void cmdDisableTask(void);
