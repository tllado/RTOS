// Timer1.c
// Runs on TM4C123 
// Use Timer3 in 32-bit periodic mode to request interrupts at a periodic rate
// Brandon Boesch
// Ce Wei
// February 21, 2016


// ***************** Timer3_Init ****************
// Activate Timer3 interrupts periodically.  Timer1 is in charge of handling
// background threads in the OS.  Timer1 has highest priority
// Inputs:  period in units (1/clockfreq)
// Outputs: none
void Timer1_Init(unsigned long period);


// ***************** Timer1A_Handler ******************
void Timer1A_Handler(void);


// ***************** Timer1A_Enable ******************
void Timer1A_Enable(void);


// ***************** Timer1A_Disable ******************
void Timer1A_Disable(void);
