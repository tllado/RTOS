// Interpreter.h
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// January 30, 2016

#define CMD_LEN 10                           // maximum number of letters allowed for command names.
#define NUM_OF(x) (sizeof(x)/sizeof(x[0]))   // counts the # of available commands.


// ************* Stucts ****************************************************
typedef struct{
  const char *name;
  void (*func)(void);
	const char *tag;
}cmdTable;
// **************************************************************************


//------------Interpreter_Init-------
// Initialize interpreter
void Interpreter_Init(void);

//------------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void);

//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void);

//------------cmdClear-----------------
// Clears the LCD screen.
void cmdClear(void);

//------------cmdPrintP-----------------
// print the following performance measures: 
// time-jitter, number of data points lost, number of calculations performed
// i.e., NumSamples, Num4Ground, MaxJitter, DataLost, FilterWork, PIDwork
void cmdPrintP(void);

//------------cmdPrintD-----------------
// print the following debugging parameters: 
// x[], y[] 
void cmdPrintD(void);

//------------cmdProfile-----------------
// Menu for profiler. 
// Includes the following commands:
//   clear - Clears the histogram profile
//   restart - Restarts the profiling process
//   print - Print the profile results
void cmdProfile(void);
