// Lab3.c
// Runs on TM4C123
// Real Time Operating System for Labs 3
// Brandon Boesch
// Ce Wei
// February 21, 2016

// Jonathan W. Valvano 1/31/14, valvano@mail.utexas.edu
// EE445M/EE380L.6 
// You may use, edit, run or distribute this file 
// You are free to change the syntax/organization of this file

// LED outputs to logic analyzer for OS profile 
// PF1 is preemptive thread switch
// PF2 is periodic task, samples PD3
// PF3 is SW1 task (touch PF4 button)

// Button inputs
// PF0 is SW2 task (Lab3)
// PF4 is SW1 button input

// Analog inputs
// PD3 Ain3 sampled at 2k, sequencer 2, by DAS software start in ISR
// PD2 Ain5 sampled at 250Hz, sequencer 3, by Producer, timer tigger

//************Timer Resources*******************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - OS_AddPeriodicThread(), Os_AddPeriodicThread() ; priority = 0
//  Timer2A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//  Timer3A - OS_Sleep() ; priority = 3
//**********************************************

#include <string.h> 
#include <stdint.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab3.h"
#include "ST7735.h"
#include "UART.h"
#include "OS.h"
#include "ADC.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Buttons.h"

//********************Globals***********************************************
long x[64],y[64];            // input and output arrays for FFT
unsigned long PIDWork;       // current number of PID calculations finished
unsigned long FilterWork;    // number of digital filter calculations finished
unsigned long NumSamples;    // incremented every ADC sample, in Producer
unsigned long DataLost;      // data sent by Producer, but not received by Consumer
long MaxJitter;              // largest time jitter between interrupts in usec for periodic task in Main
long MaxJitterA;             // largest time jitter between interrupts in usec for TaskA in testmain5 
long MaxJitterB;             // largest time jitter between interrupts in usec for TaskB in testmain5
long *MaxJitterAPt;          // pointer to MaxJitterA. 
long *MaxJitterBPt;          // pointer to MaxJitterB. 
unsigned long const JitterSize=JITTERSIZE;
unsigned long JitterHistogram[JITTERSIZE]={0,};   // Jitter histogram for periodic task in main
unsigned long JitterHistogramA[JITTERSIZE]={0,};  // Jitter histogram for TaskA in testmain5
unsigned long JitterHistogramB[JITTERSIZE]={0,};  // Jitter histogram for TaskB in testmain5
unsigned long (*JitterHistogramAPt)[JITTERSIZE];  // pointer to JitterHistogramA
unsigned long (*JitterHistogramBPt)[JITTERSIZE];  // pointer to JitterHistogramB
bool FirstPassCompleteA;     // jitter control variable, used for task A in Testmain5
bool FirstPassCompleteB;     // jitter control variable, used for task B in Testmain5
bool *FirstPassCompleteAPt;  // pointer to FirstPassCompleteA. Used in Testmain5
bool *FirstPassCompleteBPt;  // pointer to FirstPassCompleteB. Used in Testmain5
unsigned long LastTimeA;     // time since taskA ran last. Used in Testmain5
unsigned long LastTimeB;     // time since taskB ran last. Used in Testmain5
unsigned long *LastTimeAPt;  // pointer to LastTimeA. Used in Testmain5
unsigned long *LastTimeBPt;  // pointer to LastTimeB. Used in Testmain5
//**************************************************************************


//------------------Task 1--------------------------------
// 2 kHz sampling ADC channel 1, using software start trigger
// background thread executed at 2 kHz
// 60-Hz notch high-Q, IIR filter, assuming fs=2000 Hz
// y(n) = (256x(n) -503x(n-1) + 256x(n-2) + 498y(n-1)-251y(n-2))/256 (2k sampling)
// y(n) = (256x(n) -476x(n-1) + 256x(n-2) + 471y(n-1)-251y(n-2))/256 (1k sampling)
long Filter(long data){
static long x[6]; // this MACQ needs twice
static long y[6];
static unsigned long n=3;   // 3, 4, or 5
  n++;
  if(n==6) n=3;     
  x[n] = x[n-3] = data;  // two copies of new data
  y[n] = (256*(x[n]+x[n-2])-503*x[n-1]+498*y[n-1]-251*y[n-2]+128)/256;
  y[n-3] = y[n];         // two copies of filter outputs too
  return y[n];
} 


//******** DAS *************** 
// periodic background thread, calculates 60Hz notch filter
// runs 2000 times/sec
// samples channel 4, PD3,
// inputs:  none
// outputs: none
unsigned long DASoutput;
void DAS(void){ 
  unsigned long input;  
  unsigned static long LastTime;  // time at previous ADC sample
  unsigned long thisTime;         // time at current ADC sample
  long jitter;                    // time between measured and expected, in us
  if(NumSamples < RUNLENGTH){     // finite time ran
    #ifdef PROFILER		
    PE0 ^= 0x01;
    #endif		
    input = ADC_In();             // channel set when calling ADC_InitSWTrigger
    #ifdef PROFILER		
    PE0 ^= 0x01;
    #endif		
    thisTime = OS_Time();         // current time, 12.5 ns
    DASoutput = Filter(input);
    FilterWork++;        // calculation finished
    if(FilterWork>1){    // ignore timing of first interrupt
      unsigned long diff = OS_TimeDifference(LastTime,thisTime);
      if(diff>PERIOD){
        jitter = (diff-PERIOD+4)/8;  // in 0.1 usec
      }
			else{
        jitter = (PERIOD-diff+4)/8;  // in 0.1 usec
      }
      if(jitter > MaxJitter){
        MaxJitter = jitter; // in usec
      }       // jitter should be 0
      if(jitter >= JitterSize){
        jitter = JITTERSIZE-1;
      }
      JitterHistogram[jitter]++; 
    }
    LastTime = thisTime;
    #ifdef PROFILER		
    PE0 ^= 0x01;
    #endif		
  }
}
//--------------end of Task 1-----------------------------


//------------------Task 2--------------------------------
// background thread executes with SW1 & SW2 button
// one foreground task created with button push
// foreground treads run for 2 sec and die
// ***********ButtonWork*************
void ButtonWork(void){
unsigned long myId = OS_Id(); 
  #ifdef PROFILER	
  PE1 ^= 0x02;
  #endif
  ST7735_Message(1,0,"Num4Ground =",Num4Ground); 
  #ifdef PROFILER	
  PE1 ^= 0x02;
  #endif
  OS_Sleep(50);     // set this to sleep for 50msec
  ST7735_Message(1,1,"PIDWork     =",PIDWork);
  ST7735_Message(1,2,"DataLost    =",DataLost);
  ST7735_Message(1,3,"Jitter 0.1us=",MaxJitter);
  #ifdef PROFILER	
  PE1 ^= 0x02;
  #endif
  OS_Kill();  // done, OS does not return from a Kill
} 


//************SW1Push*************
// Called when SW1 Button pushed.
// Adds another foreground task
// background threads execute once and return
void SW1Push(void){
  if(OS_MsTime() > 20){ // debounce
    OS_AddThread(&ButtonWork,100,4);
    OS_ClearMsTime();  // at least 20ms between touches
  }
}


//************SW2Push*************
// Called when SW2 Button pushed.
// Adds another foreground task
// background threads execute once and return
void SW2Push(void){
  if(OS_MsTime() > 20){ // debounce
    OS_AddThread(&ButtonWork,100,4);
    OS_ClearMsTime();  // at least 20ms between touches
  }
}
//--------------end of Task 2-----------------------------


//------------------Task 3--------------------------------
// hardware timer-triggered ADC sampling at 400Hz
// Producer runs as part of ADC ISR
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox
// Display thread updates LCD with measurement

//******** Producer *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, started by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is ready
// Your ADC ISR calls this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer(unsigned long data){  
  if(NumSamples < RUNLENGTH){   // finite time run
    NumSamples++;               // number of samples
    if(OS_Fifo_Put(data) == 0){ // send to consumer
      DataLost++;
    } 
  } 
}


//******** Consumer *************** 
// foreground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
void Consumer(void){ 
unsigned long data,DCcomponent;   // 12-bit raw ADC sample, 0 to 4095
unsigned long t;                  // time in 2.5 ms
unsigned long myId = OS_Id(); 
	
  ADC_InitHWTrigger(5, FS, &Producer); // start ADC sampling, channel 5, PD2, 400 Hz
  OS_AddThread(&Display,128,0); 
  while(NumSamples < RUNLENGTH){ 
		#ifdef PROFILER	
    PE2 = 0x04;
		#endif
    for(t = 0; t < 64; t++){   // collect 64 ADC samples
      data = OS_Fifo_Get();    // get from producer
      x[t] = data;             // real part is 0 to 4095, imaginary part is 0
    }
		#ifdef PROFILER	
    PE2 = 0x00;
		#endif
    cr4_fft_64_stm32(y,x,64);  // complex FFT of last 64 ADC values
    DCcomponent = y[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
		OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
  }
	ADC_Seq3Disable();     // Turn off ADC seq3 
  OS_Kill();  // done
}


//******** Display *************** 
// foreground thread, accepts data from consumer
// displays calculated results on the LCD
// inputs:  none                            
// outputs: none
void Display(void){ 
unsigned long data,voltage;
  ST7735_Message(0,1,"Run length = ",(RUNLENGTH)/FS);   // top half used for Display
  while(NumSamples < RUNLENGTH) { 
    data = OS_MailBox_Recv();
    voltage = 3000*data/4095;               // calibrate your device so voltage is in mV
		#ifdef PROFILER	
    PE3 = 0x08;
		#endif
    ST7735_Message(0,2,"v(mV) =",voltage);  
		#ifdef PROFILER
    PE3 = 0x00;
		#endif
  } 
  OS_Kill();  // done
} 
//--------------end of Task 3-----------------------------


//------------------Task 4--------------------------------
// foreground thread that runs without waiting or sleeping
// it executes a digital controller 
//******** PID *************** 
// foreground thread, runs a PID controller
// never blocks, never sleeps, never dies
// inputs:  none
// outputs: none
short IntTerm;     // accumulated error, RPM-sec
short PrevError;   // previous error, RPM
short Coeff[3];    // PID coefficients
short Actuator;
void PID(void){ 
  short err;  // speed error, range -100 to 100 RPM
  unsigned long myId = OS_Id(); 
  PIDWork = 0;
  IntTerm = 0;
  PrevError = 0;
  Coeff[0] = 384;   // 1.5 = 384/256 proportional coefficient
  Coeff[1] = 128;   // 0.5 = 128/256 integral coefficient
  Coeff[2] = 64;    // 0.25 = 64/256 derivative coefficient*
  while(NumSamples < RUNLENGTH) { 
    for(err = -1000; err <= 1000; err++){    // made-up data
      Actuator = PID_stm32(err,Coeff)/256;
    }
    PIDWork++;        // calculation finished
  }
  for(;;){ }          // done
}
//--------------end of Task 4-----------------------------


//------------------Task 5--------------------------------
// UART background ISR performs serial input/output
// Two software fifos are used to pass I/O data to foreground
// The interpreter runs as a foreground thread
// The UART driver should call OS_Wait(&RxDataAvailable) when foreground tries to receive
// The UART ISR should call OS_Signal(&RxDataAvailable) when it receives data from Rx
// Similarly, the transmit channel waits on a semaphore in the foreground
// and the UART ISR signals this semaphore (TxRoomLeft) when getting data from fifo
// Modify your intepreter from Lab 1, adding commands to help debug 
// Interpreter is a foreground thread, accepts input from serial port, outputs to serial port

// add the following commands, leave other commands, if they make sense
// 1) print performance measures 
//    time-jitter, number of data points lost, number of calculations performed
//    i.e., NumSamples, Num4Ground, MaxJitter, DataLost, FilterWork, PIDwork
// 2) print debugging parameters 
//    i.e., x[], y[] 
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}    
//--------------end of Task 5-----------------------------


#ifdef MAIN
//*******************final user main DEMONTRATE THIS TO TA**********
int main(void){ 
  OS_Init();           // initialize, disable interrupts
  PortE_Init();

  DataLost = 0;        // lost data between producer and consumer
  NumSamples = 0;
  MaxJitter = 0;       // in 1us units

//********initialize communication channels
  OS_MailBox_Init();
  OS_Fifo_Init(128);    // *note* 4 is not big enough

//*******attach background tasks************************
	//task 1
  ADC_InitSWTrigger(4);                // sequencer 2, channel 4, PD3, sampling in DAS()
  OS_AddPeriodicThread(&DAS,PERIOD,1); // 2 kHz real time sampling of PD3
	
	//task 2
	SW1_Init(&SW1Push,2);
  SW2_Init(&SW2Push,2);  

//********create initial foreground threads************
  //task 3
	OS_AddThread(&Consumer,128,1); 
	
  //task 4
  OS_AddThread(&PID,128,3);            // make this lowest priority

  //task 5
  OS_AddThread(&Interpreter,128,2); 
	
  OS_Launch(TIMESLICE);                // doesn't return, interrupts enabled in here
  return 0;                            // this never executes
}
#endif  // end MAIN


#ifdef TESTMAIN5
//******************* Lab 3 Preparation 2**********
// Modify this so it runs with your RTOS (i.e., fix the time units to match your OS)
// run this with 
// UART0, 115200 baud rate, used to output results 
// SYSTICK interrupts, period established by OS_Launch
// first timer interrupts, period established by first call to OS_AddPeriodicThread
// second timer interrupts, period established by second call to OS_AddPeriodicThread
// SW1 no interrupts
// SW2 no interrupts

#define counts1us 10    // number of OS_Time counts per 1us
#define workA 500       // tA = {5,50,500 us} work in Task A
#define workB 250       // tB = 250 us work in Task B

unsigned long CountA;   // number of times Task A called
unsigned long CountB;   // number of times Task B called
unsigned long Count6;   // number of times thread6 loops
unsigned long Period_TaskA = TIME_1MS;
unsigned long Period_TaskB = 2*TIME_1MS;
bool JitterCalcRun = true;


//*******PseudoWork*************
// simple time delay, simulates user program doing real work
// Input: amount of work in 100ns units (free free to change units
// Output: none
void PseudoWork(unsigned short work){
unsigned short startTime;
  startTime = OS_Time();    // time in 100ns units
  while(OS_TimeDifference(startTime,OS_Time()) <= work){} 
}


//*******JitterInit*************
// initalize global variables and pointers 
// associated with jitter calculations
void JitterInit(void){
	FirstPassCompleteA = false;
	FirstPassCompleteAPt = &FirstPassCompleteA;
  FirstPassCompleteB = false;
	FirstPassCompleteBPt = &FirstPassCompleteB;
	LastTimeAPt = &LastTimeA;
	LastTimeBPt = &LastTimeB;
	MaxJitterA = 0;       // in 1us units
	MaxJitterAPt = &MaxJitterA;
	MaxJitterB = 0;       // in 1us units
	MaxJitterBPt = &MaxJitterB;
	JitterHistogramAPt = &JitterHistogramA;
	JitterHistogramBPt = &JitterHistogramB;
}


//*******JitterCalc*************
// Calculates jitter between subsequent calls of this function
// Input: period - periodic task's period
//        *LastTime - pointer to global that holds the time at previous pass through
//        *FirstPassComplete - Pointer to global bool which determines if loop 
//                             has finished its first iteration yet
//        *JitterHistogram - Pointer to periodic task's Jitter histogram
//        *MaxJitter - Pointer to periodic task's maximum jitter value 
// Output: none
void JitterCalc(unsigned long period,unsigned long *LastTime, bool *FirstPassComplete, unsigned long (*JitterHistogramPt)[JITTERSIZE], long *MaxJitter){
	unsigned long thisTime, diff;
	long jitter;   
	if(JitterCalcRun){
		// calculate jitter between subsequent calls of PseudoWork().
		thisTime = OS_Time();            // current time, 12.5 ns	
		if(*FirstPassComplete){          // ignore timing of first interrupt
			diff = OS_TimeDifference(*LastTime,thisTime);
			if(diff>period){
				jitter = (diff-period+4)/8;  // in 0.1 usec
			}
			else{
				jitter = (period-diff+4)/8;  // in 0.1 usec	
			}
			if(jitter > *MaxJitter){
				*MaxJitter = jitter;         // in 0.1 usec
			}        
			if(jitter >= JitterSize){
				jitter = JITTERSIZE-1;
			}
			(*JitterHistogramPt)[jitter] = (*JitterHistogramPt)[jitter] + 1; 
		}
		*LastTime = thisTime;
		*FirstPassComplete = true;       // first pass complete
	}
}


//*******PrintJitter*************
// prints jitter information
void PrintJitter(void){   
	// Print JitterHistogramA[]
	for(int i = 0; i<JITTERSIZE; i++){
		UART_OutString("JitterHistogramA[");
		UART_OutUDec(i); 
		UART_OutString("] = ");
	  UART_OutUDecNL(JitterHistogramA[i]); 
	}
  UART_OutCRLF();  // newline
	// Print JitterHistogramB[]
	for(int i = 0; i<JITTERSIZE; i++){
		UART_OutString("JitterHistogramB[");
		UART_OutUDec(i); 
		UART_OutString("] = ");
	  UART_OutUDecNL(JitterHistogramB[i]); 
	}
	UART_OutCRLF();  // newline
	UART_OutString("Max jitter for thread A = ");
	UART_OutUDecNL(MaxJitterA); 
	UART_OutString("Max jitter for thread B = ");
	UART_OutUDecNL(MaxJitterB); 
	UART_OutCRLF();  // newline
  UART_OutStringNL("Print jitter data complete"); 
}


void Thread6(void){  // foreground thread
  Count6 = 0;          
  for(;;){
    Count6++; 
    PE0 ^= 0x01;        // debugging toggle bit 0  
  }
}


void Thread7(void){  // foreground thread
  UART_OutString("\n\rEE345M/EE380L, Lab 3 Preparation 2\n\r");
  OS_Sleep(10000);   // 10 seconds  
  JitterCalcRun = false;	// Only thread with write access to global variable.  Does not need to be atomic
  PrintJitter();     // print jitter information
  UART_OutString("\n\r\n\r");
  OS_Kill();
}


void TaskA(void){  // called every {1000, 2990us} in background
  PE1 = 0x02;      // debugging profile  
  CountA++;
	JitterCalc(Period_TaskA, LastTimeAPt, FirstPassCompleteAPt, JitterHistogramAPt, MaxJitterAPt);
  PseudoWork(workA*counts1us); //  do work (100ns time resolution)
	PE1 = 0x00;      // debugging profile  
}


void TaskB(void){  // called every pB in background
  PE2 = 0x04;      // debugging profile  
  CountB++;
	JitterCalc(Period_TaskB, LastTimeBPt, FirstPassCompleteBPt, JitterHistogramBPt, MaxJitterBPt);
  PseudoWork(workB*counts1us); //  do work (100ns time resolution)
	PE2 = 0x00;      // debugging profile  
}


int main(void){       // Testmain5 Lab 3
  PortE_Init();
	JitterInit();       // initalize global variables and pointers associated with jitter calculations
  OS_Init();          // initialize, disable interrupts
  OS_AddThread(&Thread6,128,2); 
  OS_AddThread(&Thread7,128,1); 
  OS_AddPeriodicThread(&TaskA,Period_TaskA,0);  // 1 ms, higher priority
  OS_AddPeriodicThread(&TaskB,Period_TaskB,1);  // 2 ms, lower priority
	
  OS_Launch(TIME_2MS);  // 2ms, doesn't return, interrupts enabled in here
  return 0;             // this never executes
}
#endif // end TESTMAIN5


#ifdef TESTMAIN6
//******************* Lab 3 Preparation 4**********
// Modify this so it runs with your RTOS used to test blocking semaphores
// run this with 
// UART0, 115200 baud rate,  used to output results 
// SYSTICK interrupts, period established by OS_Launch
// first timer interrupts, period established by first call to OS_AddPeriodicThread
// second timer interrupts, period established by second call to OS_AddPeriodicThread
// SW1 no interrupts, 
// SW2 no interrupts
Sema4Type s;            // test of this counting semaphore
unsigned long Count6;   // number of times thread6 loops
unsigned long SignalCount1;   // number of times s is signaled
unsigned long SignalCount2;   // number of times s is signaled
unsigned long SignalCount3;   // number of times s is signaled
unsigned long WaitCount1;     // number of times s is successfully waited on
unsigned long WaitCount2;     // number of times s is successfully waited on
unsigned long WaitCount3;     // number of times s is successfully waited on
#define MAXCOUNT 20000
void OutputThread(void){  // foreground thread
  UART_OutString("\n\rEE345M/EE380L, Lab 3 Preparation 4\n\r");
  while(SignalCount1+SignalCount2+SignalCount3<100*MAXCOUNT){
    OS_Sleep(1000);   // 1 second
    UART_OutString(".");
  }       
  UART_OutString(" done\n\r");
  UART_OutString("Signalled="); UART_OutUDec(SignalCount1+SignalCount2+SignalCount3);
  UART_OutString(", Waited="); UART_OutUDec(WaitCount1+WaitCount2+WaitCount3);
  UART_OutString("\n\r");
  OS_Kill();
}

void Thread6(void){  // foreground thread
  Count6 = 0;          
  for(;;){
    Count6++; 
    PE0 ^= 0x01;        // debugging toggle bit 0  
  }
}

void Wait1(void){  // foreground thread
  for(;;){
    OS_Wait(&s);    // three threads waiting
    WaitCount1++; 
  }
}


void Wait2(void){  // foreground thread
  for(;;){
    OS_Wait(&s);    // three threads waiting
    WaitCount2++; 
  }
}


void Wait3(void){   // foreground thread
  for(;;){
    OS_Wait(&s);    // three threads waiting
    WaitCount3++; 
  }
}


void Signal1(void){      // called every 799us in background
  if(SignalCount1<MAXCOUNT){
    OS_Signal(&s);
    SignalCount1++;
  }
}


// edit this so it changes the periodic rate
void Signal2(void){       // called every 1111us in background
  if(SignalCount2<MAXCOUNT){
    OS_Signal(&s);
    SignalCount2++;
		BackgroundTCBs[1].period += TIME_10US;        // increment period of TCB
  }
}


void Signal3(void){       // foreground
  while(SignalCount3<98*MAXCOUNT){
    OS_Signal(&s);
    SignalCount3++;
  }
  OS_Kill();
}


long add(const long n, const long m){
static long result;
  result = m+n;
  return result;
}


int main(void){      // Testmain6  Lab 3
  volatile unsigned long delay;
  OS_Init();           // initialize, disable interrupts
  delay = add(3,4);
  PortE_Init();
  SignalCount1 = 0;   // number of times s is signaled
  SignalCount2 = 0;   // number of times s is signaled
  SignalCount3 = 0;   // number of times s is signaled
  WaitCount1 = 0;     // number of times s is successfully waited on
  WaitCount2 = 0;     // number of times s is successfully waited on
  WaitCount3 = 0;	    // number of times s is successfully waited on
  OS_InitSemaphore(&s,0);	 // this is the test semaphore
//  OS_AddPeriodicThread(&Signal1,(799*TIME_1MS)/1000,0);   // 0.799 ms, higher priority
//  OS_AddPeriodicThread(&Signal2,(1111*TIME_1MS)/1000,1);  // 1.111 ms, lower priority
////////////////////////////////////////////////
	OS_AddPeriodicThread(&Signal1,(800*TIME_1MS)/1000,0);   // 0.800 ms, higher priority
  OS_AddPeriodicThread(&Signal2,(1000*TIME_1MS)/1000,1);  // 1 ms, lower priority
////////////////////////////////////////////////
	OS_AddThread(&Thread6,128,6);    	// idle thread to keep from crashing
  OS_AddThread(&OutputThread,128,2); 	// results output thread
  OS_AddThread(&Signal3,128,2); 	// signalling thread
  OS_AddThread(&Wait1,128,2); 	// waiting thread
  OS_AddThread(&Wait2,128,2); 	// waiting thread
  OS_AddThread(&Wait3,128,2); 	// waiting thread
 
  OS_Launch(TIME_1MS);  // 1ms, doesn't return, interrupts enabled in here
  return 0;             // this never executes
}
#endif  // end TESTMAIN6


#ifdef TESTMAIN7
//******************* Lab 3 Measurement of context switch time**********
// Run this to measure the time it takes to perform a task switch
// UART0 not needed 
// SYSTICK interrupts, period established by OS_Launch
// first timer not needed
// second timer not needed
// SW1 not needed, 
// SW2 not needed
// logic analyzer on PF1 for systick interrupt (in your OS)
//                on PE0 to measure context switch time


void Thread8(void){   // only thread running
  while(1){
    PE0 ^= 0x01;      // debugging profile  
  }
}


int main(void){       // Testmain7
  PortE_Init();
  OS_Init();          // initialize, disable interrupts
  OS_AddThread(&Thread8,128,2); 
  OS_Launch(TIME_1MS/10); // 100us, doesn't return, interrupts enabled in here
  return 0;               // this never executes
}
#endif  // end TESTMAIN7


#ifdef DEBUG
//******************* Personal testing space************************ 

// *********** Globals*****************
uint32_t SW1Cnt, SW2Cnt;  // how many times aperiodic tasks occur
Sema4Type testSema;       // test semaphore
// *************************************


void PeriodicA(void){    
  PE0 ^= 0x01;      // debugging profile  
}

void PeriodicB(void){    
  PE1 ^= 0x02;      // debugging profile  
}

void PeriodicC(void){    
  PE2 ^= 0x04;      // debugging profile  
}

void PeriodicD(void){    
  PE3 ^= 0x08;      // debugging profile  
}

void ForegroundE(void){
	uint32_t ECnt = 0;
	while(1){
	  ECnt++;
		OS_Kill();
	}
}

void ForegroundA(void){
  uint32_t ACnt = 0;
	while(1){
//		if(ACnt == 0){
//			OS_Wait(&testSema);
//		}
		ACnt++;
		OS_Suspend();
	}
}

void ForegroundB(void){
 uint32_t BCnt = 0;
	while(1){
//		if(BCnt == 0){
//			OS_Signal(&testSema);
//		}
		BCnt++;
		OS_Suspend();
	}
}

void ForegroundC(void){
	uint32_t CCnt = 0;
	while(1){
//		if(CCnt == 0){
//		  OS_Wait(&testSema);
//		}
		CCnt++;
		OS_Suspend();
	}
}

void ForegroundD(void){
	uint32_t DCnt = 0;
	while(1){
//		if(DCnt == 0){
//		  OS_Sleep(4000);
//		}
//		OS_AddThread(&ForegroundE,128,3);
		DCnt++;
		OS_Suspend();
	}
}

void Interpreter2(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}    


void SWTask(void){
	ST7735_Message(0,1,"SW1Cnt = ",SW1Cnt);   // top half used for Display
	ST7735_Message(1,1,"SW2Cnt = ",SW2Cnt);   // top half used for Display
	OS_Kill();
}


void SW1press(void){
	SW1Cnt++;
	OS_AddThread(&SWTask, 128, 2);
}


void SW2press(void){
	SW2Cnt++;
	OS_AddThread(&SWTask, 128, 2);
}


int main(void){
  PortE_Init();
  OS_Init();          // initialize, disable interrupts
	SW1Cnt = 0;
	SW2Cnt = 0;
	OS_InitSemaphore(&testSema,0);	 // test semaphore. initially not ready
  OS_AddThread(&Interpreter2,128,1);
	OS_AddThread(&ForegroundA,128,2);
	OS_AddThread(&ForegroundB,128,3);
	OS_AddThread(&ForegroundC,128,4);
	OS_AddThread(&ForegroundD,128,5);
//  OS_AddPeriodicThread(&PeriodicA,TIME_1MS,2);    
//  OS_AddPeriodicThread(&PeriodicB,2*TIME_1MS,1);  
//	OS_AddPeriodicThread(&PeriodicC,3*TIME_1MS,4);  
//  OS_AddPeriodicThread(&PeriodicD,4*TIME_1MS,3);  
	SW1_Init(&SW1press,2);
  SW2_Init(&SW2press,2);    
	

  OS_Launch(TIMESLICE);  // 2ms, doesn't return, interrupts enabled in here
  return 0;             // this never executes
}
#endif  // end DEBUG
