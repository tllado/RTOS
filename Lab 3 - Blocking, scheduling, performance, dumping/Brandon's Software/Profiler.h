// Profiler.h
// Runs on TM4C123 
// Provides digital output pins that can be attached to a scope to 
// create a profile of the system.
// Brandon Boesch
// Ce Wei
// January 31, 2016

#define PE0  (*((volatile unsigned long *)0x40024004))
#define PE1  (*((volatile unsigned long *)0x40024008))
#define PE2  (*((volatile unsigned long *)0x40024010))
#define PE3  (*((volatile unsigned long *)0x40024020))
	
#define MAX_NUM_EVENTS  100          // maximum number of thread profiles

//**********************Compilter Directives*********************************
//#define PROFILER        // Adds profiler pins PE0-3 when defined
// ************************************************************************************



// ************* Stucts ****************************************************
typedef struct Histogram{
	uint32_t threadId;                // Id of event 
	uint32_t time;                   // execution time of task
	uint32_t background;              // set true if event is a background thread 
}HistoType;
// **************************************************************************


// ************* Extern ****************************************************
extern HistoType Profiler[MAX_NUM_EVENTS];     // create a histogram for the thread profiler
extern uint32_t Event;                                 // index into Profiler
// **************************************************************************


//------------PortE_Init------------
// Initialize PortE pins PE0-3 as outputs
// Input: none
// Output: none
void PortE_Init(void);

//------------Clear------------
// Clears Profiler histogram 
// Input: none
// Output: none
void Clear(void);

//------------Restart------------
// Restarts Profiler histogram 
// Input: none
// Output: none
void Restart(void);

//------------Print------------
// Prints Profiler histogram 
// Input: none
// Output: none
void Print(void);
