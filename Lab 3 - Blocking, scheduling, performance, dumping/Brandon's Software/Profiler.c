// Profiler.c
// Runs on TM4C123 
// Provides digital output pins that can be attached 
// to a scope to create a profile of the system.
// Brandon Boesch
// Ce Wei
// January 31, 2016

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Profiler.h"
#include "OS.h"
#include "UART.h"
#include "Interrupts.h"


// *****************Globals**************************
HistoType Profiler[MAX_NUM_EVENTS];      // create a histogram for the thread profiler
uint32_t Event;                                  // index into Profiler
// **************************************************

//------------PortE_Init------------
// Initialize PortE pins PE0-3 as outputs
// Input: none
// Output: none
void PortE_Init(void){ 
  SYSCTL_RCGCGPIO_R |= 0x10;         // activate port E
	while((SYSCTL_PRGPIO_R&0x10) == 0){}; // allow time for clock to stabilize        
  GPIO_PORTE_DIR_R |= 0x0F;          // make PE3-0 output heartbeats
  GPIO_PORTE_AFSEL_R &= ~0x0F;       // disable alt funct on PE3-0
  GPIO_PORTE_DEN_R |= 0x0F;          // enable digital I/O on PE3-0
  GPIO_PORTE_PCTL_R = ~0x0000FFFF;
  GPIO_PORTE_AMSEL_R &= ~0x0F;;      // disable analog functionality on PE
}


//------------Clear------------
// Clears Profiler histogram 
// Input: none
// Output: none
void Clear(void){
	int32_t status = StartCritical();
	for(int i = 0; i<MAX_NUM_EVENTS; i++){
		Profiler[i].threadId = 0; 
		Profiler[i].time = 0;
    Profiler[i].background = 0;		
	}
	EndCritical(status);
	UART_OutCRLF();
	UART_OutStringNL("Finsihed clearing Profiler histogram");
	UART_OutCRLF();
	UART_OutString("--Enter Command--> ");
	OS_Kill();
}


//------------Restart------------
// Restarts Profiler histogram 
// Input: none
// Output: none
void Restart(void){
	int32_t status = StartCritical();
	Event = 0;
	EndCritical(status);
	UART_OutCRLF();
	UART_OutStringNL("Finsihed restarting Profiler histogram");
	UART_OutCRLF();
	UART_OutString("--Enter Command--> ");
	OS_Kill();
}


//------------Print------------
// Prints Profiler histogram 
// Input: none
// Output: none
void Print(void){
	UART_OutCRLF();
	for(int i = 0; i<MAX_NUM_EVENTS; i++){
		UART_OutString("Profiler[");
		UART_OutUDec(i); 
		UART_OutString("] --> Thread Id = ");
	  UART_OutUDec(Profiler[i].threadId); 
		UART_OutString(", Time = ");
		UART_OutUDec(Profiler[i].time*12.5); 
		UART_OutString(" (ns)");
		if(Profiler[i].background){
			UART_OutStringNL(",  Background");
		}
		else{
			UART_OutCRLF();
		}
	}
	UART_OutCRLF();
	UART_OutStringNL("Finsihed printing Profiler histogram");
	UART_OutCRLF();
	UART_OutString("--Enter Command--> ");
	OS_Kill();	
}
