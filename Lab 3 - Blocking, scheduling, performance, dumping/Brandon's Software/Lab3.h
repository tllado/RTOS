// Lab3.h
// Runs on TM4C123
// Real Time Operating System for Labs 3
// Brandon Boesch
// Ce Wei
// February 21, 2016


//**********************Compilter Directives*********************************
#define MAIN          //<-
//#define TESTMAIN5     //<- Only one of these with an     
//#define TESTMAIN6     //<- arrow should be uncommented 
//#define TESTMAIN7     //<- at any given time     
//#define DEBUG         //<-                     

// ************************************************************************************


// ******************** Constants *******************************************************
#define FS 400                      // producer/consumer sampling
#define RUNLENGTH (20*FS)           // display results and quit when NumSamples==RUNLENGTH
#define PERIOD TIME_500US           // DAS 2kHz sampling period in system time units
#define JITTERSIZE 64    
//**************************************************************************


//********************Prototypes********************************************
void cr4_fft_64_stm32(void *pssOUT, void *pssIN, unsigned short Nbin); // FFT in cr4_fft_64_stm32.s, STMicroelectronics
short PID_stm32(short Error, short *Coeff);  // PID in PID_stm32.s, STMicroelectronics
void Producer(unsigned long data);
void Display(void); 
//**************************************************************************


//********************Externs***********************************************
extern unsigned long NumSamples;    // incremented every ADC sample, in Producer
extern long MaxJitter;              // largest time jitter between interrupts in usec
extern unsigned long DataLost;      // data sent by Producer, but not received by Consumer
extern unsigned long FilterWork;    // number of digital filter calculations finished
extern unsigned long PIDWork;       // current number of PID calculations finished
extern long x[64],y[64];            // input and output arrays for FFT
//**************************************************************************
