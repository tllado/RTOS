// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// January 30, 2016

#include <stdint.h>
#include <string.h>
#include "Interpreter.h"
#include "UART.h"
#include "ST7735.h"
#include "Lab3.h"
#include "OS.h"
#include "Profiler.h"

// *************Global Variables*****************************

cmdTable Commands[] = {        // CMD_LEN defines max command name length
	{"help",     cmdHelp,        "Displays all available commands."},
	{"clear",    cmdClear,       "Clears the LCD screen."},
	{"printp",   cmdPrintP,      "Print out performance measures."},
	{"printd",   cmdPrintD,      "Print out debugging parameters."},
	{"profile", cmdProfile,    "Menu for thread profiler."}, 
};

char cmd[CMD_LEN+1];   // string to store command line inputs. +1 for null.
// *********************************************************************


void Interpreter_Init(void){
	UART_OutCRLF();
	UART_OutCRLF();
	UART_OutStringNL("******************************************************");
	UART_OutStringNL("******************************************************");
	UART_OutStringNL("Welcome to your OS.");
	UART_OutStringNL("Type \"help\" for a list of commands.");
}


//---------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void){
	UART_OutCRLF();
	UART_OutString("--Enter Command--> ");
	UART_InStringNL(cmd, CMD_LEN);  
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(cmd,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
	UART_OutStringNL("Command not recognized");
}

	
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
	UART_OutStringNL("Below is a list of availble commands:");
	for(int i = 0; i < NUM_OF(Commands); i++){
		if(i+1 <10) UART_OutString("    ");           // ****************
		else if(i+1 < 100) UART_OutString("   ");     // output formating
		UART_OutUDec(i+1);                            // 
		UART_OutString(") ");                         // ****************
		UART_OutString((char*)Commands[i].name);      // display command name
		for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
		  UART_OutString("-");                        // output formating
		}
    UART_OutStringNL((char*)Commands[i].tag);     // display command tag
	}
}


//------------cmdClear-----------------
// Clears the LCD screen.
void cmdClear(void){
	ST7735_FillScreen(ST7735_BLACK);
	UART_OutStringNL("Clear LCD screen complete"); 
}


//------------cmdPrintP-----------------
// print the following performance measures: 
// time-jitter, number of data points lost, number of calculations performed
// i.e., NumSamples, Num4Ground, MaxJitter, DataLost, FilterWork, PIDWork
void cmdPrintP(void){
	// NumSamples
	UART_OutString("NumSamples = ");
	UART_OutUDecNL(NumSamples); 
	// Num4Ground
	UART_OutString("Num4Ground = ");
	UART_OutUDecNL(Num4Ground); 
	// MaxJitter
	UART_OutString("MaxJitter = ");
	UART_OutUDecNL(MaxJitter); 
	// DataLost
	UART_OutString("DataLost = ");
	UART_OutUDecNL(DataLost); 
	// FilterWork
	UART_OutString("FilterWork = ");
	UART_OutUDecNL(FilterWork); 
	// PIDwork
	UART_OutString("PIDWork = ");
	UART_OutUDecNL(PIDWork); 
	
  UART_OutStringNL("Print performance measures complete"); 
}
	
	
//------------cmdPrintD-----------------
// print the following debugging parameters: 
// x[], y[] 
void cmdPrintD(void){
	// x[]
	for(int i = 0; i<64; i++){
		UART_OutString("x[");
		UART_OutUDec(i); 
		UART_OutString("] = ");
	  UART_OutUDecNL(x[i]); 
	}
  UART_OutCRLF();  // newline
	// y[]
	for(int i = 0; i<64; i++){
		UART_OutString("y[");
		UART_OutUDec(i); 
		UART_OutString("] = ");
	  UART_OutUDecNL(y[i]); 
	}
  UART_OutStringNL("Print debug parameters complete"); 
}
	

//------------cmdProfile-----------------
// Menu for profiler. 
// Includes the following commands:
//   clear - Clears the histogram profile
//   restart - Restarts the profiling process
//   print - Print the profile results
void cmdProfile(void){
	UART_OutStringNL("Choose a command for the profiler:");
	UART_OutStringNL("    (1) Clear - Clears the histogram profile");
	UART_OutStringNL("    (2) Restart - Restarts the profiling process");
	UART_OutStringNL("    (3) Print - Print the profile results");
	UART_OutString("--Enter Command #--> ");
	uint32_t num = UART_InUDec();
	UART_OutCRLF();
	void (*task)(void);
	switch (num){
	  case(1): task = &Clear; break;
		case(2): task = &Restart; break;
		case(3): task = &Print; break;
		default: UART_OutStringNL("Improper selection"); return;
	}		
	OS_AddThread(task, 128, 7);
}
	
