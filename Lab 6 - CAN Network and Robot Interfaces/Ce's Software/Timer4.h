// Timer4.h
// Runs on TM4C123 
// Use Timer4 in 32-bit periodic mode to request interrupts at a periodic rate
// Brandon Boesch
// Ce Wei
// February 21, 2016


// ***************** Timer4_Init ****************
// Activate Timer interrupts periodically.  Timer is in charge of running
// background threads in the OS.  This timer has highest priority
// Inputs:  period in units (1/clockfreq)
// Outputs: none
void Timer4_Init(unsigned long period);


// ***************** Timer4A_Handler ******************
void Timer4A_Handler(void);


// ***************** Timer4A_Enable ******************
void Timer4A_Enable(void);


// ***************** Timer4A_Disable ******************
void Timer4A_Disable(void);
