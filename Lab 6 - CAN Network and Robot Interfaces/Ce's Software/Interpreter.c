// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// March 20th, 2016

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "Interpreter.h"
#include "UART.h"
#include "Lab6.h"
#include "Interrupts.h"
#include "PWM.h"
#include "OS.h"
#include "Ping.h"
#include "CAN_4C123\can0.h"
#include "IR.h"


// *************Global Variables*****************************

cmdTable Commands[] = {        // CMD_LEN defines max command name length
	{"help",     cmdHelp,        "Displays all available commands."},
  {"motor",    cmdMotor,       "Sends commands to the motor."},
  {"ping",     cmdPing,        "Control ping sensors."},
	{"can",      cmdCAN,         "Control CAN"},
	{"ir",			 cmdIR,					 "Control IR sensors"}
};

char cmd[CMD_LEN+1];   // string to store command line inputs. +1 for null.
// *********************************************************************


void Interpreter_Init(void){
	printf("\n\n\r******************************************************\n\r");
	printf("                  Welcome to bOS.\n\r");
	printf("         Type \"help\" for a list of commands.\n\r");
	printf("******************************************************\n\r");
}


//---------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void){
	printf("\n\r--Enter Command--> ");
	UART_InStringNL(cmd, CMD_LEN);  
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(cmd,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
	printf("Command not recognized.\n\r");
}

	
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
	printf("Below is a list of availble commands:\n\r");
	// output formating and display command name
	for(int i = 0; i < NUM_OF(Commands); i++){
		if(i+1 <10) printf("    ");                   
		else if(i+1 < 100) printf("   ");     
		printf("%d",i+1);                             
		printf(") ");                         
		// display command name
		printf("%s",(char*)Commands[i].name);  
    // output formating		
		for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
		  printf("-");                             
		}
		// display command tag
    printf("%s\n\r",(char*)Commands[i].tag);     
	}
}


//------------cmdMotor-----------------
// Sends commands to the motor
void cmdMotor(void){
	printf("Choose a command for the motor:\n\r");
	printf("    (1) Duty - Assign duty cycle\n\r");
	printf("    (2) Forward - Motor rotates forward\n\r");
	printf("    (3) Reverse - Motor rotates in reverse\n\r");
	printf("    (4) Coast - Motor stops (fast decay)\n\r");
	printf("    (5) Brake - Motor stops (slow decay)\n\r");
	printf("--Enter Command #--> ");
	uint32_t num = UART_InUDec();
	printf("\n\r");
	
	switch (num){
		// Duty
	  case(1):{
			printf("Enter the new duty cycle (in %%): ");
			uint16_t percent = UART_InUDec();
			printf("\n\n\r");
			uint32_t duty = (PWM_FREQ*percent)/100;
			
			if((duty > PWM_FREQ) || (duty > (1<<16))){
				printf("Error, duty cycle not compatable\n\r");
			}
			else{
				int32_t status = StartCritical();
				DutyPWM0 = duty;
			  EndCritical(status);
				printf("Duty cycle assigned\n\r");
			}
			break;
		}
		
		// Forward
		case(2):{
			PWM0A_Duty(DutyPWM0);
			PWM0B_Duty(0);
			printf("Motor moving forward.\n\r");
			break;
		}
		
		// Reverse
		case(3):{
			PWM0A_Duty(0);
			PWM0B_Duty(DutyPWM0);
			printf("Motor moving in reverse.\n\r");
			break;
		}
		
		// Coast
		case(4):{
			PWM0A_Duty(0);
			PWM0B_Duty(0);
			printf("Motor is coasting.\n\r");
			break;
		}
		
		// Brake
		case(5):{
			PWM0A_Duty(DutyPWM0);
			PWM0B_Duty(DutyPWM0);
			printf("Motor is breaking.\n\r");
			break;
		}
		// Improper Selection
		default: {
			printf("Improper selection\n\r"); 
			return;
		}
	}		
}


//------------cmdPing-----------------
// Control the ping sensors.
void cmdPing(void){
	printf("Choose a command for the ping sensors:\n\r");
	printf("    (1) Add Ping - Adds all ping threads.\n\r");
	printf("    (2) Display Ping - Display a specific ping sensor's value to ST7735.\n\r");

	printf("--Enter Command #--> ");
	uint32_t selection = UART_InUDec();
	printf("\n\r");
	
	switch(selection){
		
		// *********************************
		//            Add Ping 
		// *********************************
	  case(1):{
			OS_AddThread(&Ping1, 128, 1);                                        // sends a pulse to the ping sensor #1 10 times a second
			OS_AddThread(&Ping3, 128, 1);                                        // sends a pulse to the ping sensor #3 10 times a second
			printf("Ping sensors added.\n\r");
			break;
		}
		
		// *********************************
		//          Display Ping
		// *********************************
		case(2):{
			printf("Enter the ID of the ping sensor you wish to display(1-3): ");
			uint8_t pingID = UART_InUDec();                                         // ID of ping sensor you wish to add a thread for
			printf("\n\r");                                                         // new line
			switch (pingID){
				// ping sensor 1 (J10Y header)
				case(1):{
					PingToDisplay = 1;
					break;
	    	}
				// ping sensor 2 (J12Y header)
			  case(2):{
					PingToDisplay = 2;
					break;
	    	}
				// ping sensor 3 (J11Y header)
				case(3):{
          PingToDisplay = 3;
					break;
	    	}
			  default: {
			    printf("Improper selection\n\r"); 
			    return;
		    }
			}
			
			OS_AddThread(&PingDisplay, 128, 6);                                 // display ping 3 results
			printf("Displaying ping sensor %u.\n\r", pingID);
			break;
		}

		default: {
			printf("Improper selection\n\r"); 
			return;
		}
	}		
}


//------------cmdCAN-----------------
// Control CAN.
void cmdCAN(void){
	printf("Choose a command for CAN:\n\r");
	printf("    (1) View Data - Print the current send and receive bytes.\n\r");
	printf("    (2) Lost Packets - Display how many packets were lost.\n\r");

	printf("--Enter Command #--> ");
	uint32_t selection = UART_InUDec();
	printf("\n\r");
	
	switch(selection){
		// View data
		 case(1):{
			  printf("The last sent CAN message was: \n\r");
				printf("Byte 0 = %u\n\r", XmtData[0]);
			  printf("Byte 1 = %u\n\r", XmtData[1]);
		 	  printf("Byte 2 = %u\n\r", XmtData[2]);
			  printf("Byte 3 = %u\n\r", XmtData[3]);
			 
			  printf("\n\rThe last CAN message received was: \n\r");
				printf("Byte 0 = %u\n\r", RcvData[0]);
			  printf("Byte 1 = %u\n\r", RcvData[1]);
		 	  printf("Byte 2 = %u\n\r", RcvData[2]);
			  printf("Byte 3 = %u\n\r", RcvData[3]);
			 
			  printf("\n\rView CAN data complete: \n\r");
			 
      break;
    }
		// Lost packets
    case(2):{
        printf("\n\r%lu packets have been lost.\n\r", PacketLost);
      break;
    }
    default: {
      printf("Improper selection\n\r"); 
      return;
    }
	}
}

//------------cmdIR-----------------
// Control the IR sensors.
void cmdIR(void){
	printf("Choose a command for the IR sensors:\n\r");
	printf("    (1) Add IR - Adds all IR threads.\n\r");
	printf("    (2) Display IR - Display a specific IR sensor's value to ST7735.\n\r");

	printf("--Enter Command #--> ");
	uint32_t selection = UART_InUDec();
	printf("\n\r");
	
	switch(selection){
		// *********************************
		//            Add IR 
		// *********************************
	  case(1):{
			IR_Init_HW();											//enable the HW ADC, producer, and add thread of consumer
			//IR_Init_SW();											//enable the SW ADC, background thread
			printf("All IR sensors added.\n\r");
			break;
		}
		// *********************************
		//          Display IR
		// *********************************
		case(2):{
			printf("Enter the ID of the IR sensor you wish to display(1-2): ");
			uint8_t IRID = UART_InUDec();                                         // ID of ping sensor you wish to add a thread for
			printf("\n\r");                                                         // new line
			switch (IRID){
				//display IR sensor 1
				case(1):{
					IR_IRToDisplay = 1;
					break;
	    	}
				//display IR sensor 2
				case(2):{
					IR_IRToDisplay = 2;
					break;
				}
			  default: {
			    printf("Improper selection\n\r"); 
			    return;
		    }
			}
			//enable the display
			OS_AddThread(&IRDisplay, 128, 6);										// display IR results
			printf("Displaying IR sensor %u.\n\r", IRID);
			break;
		}
		// *********************************
		//          default
		// *********************************
		default: {
			printf("Improper selection\n\r"); 
			return;
		}
	}		
}
