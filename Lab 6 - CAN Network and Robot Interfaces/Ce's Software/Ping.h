// Ping.h
// Runs on TM4C123
// Use Timer3A in 24-bit edge time mode to request interrupts on the 
// rising and falling edge of PB2 (T3CCP0), to measure pulse width.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 4th, 2016


//********************Constants*********************************************  
#define PB2                     (*((volatile unsigned long *)0x40005010))
#define PB4                     (*((volatile unsigned long *)0x40005040))
#define NVIC_EN1_INT35          1<<(35-32)  // Interrupt 35 enable   
#define NVIC_EN0_INT21          1<<21       // Interrupt 21 enable
#define TIMER_TAILR_TAILRL_M    0x0000FFFF  // GPTM TimerA Interval Load Register Low

#define TEMP_CELSIUS            25
#define C_AIR                   (3315+(6*TEMP_CELSIUS))/1000   // speed of air in sound (.1cm/ms)  
//#define C_AIR                   (3315+(6*TEMP_CELSIUS))/100   // speed of air in sound (cm/ms)  
//**************************************************************************


//********************Externs***********************************************
extern uint8_t PingToDisplay;          // determines which ping sensor's data to display in PingDisplay()
//**************************************************************************


//********************Prototypes********************************************

//******** Ping_Init *************** 
// Initializes ping timers for 24-bit edge time mode and request 
// interrupts on the rising and falling edge of specified CCPO pin.
// Max pulse measurement is (2^24-1)*12.5ns = 209.7151ms
// Min pulse measurement determined by time to run ISR, which is about 1us
// inputs:  none
// outputs: none
void Ping_Init(void);

//******** SendPulse *************** 
// Issue a 40kHz sound pulse by sending a short 
// logical high burst ~(5us) to the ping sensor.
// Called from PulsePing().
// inputs:  pingID - the ID of the ping sensor you wish to send a pulse to
// outputs: none
void SendPulse(uint8_t pingID);

//******** ConfigForCapture *************** 
// Configure PB2 as an input capture pin with a pull up resistor
// Called from PulsePing().
// inputs:  pingID - the ID of the ping sensor you wish configure for input capture
// outputs: none
void ConfigForCapture(uint8_t pingID);

//******** Ping1 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping1(void);

//******** Ping3 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping3(void);

//******** PingDisplay *************** 
// Foreground thread.
// Display ping sensor readings 10 times a second
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void PingDisplay(void);
