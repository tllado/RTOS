// PulseMeasure.c
// Runs on TM4C123
// Use Timer3A in 24-bit edge time mode to request interrupts on the 
// rising and falling edge of PB2 (T3CCP0), to measure pulse width.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 4th, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "PulseMeasure.h"
#include "ST7735.h"
#include "OS.h"
#include "Interrupts.h"
#include "Profiler.h"
#include "SysTick.h"
                           
//******************** Globals ***********************************************
uint8_t BeginMeasure = 0;     // binary control to determine which task to perform in Timer3 handler
uint32_t StartTime;           // Timer count on at rising edge.
uint32_t PulseTime;           // Time of Ping pulse (ms)
//****************************************************************************


//******** PulseMeasure_Init *************** 
// Foreground thread. 
// Initializes specified timer for 24-bit edge time mode and request 
// interrupts on the rising and falling edge of specified CCPO pin.
// Max pulse measurement is (2^24-1)*12.5ns = 209.7151ms
// Min pulse measurement determined by time to run ISR, which is about 1us
// inputs:  none
// outputs: none
void PulseMeasure_Init(void){
	// initalize CCPO pin that causes interrupt	
  SYSCTL_RCGCGPIO_R |= 0x02;             // activate clock for port B      
	while((SYSCTL_PRGPIO_R&0x02) == 0){};  // allow time for clock to stabilize	
  GPIO_PORTB_DIR_R &= ~0x04;             // make PB2 input
  GPIO_PORTB_AFSEL_R |= 0x04;            // enable alt funct on PB2
  GPIO_PORTB_DEN_R |= 0x04;              // enable digital I/O on PB2
  GPIO_PORTB_AMSEL_R &= ~0x04;           // disable analog functionality on PB2
	GPIO_PORTB_PDR_R |= 0x04;              // enable weak pull-down on PB2
  GPIO_PORTB_PCTL_R = (GPIO_PORTB_PCTL_R&0xFFFFF0FF)+0x00000700;  // configure PB2 as T0CCP0

  // initalize timer used for input capture
	SYSCTL_RCGCTIMER_R |= 0x08;            // activate TIMER3  
	while((SYSCTL_PRTIMER_R&0x08) == 0){}; // allow time for clock to stabilize	
  TIMER3_CTL_R &= ~TIMER_CTL_TAEN;       // disable timer3A during setup
  TIMER3_CFG_R = TIMER_CFG_16_BIT;       // configure for 16-bit timer mode                                   
  TIMER3_TAMR_R = (TIMER_TAMR_TACMR|TIMER_TAMR_TAMR_CAP); // configure for 24-bit capture mode                                 
	TIMER3_CTL_R |= TIMER_CTL_TAEVENT_NEG; // configure as neg edge event
  TIMER3_TAILR_R = TIMER_TAILR_TAILRL_M; // start value
  TIMER3_TAPR_R = 0xFF;                  // activate prescale, creating 24-bit
  TIMER3_IMR_R |= TIMER_IMR_CAEIM;       // enable capture match interrupt
  TIMER3_ICR_R = TIMER_ICR_CAECINT;      // clear timer3A capture match flag
  TIMER3_CTL_R |= TIMER_CTL_TAEN;        // enable timer3A 16-b, +edge timing, interrupts
                                   
  NVIC_PRI4_R = (NVIC_PRI8_R&0x00FFFFFF)|0x20000000; // Timer3A priority = 1, top 3 bits
  NVIC_EN1_R = NVIC_EN1_INT35;           // enable interrupt 35 in NVIC.  Uses EN1 since IRQ > 31
}

// ******** Timer3A_Handler **************
// Captures pulse width.

void Timer3A_Handler(void){
	TIMER3_ICR_R = TIMER_ICR_CAECINT;                                   // acknowledge timer3A capture match
	int32_t status = StartCritical();

	// capture time on falling edge
	uint32_t endTime = TIMER3_TAR_R;
	PulseTime = OS_TimeDifference(StartTime,endTime)/8000;             // how long the pulse was high (10ms units)
		
	// set pin back in default state for next signal pulse
	GPIO_PORTB_PDR_R |= 0x04;                                           // enable weak pull-down on PB2
	EndCritical(status);
}


//******** SendPulse *************** 
// Send a short burst ~(5us) to the ping sensor, 
// which issues a sound pulse.
// inputs:  none
// outputs: none
void SendPulse(void){
	int32_t status = StartCritical();
	TIMER3_CTL_R &= ~TIMER_CTL_TAEN;       // disable timer3A during pulse
	
	// set PB2 as a GPIO output
	GPIO_PORTB_DIR_R |= 0x04;              // make PB2 output
  GPIO_PORTB_AFSEL_R &= ~0x04;           // disable alt funct on PB2
  GPIO_PORTB_PCTL_R = (GPIO_PORTB_PCTL_R&0xFFFFF0FF)+0x00000000;  // configure PB2 as GPIO
	
	// create 5us pulse
	PB2 = 0x04;
	SysTick_Wait(400);    // 5us / 12.5ns = 400
	PB2 = 0x00;	
	EndCritical(status);
}

//******** CapturePulse *************** 
// configure PB2 as an input capture pin with a pull up resistor
// inputs:  none
// outputs: none
void CapturePulse(void){
	int32_t status = StartCritical();
	
	// set PB2 as a input capture pin
	GPIO_PORTB_DIR_R &= ~0x04;             // make PB2 input
	GPIO_PORTB_AFSEL_R |= 0x04;            // enable alt funct on PB2
	GPIO_PORTB_PUR_R |= 0x04;              // enable weak pull-up on PB2
	GPIO_PORTB_PCTL_R = (GPIO_PORTB_PCTL_R&0xFFFFF0FF)+0x00000700;  // configure PB2 as T0CCP0
	TIMER3_ICR_R = TIMER_ICR_CAECINT;      // clear timer3A capture match flag
  TIMER3_CTL_R |= TIMER_CTL_TAEN;        // re-enable timer3A since SendPulse disabled it
	StartTime = TIMER3_TAR_R;              // set startTime for measuring Ping pulse
	EndCritical(status);
}

