// PulseMeasure.h
// Runs on TM4C123
// Use Timer3A in 24-bit edge time mode to request interrupts on the 
// rising and falling edge of PB2 (T3CCP0), to measure pulse width.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 4th, 2016


//********************Constants*********************************************  
#define PB2                     (*((volatile unsigned long *)0x40005010))
#define NVIC_EN1_INT35          1<<(35-32)  // Interrupt 35 enable   
#define TIMER_TAILR_TAILRL_M    0x0000FFFF  // GPTM TimerA Interval Load Register Low

#define TEMP_CELSIUS            21
#define C_AIR                   (3315+(6*TEMP_CELSIUS))/100   // speed of air in sound (cm/ms)  
//**************************************************************************


//********************Externs***********************************************
 extern uint32_t PulseTime;           // Time of Ping pulse (ms)
//**************************************************************************


//********************Prototypes********************************************

//******** PulseMeasure_Init *************** 
// Foreground thread. 
// Initializes specified timer for 24-bit edge time mode and request 
// interrupts on the rising and falling edge of specified CCPO pin.
// Max pulse measurement is (2^24-1)*12.5ns = 209.7151ms
// Min pulse measurement determined by time to run ISR, which is about 1us
// inputs:  none
// outputs: none
void PulseMeasure_Init(void);

//******** SendPulse *************** 
// Send a short burst ~(5us) to the ping sensor, 
// which issues a sound pulse.
// inputs:  none
// outputs: none
void SendPulse(void);


//******** CapturePulse *************** 
// configure PB2 as an input capture pin with a pull up resistor
// inputs:  none
// outputs: none
void CapturePulse(void);
//**************************************************************************


