// Buttons.c
// Runs on LM4F120 or TM4C123
// Request an interrupt on the falling edge of PF4 (when the user
// button is pressed) and increment a counter in the interrupt.  Note
// that button bouncing is not addressed.
// Daniel Valvano
// May 3, 2015

/* This example accompanies the book
   "Embedded Systems: Introduction to ARM Cortex M Microcontrollers"
   ISBN: 978-1469998749, Jonathan Valvano, copyright (c) 2015
   Volume 1, Program 9.4
   
   "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2014
   Volume 2, Program 5.6, Section 5.5

 Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "stdbool.h"
#include "Buttons.h"
#include "Interrupts.h"
#include "OS.h"


// ***************Globals**********************
void(*SW1Task)(void);     // user function on falling edge of SW1(PF4)
void(*SW2Task)(void);     // user function on falling edge of SW2(PF0)
uint8_t SW1Priority;      // priority of SW1's user task
uint8_t SW2Priority;      // priority of SW2's user task
// ********************************************


// ******** SW1_Init ************
// Initialize Port F4 to handle edge triggered interrupt when SW1 is pressed. 
// Store SW1's task and priority.
// input:  *task - pointer to SW1's user task
//         priority - SW1's task priority
// output: none
void SW1_Init(void(*task)(void), uint8_t priority){ 
  int32_t status = StartCritical();
  SW1Task = task;               // save user task for SW1
  SW1Priority = priority;       // save priority for SW1
  SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F   
  while((SYSCTL_PRGPIO_R&0x20) == 0){}; // allow time for clock to stabilize
  GPIO_PORTF_DIR_R &= ~0x10;    // (c) make PF4 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x10;  //     disable alt funct on PF4
  GPIO_PORTF_DEN_R |= 0x10;     //     enable digital I/O on PF4   
  GPIO_PORTF_PCTL_R &= ~0x000F0000; // configure PF4 as GPIO
  GPIO_PORTF_AMSEL_R &= ~0x10;  //     disable analog functionality on PF4
  GPIO_PORTF_PUR_R |= 0x10;     //     enable weak pull-up on PF4
  GPIO_PORTF_IS_R &= ~0x10;     // (d) PF4 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~0x10;    //     PF4 is not both edges
  GPIO_PORTF_IEV_R &= ~0x10;    //     PF4 falling edge event
  GPIO_PORTF_ICR_R = 0x10;      // (e) clear flag4
  GPIO_PORTF_IM_R |= 0x10;      // (f) arm interrupt on PF4 
  NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|(0 << 21); // priority 0
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
  EndCritical(status);
}


// ******** SW2_Init ************
// Initialize Port F0 to handle edge triggered interrupt when SW2 is pressed. 
// Store SW2's task and priority.
// input:  *task - pointer to SW2's user task
//         priority - SW2's task priority
// output: none
void SW2_Init(void(*task)(void), uint8_t priority){ 
  int32_t status = StartCritical();
  SW2Task = task;               // save user task for SW2
  SW2Priority = priority;       // save priority for SW2
  SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F   
  while((SYSCTL_PRGPIO_R&0x20) == 0){}; // allow time for clock to stabilize
  GPIO_PORTF_LOCK_R = GPIO_LOCK_KEY;    // b1) unlock GPIO Port F Commit Register
  GPIO_PORTF_CR_R |= (SW1|SW2);         // b2) enable commit for PF4 and PF0
  GPIO_PORTF_DIR_R &= ~0x01;    // (d) make PF0 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x01;  //     disable alt funct on PF0
  GPIO_PORTF_DEN_R |= 0x01;     //     enable digital I/O on PF0   
  GPIO_PORTF_PCTL_R &= ~0x0000000F; // configure PF0 as GPIO
  GPIO_PORTF_AMSEL_R &= ~0x01;  //     disable analog functionality on PF0
  GPIO_PORTF_PUR_R |= 0x01;     //     enable weak pull-up on PF0
  GPIO_PORTF_IS_R &= ~0x01;     // (e) PF0 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~0x01;    //     PF0 is not both edges
  GPIO_PORTF_IEV_R &= ~0x01;    //     PF0 falling edge event
  GPIO_PORTF_ICR_R = 0x01;      // (f) clear flag0
  GPIO_PORTF_IM_R |= 0x01;      // (g) arm interrupt on PF0 
  NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|(0 << 21); // priority 0
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
  EndCritical(status);
}

// ***************** GPIOPortF_Handler ******************
// Adds an aperiodic background task to whenever the SW1(PF4) or SW2(PF0) buttons are pressed
void GPIOPortF_Handler(void){
  if(GPIO_PORTF_RIS_R & 0x01){  // poll PF0(SW2)
    GPIO_PORTF_ICR_R = 0x01;    // acknowledge flag0
    //SW2Task();
    OS_AddAperiodicThread(SW2Task, SW2Priority); 
  }
  if(GPIO_PORTF_RIS_R & 0x10){  // poll PF4(SW1)
    GPIO_PORTF_ICR_R = 0x10;    // acknowledge flag4
    //SW1Task();
    OS_AddAperiodicThread(SW1Task, SW1Priority);
  }
}


// ***************** Switch_Enable ******************
void Switch_Enable(void){
  NVIC_EN0_R = 1<<30;                // enable IRQ 30 in NVIC
}


// ***************** Switch_Disable ******************
void Switch_Disable(void){
  NVIC_DIS0_R = 1<<30;               // disable IRQ 30 in NVIC
}

