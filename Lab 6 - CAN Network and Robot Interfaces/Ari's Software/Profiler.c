// Profiler.c
// Runs on TM4C123 
// Provides digital output pins on PortF that can be  
// attached to a scope to create a profile of the system.
// Brandon Boesch
// Ce Wei
// March 19th, 2016

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Profiler.h"


//******** PortF_Init **************
// Initalize port F for profiling pins
// inputs:  none
// outputs: none
void PortF_Init(void){  
  unsigned long volatile delay;
  SYSCTL_RCGCGPIO_R |= 0x20;            // activate port F
  while((SYSCTL_PRGPIO_R&0x20) == 0){}; // allow time for clock to stabilize
  GPIO_PORTF_DIR_R |= 0x0E;             // make PF3-1 output (PF3-1 built-in LEDs)
  GPIO_PORTF_AFSEL_R &= ~0x0E;          // disable alt funct on PF3-1
  GPIO_PORTF_DEN_R |= 0x0E;             // enable digital I/O on PF3-1
  GPIO_PORTF_PCTL_R &= ~0x0000FFF0;     // configure PF3-1 as GPIO
  GPIO_PORTF_AMSEL_R &= ~0x0E;          // disable analog functionality on PF3-1
}

