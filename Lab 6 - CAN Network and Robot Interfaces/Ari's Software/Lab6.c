// Lab6.c
// Runs on TM4C123
// Real Time Operating System for Lab 6
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 3rd, 2016


// ************ ST7735's Interface*********************************************
// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground
// ****************************************************************************


//************ Timer Resources *************************************************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 0
//  Timer2A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//******************************************************************************


//************ OS's Backgroud Threads ******************************************
//  OS_Aging - priority = 7 (only added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 3
//******************************************************************************


//******************** Libraries ***********************************************
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab6.h"
#include "OS.h"
#include "UART.h"
#include "ST7735.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"
#include "Interrupts.h"
#include "ADC.h"
#include "PulseMeasure.h"
#include "PWM.h"
#include "SysTick.h"
#include "CAN_4C123\can0.h"
#include "Buttons.h"
//**************************************************************************


//******************** Globals ***********************************************
uint32_t IdleCount;            // counts how many iterations IdleTask() is ran.
uint16_t DutyPWM0A;            // duty cycle for PWM0A
uint16_t DutyPWM0B;            // duty cycle for PWM0B
uint16_t DutyPWM0;             // duty cycle for motor
//****************************************************************************


//******** IdleTask  *************** 
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
    PF1 ^= 0x02;        // debugging profiler 
    IdleCount++;        // debugging 
    WaitForInterrupt(); // low-power mode
  }
}


//******** Interpreter **************
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none
void Interpreter(void){   
  Interpreter_Init();
  while(1){
    Interpreter_Parse();
  }
}


//******** ADC_Producer *************** 
// Background thread. 
// Called from the hardware triggered ADC's ISR.
// Runs at the sampling rate defined in the parameter for ADC_InitHWTrigger.
// Runs when ADC data is ready and is passed a 12-bit data sample.
// Use this function to send data to the consumer via FIFO.
// inputs:  data - HW triggered ADC sample data
// outputs: none
void ADC_Producer(unsigned long data){  
// send data to a consumer via FIFO
// if(OS_Fifo_Put(data) == 0){ 
//   DataLost++;
// } 
}


//******************* MAIN **************************************************
#ifdef MAIN
int main(void){  
  // intialize globals
  IdleCount = 0;
  
  // initalize modules  
  OS_Init();                             // initialize OS, disable interrupts
  PortF_Init();                          // initialize Port F profiling
  ADC_InitHWTrigger(5, ADC_SAMPLE_RATE, &ADC_Producer);   // start ADC sampling, channel 5, PD2, 400 Hz

  // create initial foreground threads
  OS_AddThread(&Interpreter, 128, 2); 
  OS_AddThread(&IdleTask, 128, 7);       // runs when nothing useful to do
 
  OS_Launch(TIMESLICE);                  // doesn't return, interrupts enabled in here
  return 0;                              // this never executes
}
#endif  
//******************* End MAIN **********************************************


//******************* DEBUG **************************************************
#ifdef DEBUG


//******** PulsePing *************** 
// Send a pulse to the Ping sensor 10 times a second.
// Plot results on ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void PulsePing(void){
  // setup plot screen
  ST7735_SetCursor(0,0);
  ST7735_OutString("distance Vs. Time");
  ST7735_PlotClear(0,63);     // graph range from 0 to 63
  
  // send pulses 10 times a second
  while(1){
    SendPulse();         // send a single pulse to the Ping sensor
    SysTick_Wait(60000); // wait for holdoff time = 750 us.  
    CapturePulse();      // capture pulse width
    
    // ensite interrupt.  This will be removed once sensor is working
    //////////////////////////////////////////////////////////////////////
    OS_Sleep(9);                           // range of sensor is 115us(2cm) - 18.5ms(300cm)
    int32_t status = StartCritical();
    GPIO_PORTB_PDR_R |= 0x04;              // enable weak pull-down on PB2
    EndCritical(status);
    //////////////////////////////////////////////////////////////////////////
    
    OS_Sleep(100);   // sleep 100ms.
    
    // plot results
    uint16_t dist = C_AIR * PulseTime/2;
    ST7735_PlotLine(dist/6);   // divide to fit to screen
    ST7735_PlotNextErase();
    //  ST7735_Message(0,6, "dist(cm) = ", dist);
  }
}


uint8_t XmtData[4];
uint8_t RcvData[4];
uint32_t RcvCount=0;
uint8_t sequenceNum=0;  


//******** CanSend *************** 
// Background Thread
// send data on CAN
// inputs:  none
// outputs: none
void CanSend(void){
  XmtData[0] = PF0<<1;  // 0 or 2
  XmtData[1] = PF4>>2;  // 0 or 4
  XmtData[2] = 0;       // unassigned field
  XmtData[3] = sequenceNum;  // sequence count
  CAN0_SendData(XmtData);
  sequenceNum++;
}


//******** CanReceive *************** 
// ForeGround Thread
// receive data on CAN
// inputs:  none
// outputs: none
void CanReceive(void){
  while(1){
    if(CAN0_GetMailNonBlock(RcvData)){
      RcvCount++;
      PF1 = RcvData[0];
      PF2 = RcvData[1];
      PF3 = RcvCount;   // heartbeat
    }
  }
}


//******** ButtonWork *************** 
// Aperiodic Background thread.
// Only here so that switches get initalize
// inputs:  none
// outputs: none
void ButtonWork(void){
  // do nothing
}



int main(void){  
  // intialize globals
  IdleCount = 0;
  DutyPWM0A = 0;                   
  DutyPWM0B = 0;                    
  
  // initalize modules  
  OS_Init();                             // initialize OS, disable interrupts
  PortF_Init();                          // initialize Port F profiling
//  PulseMeasure_Init();                   // initialize 24-bit timer3A in capture mode
//  PWM0A_Init(PWM_FREQ, DutyPWM0A);       // initialize PWM0A
//  PWM0B_Init(PWM_FREQ, DutyPWM0B);       // initialize PWM0B
  
  CAN0_Open();                          // initalize CAN0
   

  // create initial foreground threads
  OS_AddThread(&Interpreter, 128, 2); 
//  OS_AddThread(&IdleTask, 128, 7);       // runs when nothing useful to do
//  OS_AddThread(&PulsePing, 128, 1);      // sends a pulse to the ping sensor 10 times a second

  OS_AddPeriodicThread(&CanSend, 1600000, 3);
  OS_AddThread(&CanReceive, 128, 1);
  OS_AddButtonTask(&ButtonWork, PF0_TASK, 4);
  OS_AddButtonTask(&ButtonWork, PF4_TASK, 4);
  OS_Launch(TIMESLICE);                  // doesn't return, interrupts enabled in here
  return 0;                              // this never executes
}
#endif  
//******************* End DEBUG **********************************************
  
