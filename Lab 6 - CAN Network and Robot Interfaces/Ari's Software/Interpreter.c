// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// March 20th, 2016

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "Interpreter.h"
#include "UART.h"
#include "Lab6.h"
#include "Interrupts.h"
#include "PWM.h"

extern unsigned long PacketLost;// packets lost
extern uint8_t XmtData[4];// sent four bytes
extern uint8_t RcvData[4];// received four bytes
void cmdPacket(void);

// *************Global Variables*****************************

cmdTable Commands[] = {        // CMD_LEN defines max command name length
  {"help",     cmdHelp,        "Displays all available commands."},
  {"motor",    cmdMotor,       "Sends commands to the motor."},
  {"data",     cmdData,        "Print current sent and received four bytes."},
  {"lost",     cmdPacket,      "View how many packets are lost."},
};

char cmd[CMD_LEN+1];   // string to store command line inputs. +1 for null.
// *********************************************************************


void Interpreter_Init(void){
  printf("\n\n\r******************************************************\n\r");
  printf("                  Welcome to bOS.\n\r");
  printf("         Type \"help\" for a list of commands.\n\r");
  printf("******************************************************\n\r");
}


//---------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void){
  printf("\n\r--Enter Command--> ");
  UART_InStringNL(cmd, CMD_LEN);  
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(cmd,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
  printf("Command not recognized.\n\r");
}

  
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
  printf("Below is a list of availble commands:\n\r");
  // output formating and display command name
  for(int i = 0; i < NUM_OF(Commands); i++){
    if(i+1 <10) printf("    ");                   
    else if(i+1 < 100) printf("   ");     
    printf("%d",i+1);                             
    printf(") ");                         
    // display command name
    printf("%s",(char*)Commands[i].name);  
    // output formating    
    for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
      printf("-");                             
    }
    // display command tag
    printf("%s\n\r",(char*)Commands[i].tag);     
  }
}


//------------cmdMotor-----------------
// Sends commands to the motor
void cmdMotor(void){
  printf("Choose a command for the motor:\n\r");
  printf("    (1) Duty - Assign duty cycle\n\r");
  printf("    (2) Forward - Motor rotates forward\n\r");
  printf("    (3) Reverse - Motor rotates in reverse\n\r");
  printf("    (4) Coast - Motor stops (fast decay)\n\r");
  printf("    (5) Brake - Motor stops (slow decay)\n\r");
  printf("--Enter Command #--> ");
  uint32_t num = UART_InUDec();
  printf("\n\r");
  
  switch (num){
    case(1):{
      printf("Enter the new duty cycle (in %%): ");
      uint16_t percent = UART_InUDec();
      printf("\n\n\r");
      uint32_t duty = (PWM_FREQ*percent)/100;
      
      if((duty > PWM_FREQ) || (duty > (1<<16))){
        printf("Error, duty cycle not compatable\n\r");
      }
      else{
        int32_t status = StartCritical();
        DutyPWM0 = duty;
        EndCritical(status);
        printf("Duty cycle assigned\n\r");
      }
      break;
    }
    case(2):{
      PWM0A_Duty(DutyPWM0);
      PWM0B_Duty(0);
      printf("Motor moving forward.\n\r");
      break;
    }
    case(3):{
      PWM0A_Duty(0);
      PWM0B_Duty(DutyPWM0);
      printf("Motor moving in reverse.\n\r");
      break;
    }
    case(4):{
      PWM0A_Duty(0);
      PWM0B_Duty(0);
      printf("Motor is coasting.\n\r");
      break;
    }
    case(5):{
      PWM0A_Duty(DutyPWM0);
      PWM0B_Duty(DutyPWM0);
      printf("Motor is breaking.\n\r");
      break;
    }
    default: {
      printf("Improper selection\n\r"); 
      return;
    }
  }    
}

//------------cmdDara-----------------
// Print current sent and received four bytes.
void cmdData(void){
  printf("The current sent four bytes with bytes 0, 1, 2, 3 are: ");
  printf("%u, %u, %u, %u\n\r",XmtData[0],XmtData[1],XmtData[2],XmtData[3]);
  printf("The current received four bytes with bytes 0, 1, 2, 3 are: ");
  printf("%u, %u, %u, %u\n\r",RcvData[0],RcvData[1],RcvData[2],RcvData[3]);
}

//------------cmdPacket-----------------
// View how many packets are lost.
void cmdPacket(void){
  printf("There are %lu packets that are lost.\n\r", PacketLost);
}
