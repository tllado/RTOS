// PWM.h
// Provides PWM outputs on select microcontoller pins
// Runs on TM4C123
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 3rd, 2016


// The following table provides sample parameter
// inputs for the functions in this module.
// ---------------------------
// Freq(Hz)    Duty      duty%
// ---------------------------
// 40000       30000      75%
// 40000       10000     25%
// 40000        4000     10%
//  4000        2000     50%
//  1000         900     90%
//  1000         100     10%
// ---------------------------


//********************Constants*********************************************
#define PWM_0_GENA_ACTCMPAD_ONE 0x000000C0  // Set the output signal to 1
#define PWM_0_GENA_ACTLOAD_ZERO 0x00000008  // Set the output signal to 0
#define PWM_0_GENB_ACTCMPBD_ONE 0x00000C00  // Set the output signal to 1
#define PWM_0_GENB_ACTLOAD_ZERO 0x00000008  // Set the output signal to 0

#define SYSCTL_RCC_USEPWMDIV    0x00100000  // Enable PWM Clock Divisor
#define SYSCTL_RCC_PWMDIV_M     0x000E0000  // PWM Unit Clock Divisor
#define SYSCTL_RCC_PWMDIV_2     0x00000000  // /2
//**************************************************************************


//********************Prototypes********************************************

// ******** PWM0A_Init ************
// Initialize PWM0A (M0PWM0 on PB6).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM1
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB6/M0PWM0
void PWM0A_Init(uint16_t period, uint16_t duty);

// ******** PWM0A_Duty ************
// Adjust the duty cycle of PWM signal on PB6
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM0A_Duty(uint16_t duty);

// ******** PWM0B_Init ************
// Initialize PWM0B (M0PWM1 on PB7).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM0
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB7/M0PWM1
void PWM0B_Init(uint16_t period, uint16_t duty);

// ******** PWM0B_Duty ************
// Adjust the duty cycle of PWM signal on PB7
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM0B_Duty(uint16_t duty);

