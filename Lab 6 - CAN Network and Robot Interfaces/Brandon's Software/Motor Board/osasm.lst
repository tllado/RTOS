


ARM Macro Assembler    Page 1 


    1 00000000         ;/******************************************************
                       ***********************/
    2 00000000         ; OSasm.s: low-level OS commands, written in assembly   
                                           */
    3 00000000         ; Runs on LM4F120/TM4C123
    4 00000000         ; A very simple real time operating system with minimal 
                       features.
    5 00000000         ; Daniel Valvano
    6 00000000         ; January 29, 2015
    7 00000000         ;
    8 00000000         ; This example accompanies the book
    9 00000000         ;  "Embedded Systems: Real Time Interfacing to ARM Corte
                       x M Microcontrollers",
   10 00000000         ;  ISBN: 978-1463590154, Jonathan Valvano, copyright (c)
                        2015
   11 00000000         ;
   12 00000000         ;  Programs 4.4 through 4.12, section 4.2
   13 00000000         ;
   14 00000000         ;Copyright 2015 by Jonathan W. Valvano, valvano@mail.ute
                       xas.edu
   15 00000000         ;    You may use, edit, run or distribute this file
   16 00000000         ;    as long as the above copyright notice remains
   17 00000000         ; THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHE
                       THER EXPRESS, IMPLIED
   18 00000000         ; OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED W
                       ARRANTIES OF
   19 00000000         ; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE A
                       PPLY TO THIS SOFTWARE.
   20 00000000         ; VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR
                        SPECIAL, INCIDENTAL,
   21 00000000         ; OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
   22 00000000         ; For more information about my classes, my research, an
                       d my books, see
   23 00000000         ; http://users.ece.utexas.edu/~valvano/
   24 00000000         ; */
   25 00000000         
   26 00000000         
   27 00000000 40024020 
                       PE3     EQU              0x40024020  ; access PE3 for pr
                                                            ofiling
   28 00000000         
   29 00000000                 AREA             DATA
   30 00000000 00 00 00 
              00       Last    SPACE            4           ; allocate a global
                                                             of 4 bytes. Last t
                                                            ime on Timer2
   31 00000004         
   32 00000004         
   33 00000004                 AREA             |.text|, CODE, READONLY, ALIGN=
2
   34 00000000                 THUMB
   35 00000000                 REQUIRE8
   36 00000000                 PRESERVE8
   37 00000000         
   38 00000000                 EXTERN           RunPt       ; currently running
                                                             thread
   39 00000000                 EXTERN           PriorityLists ; array of differ
                                                            ent priority's link
                                                            ed lists 



ARM Macro Assembler    Page 2 


   40 00000000                 EXTERN           MaxNumPri   ; Maximum number of
                                                             priority levels 
   41 00000000                 EXTERN           Scheduler   ; passed in from OS
                                                            .c. Determines whic
                                                            h scheduler to run 
                                                            (1 = Priority, 0 = 
                                                            RoundRobin) 
   42 00000000         
   43 00000000                 EXPORT           StartOS_ASM
   44 00000000                 EXPORT           SysTick_Handler
   45 00000000                 EXPORT           OS_Wait_ASM
   46 00000000                 EXPORT           OS_Signal_ASM
   47 00000000         
   48 00000000         
   49 00000000         ;------SysTick_Handler----------
   50 00000000         ;Determines which scheduler should be ran, RoundRobin or
                        Priority
   51 00000000         SysTick_Handler                      ; 1)    saves R0-R3
                                                            ,R12,LR,PC,PSR, pre
                                                            vents interrupts, 
   52 00000000 B672            CPSID            I           ; 1.1)  prevent int
                                                            errupt during switc
                                                            h
   53 00000002 E92D 0FF0       PUSH             {R4-R11}    ; 1.2)  save remain
                                                            ing regs r4-11     
                                                               
   54 00000006 4846            LDR              R0, =Scheduler ; 1.3)  R0 = poi
                                                            nter to Scheduler v
                                                            ariable        
   55 00000008 6800            LDR              R0, [R0]    ; 1.4)  R0 = Schedu
                                                            ler variable
   56 0000000A 2800            CMP              R0, #0      ; 1.5)  check if Sc
                                                            heduler variable = 
                                                            0
   57 0000000C D02B            BEQ              RoundRobin  ; 1.6)  if R0 = 0, 
                                                            use RoundRobin sche
                                                            duler
   58 0000000E E7FF            B                Priority    ; 1.7)  else, use P
                                                            riority scheduler
   59 00000010         
   60 00000010         
   61 00000010         ;------Priority----------
   62 00000010         ; A priority schedular that performs a context switch on
                        the thread
   63 00000010         ; with the highest tempPriority.  If multiple threads ha
                       ve the same tempPriority
   64 00000010         ; then a round robin algorithm is used to schedule the n
                       ext thread.
   65 00000010         ; Assumes all priority levels have their own linked list
                        of active threads.
   66 00000010         ; This implementation uses aging to prevent starvation o
                       f threads.       
   67 00000010         Priority                             ; 1)    Priority Sc
                                                            heduler. Initialize
                                                            s registers for use
                                                            , increment old Run
                                                            Pt's RunCnt.
   68 00000010 4844            LDR              R0, =RunPt  ; 1.1)  R0= pointer
                                                             to RunPt, old thre



ARM Macro Assembler    Page 3 


                                                            ad
   69 00000012 6801            LDR              R1, [R0]    ; 1.2)  R1 = RunPt
   70 00000014 694E            LDR              R6, [R1, #20] ; 1.3)  R6 = RunP
                                                            t->RunCnt
   71 00000016 F106 0601       ADD              R6, #1      ; 1.4)  R6 = RunCnt
                                                            +1
   72 0000001A 614E            STR              R6, [R1, #20] ; 1.5)  RunPt->Ru
                                                            nCnt = RunPt->RunCn
                                                            t + 1
   73 0000001C 690A            LDR              R2, [R1, #16] ; 1.6)  R2 = RunP
                                                            t->tempPriority
   74 0000001E 4B42            LDR              R3, =PriorityLists ; 1.7)  R3 =
                                                             pointer to Priorit
                                                            yLists
   75 00000020 F04F 0400       MOV              R4, #0      ; 1.8)  R4 = 0. Cou
                                                            nter for cycling th
                                                            rough priority leve
                                                            ls. Starts at highe
                                                            st level
   76 00000024 F853 5034       LDR              R5, [R3,R4,LSL #3] ; 1.8)  R5 =
                                                             PriorityLists[R4].
                                                            Value.  LSL #3 = si
                                                            ze of element in Pr
                                                            iorityList       
   77 00000028         
   78 00000028         CheckList                            ; 2)    Find new Ru
                                                            nPt by scanning tho
                                                            ugh all priority li
                                                            sts 
   79 00000028         ;       until a thread is found. If current list's prior
                       ity == RunPt->tempPriority 
   80 00000028         ;       priority, then use RoundRobin.
   81 00000028 2D00            CMP              R5, #0      ; 2.1)  determine h
                                                            ow many threads are
                                                             in current priorit
                                                            y list.
   82 0000002A D002            BEQ              NextList    ; 2.2)  if current 
                                                            priority level has 
                                                            zero threads, move 
                                                            to next list 
   83 0000002C 4294            CMP              R4, R2      ; 2.3)  check if(cu
                                                            rrent list's priori
                                                            ty == RunPt->tempPr
                                                            iority)
   84 0000002E D005            BEQ              SameList    ; 2.4)  if (current
                                                             list's priority ==
                                                             RunPt->tempPriorit
                                                            y), then use RoundR
                                                            obin on same list
   85 00000030 E00B            B                Update      ; 2.5)  else, move 
                                                            RunPt to this prior
                                                            ity level
   86 00000032         
   87 00000032         NextList                             ; 3)    Move to nex
                                                            t priority level, a
                                                            nd find its priorit
                                                            y value
   88 00000032 F104 0401       ADD              R4, #1      ; 3.1)  else, move 
                                                            to next priority le



ARM Macro Assembler    Page 4 


                                                            vel
   89 00000036 F853 5034       LDR              R5, [R3,R4,LSL #3] ; 3.2)  R5 =
                                                             PriorityLists[R4].
                                                            Value.  LSL #3 = si
                                                            ze of element in Pr
                                                            iorityList       
   90 0000003A E7F5            B                CheckList   ; 3.3)  check if th
                                                            is priority list ha
                                                            s any elements to c
                                                            hoose
   91 0000003C         
   92 0000003C         SameList                             ; 4)    Round robin
                                                             scheduling on curr
                                                            ent priority list 
   93 0000003C F8C1 D000       STR              SP, [R1]    ; 4.1)  Save SP int
                                                            o TCB
   94 00000040 6849            LDR              R1, [R1, #4] ; 4.2)  R1 = RunPt
                                                            ->next
   95 00000042 6001            STR              R1, [R0]    ; 4.3)  RunPt = R1
   96 00000044 F8D1 D000       LDR              SP, [R1]    ; 4.4)  new thread 
                                                            SP; SP = RunPt->sp;
                                                            
   97 00000048 E009            B                Finish      ; 4.5)  finalize co
                                                            ntext switch
   98 0000004A         
   99 0000004A         Update                               ; 5)    Sets RunPt 
                                                            to first active thr
                                                            ead in priority lev
                                                            el stored in R4    
                                                            
  100 0000004A F8C1 D000       STR              SP, [R1]    ; 5.1)  Save SP int
                                                            o TCB
  101 0000004E EA4F 04C4       LSL              R4, #3      ; 5.2)  find offset
                                                             for priority list 
                                                            based of priority l
                                                            evel, R4      
  102 00000052 F104 0404       ADD              R4, #4      ; 5.3)  create offs
                                                            et to list
  103 00000056 5919            LDR              R1, [R3, R4] ; 5.4)  R1 = Prior
                                                            ityLists[RunPt->tem
                                                            pPriority].list
  104 00000058 6001            STR              R1, [R0]    ; 5.5)  RunPt = R1
  105 0000005A F8D1 D000       LDR              SP, [R1]    ; 5.6)  new thread 
                                                            SP; SP = RunPt->sp;
                                                            
  106 0000005E         
  107 0000005E         Finish                               ; 6)    restore reg
                                                             r4-11, reenable in
                                                            terrupts, and BX LR
                                                            
  108 0000005E E8BD 0FF0       POP              {R4-R11}    ; 6.1)  restore reg
                                                            s r4-11
  109 00000062 B662            CPSIE            I           ; 6.2)  tasks run w
                                                            ith interrupts enab
                                                            led
  110 00000064 4770            BX               LR          ; 6.3)  restore R0-
                                                            R3,R12,LR,PC,PSR
  111 00000066         
  112 00000066         



ARM Macro Assembler    Page 5 


  113 00000066         
  114 00000066         ;------RoundRobin----------
  115 00000066         ; A roundrobin schedular that performs a context switch 
                       on the next active thread
  116 00000066         ; Assumes all priority levels have their own linked list
                        of active threads.
  117 00000066         ; This implementation does not use aging since starving 
                       is not an issue.
  118 00000066         
  119 00000066         ; if(RunPt->next == PriorityLists[RunPt->tempPriority].l
                       ist){ 
  120 00000066         ;   int testP = RunPt->tempPriority + 1;
  121 00000066         ;   while(PriorityLists[testP].Value == 0){
  122 00000066         ;     testP++;
  123 00000066         ;     testP % 8;
  124 00000066         ;   }
  125 00000066         ;   RunPt = PriorityLists[testP].list;
  126 00000066         ; }
  127 00000066         ; else {
  128 00000066         ;   RunPt = RunPt->next;
  129 00000066         ; }
  130 00000066         ;                  
  131 00000066         RoundRobin                           ; 1)    initializes
                                                             registers for use
  132 00000066 482F            LDR              R0, =RunPt  ; 1.1)  R0= pointer
                                                             to RunPt, old thre
                                                            ad
  133 00000068 6801            LDR              R1, [R0]    ; 1.2)  R1 = RunPt
  134 0000006A 690A            LDR              R2, [R1, #16] ; 1.3)  R2 = RunP
                                                            t->tempPriority
  135 0000006C 4B2E            LDR              R3, =PriorityLists ; 1.4)  R3 =
                                                             pointer to Priorit
                                                            yLists
  136 0000006E 684C            LDR              R4, [R1, #4] ; 1.5)  R4 = RunPt
                                                            ->next
  137 00000070 EA4F 05C2       LSL              R5, R2, #3  ; 1.6)  LSL #3 = si
                                                            ze of element in Pr
                                                            iorityList
  138 00000074 441D            ADD              R5, R3      ; 1.7)  R5 = &Prior
                                                            ityLists[RunPt->tem
                                                            pPriority]
  139 00000076 686E            LDR              R6, [R5, #4] ; 1.8)  R6 = Prior
                                                            ityLists[RunPt->tem
                                                            pPriority].list
  140 00000078 4F2C            LDR              R7, =MaxNumPri ; 1.9)  R7 = poi
                                                            nter to MaxNumPri
  141 0000007A 683F            LDR              R7, [R7]    ; 1.10) R7 = MaxNum
                                                            Pri
  142 0000007C         
  143 0000007C         CheckList2                           ; 2)    Find new Ru
                                                            nPt. If at the end 
                                                            of current priority
                                                             list scan
  144 0000007C         ;       through other priority lists until an active thr
                       ead is found.
  145 0000007C         ;       Else just use next pointer.
  146 0000007C 42B4            CMP              R4, R6      ; 2.1)  RunPt->next
                                                             == PriorityLists[R
                                                            unPt->tempPriority]



ARM Macro Assembler    Page 6 


                                                            .list
  147 0000007E D000            BEQ              NextList2   ; 2.2)  If true, yo
                                                            u have reached the 
                                                            end of this priorit
                                                            y level's list.  Mo
                                                            ve to a new list
  148 00000080 E013            B                SameList2   ; 2.3)  Else perfor
                                                            m roundrobin on cur
                                                            rent list
  149 00000082         
  150 00000082         NextList2                            ; 3  )  Cycle throu
                                                            gh Priority lists u
                                                            ntil an active TCB 
                                                            is found. 
  151 00000082         ;       Once found update RunPt and stack.
  152 00000082 F102 0201       ADD              R2, #1      ; 3.1)  R2 = RunPt-
                                                            >tempPriority + 1
  153 00000086 42BA            CMP              R2, R7      ; 3.2)  R3 == MaxNu
                                                            mPri
  154 00000088 BF08            IT               EQ          ; 3.3)  If true, R2
                                                             = 0. Start looking
                                                             at the top of prio
                                                            rity list
  155 0000008A 2200            MOVEQ            R2, #0      ; 3.4)  ----------s
                                                            ee previous instruc
                                                            tion comment-------
                                                            ------
  156 0000008C F853 8032       LDR              R8, [R3,R2,LSL #3] ; 3.5)  R8 =
                                                             PriorityLists[R2].
                                                            Value.  LSL #3 = si
                                                            ze of element in Pr
                                                            iorityList 
  157 00000090 F1B8 0F00       CMP              R8, #0      ; 3.6)  PriorityLis
                                                            ts[R2].Value == 0
  158 00000094 D0F5            BEQ              NextList2   ; 3.7)  If true, tr
                                                            y for next priority
                                                             level's list
  159 00000096         ; 3.8)  else, RunPt = PriorityLists[R2].list
  160 00000096 EA4F 05C2       LSL              R5, R2, #3  ; 3.8)  LSL #3 = si
                                                            ze of element in Pr
                                                            iorityList
  161 0000009A 441D            ADD              R5, R3      ; 3.9)  R5 = &Prior
                                                            ityLists[RunPt->tem
                                                            pPriority]
  162 0000009C 686E            LDR              R6, [R5, #4] ; 3.10)  R6 = Prio
                                                            rityLists[RunPt->te
                                                            mpPriority].list
  163 0000009E F8C1 D000       STR              SP, [R1]    ; 3.11) Save SP int
                                                            o TCB
  164 000000A2 6006            STR              R6, [R0]    ; 3.12)  RunPt = Ru
                                                            nPt->next
  165 000000A4 F8D6 D000       LDR              SP, [R6]    ; 3.13)  new thread
                                                             SP; SP = RunPt->sp
                                                            ;
  166 000000A8 E004            B                Finish2     ; 3.14)  finalize c
                                                            ontext switch
  167 000000AA         
  168 000000AA         SameList2                            ; 4)    Round robin
                                                             scheduling on curr



ARM Macro Assembler    Page 7 


                                                            ent priority list 
  169 000000AA F8C1 D000       STR              SP, [R1]    ; 4.1)  Save SP int
                                                            o TCB
  170 000000AE 6004            STR              R4, [R0]    ; 4.3)  RunPt = Run
                                                            Pt->next
  171 000000B0 F8D4 D000       LDR              SP, [R4]    ; 4.4)  new thread 
                                                            SP; SP = RunPt->sp;
                                                            
  172 000000B4         
  173 000000B4         Finish2                              ; 5)    restore reg
                                                             r4-11, reenable in
                                                            terrupts, and BX LR
                                                            
  174 000000B4 E8BD 0FF0       POP              {R4-R11}    ; 5.1)  restore reg
                                                            s r4-11
  175 000000B8 B662            CPSIE            I           ; 5.2)  tasks run w
                                                            ith interrupts enab
                                                            led
  176 000000BA 4770            BX               LR          ; 5.3)  restore R0-
                                                            R3,R12,LR,PC,PSR
  177 000000BC         
  178 000000BC         
  179 000000BC         
  180 000000BC         ;------SysTick_Handler2----------
  181 000000BC         ; A roundrobin schedular that performs a context switch.
                        Assumes all 
  182 000000BC         ; active threads are in one linked list.
  183 000000BC         SysTick_Handler2                     ; 1)   Saves R0-R3,
                                                            R12,LR,PC,PSR
  184 000000BC B672            CPSID            I           ; 2)   Prevent inte
                                                            rrupt during switch
                                                            
  185 000000BE E92D 0FF0       PUSH             {R4-R11}    ; 3)   Save remaini
                                                            ng regs r4-11
  186 000000C2         ;LDR     R4, =PE3           ; 4.1) Toggle PE3 twice. R4 
                       = &PE3
  187 000000C2         ;LDR     R5, [R4]           ; 4.2) R5 = PE3
  188 000000C2         ;EOR     R5, #0x08     ; 4.3) R5 ^= 0x8
  189 000000C2         ;STR     R5, [R4]           ; 4.4) PE3 ^= 0x8
  190 000000C2         ;EOR     R5, #0x08          ; 4.5) R5 ^= 0x8
  191 000000C2         ;STR     R5, [R4]           ; 4.6) PE3 ^= 0x8
  192 000000C2 4818            LDR              R0, =RunPt  ; 5.1) R0=pointer t
                                                            o RunPt, old thread
                                                            
  193 000000C4 6801            LDR              R1, [R0]    ; 5.2) R1 = RunPt
  194 000000C6 F8C1 D000       STR              SP, [R1]    ; 6)   Save SP into
                                                             TCB
  195 000000CA 6849            LDR              R1, [R1, #4] ; 7.1) R1 = RunPt-
                                                            >next
  196 000000CC 6001            STR              R1, [R0]    ; 7.2) RunPt = R1
  197 000000CE F8D1 D000       LDR              SP, [R1]    ; 8)   new thread S
                                                            P; SP = RunPt->sp;
  198 000000D2         ;EOR     R5, #0x08      ; 9.1) Toggle PE3. R5 ^= 0x8
  199 000000D2         ;STR     R5, [R4]           ; 9.2) PE3 ^= 0x8
  200 000000D2 E8BD 0FF0       POP              {R4-R11}    ; 10)  restore regs
                                                             r4-11
  201 000000D6 B662            CPSIE            I           ; 11)  tasks run wi
                                                            th interrupts enabl
                                                            ed



ARM Macro Assembler    Page 8 


  202 000000D8 4770            BX               LR          ; 12)  restore R0-R
                                                            3,R12,LR,PC,PSR
  203 000000DA         
  204 000000DA         
  205 000000DA         
  206 000000DA         StartOS_ASM
  207 000000DA 4812            LDR              R0, =RunPt  ; currently running
                                                             thread
  208 000000DC 6802            LDR              R2, [R0]    ; R2 = value of Run
                                                            Pt
  209 000000DE F8D2 D000       LDR              SP, [R2]    ; new thread SP; SP
                                                             = RunPt->stackPoin
                                                            ter;
  210 000000E2 E8BD 0FF0       POP              {R4-R11}    ; restore regs r4-1
                                                            1
  211 000000E6 BC0F            POP              {R0-R3}     ; restore regs r0-3
                                                            
  212 000000E8 F85D CB04       POP              {R12}
  213 000000EC F85D EB04       POP              {LR}        ; discard LR from i
                                                            nitial stack
  214 000000F0 F85D EB04       POP              {LR}        ; start location
  215 000000F4 BC02            POP              {R1}        ; discard PSR
  216 000000F6 B662            CPSIE            I           ; Enable interrupts
                                                             at processor level
                                                            
  217 000000F8 4770            BX               LR          ; start first threa
                                                            d
  218 000000FA         
  219 000000FA         
  220 000000FA         OS_Wait_ASM
  221 000000FA E850 1F00       LDREX            R1, [R0]    ;Counter
  222 000000FE 3901            SUBS             R1, #1      ;Counter - 1
  223 00000100 BF5C            ITT              PL          ;OK if >= 0
  224 00000102 E840 1200       STREXPL          R2, R1, [R0] ;try update
  225 00000106 2A00            CMPPL            R2, #0      ;succeed?
  226 00000108 D1FE            BNE              OS_Wait_ASM ;no, try again
  227 0000010A 4770            BX               LR
  228 0000010C         
  229 0000010C         
  230 0000010C         OS_Signal_ASM
  231 0000010C E850 1F00       LDREX            R1, [R0]    ;Counter
  232 00000110 F101 0101       ADD              R1, #1      ;Counter + 1
  233 00000114 E840 1200       STREX            R2, R1, [R0] ;try update
  234 00000118 2A00            CMP              R2, #0      ;succeed?
  235 0000011A D1FE            BNE              OS_Signal_ASM ;try again
  236 0000011C 4770            BX               LR
  237 0000011E         
  238 0000011E         
  239 0000011E 00 00           ALIGN
  240 00000120                 END
              00000000 
              00000000 
              00000000 
              00000000 
Command Line: --debug --xref --cpu=Cortex-M4 --apcs=interwork --depend=.\osasm.
d -o.\osasm.o -IC:\Keil\ARM\RV31\INC -IC:\Keil\ARM\CMSIS\Include -IC:\Keil\ARM\
Inc\Luminary --list=.\osasm.lst OSasm.s



ARM Macro Assembler    Page 1 Alphabetic symbol ordering
Relocatable symbols

DATA 00000000

Symbol: DATA
   Definitions
      At line 29 in file OSasm.s
   Uses
      None
Comment: DATA unused
Last 00000000

Symbol: Last
   Definitions
      At line 30 in file OSasm.s
   Uses
      None
Comment: Last unused
2 symbols



ARM Macro Assembler    Page 1 Alphabetic symbol ordering
Relocatable symbols

.text 00000000

Symbol: .text
   Definitions
      At line 33 in file OSasm.s
   Uses
      None
Comment: .text unused
CheckList 00000028

Symbol: CheckList
   Definitions
      At line 78 in file OSasm.s
   Uses
      At line 90 in file OSasm.s
Comment: CheckList used once
CheckList2 0000007C

Symbol: CheckList2
   Definitions
      At line 143 in file OSasm.s
   Uses
      None
Comment: CheckList2 unused
Finish 0000005E

Symbol: Finish
   Definitions
      At line 107 in file OSasm.s
   Uses
      At line 97 in file OSasm.s
Comment: Finish used once
Finish2 000000B4

Symbol: Finish2
   Definitions
      At line 173 in file OSasm.s
   Uses
      At line 166 in file OSasm.s
Comment: Finish2 used once
NextList 00000032

Symbol: NextList
   Definitions
      At line 87 in file OSasm.s
   Uses
      At line 82 in file OSasm.s
Comment: NextList used once
NextList2 00000082

Symbol: NextList2
   Definitions
      At line 150 in file OSasm.s
   Uses
      At line 147 in file OSasm.s
      At line 158 in file OSasm.s

OS_Signal_ASM 0000010C




ARM Macro Assembler    Page 2 Alphabetic symbol ordering
Relocatable symbols

Symbol: OS_Signal_ASM
   Definitions
      At line 230 in file OSasm.s
   Uses
      At line 46 in file OSasm.s
      At line 235 in file OSasm.s

OS_Wait_ASM 000000FA

Symbol: OS_Wait_ASM
   Definitions
      At line 220 in file OSasm.s
   Uses
      At line 45 in file OSasm.s
      At line 226 in file OSasm.s

Priority 00000010

Symbol: Priority
   Definitions
      At line 67 in file OSasm.s
   Uses
      At line 58 in file OSasm.s
Comment: Priority used once
RoundRobin 00000066

Symbol: RoundRobin
   Definitions
      At line 131 in file OSasm.s
   Uses
      At line 57 in file OSasm.s
Comment: RoundRobin used once
SameList 0000003C

Symbol: SameList
   Definitions
      At line 92 in file OSasm.s
   Uses
      At line 84 in file OSasm.s
Comment: SameList used once
SameList2 000000AA

Symbol: SameList2
   Definitions
      At line 168 in file OSasm.s
   Uses
      At line 148 in file OSasm.s
Comment: SameList2 used once
StartOS_ASM 000000DA

Symbol: StartOS_ASM
   Definitions
      At line 206 in file OSasm.s
   Uses
      At line 43 in file OSasm.s
Comment: StartOS_ASM used once
SysTick_Handler 00000000

Symbol: SysTick_Handler



ARM Macro Assembler    Page 3 Alphabetic symbol ordering
Relocatable symbols

   Definitions
      At line 51 in file OSasm.s
   Uses
      At line 44 in file OSasm.s
Comment: SysTick_Handler used once
SysTick_Handler2 000000BC

Symbol: SysTick_Handler2
   Definitions
      At line 183 in file OSasm.s
   Uses
      None
Comment: SysTick_Handler2 unused
Update 0000004A

Symbol: Update
   Definitions
      At line 99 in file OSasm.s
   Uses
      At line 85 in file OSasm.s
Comment: Update used once
17 symbols



ARM Macro Assembler    Page 1 Alphabetic symbol ordering
Absolute symbols

PE3 40024020

Symbol: PE3
   Definitions
      At line 27 in file OSasm.s
   Uses
      None
Comment: PE3 unused
1 symbol



ARM Macro Assembler    Page 1 Alphabetic symbol ordering
External symbols

MaxNumPri 00000000

Symbol: MaxNumPri
   Definitions
      At line 40 in file OSasm.s
   Uses
      At line 140 in file OSasm.s
Comment: MaxNumPri used once
PriorityLists 00000000

Symbol: PriorityLists
   Definitions
      At line 39 in file OSasm.s
   Uses
      At line 74 in file OSasm.s
      At line 135 in file OSasm.s

RunPt 00000000

Symbol: RunPt
   Definitions
      At line 38 in file OSasm.s
   Uses
      At line 68 in file OSasm.s
      At line 132 in file OSasm.s
      At line 192 in file OSasm.s
      At line 207 in file OSasm.s

Scheduler 00000000

Symbol: Scheduler
   Definitions
      At line 41 in file OSasm.s
   Uses
      At line 54 in file OSasm.s
Comment: Scheduler used once
4 symbols
356 symbols in table
