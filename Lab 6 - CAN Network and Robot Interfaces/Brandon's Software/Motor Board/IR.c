// IR.c
// Runs on TM4C123
// Background thread using Timer1A to execute the function that getting the
// data from pre-initialized ADC, to implement a SW trigger ADC
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 8th, 2016

#include <stdint.h>
#include "Interrupts.h"
#include "SysTick.h"
#include "ADC.h"
#include "ST7735.h"
#include "OS.h"
#include "IR.h"

//******************** Globals ***********************************************
uint8_t IR_IRToDisplay;        // determines which IR sensor's data to display in IRDisplay()
long x1[64],y1[64];         // input and output arrays for FFT IR 1
long x2[64],y2[64];         // input and output arrays for FFT IR 2
//****************************************************************************

//********************Prototypes********************************************
// FFT in cr4_fft_64_stm32.s, STMicroelectronics
void cr4_fft_64_stm32(void *pssOUT, void *pssIN, unsigned short Nbin);
//**************************************************************************

//private function prototype;
void Producer(unsigned long data);
void Producer2(unsigned long data);
void Consumer(void);
void Consumer2(void);

//private global
uint32_t DataLost = 0;
unsigned long DCcomponent1, DCcomponent2;

//******** IR_Init_HW ***************
// Timer 0A, HW ADC with the corresponding sampling frequency
void IR_Init_HW(void){
	OS_AddThread(&Consumer,128,1);		//set the consumer to use the fifo in ADC
	//OS_AddThread(&Consumer2,128,1);		//set the consumer to use the fifo2 in ADC
}
//*************** Timer trigger ADC for IR Sensor ***************
// HARDWARE timer-triggered ADC sampling at 400Hz
// Producer runs as PART of ADC ISR(ISR run producer)
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
// Display thread updates LCD with measurement
//
//******** Producer *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer(unsigned long data){
	if(OS_Fifo_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}
//******** Producer *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
//void Producer2(unsigned long data){
//	if(OS_Fifo2_Put(data) == 0){ // send to consumer
//		DataLost++;
//	} 
//}

//******** Consumer *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data1;
	ADC_InitHWTrigger(IR_1, IR_FREQUENCY, &Producer);		//enable HW ADC for IR sensor 1
	//ADC_InitHWTrigger2(IR_2, IR_FREQUENCY, &Producer2);		//enable HW ADC for IR sensor 2
  while(1){
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 1
      data1 = OS_Fifo_Get();    // get from producer
      x1[t] = data1;             // real part is 0 to 4095, imaginary part is 0
    }
    cr4_fft_64_stm32(y1,x1,64);  // complex FFT of last 64 ADC values
    DCcomponent1 = y1[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}
//******** Consumer *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
//void Consumer2(void){
//	//OS_AddThread(&Display,128,0);
//	int t = 0;
//	unsigned long data2;
//	ADC_InitHWTrigger2(IR_2, IR_FREQUENCY, &Producer);		//enable HW ADC for IR sensor 1
//  while(1){
//    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
//			//sensor 1
//      data2 = OS_Fifo_Get();    // get from producer
//      x2[t] = data2;             // real part is 0 to 4095, imaginary part is 0
//    }
//    cr4_fft_64_stm32(y2,x2,64);  // complex FFT of last 64 ADC values
//    DCcomponent2 = y2[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
//    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
//	}
//}


//******** IRDisplay *************** 
// Foreground thread.
// Display IR sensor with the timer interval according to the ADC sampling rate for IR sensor
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void IRDisplay(void){
	// setup plot screen
	ST7735_SetCursor(0,0);
	ST7735_OutString("Distance Vs. Time");
	ST7735_PlotClear(0,63);     // graph range from 0 to 63
	unsigned long voltage, distance;
	unsigned long tempDCcomponent;
	// plot results
	while(1){
		//the IR sensor want to display
		switch (IR_IRToDisplay){
			case(1):{
        tempDCcomponent = DCcomponent1;
				break;
			}
			case(2):{
				tempDCcomponent = DCcomponent2;
				break;
			}
		}
		//DCcomponent = OS_MailBox_Recv();			//DC component
		voltage = 3000*tempDCcomponent/4095;               // calibrate your device so voltage is in mV
		distance = 10000/(((voltage - 100)*10000/26500) + 125);
		if(distance<90){                                     // max sensor reading is 3 meters
			distance+=2;                                        // calibration
			OS_bWait(&LCDFree);                             // capture resource
			ST7735_FillRect(66, 10, 60, 10, ST7735_BLACK);  // original
			OS_bSignal(&LCDFree);                           // release resource
			ST7735_Message(0,1,"dist(cm) = ",distance);         // display distance to screen
			ST7735_PlotLine(distance/5);                        // divide to fit to screen
			ST7735_PlotNextErase();
		}
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);  
	}
}

//******** DAS *************** 
// BACKGROUND thread, calculates 60Hz notch filter
// runs 2000 times/sec
// samples channel 4, PD3,
// inputs:  none
// outputs: none
void Consumer_SW(void){
	int t;
	unsigned long data2;
  while(1){
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data2 = ADC_In();           // channel set when calling ADC_Init = OS_Fifo_Get();    // get from producer
      x2[t] = data2;             // real part is 0 to 4095, imaginary part is 0
    }
    cr4_fft_64_stm32(y2,x2,64);  // complex FFT of last 64 ADC values
    DCcomponent2 = y2[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero		
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}

//******** IR_Init_SW *************** 
void IR_Init_SW(void){
	ADC_InitSWTrigger(IR_2);  // sequencer 2, channel 1, PE2, sampling in DAS(), set it REALLY quick!
	OS_AddPeriodicThread(&Consumer_SW,IR_FREQUENCY,1); // 2 kHz real time sampling of PD3, DAS will go to get it in 2K Hz
}


//******** IRAccuracy *************** 
// Foreground thread.
// Initalizes sensor, collects data, and displays results. 
// Used to determine accuracy of sensors
// inputs:  none
// outputs: none
uint8_t debounceIR = 0;               // only add one thread when button is pressed
uint8_t testNumIR = 0;                // current iteration of test
uint32_t IRBuff[5];               // buffer to store data points
uint8_t IRID = 1;                 // ID of ping sensor you are testing. Ping 1 thru 3.
unsigned long tempDCcomponent;
unsigned int voltage, distance;
void IRAccuracy(void){
	int32_t status = StartCritical();
	if(!debounceIR){
		debounceIR = 1;
		testNumIR++;
		EndCritical(status);
	  // send pulses 10 times a second
	  for(int i = 0; i<5 ;i++){
			OS_Sleep(100); 
			switch (IRID){
				// ping sensor 1 (J10Y header)
				case(1):{
					tempDCcomponent = DCcomponent1;
					break;
	    	}
				// ping sensor 2 (J12Y header)
			  case(2):{
					//pulseTime = PulseTime2;
					break;
	    	}
				// ping sensor 3 (J11Y header)
				case(3):{
          //pulseTime = PulseTime3;
					break;
	    	}
			}
			int32_t status = StartCritical();
			voltage = 3000*tempDCcomponent/4095;               // calibrate your device so voltage is in mV
			distance = 10000/(((voltage - 100)*10000/26500) + 125);
			if(distance < 80){          // distance in cm
			  IRBuff[i] = distance;                   // store to buffer for display later
				EndCritical(status);
			}
			else{
				i--;                                       // erroneous data. retry data point
				EndCritical(status);
			}
		}
		ST7735_FillScreen(0);            // set screen to black
		ST7735_Message(0,0,"Test # ", testNumIR);
		for(int i = 0; i<5; i++){
			uint32_t dist = (IRBuff[i]);    // distance in cm
		  ST7735_Message(0,i+1,"Dist(cm) = ", dist);   // display results
		}
		debounceIR = 0;
	}
	EndCritical(status);
	OS_Kill();
}
