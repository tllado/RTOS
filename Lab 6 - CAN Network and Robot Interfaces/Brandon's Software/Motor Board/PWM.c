// PWM.c
// Provides PWM outputs on select microcontoller pins
// Runs on TM4C123
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 3rd, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PWM.h"


// The following table provides sample parameter
// inputs for the functions in this module.
// ---------------------------
// Freq(Hz)    Duty      duty%
// ---------------------------
// 40000       30000			75%
// 40000       10000     25%
// 40000        4000     10%
//  4000        2000     50%
//  1000         900     90%
//  1000         100     10%
// ---------------------------


// ******** PWM0A_Init ************
// Initialize PWM0A (M0PWM0 on PB6).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM1
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB6/M0PWM0
void PWM0A_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= 0x01;             // 1) activate PWM0
  SYSCTL_RCGCGPIO_R |= 0x02;            // 2) activate port B
  while((SYSCTL_PRGPIO_R&0x02) == 0){}; // allow time to finish activating
  GPIO_PORTB_AFSEL_R |= 0x40;           // enable alt funct on PB6
  GPIO_PORTB_PCTL_R |= (GPIO_PORTB_PCTL_R&0xF0FFFFFF)|0x04000000;  // configure PB6 as M0PWM0
  GPIO_PORTB_AMSEL_R &= ~0x40;          // disable analog functionality on PB6
  GPIO_PORTB_DEN_R |= 0x40;             // enable digital I/O on PB6
  SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV; // 3) use PWM divider
  SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M; //    clear PWM divider field
  SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_2;  //    configure for /2 divider
  PWM0_0_CTL_R = 0;                     // 4) re-loading down-counting mode
  PWM0_0_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);  // PB6 goes low on LOAD, high on CMPA down
  PWM0_0_LOAD_R = period - 1;           // 5) cycles needed to count down to 0
  PWM0_0_CMPA_R = duty - 1;             // 6) count value when output rises
  PWM0_0_CTL_R |= 0x00000001;           // 7) start PWM0
  PWM0_ENABLE_R |= 0x00000001;          // enable PB6/M0PWM0
}


// ******** PWM0A_Duty ************
// Adjust the duty cycle of PWM signal on PB6
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM0A_Duty(uint16_t duty){
  PWM0_0_CMPA_R = duty - 1;             // 6) count value when output rises
}


// ******** PWM0B_Init ************
// Initialize PWM0B (M0PWM1 on PB7).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM0
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB7/M0PWM1
void PWM0B_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= 0x01;             // 1) activate PWM0
  SYSCTL_RCGCGPIO_R |= 0x02;            // 2) activate port B
  while((SYSCTL_PRGPIO_R&0x02) == 0){}; // allow time to finish activating
  GPIO_PORTB_AFSEL_R |= 0x80;           // enable alt funct on PB7
  GPIO_PORTB_PCTL_R |= (GPIO_PORTB_PCTL_R&0x0FFFFFFF)|0x40000000;  // configure PB7 as M0PWM1
  GPIO_PORTB_AMSEL_R &= ~0x80;          // disable analog functionality on PB7
  GPIO_PORTB_DEN_R |= 0x80;             // enable digital I/O on PB7
  SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV; // 3) use PWM divider
  SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M; //    clear PWM divider field
  SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_2;  //    configure for /2 divider
  PWM0_0_CTL_R = 0;                     // 4) re-loading down-counting mode
  PWM0_0_GENB_R = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO); // PB7 goes low on LOAD, high on CMPB down
  PWM0_0_LOAD_R = period - 1;           // 5) cycles needed to count down to 0
  PWM0_0_CMPB_R = duty - 1;             // 6) count value when output rises
  PWM0_0_CTL_R |= 0x00000001;           // 7) start PWM0
  PWM0_ENABLE_R |= 0x00000002;          // enable PB7/M0PWM1
}


// ******** PWM0B_Duty ************
// Adjust the duty cycle of PWM signal on PB7
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM0B_Duty(uint16_t duty){
  PWM0_0_CMPB_R = duty - 1;             // 6) count value when output rises
}

