// Timer4.c
// Runs on TM4C123 
// Use Timer4 in 32-bit periodic mode to request interrupts at a periodic rate.
// Timer4 is in charge of running background threads in the OS.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// February 21, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Interrupts.h"
#include "Timer4.h"
#include "Lab6.h"
#include "OS.h"


//***********Global Variables******************
void (*BackgroundTask)(void);   // background function called by Timer1 ISR handler
//*********************************************

// ***************** Timer4_Init ****************
// Activate Timer interrupts periodically.  Timer is in charge of running
// background threads in the OS.  This timer has highest priority
// Inputs:  period in units (1/clockfreq)
// Outputs: none
void Timer4_Init(unsigned long period){
  SYSCTL_RCGCTIMER_R |= 0x10;                         // 0) activate TIMER4
	while((SYSCTL_PRTIMER_R&0x10) == 0){};              // allow time for clock to stabilize
  TIMER4_CTL_R = 0x00000000;                          // 1) disable TIMER4A during setup
  TIMER4_CFG_R = 0x00000000;                          // 2) configure for 32-bit mode
  TIMER4_TAMR_R = 0x00000002;                         // 3) configure for periodic mode, default down-count settings
  TIMER4_TAILR_R = period-1;                          // 4) reload value
  TIMER4_TAPR_R = 0;                                  // 5) bus clock resolution
  TIMER4_ICR_R = 0x00000001;                          // 6) clear TIMER4A timeout flag
  TIMER4_IMR_R = 0x00000001;                          // 7) arm timeout interrupt
  NVIC_PRI17_R = (NVIC_PRI17_R&0xFF00FFFF)|(0 << 17); // 8) priority 0 
}

// ***************** Timer4A_Handler ******************
// Timer4 is in charge of running background threads in the OS.
void Timer4A_Handler(void){
  TIMER4_ICR_R = TIMER_ICR_TATOCINT;                  // acknowledge TIMER4A timeout
	int32_t status = StartCritical();
	TCBTypeBGround *tempTCBPt = FirstBackgroundPt;      // tempTCBPt used to cycle through TCB list. Start at front of line
  do{                                                 // cycle through all backgroundTCBs
		TCBTypeBGround *nextTCBPt = tempTCBPt->next;      // Save the next backgroundTCB to check, incase it is lost in OS_KillAperiodic
		BackgroundTCBs[tempTCBPt->id].sleepCnt -= 1;      // decrement backgroundTCB's sleep counter
		if(BackgroundTCBs[tempTCBPt->id].sleepCnt == 0){  // execute background task
			BackgroundTask = tempTCBPt->task;
			BackgroundTask();
			if(tempTCBPt->aperiodic){                            // if true, thread is aperiodic
				OS_KillAperiodic(tempTCBPt->id);                   // kill aperidic thread. Remove it from backgroundTCB list and decrement NumBGround
		  }
		  else{                                                // else, thread is periodic
				BackgroundTCBs[tempTCBPt->id].sleepCnt = SLEEPCNT_CONVERT(BackgroundTCBs[tempTCBPt->id].period);  // reset the background threads sleepcnt after execution
		  }
		}
		tempTCBPt = nextTCBPt;                                 // access next TCB in list
	}while(tempTCBPt != FirstBackgroundPt);
	EndCritical(status);
}
	

// ***************** Timer4A_Enable ******************
void Timer4A_Enable(void){
	NVIC_EN2_R = 1<<(70-(32*2));       // enable IRQ 70 in NVIC
	TIMER4_CTL_R = 0x00000001;         // enable TIMER4A
}


// ***************** Timer1A_Disable ******************
void Timer4A_Disable(void){
	NVIC_DIS2_R = 1<<(70-(32*2));      // disable IRQ 70 in NVIC
	TIMER4_CTL_R = 0x00000000;         // disable TIMER4A
}


