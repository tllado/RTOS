// PWM.c
// Runs on TM4C123 
// Initiates and updates PWM signals on PB4, PB5, PB6, PB7, PD0, and PD1 pins
//  for motor and servo control
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 14th, 2016

///////////////////////////////////////////////////////////////////////////////
// Dependencies
#include <stdint.h>
#include "tm4c123gh6pm.h"

///////////////////////////////////////////////////////////////////////////////
// User Settings
const uint16_t motFrq = 800;        // Hz
const uint16_t srvFrq = 50;         // Hz
const uint32_t sysFrq = 80000000;   // Hz
const uint16_t PWMDiv = 32;         // System Clock ticks per PWM Clock tick

///////////////////////////////////////////////////////////////////////////////
// Global Variables
const uint16_t motPrdDef = sysFrq/PWMDiv/motFrq;
const uint16_t motDtyDef = 0;                       // idle
const uint16_t srvPrdDef = sysFrq/PWMDiv/srvFrq;
const uint16_t srvDtyDef = srvPrdDef * 1.5/20 - 1;  // 1.5ms, center

///////////////////////////////////////////////////////////////////////////////
// PWMInit()
// Initializes PB4,PB5,PB6,PB7 at motFrq frequency and 0 duty cycle.
//  Initializes PD0,PD1 at srvFrq frequency and 1.5ms duty cycle.
// Takes no input.
// Gives no output.
void PWMInit(void) {
    // Initialize PB4, PB5, PB6, PB7, PD0, and PD1 for PWM
    SYSCTL_RCGCPWM_R |= SYSCTL_RCGCPWM_R0;  // activate clock for PWM Mod 0
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R1|SYSCTL_RCGCGPIO_R3;
                                            // activate clock for Port B
    while((SYSCTL_PRGPIO_R&SYSCTL_PRGPIO_R1) == 0){};
                                            // allow time to finish activating
    while((SYSCTL_PRGPIO_R&SYSCTL_PRGPIO_R3) == 0){};
                                            // allow time to finish activating
    GPIO_PORTB_AFSEL_R |= 0xF0;             // enable alt funct on PB4-7
    GPIO_PORTB_PCTL_R &= ~0xFFFF0000;
    GPIO_PORTB_PCTL_R |= 0x44440000;        // configure PB4-7 as PWM
    GPIO_PORTB_AMSEL_R &= ~0xF0;            // disable analog function on PB4-7
    GPIO_PORTB_DEN_R |= 0xF0;               // enable digital I/O on PB4-7
    GPIO_PORTD_AFSEL_R |= 0x03;             // enable alt funct on PD0-1
    GPIO_PORTD_PCTL_R &= ~0x000000FF;
    GPIO_PORTD_PCTL_R |= 0x00000044;        // configure PD0-1 as PWM
    GPIO_PORTD_AMSEL_R &= ~0x03;            // disable analog function on PD0-1
    GPIO_PORTD_DEN_R |= 0x03;               // enable digital I/O on PD0-1
        
    // Set PWM clock to fractin of system clock
    SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV;
    SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M;
    SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_32;
        
    // Setup PB4-7, PD0-1 for motor control
    PWM0_0_CTL_R = 0;                       // set countdown mode for Mod0 Blk0
    PWM0_1_CTL_R = 0;                       // set countdown mode for Mod0 Blk1
    PWM0_3_CTL_R = 0;                       // set countdown mode for Mod0 Blk3
    PWM0_0_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);
    PWM0_0_GENB_R = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO);
    PWM0_1_GENA_R = (PWM_1_GENA_ACTCMPAD_ONE|PWM_1_GENA_ACTLOAD_ZERO);
    PWM0_1_GENB_R = (PWM_1_GENB_ACTCMPBD_ONE|PWM_1_GENB_ACTLOAD_ZERO);
    PWM0_3_GENA_R = (PWM_3_GENA_ACTCMPAD_ONE|PWM_3_GENA_ACTLOAD_ZERO);
    PWM0_3_GENB_R = (PWM_3_GENB_ACTCMPBD_ONE|PWM_3_GENB_ACTLOAD_ZERO);
                                            // define signal triggers
    PWM0_0_LOAD_R = motPrdDef;
    PWM0_1_LOAD_R = motPrdDef;
    PWM0_3_LOAD_R = srvPrdDef;              // set counter reset value (period)
    PWM0_0_CMPA_R = motDtyDef;              // set comparator value (duty)
    PWM0_0_CMPB_R = motDtyDef;
    PWM0_1_CMPA_R = motDtyDef;
    PWM0_1_CMPB_R = motDtyDef;
    PWM0_3_CMPA_R = srvDtyDef;
    PWM0_3_CMPB_R = srvDtyDef;
    PWM0_0_CTL_R |= PWM_0_CTL_ENABLE;       // start Mod 0 Blk 0
    PWM0_1_CTL_R |= PWM_1_CTL_ENABLE;       // start Mod 0 Blk 1
    PWM0_3_CTL_R |= PWM_3_CTL_ENABLE;       // start Mod 0 Blk 3
    PWM0_ENABLE_R |= (PWM_ENABLE_PWM0EN|PWM_ENABLE_PWM1EN|PWM_ENABLE_PWM2EN|\
    PWM_ENABLE_PWM3EN|PWM_ENABLE_PWM6EN|PWM_ENABLE_PWM7EN);
                                            // enable PWM Module 0
}

///////////////////////////////////////////////////////////////////////////////
// updateDuties()
// Converts two motor speeds to four PWM duty values, converts one servo
//  position into one PWM duty cycle, and updates six corresponding registries.
// Takes input of three float values representing two motor speeds and one
//  servo position. All values are assumed to be <1.0, >-1.0.
// Gives no output.
void updateDuties(float speedL, float speedR, float servoPos){
    // Set left motor speed
    if(speedL > 0) {
        PWM0_0_CMPA_R = motPrdDef*speedL - 2;
        PWM0_0_CMPB_R = 0;
    }
    else if (speedL < 0) {
        PWM0_0_CMPA_R = 0;
        PWM0_0_CMPB_R = motPrdDef*(-1*speedL) - 2;
    }
    else {
        PWM0_0_CMPA_R = 0;
        PWM0_0_CMPB_R = 0;
    }
    
    // Set right motor speed
    if(speedR > 0) {
        PWM0_1_CMPA_R = 0;
        PWM0_1_CMPB_R = motPrdDef*speedR - 2;
    }
    else if (speedR < 0) {
        PWM0_1_CMPA_R = motPrdDef*(-1*speedR) - 2;
        PWM0_1_CMPB_R = 0;
    }
    else {
        PWM0_1_CMPA_R = 0;
        PWM0_1_CMPB_R = 0;
    }

    // Set servo positions
    PWM0_3_CMPA_R = srvDtyDef*(1 + servoPos/3) - 2;
    PWM0_3_CMPB_R = 0;  // Currently not in use
}
