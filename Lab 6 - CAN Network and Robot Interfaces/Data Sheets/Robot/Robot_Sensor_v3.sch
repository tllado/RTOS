<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<description>&lt;b&gt;Robot PCB, Sensor board for EE445M, version 3&lt;/b&gt;
&lt;p&gt;
Jonathan Valvano, Daniel Valvano, &lt;br/&gt;
Steven Prickett &lt;br/&gt;
Brent Cameron Wylie&lt;br/&gt;
Others on the team :
Kelvin Le ,
Philippe Dollo ,
maxsigrist@utexas.edu,
dho@utexas.edu&lt;p&gt;

This PCB implements labs associated with the Embbeded Systems books&lt;br/&gt;
&lt;u&gt;Embedded Systems:
Introduction to ARM Cortex-M Microcontrollers&lt;/u&gt;, 2015, ISBN:
978-1477508992, &lt;a target="_blank"
href="http://users.ece.utexas.edu/%7Evalvano/arm/outline1.htm"&gt;&lt;b&gt;http://users.ece.utexas.edu/~valvano/arm/outline1.htm&lt;/b&gt;&lt;/a&gt;&lt;br&gt;
&lt;u&gt;Embedded Systems: Real-Time
Interfacing to ARM
Cortex-M Microcontrollers&lt;/u&gt;, 2015, ISBN: 978-1463590154, &lt;b&gt;&lt;a target="_blank"
href="http://users.ece.utexas.edu/%7Evalvano/arm/outline.htm"&gt;http://users.ece.utexas.edu/~valvano/arm/outline.htm&lt;/a&gt;&lt;/b&gt;&lt;br&gt;
&lt;u&gt;Embedded Systems: Real-Time Operating
Systems for the ARM Cortex-M Microcontrollers &lt;/u&gt;, 2015,
ISBN: 978-1466468863, &lt;b&gt;&lt;a target="_blank"
href="http://users.ece.utexas.edu/%7Evalvano/arm/outline3.htm"&gt;http://users.ece.utexas.edu/~valvano/arm/outline3.htm&lt;/a&gt;&lt;/b&gt;&lt;br&gt;
Sensor Board Features
&lt;ol&gt;
&lt;li&gt;TM4C123 LaunchPad connected to sensor board via CAN&lt;/li&gt;
&lt;li&gt;Two bumper inputs (2-pin 0.1in male headers)&lt;/li&gt;
&lt;li&gt;Color LCD display/SD card (10-pin 0.1in female socket)&lt;/li&gt;
&lt;li&gt;Four analog IR distance inputs  (4-pin 0.2mm Seeed male headers)&lt;/li&gt;
&lt;li&gt;Four input capture, 3/4 pin ultrasonic sensor  (3-pin and 4-pin 0.1in male headers)&lt;/li&gt;
&lt;li&gt;Wifi using ESP8266 module  (2by 5-pin 0.1in male header)&lt;/li&gt;
&lt;/ol&gt;
December 9, 2015
&lt;/p&gt;</description>
<libraries>
<library name="EE445L">
<description>&lt;b&gt;EE445L Library&lt;/b&gt;
&lt;p&gt;
This library has been compiled by EE445L professors, TAs,
and students.  It is meant as a service to students,
simplifying the design, manufacturing, and testing of PCBs associated with the 
Cirqoid PCB milling machine in the College of Engineering &lt;b&gt;Makerspace&lt;/b&gt;.
For more information about the PCB mill see &lt;b&gt;http://cirqoid.com/&lt;/b&gt;.
There are a number of restrictions that we will need to follow to simplify the PCB construction.
&lt;p&gt;
&lt;b&gt;PCB size.&lt;/b&gt; There are two standard sizes of the PCB material one can order from Cirqoid: 100 by 75 mm, and 100 by 160 mm. The smaller size allows you to design the  PCB in the freeware version of Eagle. You can of course, set the board size to be less than 100 by 75 mm.
&lt;p&gt;
&lt;b&gt;Number of layers.&lt;/b&gt; You can create single or double layer PCBs with the Cirqoid PCB mill.
&lt;p&gt;
&lt;b&gt;Drill sizes.&lt;/b&gt; The Makerspace only has specific drill sizes. This library will restrict drill sizes to these four:
 0.0157in=0.4mm,
 0.0315in=0.8mm,
 0.0433in=1.1mm, and
 0.120in=3.0mm. You can only drill holes of a certain size, if you have a drill of that size. Second, the fewer drill sizes you use the faster it will be to manufacture the board.
&lt;p&gt;
&lt;b&gt;Placement of vias.&lt;/b&gt; The Cirqoid PCB mill will not pour copper into vias which run from the top to bottom layers. Therefore you will need to place wire into the vias and solder on both sides. This means one must be careful where the vias are located (e.g., not under chips_ so you will have access to solder both layers.&lt;p&gt;
Use the &lt;b&gt;ADD&lt;/b&gt; command in the Schematic Editor or Layout Editor
window to search for a certain device or package!
&lt;p&gt;
If you are going to create new parts, please do not add them to this library. This is because we routinely make updates to the this library, and if you add your part to this library, it will/may be lost when you install a newer version of the EE445L library.
&lt;p&gt;
Please let me know if you find any mistakes at &lt;b&gt;valvano@mail.utexas.edu&lt;/b&gt;. Thank you and have fun,&lt;p&gt;
Jonathan Valvano, University of Texas at Austin, 
UTA7.343, 1616 Guadalupe Street, Austin, TX, 78701</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="RES200MIL">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<pad name="2" x="2.54" y="0" drill="0.8"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="RES300MIL">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8"/>
<pad name="2" x="3.81" y="0" drill="0.8"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="RES400MIL">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8"/>
<pad name="2" x="5.08" y="0" drill="0.8"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="RES_VERTICAL">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8"/>
<pad name="2" x="1.27" y="0" drill="0.8"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round, , 0.8mm holes, 100 mil spacing</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="-2.5908" y1="1.7272" x2="-1.8542" y2="1.7272" width="0.127" layer="21"/>
<wire x1="-2.2352" y1="1.3208" x2="-2.2352" y2="2.1082" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8" diameter="1.6764"/>
<pad name="K" x="1.27" y="0" drill="0.8" diameter="1.6764" shape="square"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round, 0.8mm holes, 100 mil spacing</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8" diameter="1.6764"/>
<pad name="K" x="1.27" y="0" drill="0.8" diameter="1.6764" shape="square"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TM4C123LAUNCHPAD">
<description>&lt;B&gt;Two Dual In Line&lt;/B&gt;&lt;br&gt;
Samtec ESW-110-37-L-D&lt;br&gt;
or FCI 67997-210HLF, Digikey 609-3236-ND</description>
<wire x1="-19.05" y1="-11.303" x2="-19.05" y2="13.843" width="0.1524" layer="25"/>
<wire x1="-24.13" y1="13.843" x2="-19.05" y2="13.843" width="0.1524" layer="25"/>
<wire x1="-24.13" y1="-11.303" x2="-19.05" y2="-11.303" width="0.1524" layer="25"/>
<pad name="1" x="-22.86" y="12.7" drill="0.9" shape="square" rot="R90"/>
<pad name="2" x="-22.86" y="10.16" drill="0.9"/>
<pad name="3" x="-22.86" y="7.62" drill="0.9"/>
<pad name="4" x="-22.86" y="5.08" drill="0.9"/>
<pad name="5" x="-22.86" y="2.54" drill="0.9"/>
<pad name="6" x="-22.86" y="0" drill="0.9"/>
<pad name="7" x="-22.86" y="-2.54" drill="0.9"/>
<pad name="8" x="-22.86" y="-5.08" drill="0.9"/>
<pad name="9" x="-22.86" y="-7.62" drill="0.9"/>
<pad name="10" x="-22.86" y="-10.16" drill="0.9"/>
<pad name="11" x="22.86" y="-10.16" drill="0.9"/>
<pad name="12" x="22.86" y="-7.62" drill="0.9"/>
<pad name="13" x="22.86" y="-5.08" drill="0.9"/>
<pad name="14" x="22.86" y="-2.54" drill="0.9"/>
<pad name="15" x="22.86" y="0" drill="0.9"/>
<pad name="16" x="22.86" y="2.54" drill="0.9"/>
<pad name="17" x="22.86" y="5.08" drill="0.9"/>
<pad name="18" x="22.86" y="7.62" drill="0.9"/>
<pad name="19" x="22.86" y="10.16" drill="0.9"/>
<pad name="20" x="22.86" y="12.7" drill="0.9"/>
<pad name="21" x="-20.32" y="12.7" drill="0.9"/>
<pad name="22" x="-20.32" y="10.16" drill="0.9"/>
<pad name="23" x="-20.32" y="7.62" drill="0.9"/>
<pad name="24" x="-20.32" y="5.08" drill="0.9"/>
<pad name="25" x="-20.32" y="2.54" drill="0.9"/>
<pad name="26" x="-20.32" y="0" drill="0.9"/>
<pad name="27" x="-20.32" y="-2.54" drill="0.9"/>
<pad name="28" x="-20.32" y="-5.08" drill="0.9"/>
<pad name="29" x="-20.32" y="-7.62" drill="0.9"/>
<pad name="30" x="-20.32" y="-10.16" drill="0.9"/>
<pad name="31" x="20.32" y="-10.16" drill="0.9"/>
<pad name="32" x="20.32" y="-7.62" drill="0.9"/>
<pad name="33" x="20.32" y="-5.08" drill="0.9"/>
<pad name="34" x="20.32" y="-2.54" drill="0.9"/>
<pad name="35" x="20.32" y="0" drill="0.9"/>
<pad name="36" x="20.32" y="2.54" drill="0.9"/>
<pad name="37" x="20.32" y="5.08" drill="0.9"/>
<pad name="38" x="20.32" y="7.62" drill="0.9"/>
<pad name="39" x="20.32" y="10.16" drill="0.9"/>
<pad name="40" x="20.32" y="12.7" drill="0.9"/>
<text x="-18.034" y="17.399" size="1.778" layer="25" font="vector" ratio="10" rot="R180">&gt;NAME</text>
<text x="17.78" y="15.5448" size="1.778" layer="25" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-24.13" y1="-11.303" x2="-24.13" y2="13.843" width="0.1524" layer="25"/>
<text x="-24.13" y="13.97" size="1.27" layer="25" font="vector" rot="SR0">3.3</text>
<text x="-24.13" y="-12.7" size="1.27" layer="25" font="vector" rot="SR0">A7</text>
<text x="21.59" y="-12.7" size="1.27" layer="25" font="vector" rot="SR0">A2</text>
<text x="22.86" y="13.97" size="1.27" layer="25" font="vector" rot="SR0">G</text>
<text x="-20.32" y="13.97" size="1.27" layer="25" font="vector" rot="SR0">5</text>
<text x="-21.59" y="-12.7" size="1.27" layer="25" font="vector" rot="SR0">F1</text>
<text x="19.05" y="-12.7" size="1.27" layer="25" font="vector" rot="SR0">F4</text>
<text x="19.05" y="13.97" size="1.27" layer="25" font="vector" rot="SR0">F2</text>
<wire x1="19.05" y1="13.843" x2="24.13" y2="13.843" width="0.1524" layer="25"/>
<wire x1="19.05" y1="-11.303" x2="19.05" y2="13.843" width="0.1524" layer="25"/>
<wire x1="19.05" y1="-11.303" x2="24.13" y2="-11.303" width="0.1524" layer="25"/>
<wire x1="24.13" y1="-11.303" x2="24.13" y2="13.843" width="0.1524" layer="25"/>
<wire x1="-25.4" y1="-22.86" x2="25.4" y2="-22.86" width="0.3048" layer="25"/>
<wire x1="25.4" y1="-22.86" x2="25.4" y2="43.18" width="0.3048" layer="25"/>
<wire x1="25.4" y1="43.18" x2="-25.4" y2="43.18" width="0.3048" layer="25"/>
<wire x1="-25.4" y1="43.18" x2="-25.4" y2="-22.86" width="0.3048" layer="25"/>
</package>
<package name="DIL08">
<description>&lt;b&gt;Dual In Line&lt;/b&gt; 0.8mm holes, 2.54mm=100mil spacing</description>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8" diameter="1.6002" shape="square"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8" diameter="1.6002"/>
<pad name="5" x="3.81" y="3.81" drill="0.8" diameter="1.6002"/>
<pad name="6" x="1.27" y="3.81" drill="0.8" diameter="1.6002"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8" diameter="1.6002"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8" diameter="1.6002"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8" diameter="1.6002"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8" diameter="1.6002"/>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.715" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
</package>
<package name="1X04">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1" shape="square" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-5.1562" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
<package name="1X04/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="1" shape="square" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="1" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="1" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="1" shape="long" rot="R90"/>
<text x="-5.715" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.985" y="-4.445" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
</package>
<package name="1X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1" shape="square" rot="R90"/>
<pad name="2" x="0" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="1X03/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-2.54" y="-3.81" drill="1" shape="square" rot="R90"/>
<pad name="2" x="0" y="-3.81" drill="1" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="1" shape="long" rot="R90"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
</package>
<package name="CAP100MIL">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm = 100mil, 0.8mm holes</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8"/>
<pad name="2" x="1.27" y="0" drill="0.8"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CAP200MIL">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm = 200mil, 0.8mm holes</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<pad name="2" x="2.54" y="0" drill="0.8"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="square" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1" shape="square" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
<package name="ESP8266_01">
<pad name="RX" x="1.27" y="-3.81" drill="1" diameter="1.778" rot="R180"/>
<pad name="GPIO_0" x="1.27" y="-1.27" drill="1" diameter="1.778" rot="R180"/>
<pad name="GPIO_2" x="1.27" y="1.27" drill="1" diameter="1.778" rot="R180"/>
<pad name="GND" x="1.27" y="3.81" drill="1" diameter="1.778" rot="R180"/>
<pad name="TX" x="-1.27" y="3.81" drill="1" diameter="1.778" rot="R180"/>
<pad name="CH_PD" x="-1.27" y="1.27" drill="1" diameter="1.778" rot="R180"/>
<pad name="RESET" x="-1.27" y="-1.27" drill="1" diameter="1.778" rot="R180"/>
<pad name="VCC" x="-1.27" y="-3.81" drill="1" diameter="1.778" rot="R180"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="7.62" width="0.127" layer="21"/>
<wire x1="-2.54" y1="7.62" x2="22.86" y2="7.62" width="0.127" layer="21"/>
<wire x1="22.86" y1="7.62" x2="22.86" y2="-7.62" width="0.127" layer="21"/>
<wire x1="22.86" y1="-7.62" x2="-2.54" y2="-7.62" width="0.127" layer="21"/>
<text x="20.32" y="0" size="1.778" layer="21" font="vector" ratio="15" rot="R270" align="center">&gt;NAME</text>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
</package>
<package name="SO08">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt;&lt;br&gt;Also called SOIC&lt;br&gt;0.05in (1.27mm) between pins&lt;br&gt;
0.21in (5.4mm) center to center of pins&lt;br&gt;
0.27in (6.9mm) tip to tip</description>
<wire x1="-2.362" y1="-1.803" x2="2.362" y2="-1.803" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-1.803" x2="2.362" y2="1.803" width="0.1524" layer="21"/>
<wire x1="2.362" y1="1.803" x2="-2.362" y2="1.803" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="1.803" x2="-2.362" y2="-1.803" width="0.1524" layer="21"/>
<circle x="-1.8034" y="-0.9906" radius="0.3556" width="0.0508" layer="21"/>
<smd name="1" x="-1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="-1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="-0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<text x="4.0005" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.7305" y="-2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-2.8702" x2="-1.7272" y2="-1.8542" layer="51"/>
<rectangle x1="-0.8128" y1="-2.8702" x2="-0.4572" y2="-1.8542" layer="51"/>
<rectangle x1="0.4572" y1="-2.8702" x2="0.8128" y2="-1.8542" layer="51"/>
<rectangle x1="1.7272" y1="-2.8702" x2="2.0828" y2="-1.8542" layer="51"/>
<rectangle x1="-2.0828" y1="1.8542" x2="-1.7272" y2="2.8702" layer="51"/>
<rectangle x1="-0.8128" y1="1.8542" x2="-0.4572" y2="2.8702" layer="51"/>
<rectangle x1="0.4572" y1="1.8542" x2="0.8128" y2="2.8702" layer="51"/>
<rectangle x1="1.7272" y1="1.8542" x2="2.0828" y2="2.8702" layer="51"/>
</package>
<package name="ST7735">
<description>&lt;b&gt;ST7735 LCD&lt;/b&gt;&lt;p&gt;
SamTec 10-pin LCD connector, BCS-110-L-S-TE&lt;br&gt;
Visible image is 1.1 by 1.4 inch&lt;br&gt;
Mounting hardware 5-56 screw</description>
<wire x1="-15.875" y1="22.86" x2="-15.875" y2="17.145" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="17.145" x2="-15.875" y2="-29.591" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-29.591" x2="-15.875" y2="-33.02" width="0.1524" layer="21"/>
<wire x1="18.415" y1="-33.02" x2="-15.875" y2="-33.02" width="0.1524" layer="21"/>
<wire x1="18.415" y1="22.86" x2="18.415" y2="17.145" width="0.1524" layer="21"/>
<wire x1="18.415" y1="17.145" x2="18.415" y2="-29.591" width="0.1524" layer="21"/>
<wire x1="18.415" y1="-29.591" x2="18.415" y2="-33.02" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="22.86" x2="18.415" y2="22.86" width="0.1524" layer="21"/>
<pad name="1" x="-10.16" y="-30.48" drill="1" shape="square" rot="R90"/>
<pad name="2" x="-7.62" y="-30.48" drill="1" shape="long" rot="R90"/>
<pad name="3" x="-5.08" y="-30.48" drill="1" shape="long" rot="R90"/>
<pad name="4" x="-2.54" y="-30.48" drill="1" shape="long" rot="R90"/>
<pad name="5" x="0" y="-30.48" drill="1" shape="long" rot="R90"/>
<pad name="6" x="2.54" y="-30.48" drill="1" shape="long" rot="R90"/>
<pad name="7" x="5.08" y="-30.48" drill="1" shape="long" rot="R90"/>
<pad name="8" x="7.62" y="-30.48" drill="1" shape="long" rot="R90"/>
<pad name="9" x="10.16" y="-30.48" drill="1" shape="long" rot="R90"/>
<pad name="10" x="12.7" y="-30.48" drill="1" shape="long" rot="R90"/>
<text x="-7.62" y="20.32" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.651" y="20.32" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<hole x="-13.335" y="20.32" drill="3"/>
<hole x="15.875" y="20.32" drill="3"/>
<wire x1="-15.875" y1="17.145" x2="18.415" y2="17.145" width="0.127" layer="21"/>
<wire x1="18.415" y1="-29.591" x2="-15.875" y2="-29.591" width="0.127" layer="21"/>
<text x="-11.43" y="-27.94" size="1.27" layer="21">1</text>
<wire x1="15.24" y1="13.97" x2="15.24" y2="-21.59" width="0.127" layer="21"/>
<wire x1="15.24" y1="-21.59" x2="-12.7" y2="-21.59" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-21.59" x2="-12.7" y2="13.97" width="0.127" layer="21"/>
<wire x1="-12.7" y1="13.97" x2="15.24" y2="13.97" width="0.127" layer="21"/>
<text x="-13.97" y="15.24" size="1.27" layer="21">(0,0)</text>
<text x="10.16" y="-24.13" size="1.27" layer="21">(128,160)</text>
<wire x1="-8.89" y1="6.35" x2="-8.89" y2="12.7" width="0.127" layer="21"/>
<wire x1="-8.89" y1="12.7" x2="-10.16" y2="11.43" width="0.127" layer="21"/>
<wire x1="-8.89" y1="12.7" x2="-7.62" y2="11.43" width="0.127" layer="21"/>
<text x="-10.16" y="3.81" size="1.27" layer="21">Up</text>
</package>
<package name="P1.1MM">
<description>&lt;b&gt;TEST PAD&lt;/b&gt; 1.1mm hole</description>
<circle x="0" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.1" diameter="2.159" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="1_04X2MM">
<text x="-6" y="1.5" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="4.75" y="-0.25" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5" y1="0.5" x2="-4.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="1" x2="-3.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-3.5" y1="1" x2="-3" y2="0.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="-0.5" x2="-3.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-3.5" y1="-1" x2="-4.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-1" x2="-5" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-5" y1="0.5" x2="-5" y2="-0.5" width="0.1524" layer="21"/>
<pad name="1" x="-4" y="0" drill="1.016" diameter="1.3" shape="square" rot="R90"/>
<rectangle x1="-4.254" y1="-0.254" x2="-3.746" y2="0.254" layer="51"/>
<wire x1="-3" y1="0.5" x2="-2.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="1" x2="-1.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="1" x2="-1" y2="0.5" width="0.1524" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-1" x2="-2.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="-1" x2="-3" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="0.5" x2="-3" y2="-0.5" width="0.1524" layer="21"/>
<pad name="2" x="-2" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<rectangle x1="-2.254" y1="-0.254" x2="-1.746" y2="0.254" layer="51"/>
<wire x1="-1" y1="0.5" x2="-0.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="1" x2="0.5" y2="1" width="0.1524" layer="21"/>
<wire x1="0.5" y1="1" x2="1" y2="0.5" width="0.1524" layer="21"/>
<wire x1="1" y1="-0.5" x2="0.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="0.5" y1="-1" x2="-0.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="-1" x2="-1" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.1524" layer="21"/>
<pad name="3" x="0" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<wire x1="1" y1="0.5" x2="1.5" y2="1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="1" x2="2.5" y2="1" width="0.1524" layer="21"/>
<wire x1="2.5" y1="1" x2="3" y2="0.5" width="0.1524" layer="21"/>
<wire x1="3" y1="-0.5" x2="2.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="2.5" y1="-1" x2="1.5" y2="-1" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-1" x2="1" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="1" y1="0.5" x2="1" y2="-0.5" width="0.1524" layer="21"/>
<pad name="4" x="2" y="0" drill="1.016" diameter="1.3" rot="R90"/>
<rectangle x1="1.746" y1="-0.254" x2="2.254" y2="0.254" layer="51"/>
<wire x1="3" y1="0.5" x2="3" y2="-0.5" width="0.1524" layer="21"/>
<wire x1="4" y1="-7" x2="1.5" y2="-7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-7" x2="-3.5" y2="-7" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-7" x2="-3.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3" x2="1.5" y2="-3" width="0.127" layer="21"/>
<wire x1="1.5" y1="-3" x2="1.5" y2="-7" width="0.127" layer="21"/>
<wire x1="-4" y1="0" x2="-4" y2="-5.5" width="0.3048" layer="21"/>
<wire x1="-2" y1="0" x2="-2" y2="-5.5" width="0.3048" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-5.5" width="0.3048" layer="21"/>
<wire x1="2" y1="0" x2="2" y2="-5.5" width="0.3048" layer="21"/>
<wire x1="-3.5" y1="-7" x2="-6" y2="-7" width="0.127" layer="21"/>
<wire x1="-6" y1="-7" x2="-6" y2="1" width="0.127" layer="21"/>
<wire x1="-6" y1="1" x2="4" y2="1" width="0.127" layer="21"/>
<wire x1="4" y1="1" x2="4" y2="-7" width="0.127" layer="21"/>
</package>
<package name="TO220-3">
<description>&lt;b&gt;TO-220&lt;/b&gt;</description>
<pad name="1" x="-2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-5.08" y1="3.175" x2="5.08" y2="3.175" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.175" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-1.905" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="-5.08" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="3.175" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<circle x="-4.1275" y="1.27" radius="0.3175" width="0.127" layer="21"/>
</package>
<package name="CAP200MILPOLARIZED">
<description>&lt;b&gt;POLARIZED CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm = 200mil, 0.8mm holes</description>
<wire x1="-2.8702" y1="1.3574" x2="2.8702" y2="1.3574" width="0.1524" layer="21" curve="-129.378377"/>
<wire x1="-2.8702" y1="-1.3574" x2="2.8702" y2="-1.3574" width="0.1524" layer="21" curve="129.378377"/>
<wire x1="-2.8702" y1="1.3574" x2="-2.8702" y2="-1.3574" width="0.1524" layer="51" curve="50.621623"/>
<wire x1="2.8702" y1="-1.3574" x2="2.8702" y2="1.3574" width="0.1524" layer="51" curve="50.621623"/>
<wire x1="-1.397" y1="0" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="0.635" x2="-1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.381" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="0.8" diameter="1.905"/>
<pad name="-" x="2.54" y="0" drill="0.8" diameter="1.905" shape="square"/>
<text x="3.048" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.048" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-4.445" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="TM4C123LAUNCHPAD">
<description>&lt;b&gt;TM4C123 LaunchPad&lt;/b&gt; has 40 pins</description>
<pin name="+3.3V" x="-35.56" y="10.16" length="short" direction="pwr"/>
<pin name="PB5" x="-35.56" y="7.62" length="short"/>
<pin name="PB0" x="-35.56" y="5.08" length="short"/>
<pin name="PB1" x="-35.56" y="2.54" length="short"/>
<pin name="PE4" x="-35.56" y="0" length="short"/>
<pin name="PA5" x="-35.56" y="-7.62" length="short"/>
<pin name="PA6" x="-35.56" y="-10.16" length="short"/>
<pin name="PA7" x="-35.56" y="-12.7" length="short"/>
<pin name="PA2" x="48.26" y="-12.7" length="short" rot="R180"/>
<pin name="PA3" x="48.26" y="-10.16" length="short" rot="R180"/>
<pin name="PA4" x="48.26" y="-7.62" length="short" rot="R180"/>
<pin name="PB6" x="48.26" y="-5.08" length="short" rot="R180"/>
<pin name="PB7" x="48.26" y="-2.54" length="short" rot="R180"/>
<pin name="RESET" x="48.26" y="0" length="short" direction="in" rot="R180"/>
<pin name="PF0" x="48.26" y="2.54" length="short" rot="R180"/>
<pin name="PE0" x="48.26" y="5.08" length="short" rot="R180"/>
<pin name="PB2" x="48.26" y="7.62" length="short" rot="R180"/>
<pin name="GND2" x="48.26" y="10.16" length="short" direction="pwr" rot="R180"/>
<pin name="+5V" x="-12.7" y="10.16" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="-12.7" y="7.62" length="short" direction="pwr" rot="R180"/>
<pin name="PD0" x="-12.7" y="5.08" length="short" rot="R180"/>
<pin name="PD1" x="-12.7" y="2.54" length="short" rot="R180"/>
<pin name="PE2" x="-12.7" y="-7.62" length="short" rot="R180"/>
<pin name="PE3" x="-12.7" y="-10.16" length="short" rot="R180"/>
<pin name="PF1" x="-12.7" y="-12.7" length="short" rot="R180"/>
<pin name="PF4" x="25.4" y="-12.7" length="short"/>
<pin name="PD7" x="25.4" y="-10.16" length="short"/>
<pin name="PD6" x="25.4" y="-7.62" length="short"/>
<pin name="PC7" x="25.4" y="-5.08" length="short"/>
<pin name="PC6" x="25.4" y="-2.54" length="short"/>
<pin name="PC5" x="25.4" y="0" length="short"/>
<pin name="PC4" x="25.4" y="2.54" length="short"/>
<pin name="PB3" x="25.4" y="5.08" length="short"/>
<pin name="PF3" x="25.4" y="7.62" length="short"/>
<pin name="PF2" x="25.4" y="10.16" length="short"/>
<pin name="PE5" x="-35.56" y="-2.54" length="short"/>
<pin name="PE1" x="-12.7" y="-5.08" length="short" rot="R180"/>
<pin name="PB4" x="-35.56" y="-5.08" length="short"/>
<pin name="PD3" x="-12.7" y="-2.54" length="short" rot="R180"/>
<pin name="PD2" x="-12.7" y="0" length="short" rot="R180"/>
<wire x1="-33.02" y1="15.24" x2="-33.02" y2="-15.24" width="0.2" layer="94"/>
<wire x1="-33.02" y1="-15.24" x2="-15.24" y2="-15.24" width="0.2" layer="94"/>
<text x="-30.48" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<text x="-30.48" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-15.24" y1="15.24" x2="-33.02" y2="15.24" width="0.2" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="15.24" width="0.2" layer="94"/>
<text x="-30.48" y="12.7" size="1.778" layer="94">J1</text>
<text x="-20.32" y="12.7" size="1.778" layer="94">J3</text>
<text x="30.48" y="12.7" size="1.778" layer="94">J4</text>
<text x="40.64" y="12.7" size="1.778" layer="94">J2</text>
<wire x1="27.94" y1="15.24" x2="27.94" y2="-15.24" width="0.2" layer="94"/>
<wire x1="45.72" y1="-15.24" x2="45.72" y2="15.24" width="0.2" layer="94"/>
<wire x1="45.72" y1="15.24" x2="27.94" y2="15.24" width="0.2" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="45.72" y2="-15.24" width="0.2" layer="94"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="MCP2551">
<description>&lt;b&gt;MCP2551&lt;/b&gt; CAN driver</description>
<wire x1="-17.78" y1="20.32" x2="-17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-10.16" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-17.78" y2="20.32" width="0.254" layer="94"/>
<text x="-3.81" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-14.224" y="12.7" size="1.27" layer="91">+V</text>
<text x="-13.716" y="-4.064" size="1.27" layer="91">-V</text>
<text x="-17.018" y="4.572" size="1.524" layer="94">TxD</text>
<text x="-17.018" y="-0.508" size="1.524" layer="94">Vref</text>
<pin name="RXD" x="-20.32" y="10.16" visible="pad" length="short" direction="in"/>
<pin name="CANH" x="-2.54" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="VSS" x="-12.7" y="-10.16" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VDD" x="-12.7" y="20.32" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="CANL" x="-2.54" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="TXD" x="-20.32" y="5.08" visible="pad" length="short" direction="out"/>
<pin name="VREF" x="-20.32" y="0" visible="pad" length="short" direction="pas"/>
<pin name="RS" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R90"/>
<text x="-17.018" y="9.652" size="1.524" layer="94">RxD</text>
<text x="-11.938" y="7.112" size="1.524" layer="94">CANH</text>
<text x="-10.668" y="-0.508" size="1.524" layer="94">Rs</text>
<text x="-11.938" y="3.302" size="1.524" layer="94">CANL</text>
</symbol>
<symbol name="PINHD4">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-4.826" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="-2.54" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="ESP-01">
<pin name="RX" x="-17.78" y="7.62" visible="pin" length="middle"/>
<pin name="GPIO_0" x="-17.78" y="2.54" visible="pin" length="middle"/>
<pin name="GPIO_2" x="-17.78" y="-2.54" visible="pin" length="middle"/>
<pin name="GND" x="-17.78" y="-7.62" visible="pin" length="middle"/>
<pin name="TX" x="17.78" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="CH_PD" x="17.78" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="RESET" x="17.78" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="VCC" x="17.78" y="7.62" visible="pin" length="middle" rot="R180"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<text x="0" y="-11.684" size="1.778" layer="95" font="vector" ratio="15" align="center">&gt;NAME</text>
</symbol>
<symbol name="ST7735">
<wire x1="-25.4" y1="13.97" x2="7.62" y2="13.97" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="13.97" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-25.4" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-25.4" y1="13.97" x2="-25.4" y2="-5.08" width="0.4064" layer="94"/>
<text x="-22.86" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="-8.89" y="11.43" size="1.778" layer="96">&gt;VALUE</text>
<pin name="TFT_CS" x="-7.62" y="-7.62" length="short" direction="in" rot="R90"/>
<pin name="VCC" x="-17.78" y="-7.62" length="short" direction="pwr" rot="R90"/>
<pin name="CARD_CS" x="-10.16" y="-7.62" length="short" direction="in" rot="R90"/>
<pin name="SCLK" x="-2.54" y="-7.62" length="short" direction="in" rot="R90"/>
<pin name="MISO" x="0" y="-7.62" length="short" direction="out" rot="R90"/>
<pin name="GND" x="-20.32" y="-7.62" length="short" direction="pwr" rot="R90"/>
<pin name="LIGHT" x="2.54" y="-7.62" length="short" direction="pas" rot="R90"/>
<pin name="C/D" x="-12.7" y="-7.62" length="short" direction="in" rot="R90"/>
<pin name="RESET" x="-15.24" y="-7.62" length="short" direction="in" rot="R90"/>
<pin name="MOSI" x="-5.08" y="-7.62" length="short" direction="in" rot="R90"/>
<text x="-24.13" y="7.62" size="1.778" layer="94">160x128 pixels</text>
<text x="-6.35" y="7.62" size="1.778" layer="94">18-bit color</text>
</symbol>
<symbol name="TP">
<wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<text x="-1.27" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-1.27" size="1.778" layer="97">&gt;TP_SIGNAL_NAME</text>
<pin name="TP" x="0" y="-2.54" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="78XX">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="2.54" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">GND</text>
<text x="-4.445" y="-0.635" size="1.524" layer="95">IN</text>
<text x="0.635" y="-0.635" size="1.524" layer="95">OUT</text>
<pin name="IN" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="off" length="short" direction="in" rot="R90"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CPOL-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="OP">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.032" y1="2.54" x2="-1.016" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="3.048" x2="-1.524" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.54" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<text x="3.81" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="IN+" x="-5.08" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="IN-" x="-5.08" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="10.16" y="0" visible="pad" length="short" direction="out" rot="R180"/>
</symbol>
<symbol name="PWR+-">
<pin name="V+" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="V-" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;+3.3V power signal&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="RES200MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="RES300MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="RES400MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="RES_VERTICAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;Avago Technologies LED&lt;/b&gt;&lt;p&gt;
Green 2mA 5mm diffused,	HLMP-4740,	Digikey 516-1327-ND&lt;p&gt;
Red 1.6V 1mA 5mm diffused,	HLMP-D150,	Digikey 516-1323-ND&lt;p&gt;
Yellow 2mA 5mm diffused,	HLMP-4719,	Digikey 516-1326-ND&lt;p&gt;
Orange 10mA 5mm diffused	HLMP-D401,	Digikey 516-1330-ND&lt;p&gt;
Blue 10mA 5mm diffused</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EK-TM4C123GXL" prefix="IC">
<description>&lt;b&gt;Texas Instruments TM4C123 LaunchPad&lt;/b&gt;&lt;p&gt;EK-TM4C123GXL
&lt;a href="http://www.ti.com/tool/ek-tm4c123gxl"&gt;www.ti.com/tool/ek-tm4c123gxl&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TM4C123LAUNCHPAD" x="0" y="0"/>
</gates>
<devices>
<device name="P" package="TM4C123LAUNCHPAD">
<connects>
<connect gate="G$1" pin="+3.3V" pad="1"/>
<connect gate="G$1" pin="+5V" pad="21"/>
<connect gate="G$1" pin="GND" pad="22"/>
<connect gate="G$1" pin="GND2" pad="20"/>
<connect gate="G$1" pin="PA2" pad="11"/>
<connect gate="G$1" pin="PA3" pad="12"/>
<connect gate="G$1" pin="PA4" pad="13"/>
<connect gate="G$1" pin="PA5" pad="8"/>
<connect gate="G$1" pin="PA6" pad="9"/>
<connect gate="G$1" pin="PA7" pad="10"/>
<connect gate="G$1" pin="PB0" pad="3"/>
<connect gate="G$1" pin="PB1" pad="4"/>
<connect gate="G$1" pin="PB2" pad="19"/>
<connect gate="G$1" pin="PB3" pad="38"/>
<connect gate="G$1" pin="PB4" pad="7"/>
<connect gate="G$1" pin="PB5" pad="2"/>
<connect gate="G$1" pin="PB6" pad="14"/>
<connect gate="G$1" pin="PB7" pad="15"/>
<connect gate="G$1" pin="PC4" pad="37"/>
<connect gate="G$1" pin="PC5" pad="36"/>
<connect gate="G$1" pin="PC6" pad="35"/>
<connect gate="G$1" pin="PC7" pad="34"/>
<connect gate="G$1" pin="PD0" pad="23"/>
<connect gate="G$1" pin="PD1" pad="24"/>
<connect gate="G$1" pin="PD2" pad="25"/>
<connect gate="G$1" pin="PD3" pad="26"/>
<connect gate="G$1" pin="PD6" pad="33"/>
<connect gate="G$1" pin="PD7" pad="32"/>
<connect gate="G$1" pin="PE0" pad="18"/>
<connect gate="G$1" pin="PE1" pad="27"/>
<connect gate="G$1" pin="PE2" pad="28"/>
<connect gate="G$1" pin="PE3" pad="29"/>
<connect gate="G$1" pin="PE4" pad="5"/>
<connect gate="G$1" pin="PE5" pad="6"/>
<connect gate="G$1" pin="PF0" pad="17"/>
<connect gate="G$1" pin="PF1" pad="30"/>
<connect gate="G$1" pin="PF2" pad="40"/>
<connect gate="G$1" pin="PF3" pad="39"/>
<connect gate="G$1" pin="PF4" pad="31"/>
<connect gate="G$1" pin="RESET" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;+5.0V power signal&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP2551P">
<description>&lt;b&gt;MCP2551p&lt;/b&gt;&lt;br&gt;
CAN interface driver</description>
<gates>
<gate name="G$1" symbol="MCP2551" x="10.16" y="-5.08"/>
</gates>
<devices>
<device name="" package="DIL08">
<connects>
<connect gate="G$1" pin="CANH" pad="7"/>
<connect gate="G$1" pin="CANL" pad="6"/>
<connect gate="G$1" pin="RS" pad="8"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
<connect gate="G$1" pin="VREF" pad="5"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOIC" package="SO08">
<connects>
<connect gate="G$1" pin="CANH" pad="7"/>
<connect gate="G$1" pin="CANL" pad="6"/>
<connect gate="G$1" pin="RS" pad="8"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
<connect gate="G$1" pin="VREF" pad="5"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X4" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X04/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="025-024X044" package="CAP100MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="CAP200MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESP-01">
<gates>
<gate name="G$1" symbol="ESP-01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESP8266_01">
<connects>
<connect gate="G$1" pin="CH_PD" pad="CH_PD"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GPIO_0" pad="GPIO_0"/>
<connect gate="G$1" pin="GPIO_2" pad="GPIO_2"/>
<connect gate="G$1" pin="RESET" pad="RESET"/>
<connect gate="G$1" pin="RX" pad="RX"/>
<connect gate="G$1" pin="TX" pad="TX"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ST7735" prefix="DIS">
<description>&lt;b&gt;Sitronix ST7735R&lt;/b&gt;&lt;p&gt;
 18-bit color, 1.8" TFT LCD display, 128*160,	&lt;br&gt;
&lt;a href="http://www.adafruit.com/products/358"&gt;http://www.adafruit.com/products/358&lt;/a&gt;&lt;br&gt;
// Backlight (pin 10) connected to +3.3 V&lt;br&gt;
// MISO (pin 9) PA4 (SSI0Rx) &lt;br&gt;
// SCK (pin 8) connected to PA2 (SSI0Clk)&lt;br&gt;
// MOSI (pin 7) connected to PA5 (SSI0Tx)&lt;br&gt;
// TFT_CS (pin 6) connected to PA3 (SSI0Fss)&lt;br&gt;
// CARD_CS (pin 5) connected to PD7 GPIO output(for SDC)&lt;br&gt;
// Data/Command (pin 4) connected to PA6 (GPIO), high for data, low for command&lt;br&gt;
// RESET (pin 3) connected to PA7 (GPIO)&lt;br&gt;
// VCC (pin 2) connected to +3.3 V&lt;br&gt;
// Gnd (pin 1) connected to ground</description>
<gates>
<gate name="A" symbol="ST7735" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ST7735">
<connects>
<connect gate="A" pin="C/D" pad="4"/>
<connect gate="A" pin="CARD_CS" pad="5"/>
<connect gate="A" pin="GND" pad="1"/>
<connect gate="A" pin="LIGHT" pad="10"/>
<connect gate="A" pin="MISO" pad="9"/>
<connect gate="A" pin="MOSI" pad="7"/>
<connect gate="A" pin="RESET" pad="3"/>
<connect gate="A" pin="SCLK" pad="8"/>
<connect gate="A" pin="TFT_CS" pad="6"/>
<connect gate="A" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP" prefix="TP">
<description>&lt;b&gt;Test pad&lt;/b&gt;&lt;br&gt;
Test point, black,	Keystone Electronics	5001,	Digikey	5001K-ND&lt;br&gt;
Test point, orange,	Keystone Electronics	5003,	Digikey	5003K-ND&lt;br&gt;
Test point, yellow,	Keystone Electronics	5004,	Digikey	5004K-ND&lt;br&gt;
Test point, white,	Keystone Electronics	5002,	Digikey	5002K-ND&lt;br&gt;
Test point, red,	Keystone Electronics	5000,	Digikey	5000K-ND</description>
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="_KEYSTONE500X" package="P1.1MM">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SEEED-4">
<description>&lt;b&gt;Seeedstudio grove&lt;/b&gt;&lt;br&gt;
2mm spacing, Valvano&lt;br&gt;
&lt;a href="http://www.seeedstudio.com/depot/Grove-Universal-4-pin-connector-9010-PCs-p-790.html?cPath=44_80"&gt;http://www.seeedstudio.com/depot/Grove-Universal-4-pin-connector-9010-PCs-p-790.html?cPath=44_80&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD4" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="1_04X2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM2937">
<description>&lt;b&gt;LM2937&lt;/b&gt;&lt;br&gt;
2.5-V and 3.3-V 400-mA and 500-mA Voltage Regulators</description>
<gates>
<gate name="G$1" symbol="78XX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO220-3">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_POLARIZED" prefix="C" uservalue="yes">
<description>&lt;B&gt;POLARIZED CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="CPOL-US" x="0" y="0"/>
</gates>
<devices>
<device name="E5-6" package="CAP200MILPOLARIZED">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OPA2350" prefix="U">
<description>&lt;b&gt;2.7V to 5.5V Rail-to-Rail I/O Dual Amplifier&lt;/b&gt;&lt;p&gt;
High-Speed 38MHz, Single-Supply,  slew rate 22V/us, 500uV offset, 4uV noise</description>
<gates>
<gate name="A" symbol="OP" x="-10.16" y="7.62"/>
<gate name="B" symbol="OP" x="-10.16" y="-7.62"/>
<gate name="P" symbol="PWR+-" x="10.16" y="0"/>
</gates>
<devices>
<device name="N" package="DIL08">
<connects>
<connect gate="A" pin="IN+" pad="3"/>
<connect gate="A" pin="IN-" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="IN+" pad="5"/>
<connect gate="B" pin="IN-" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X02">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X2">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="0" size="0.4064" layer="27">&gt;Value</text>
<text x="-1.8415" y="-3.81" size="1.27" layer="21" font="vector" ratio="15" align="center-right">&gt;TOP_NEG</text>
<text x="-1.8415" y="-3.81" size="1.27" layer="22" font="vector" ratio="15" align="center-right">&gt;BTM_NEG</text>
<text x="1.8415" y="-3.81" size="1.27" layer="21" font="vector" ratio="15" rot="R180" align="center-right">&gt;TOP_POS</text>
<text x="1.8415" y="-3.81" size="1.27" layer="22" font="vector" ratio="15" rot="R180" align="center-right">&gt;BTM_POS</text>
</package>
<package name="1X02_BIG">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
</package>
<package name="JST-2-SMD-VERT">
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="2.54" y="-5.08" size="1.27" layer="25">&gt;Name</text>
<text x="2.24" y="3.48" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="R_SW_TH">
<wire x1="-1.651" y1="19.2532" x2="-1.651" y2="-1.3716" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-1.3716" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="19.2532" x2="13.589" y2="19.2532" width="0.2032" layer="21"/>
<wire x1="13.589" y1="19.2532" x2="13.589" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="13.589" y1="-2.2352" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.6002"/>
<pad name="P$2" x="0" y="16.9926" drill="1.6002"/>
<pad name="P$3" x="12.0904" y="15.494" drill="1.6002"/>
<pad name="P$4" x="12.0904" y="8.4582" drill="1.6002"/>
</package>
<package name="SCREWTERMINAL-5MM-2">
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="MOLEX-1X2_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
</package>
<package name="1X02_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-2-PTH">
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
</package>
<package name="1X02_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;H3&gt;JST-2-PTH-KIT&lt;/h3&gt;
2-Pin JST, through-hole connector&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.127" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST-2-PTH-VERT">
<wire x1="-2.95" y1="-2.25" x2="-2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="2.25" x2="2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="2.25" x2="2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-1.984" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="2.016" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="0.616" y="0.75" size="1.27" layer="51">+</text>
<text x="-1.384" y="0.75" size="1.27" layer="51">-</text>
</package>
</packages>
<symbols>
<symbol name="M02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M02" prefix="JP" uservalue="yes">
<description>Standard 2-pin 0.1" header. Use with &lt;br&gt;
- straight break away headers ( PRT-00116)&lt;br&gt;
- right angle break away headers (PRT-00553)&lt;br&gt;
- swiss pins (PRT-00743)&lt;br&gt;
- machine pins (PRT-00117)&lt;br&gt;
- female headers (PRT-00115)&lt;br&gt;&lt;br&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER" package="R_SW_TH">
<connects>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="2" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="STOREFRONT_ID" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-2-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="SUPPLY1" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY2" library="EE445L" deviceset="GND" device=""/>
<part name="LED1" library="EE445L" deviceset="LED" device="5MM" value="Green"/>
<part name="SUPPLY3" library="EE445L" deviceset="GND" device=""/>
<part name="LAUNCHPAD" library="EE445L" deviceset="EK-TM4C123GXL" device="P"/>
<part name="+3V5" library="EE445L" deviceset="+3V3" device=""/>
<part name="P+2" library="EE445L" deviceset="+5V" device=""/>
<part name="+3V7" library="EE445L" deviceset="+3V3" device=""/>
<part name="SUPPLY19" library="EE445L" deviceset="GND" device=""/>
<part name="J9X" library="EE445L" deviceset="PINHD-1X4" device=""/>
<part name="J9Y" library="EE445L" deviceset="PINHD-1X3" device=""/>
<part name="J10Y" library="EE445L" deviceset="PINHD-1X3" device=""/>
<part name="J11Y" library="EE445L" deviceset="PINHD-1X3" device=""/>
<part name="J12Y" library="EE445L" deviceset="PINHD-1X3" device=""/>
<part name="J10X" library="EE445L" deviceset="PINHD-1X4" device=""/>
<part name="J11X" library="EE445L" deviceset="PINHD-1X4" device=""/>
<part name="J12X" library="EE445L" deviceset="PINHD-1X4" device=""/>
<part name="LED2" library="EE445L" deviceset="LED" device="5MM" value="Green"/>
<part name="SUPPLY10" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY11" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY12" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY13" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY14" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY15" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY16" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY17" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY18" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY20" library="EE445L" deviceset="GND" device=""/>
<part name="P+4" library="EE445L" deviceset="+5V" device=""/>
<part name="SUPPLY22" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY23" library="EE445L" deviceset="GND" device=""/>
<part name="ESP8266" library="EE445L" deviceset="ESP-01" device=""/>
<part name="ESPFLASH" library="EE445L" deviceset="PINHD-1X3" device=""/>
<part name="SUPPLY26" library="EE445L" deviceset="GND" device=""/>
<part name="+3V4" library="EE445L" deviceset="+3V3" device=""/>
<part name="SUPPLY32" library="EE445L" deviceset="GND" device=""/>
<part name="+3V6" library="EE445L" deviceset="+3V3" device=""/>
<part name="SUPPLY5" library="EE445L" deviceset="GND" device=""/>
<part name="J4" library="EE445L" deviceset="ST7735" device=""/>
<part name="+3V9" library="EE445L" deviceset="+3V3" device=""/>
<part name="SUPPLY33" library="EE445L" deviceset="GND" device=""/>
<part name="P+8" library="EE445L" deviceset="+5V" device=""/>
<part name="P+9" library="EE445L" deviceset="+5V" device=""/>
<part name="P+10" library="EE445L" deviceset="+5V" device=""/>
<part name="P+11" library="EE445L" deviceset="+5V" device=""/>
<part name="P+12" library="EE445L" deviceset="+5V" device=""/>
<part name="P+13" library="EE445L" deviceset="+5V" device=""/>
<part name="P+14" library="EE445L" deviceset="+5V" device=""/>
<part name="P+15" library="EE445L" deviceset="+5V" device=""/>
<part name="+3V8" library="EE445L" deviceset="+3V3" device=""/>
<part name="P+16" library="EE445L" deviceset="+5V" device=""/>
<part name="SUPPLY35" library="EE445L" deviceset="GND" device=""/>
<part name="P+20" library="EE445L" deviceset="+5V" device=""/>
<part name="SUPPLY43" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY46" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY47" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY48" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY49" library="EE445L" deviceset="GND" device=""/>
<part name="LED3" library="EE445L" deviceset="LED" device="5MM" value="Green"/>
<part name="SUPPLY50" library="EE445L" deviceset="GND" device=""/>
<part name="A3" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="A2" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="A1" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="A0" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="+5V" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="+3.3V" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="+3.3V2" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="GND2" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="GND" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="J2" library="EE445L" deviceset="PINHD-1X2" device=""/>
<part name="J3" library="EE445L" deviceset="PINHD-1X2" device=""/>
<part name="J14" library="SparkFun-Connectors" deviceset="M02" device="3.5MM"/>
<part name="J13" library="SparkFun-Connectors" deviceset="M02" device="3.5MM"/>
<part name="J5" library="EE445L" deviceset="SEEED-4" device=""/>
<part name="J6" library="EE445L" deviceset="SEEED-4" device=""/>
<part name="J7" library="EE445L" deviceset="SEEED-4" device=""/>
<part name="J8" library="EE445L" deviceset="SEEED-4" device=""/>
<part name="JP1" library="EE445L" deviceset="PINHD-1X2" device=""/>
<part name="U1" library="EE445L" deviceset="LM2937" device=""/>
<part name="C2" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="4.7uF"/>
<part name="C1" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="4.7uF"/>
<part name="R6" library="EE445L" deviceset="RESISTOR" device="0204/5" value="120"/>
<part name="R7" library="EE445L" deviceset="RESISTOR" device="0204/5" value="1.6k"/>
<part name="R8" library="EE445L" deviceset="RESISTOR" device="0204/5" value="20k"/>
<part name="R4" library="EE445L" deviceset="RESISTOR" device="0204/5" value="10k"/>
<part name="R1" library="EE445L" deviceset="RESISTOR" device="0204/5" value="470"/>
<part name="R2" library="EE445L" deviceset="RESISTOR" device="0204/5" value="1.0k"/>
<part name="R5" library="EE445L" deviceset="RESISTOR" device="0204/5" value="10k"/>
<part name="R3" library="EE445L" deviceset="RESISTOR" device="0204/5" value="470"/>
<part name="R15" library="EE445L" deviceset="RESISTOR" device="0204/5" value="10k"/>
<part name="R9" library="EE445L" deviceset="RESISTOR" device="0204/5" value="1.6k"/>
<part name="R11" library="EE445L" deviceset="RESISTOR" device="0204/5" value="1.6k"/>
<part name="R13" library="EE445L" deviceset="RESISTOR" device="0204/5" value="1.6k"/>
<part name="R10" library="EE445L" deviceset="RESISTOR" device="0204/5" value="20k"/>
<part name="R12" library="EE445L" deviceset="RESISTOR" device="0204/5" value="20k"/>
<part name="R14" library="EE445L" deviceset="RESISTOR" device="0204/5" value="20k"/>
<part name="C3" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="22uF"/>
<part name="C4" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="22uF"/>
<part name="C5" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="22uF"/>
<part name="C6" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="22uF"/>
<part name="C7" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="220nF"/>
<part name="C9" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="220nF"/>
<part name="C11" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="220nF"/>
<part name="C13" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="220nF"/>
<part name="C15" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="0.1uF"/>
<part name="C17" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="0.1uF"/>
<part name="C8" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="1.5uF"/>
<part name="C10" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="1.5uF"/>
<part name="C12" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="1.5uF"/>
<part name="C14" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="1.5uF"/>
<part name="PB7" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="PB6" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="PB5" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="PB4" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="PB3" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="PC5" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="PB2" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="PC4" library="EE445L" deviceset="TP" device="_KEYSTONE500X"/>
<part name="U4" library="EE445L" deviceset="MCP2551P" device=""/>
<part name="U2" library="EE445L" deviceset="OPA2350" device="N"/>
<part name="U3" library="EE445L" deviceset="OPA2350" device="N"/>
<part name="C16" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="0.1uF"/>
<part name="SUPPLY4" library="EE445L" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="100.33" y="30.988" size="1.778" layer="97" align="center">Ultrasonic sensors</text>
<text x="115.57" y="-39.37" size="1.778" layer="97">Timer T2</text>
<text x="115.57" y="-17.78" size="1.778" layer="97">Timer T3</text>
<text x="115.57" y="3.81" size="1.778" layer="97">Timer T1</text>
<text x="115.57" y="22.86" size="1.778" layer="97">Timer T0</text>
<text x="78.74" y="21.59" size="1.778" layer="97">HC-SR04</text>
<text x="100.33" y="22.86" size="1.778" layer="97">3-pin Ping)))</text>
<text x="76.2" y="17.78" size="1.778" layer="97">Vcc</text>
<text x="76.2" y="15.24" size="1.778" layer="97">Trig</text>
<text x="76.2" y="12.7" size="1.778" layer="97">Echo</text>
<text x="76.2" y="10.16" size="1.778" layer="97">Gnd</text>
<text x="76.2" y="-1.27" size="1.778" layer="97">Vcc</text>
<text x="76.2" y="-3.81" size="1.778" layer="97">Trig</text>
<text x="76.2" y="-6.35" size="1.778" layer="97">Echo</text>
<text x="76.2" y="-8.89" size="1.778" layer="97">Gnd</text>
<text x="76.2" y="-22.86" size="1.778" layer="97">Vcc</text>
<text x="76.2" y="-25.4" size="1.778" layer="97">Trig</text>
<text x="76.2" y="-27.94" size="1.778" layer="97">Echo</text>
<text x="76.2" y="-30.48" size="1.778" layer="97">Gnd</text>
<text x="76.2" y="-44.45" size="1.778" layer="97">Vcc</text>
<text x="76.2" y="-46.99" size="1.778" layer="97">Trig</text>
<text x="76.2" y="-49.53" size="1.778" layer="97">Echo</text>
<text x="76.2" y="-52.07" size="1.778" layer="97">Gnd</text>
<text x="5.588" y="30.988" size="1.778" layer="97" align="center">IR Distance Sensors    Sharp GP2Y0A21YK</text>
<text x="-36.068" y="11.684" size="1.778" layer="97">Vcc</text>
<text x="-36.068" y="8.89" size="1.778" layer="97">Gnd</text>
<text x="-35.814" y="-6.35" size="1.778" layer="97">Vcc</text>
<text x="-35.814" y="-3.81" size="1.778" layer="97">Vo</text>
<text x="-36.068" y="-8.89" size="1.778" layer="97">Gnd</text>
<text x="-36.83" y="-24.13" size="1.778" layer="97">Vcc</text>
<text x="-36.83" y="-21.59" size="1.778" layer="97">Vo</text>
<text x="-36.83" y="-26.67" size="1.778" layer="97">Gnd</text>
<text x="-36.83" y="-41.91" size="1.778" layer="97">Vcc</text>
<text x="-36.83" y="-39.37" size="1.778" layer="97">Vo</text>
<text x="-36.83" y="-44.45" size="1.778" layer="97">Gnd</text>
<text x="27.94" y="121.92" size="1.778" layer="97">Bumper Switch Headers</text>
<frame x1="-133.35" y1="-66.04" x2="133.35" y2="130.81" columns="8" rows="5" layer="97"/>
<text x="-64.77" y="46.482" size="1.778" layer="97" rot="R90">To flash firmware,
tie GPIO0 to GND</text>
<text x="-77.47" y="121.92" size="5.08" layer="95" align="center">Sensor Board version 3</text>
<text x="-29.21" y="67.564" size="1.27" layer="97">PD7 needs unlock</text>
<text x="-49.022" y="75.438" size="1.27" layer="95">SW1</text>
<text x="-21.844" y="92.202" size="1.27" layer="95">SW2</text>
<wire x1="72.39" y1="29.21" x2="128.27" y2="29.21" width="0.1524" layer="97"/>
<wire x1="128.27" y1="29.21" x2="128.27" y2="-59.69" width="0.1524" layer="97"/>
<wire x1="128.27" y1="-59.69" x2="72.39" y2="-59.69" width="0.1524" layer="97"/>
<wire x1="72.39" y1="-59.69" x2="72.39" y2="29.21" width="0.1524" layer="97"/>
<wire x1="72.39" y1="29.21" x2="72.39" y2="33.02" width="0.1524" layer="97"/>
<wire x1="72.39" y1="33.02" x2="128.27" y2="33.02" width="0.1524" layer="97"/>
<wire x1="128.27" y1="33.02" x2="128.27" y2="29.21" width="0.1524" layer="97"/>
<wire x1="69.85" y1="-59.69" x2="69.85" y2="29.21" width="0.1524" layer="97"/>
<wire x1="69.85" y1="29.21" x2="-53.34" y2="29.21" width="0.1524" layer="97"/>
<wire x1="-53.34" y1="29.21" x2="-53.34" y2="-59.69" width="0.1524" layer="97"/>
<wire x1="-53.34" y1="-59.69" x2="69.85" y2="-59.69" width="0.1524" layer="97"/>
<wire x1="-53.34" y1="29.21" x2="-53.34" y2="33.02" width="0.1524" layer="97"/>
<wire x1="-53.34" y1="33.02" x2="69.85" y2="33.02" width="0.1524" layer="97"/>
<wire x1="69.85" y1="33.02" x2="69.85" y2="29.21" width="0.1524" layer="97"/>
<wire x1="15.24" y1="83.82" x2="69.85" y2="83.82" width="0.1524" layer="97"/>
<wire x1="69.85" y1="83.82" x2="69.85" y2="120.65" width="0.1524" layer="97"/>
<wire x1="69.85" y1="120.65" x2="15.24" y2="120.65" width="0.1524" layer="97"/>
<wire x1="15.24" y1="120.65" x2="15.24" y2="83.82" width="0.1524" layer="97"/>
<wire x1="15.24" y1="120.65" x2="15.24" y2="124.46" width="0.1524" layer="97"/>
<wire x1="15.24" y1="124.46" x2="69.85" y2="124.46" width="0.1524" layer="97"/>
<wire x1="69.85" y1="124.46" x2="69.85" y2="120.65" width="0.1524" layer="97"/>
<text x="62.992" y="42.164" size="1.27" layer="97" rot="R180">2V, 3mA
R=(3.3-2)/0.003=430 =&gt; 470
R=(5-2)/0.003= 1000</text>
<text x="-122.682" y="-57.658" size="1.778" layer="95">December 8, 2015</text>
<text x="11.938" y="89.154" size="1.27" layer="97" rot="R180">PB0,1 are not 5V tolerant</text>
<text x="-87.122" y="39.37" size="1.778" layer="97">LM2937-3.3
With 500 mA  Current</text>
<wire x1="35.56" y1="-133.35" x2="-95.25" y2="-133.35" width="0.1524" layer="97"/>
<text x="-120.396" y="48.006" size="1.778" layer="97">+3v3 supply   LM2937 LDO Converter</text>
<wire x1="72.39" y1="72.39" x2="128.27" y2="72.39" width="0.1524" layer="97"/>
<wire x1="128.27" y1="72.39" x2="128.27" y2="120.65" width="0.1524" layer="97"/>
<wire x1="128.27" y1="120.65" x2="72.39" y2="120.65" width="0.1524" layer="97"/>
<wire x1="72.39" y1="120.65" x2="72.39" y2="72.39" width="0.1524" layer="97"/>
<wire x1="72.39" y1="120.65" x2="72.39" y2="124.46" width="0.1524" layer="97"/>
<wire x1="72.39" y1="124.46" x2="128.27" y2="124.46" width="0.1524" layer="97"/>
<wire x1="128.27" y1="124.46" x2="128.27" y2="120.65" width="0.1524" layer="97"/>
<text x="98.298" y="122.428" size="1.778" layer="97" align="center">ST7735 LCD / SDC Header</text>
<wire x1="69.85" y1="35.56" x2="69.85" y2="77.47" width="0.1524" layer="97"/>
<wire x1="69.85" y1="77.47" x2="15.24" y2="77.47" width="0.1524" layer="97"/>
<wire x1="15.24" y1="77.47" x2="15.24" y2="35.56" width="0.1524" layer="97"/>
<wire x1="15.24" y1="35.56" x2="69.85" y2="35.56" width="0.1524" layer="97"/>
<wire x1="15.24" y1="77.47" x2="15.24" y2="81.28" width="0.1524" layer="97"/>
<wire x1="15.24" y1="81.28" x2="69.85" y2="81.28" width="0.1524" layer="97"/>
<wire x1="69.85" y1="81.28" x2="69.85" y2="77.47" width="0.1524" layer="97"/>
<text x="20.066" y="78.486" size="1.778" layer="97">Power Indicator LEDs</text>
<wire x1="40.64" y1="-133.35" x2="128.27" y2="-133.35" width="0.1524" layer="97"/>
<text x="-23.876" y="70.358" size="1.778" layer="97" align="center">ESP8266 WiFi Interface</text>
<text x="-35.814" y="14.224" size="1.778" layer="97">Vo</text>
<text x="11.43" y="19.05" size="1.778" layer="97" align="center">2-Pole Butterworth
50Hz cutoff</text>
<wire x1="-55.88" y1="-59.69" x2="-55.88" y2="0" width="0.1524" layer="95"/>
<wire x1="-55.88" y1="0" x2="-125.73" y2="0" width="0.1524" layer="95"/>
<wire x1="-125.73" y1="0" x2="-125.73" y2="-59.69" width="0.1524" layer="95"/>
<wire x1="-125.73" y1="-59.69" x2="-55.88" y2="-59.69" width="0.1524" layer="95"/>
<wire x1="-125.73" y1="0" x2="-125.73" y2="3.81" width="0.1524" layer="95"/>
<wire x1="-125.73" y1="3.81" x2="-55.88" y2="3.81" width="0.1524" layer="95"/>
<wire x1="-55.88" y1="3.81" x2="-55.88" y2="0" width="0.1524" layer="95"/>
<text x="-97.79" y="1.27" size="1.778" layer="97">CAN interface</text>
<wire x1="-132.08" y1="-133.35" x2="-100.33" y2="-133.35" width="0.1524" layer="95"/>
<text x="-126.238" y="37.846" size="1.778" layer="97">+5V Input</text>
<text x="-38.1" y="111.76" size="1.778" layer="95">Jon Valvano / Daniel Valvano/ Steven Prickett
December 8, 2015
EE445M/EE380L.6</text>
<wire x1="72.39" y1="71.12" x2="128.27" y2="71.12" width="0.1524" layer="97"/>
<wire x1="128.27" y1="67.31" x2="72.39" y2="67.31" width="0.1524" layer="97"/>
<wire x1="72.39" y1="71.12" x2="72.39" y2="36.83" width="0.1524" layer="97"/>
<wire x1="128.27" y1="71.12" x2="128.27" y2="36.83" width="0.1524" layer="97"/>
<text x="100.33" y="69.088" size="1.778" layer="97" align="center">Test points</text>
<wire x1="128.27" y1="36.83" x2="72.39" y2="36.83" width="0.1524" layer="97"/>
<text x="-15.24" y="38.1" size="1.778" layer="97">PD6 U2Rx
PD7 U2Tx</text>
<text x="99.06" y="73.66" size="1.778" layer="97">PA5 SSI0Tx
PA4 SSI0Rx
PA3 SSI0Fss
PA2 SSI0Clk</text>
<text x="-26.67" y="-58.42" size="1.778" layer="97">PE3 Ain0
PE2 Aiin1
PE1 Ain2
PE0 Ain3</text>
<text x="-13.97" y="16.51" size="1.778" layer="97">PD3 Ain4
PD2 Ain5
PD1 Ain6
PD0 Ain7</text>
</plain>
<instances>
<instance part="SUPPLY1" gate="GND" x="-64.77" y="91.44"/>
<instance part="SUPPLY2" gate="GND" x="-5.08" y="93.98" smashed="yes">
<attribute name="VALUE" x="-4.445" y="95.631" size="1.778" layer="96"/>
</instance>
<instance part="LED1" gate="G$1" x="24.13" y="55.372"/>
<instance part="SUPPLY3" gate="GND" x="24.13" y="45.72"/>
<instance part="LAUNCHPAD" gate="G$1" x="-71.12" y="90.17"/>
<instance part="+3V5" gate="G$1" x="-111.76" y="106.68"/>
<instance part="P+2" gate="1" x="-64.77" y="106.68" smashed="yes">
<attribute name="VALUE" x="-66.04" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="+3V7" gate="G$1" x="40.64" y="25.4" smashed="yes">
<attribute name="VALUE" x="43.18" y="27.94" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY19" gate="GND" x="43.942" y="-0.762" smashed="yes"/>
<instance part="J9X" gate="A" x="83.82" y="15.24" rot="R180"/>
<instance part="J9Y" gate="A" x="99.06" y="13.97" rot="R180"/>
<instance part="J10Y" gate="A" x="99.06" y="-5.08" rot="R180"/>
<instance part="J11Y" gate="A" x="99.06" y="-26.67" rot="R180"/>
<instance part="J12Y" gate="A" x="99.06" y="-48.26" rot="R180"/>
<instance part="J10X" gate="A" x="83.82" y="-3.81" rot="R180"/>
<instance part="J11X" gate="A" x="83.82" y="-25.4" rot="R180"/>
<instance part="J12X" gate="A" x="83.82" y="-46.99" rot="R180"/>
<instance part="LED2" gate="G$1" x="40.64" y="57.15"/>
<instance part="SUPPLY10" gate="GND" x="40.64" y="48.26"/>
<instance part="SUPPLY11" gate="GND" x="111.76" y="7.62" smashed="yes">
<attribute name="VALUE" x="113.665" y="8.001" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY12" gate="GND" x="111.76" y="-11.43" smashed="yes">
<attribute name="VALUE" x="113.665" y="-11.049" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY13" gate="GND" x="111.76" y="-33.02" smashed="yes">
<attribute name="VALUE" x="113.665" y="-32.639" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY14" gate="GND" x="111.76" y="-54.61" smashed="yes">
<attribute name="VALUE" x="113.665" y="-54.229" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY15" gate="GND" x="-12.7" y="2.54" smashed="yes"/>
<instance part="SUPPLY16" gate="GND" x="-12.7" y="-15.24" smashed="yes"/>
<instance part="SUPPLY17" gate="GND" x="-12.7" y="-33.02" smashed="yes"/>
<instance part="SUPPLY18" gate="GND" x="-12.7" y="-50.8" smashed="yes"/>
<instance part="SUPPLY20" gate="GND" x="63.5" y="16.51" smashed="yes">
<attribute name="VALUE" x="64.135" y="18.161" size="1.778" layer="96"/>
</instance>
<instance part="P+4" gate="1" x="-86.36" y="-6.35" smashed="yes">
<attribute name="VALUE" x="-90.17" y="-6.35" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY22" gate="GND" x="-86.36" y="-49.53"/>
<instance part="SUPPLY23" gate="GND" x="-76.2" y="-15.24" smashed="yes">
<attribute name="VALUE" x="-75.565" y="-13.589" size="1.778" layer="96"/>
</instance>
<instance part="ESP8266" gate="G$1" x="-22.86" y="54.864" smashed="yes">
<attribute name="NAME" x="-22.86" y="43.18" size="1.778" layer="95" font="vector" ratio="15" align="center"/>
</instance>
<instance part="ESPFLASH" gate="A" x="-53.34" y="57.404" smashed="yes">
<attribute name="NAME" x="-62.23" y="49.911" size="1.778" layer="95"/>
<attribute name="VALUE" x="-59.69" y="49.784" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY26" gate="GND" x="-48.26" y="42.164" smashed="yes">
<attribute name="VALUE" x="-46.609" y="43.815" size="1.778" layer="96"/>
</instance>
<instance part="+3V4" gate="G$1" x="54.61" y="115.57" smashed="yes">
<attribute name="VALUE" x="57.15" y="118.11" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY32" gate="GND" x="54.61" y="88.9" smashed="yes">
<attribute name="VALUE" x="47.625" y="89.281" size="1.778" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="27.94" y="115.57" smashed="yes">
<attribute name="VALUE" x="30.48" y="118.11" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="27.94" y="88.9" smashed="yes">
<attribute name="VALUE" x="20.955" y="89.281" size="1.778" layer="96"/>
</instance>
<instance part="J4" gate="A" x="91.44" y="106.68" rot="R90"/>
<instance part="+3V9" gate="G$1" x="115.57" y="115.57"/>
<instance part="SUPPLY33" gate="GND" x="115.57" y="80.01" smashed="yes">
<attribute name="VALUE" x="116.205" y="81.661" size="1.778" layer="96"/>
</instance>
<instance part="P+8" gate="1" x="93.98" y="25.4" smashed="yes">
<attribute name="VALUE" x="92.71" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="P+9" gate="1" x="93.98" y="6.35" smashed="yes">
<attribute name="VALUE" x="92.71" y="6.35" size="1.778" layer="96"/>
</instance>
<instance part="P+10" gate="1" x="92.71" y="-13.97" smashed="yes">
<attribute name="VALUE" x="91.44" y="-13.97" size="1.778" layer="96"/>
</instance>
<instance part="P+11" gate="1" x="93.98" y="-35.56" smashed="yes">
<attribute name="VALUE" x="92.71" y="-35.56" size="1.778" layer="96"/>
</instance>
<instance part="P+12" gate="1" x="-26.67" y="-17.78" smashed="yes">
<attribute name="VALUE" x="-30.48" y="-17.78" size="1.778" layer="96"/>
</instance>
<instance part="P+13" gate="1" x="-26.67" y="-35.56" smashed="yes">
<attribute name="VALUE" x="-30.48" y="-35.56" size="1.778" layer="96"/>
</instance>
<instance part="P+14" gate="1" x="-26.67" y="0" smashed="yes">
<attribute name="VALUE" x="-30.48" y="0" size="1.778" layer="96"/>
</instance>
<instance part="P+15" gate="1" x="-26.67" y="17.78" smashed="yes">
<attribute name="VALUE" x="-30.48" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="+3V8" gate="G$1" x="24.13" y="73.66" smashed="yes">
<attribute name="VALUE" x="26.67" y="76.2" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+16" gate="1" x="40.64" y="73.66" smashed="yes">
<attribute name="VALUE" x="38.608" y="74.422" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY35" gate="GND" x="-104.394" y="9.398" smashed="yes">
<attribute name="VALUE" x="-107.569" y="10.795" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+20" gate="1" x="-92.964" y="40.132" smashed="yes">
<attribute name="VALUE" x="-94.234" y="40.132" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY43" gate="GND" x="-80.518" y="8.636" smashed="yes">
<attribute name="VALUE" x="-76.073" y="6.477" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY46" gate="GND" x="16.002" y="-22.86" smashed="yes"/>
<instance part="SUPPLY47" gate="GND" x="15.748" y="-40.64" smashed="yes"/>
<instance part="SUPPLY48" gate="GND" x="16.002" y="-57.404" smashed="yes"/>
<instance part="SUPPLY49" gate="GND" x="17.018" y="-5.08" smashed="yes"/>
<instance part="LED3" gate="G$1" x="57.15" y="54.61"/>
<instance part="SUPPLY50" gate="GND" x="57.15" y="45.974" smashed="yes">
<attribute name="VALUE" x="59.817" y="45.593" size="1.778" layer="96"/>
</instance>
<instance part="A3" gate="G$1" x="55.88" y="10.16"/>
<instance part="A2" gate="G$1" x="55.88" y="-10.16"/>
<instance part="A1" gate="G$1" x="55.88" y="-27.94"/>
<instance part="A0" gate="G$1" x="55.88" y="-45.72"/>
<instance part="+5V" gate="G$1" x="-72.39" y="104.14"/>
<instance part="+3.3V" gate="G$1" x="-109.22" y="104.14"/>
<instance part="+3.3V2" gate="G$1" x="-61.468" y="36.576"/>
<instance part="GND2" gate="G$1" x="-104.394" y="17.018" smashed="yes">
<attribute name="NAME" x="-105.664" y="18.288" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-103.124" y="15.748" size="1.778" layer="97"/>
</instance>
<instance part="GND" gate="G$1" x="-13.97" y="104.14"/>
<instance part="J2" gate="G$1" x="41.91" y="106.68"/>
<instance part="J3" gate="G$1" x="63.5" y="106.68"/>
<instance part="J14" gate="G$1" x="-121.92" y="-35.56"/>
<instance part="J13" gate="G$1" x="-123.444" y="28.448"/>
<instance part="J5" gate="G$1" x="-44.196" y="13.97" rot="R180"/>
<instance part="J6" gate="G$1" x="-44.196" y="-3.81" rot="R180"/>
<instance part="J7" gate="G$1" x="-44.958" y="-21.59" rot="R180"/>
<instance part="J8" gate="G$1" x="-44.45" y="-39.37" rot="R180"/>
<instance part="JP1" gate="G$1" x="-105.664" y="25.908" smashed="yes" rot="R270">
<attribute name="NAME" x="-106.934" y="34.163" size="1.778" layer="95"/>
<attribute name="VALUE" x="-110.744" y="32.258" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U1" gate="G$1" x="-80.518" y="30.988" smashed="yes">
<attribute name="NAME" x="-83.058" y="35.306" size="1.778" layer="95"/>
<attribute name="VALUE" x="-77.978" y="20.828" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="-61.468" y="23.876"/>
<instance part="C1" gate="G$1" x="-92.964" y="24.13"/>
<instance part="R6" gate="G$1" x="-102.87" y="-30.48" rot="R90"/>
<instance part="R7" gate="G$1" x="-5.08" y="6.35" rot="R180"/>
<instance part="R8" gate="G$1" x="10.16" y="6.35" rot="R180"/>
<instance part="R4" gate="G$1" x="27.94" y="98.044" rot="R270"/>
<instance part="R1" gate="G$1" x="24.13" y="63.754" rot="R270"/>
<instance part="R2" gate="G$1" x="40.64" y="65.278" rot="R270"/>
<instance part="R5" gate="G$1" x="54.61" y="97.536" rot="R270"/>
<instance part="R3" gate="G$1" x="57.15" y="63.5" rot="R270"/>
<instance part="R15" gate="G$1" x="2.54" y="52.324"/>
<instance part="R9" gate="G$1" x="-5.08" y="-11.43" rot="R180"/>
<instance part="R11" gate="G$1" x="-5.08" y="-29.21" rot="R180"/>
<instance part="R13" gate="G$1" x="-5.08" y="-46.99" rot="R180"/>
<instance part="R10" gate="G$1" x="10.16" y="-11.43" rot="R180"/>
<instance part="R12" gate="G$1" x="10.16" y="-29.21" rot="R180"/>
<instance part="R14" gate="G$1" x="10.16" y="-46.99" rot="R180"/>
<instance part="C3" gate="G$1" x="-21.59" y="11.43" rot="R90"/>
<instance part="C4" gate="G$1" x="-21.59" y="-6.35" rot="R90"/>
<instance part="C5" gate="G$1" x="-21.59" y="-24.13" rot="R90"/>
<instance part="C6" gate="G$1" x="-21.59" y="-41.91" rot="R90"/>
<instance part="C7" gate="G$1" x="19.05" y="2.54"/>
<instance part="C9" gate="G$1" x="19.05" y="-15.24"/>
<instance part="C11" gate="G$1" x="19.05" y="-33.02"/>
<instance part="C13" gate="G$1" x="19.05" y="-52.07"/>
<instance part="C15" gate="G$1" x="48.26" y="19.05" rot="R90"/>
<instance part="C17" gate="G$1" x="-81.28" y="-11.43" rot="R90"/>
<instance part="C8" gate="G$1" x="27.94" y="14.478" rot="R90"/>
<instance part="C10" gate="G$1" x="27.94" y="-7.62" rot="R90"/>
<instance part="C12" gate="G$1" x="27.94" y="-25.4" rot="R90"/>
<instance part="C14" gate="G$1" x="27.94" y="-43.18" rot="R90"/>
<instance part="PB7" gate="G$1" x="83.82" y="63.5"/>
<instance part="PB6" gate="G$1" x="91.44" y="55.88"/>
<instance part="PB5" gate="G$1" x="83.82" y="50.8"/>
<instance part="PB4" gate="G$1" x="91.44" y="43.18"/>
<instance part="PB3" gate="G$1" x="104.14" y="63.5"/>
<instance part="PC5" gate="G$1" x="116.84" y="58.42"/>
<instance part="PB2" gate="G$1" x="104.14" y="50.8"/>
<instance part="PC4" gate="G$1" x="116.84" y="43.18"/>
<instance part="U4" gate="G$1" x="-99.06" y="-35.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="-76.2" y="-43.18" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-68.58" y="-40.64" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="U2" gate="A" x="40.64" y="6.35"/>
<instance part="U2" gate="B" x="40.64" y="-13.97"/>
<instance part="U2" gate="P" x="40.64" y="6.35"/>
<instance part="U3" gate="A" x="40.64" y="-31.75"/>
<instance part="U3" gate="B" x="40.64" y="-49.53"/>
<instance part="U3" gate="P" x="42.164" y="-31.242"/>
<instance part="C16" gate="G$1" x="66.802" y="-26.67" smashed="yes" rot="R180">
<attribute name="NAME" x="67.437" y="-27.432" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="67.183" y="-18.542" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="61.722" y="-43.688" smashed="yes">
<attribute name="VALUE" x="64.643" y="-44.323" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="-111.76" y1="104.14" x2="-111.76" y2="100.33" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="100.33" x2="-109.22" y2="100.33" width="0.1524" layer="91"/>
<pinref part="LAUNCHPAD" gate="G$1" pin="+3.3V"/>
<pinref part="+3.3V" gate="G$1" pin="TP"/>
<wire x1="-109.22" y1="100.33" x2="-106.68" y2="100.33" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="101.6" x2="-109.22" y2="100.33" width="0.1524" layer="91"/>
<junction x="-109.22" y="100.33"/>
</segment>
<segment>
<wire x1="40.64" y1="22.86" x2="40.64" y2="19.05" width="0.1524" layer="91"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="40.64" y1="19.05" x2="40.64" y2="13.97" width="0.1524" layer="91"/>
<wire x1="45.72" y1="19.05" x2="40.64" y2="19.05" width="0.1524" layer="91"/>
<junction x="40.64" y="19.05"/>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="U2" gate="P" pin="V+"/>
</segment>
<segment>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="54.61" y1="109.22" x2="54.61" y2="113.03" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="60.96" y1="109.22" x2="54.61" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="27.94" y1="109.22" x2="27.94" y2="113.03" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="39.37" y1="109.22" x2="27.94" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="LIGHT"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="99.06" y1="109.22" x2="115.57" y2="109.22" width="0.1524" layer="91"/>
<wire x1="115.57" y1="109.22" x2="115.57" y2="113.03" width="0.1524" layer="91"/>
<pinref part="J4" gate="A" pin="VCC"/>
<wire x1="99.06" y1="88.9" x2="115.57" y2="88.9" width="0.1524" layer="91"/>
<wire x1="115.57" y1="88.9" x2="115.57" y2="109.22" width="0.1524" layer="91"/>
<junction x="115.57" y="109.22"/>
</segment>
<segment>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="24.13" y1="68.834" x2="24.13" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U3" gate="P" pin="V+"/>
<wire x1="42.164" y1="-23.622" x2="63.246" y2="-23.622" width="0.1524" layer="91"/>
<wire x1="63.246" y1="-23.622" x2="63.246" y2="-21.59" width="0.1524" layer="91"/>
<label x="59.69" y="-19.812" size="1.778" layer="95"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="63.246" y1="-21.59" x2="63.246" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="66.802" y1="-21.59" x2="63.246" y2="-21.59" width="0.1524" layer="91"/>
<junction x="63.246" y="-21.59"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<pinref part="LAUNCHPAD" gate="G$1" pin="GND"/>
<wire x1="-83.82" y1="97.79" x2="-64.77" y2="97.79" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="97.79" x2="-64.77" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="24.13" y1="50.292" x2="24.13" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="GND2"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="-22.86" y1="100.33" x2="-13.97" y2="100.33" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="100.33" x2="-5.08" y2="100.33" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="100.33" x2="-5.08" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND" gate="G$1" pin="TP"/>
<wire x1="-13.97" y1="101.6" x2="-13.97" y2="100.33" width="0.1524" layer="91"/>
<junction x="-13.97" y="100.33"/>
</segment>
<segment>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="40.64" y1="50.8" x2="40.64" y2="52.07" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J9Y" gate="A" pin="1"/>
<wire x1="101.6" y1="11.43" x2="109.22" y2="11.43" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="109.22" y1="11.43" x2="111.76" y2="11.43" width="0.1524" layer="91"/>
<wire x1="111.76" y1="11.43" x2="111.76" y2="10.16" width="0.1524" layer="91"/>
<pinref part="J9X" gate="A" pin="1"/>
<wire x1="86.36" y1="10.16" x2="109.22" y2="10.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="10.16" x2="109.22" y2="11.43" width="0.1524" layer="91"/>
<junction x="109.22" y="11.43"/>
</segment>
<segment>
<pinref part="J10Y" gate="A" pin="1"/>
<wire x1="101.6" y1="-7.62" x2="109.22" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="109.22" y1="-7.62" x2="111.76" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-7.62" x2="111.76" y2="-8.89" width="0.1524" layer="91"/>
<pinref part="J10X" gate="A" pin="1"/>
<wire x1="86.36" y1="-8.89" x2="109.22" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-8.89" x2="109.22" y2="-7.62" width="0.1524" layer="91"/>
<junction x="109.22" y="-7.62"/>
</segment>
<segment>
<pinref part="J11Y" gate="A" pin="1"/>
<wire x1="101.6" y1="-29.21" x2="107.95" y2="-29.21" width="0.1524" layer="91"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="107.95" y1="-29.21" x2="111.76" y2="-29.21" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-29.21" x2="111.76" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="J11X" gate="A" pin="1"/>
<wire x1="86.36" y1="-30.48" x2="107.95" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="107.95" y1="-30.48" x2="107.95" y2="-29.21" width="0.1524" layer="91"/>
<junction x="107.95" y="-29.21"/>
</segment>
<segment>
<pinref part="J12Y" gate="A" pin="1"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="101.6" y1="-50.8" x2="107.95" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="J12X" gate="A" pin="1"/>
<wire x1="107.95" y1="-50.8" x2="111.76" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-50.8" x2="111.76" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-52.07" x2="107.95" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="107.95" y1="-52.07" x2="107.95" y2="-50.8" width="0.1524" layer="91"/>
<junction x="107.95" y="-50.8"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="-16.51" y1="11.43" x2="-12.7" y2="11.43" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="11.43" x2="-12.7" y2="8.89" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="8.89" x2="-12.7" y2="5.08" width="0.1524" layer="91"/>
<junction x="-12.7" y="8.89"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="-41.656" y1="8.89" x2="-12.7" y2="8.89" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="-16.51" y1="-6.35" x2="-12.7" y2="-6.35" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-6.35" x2="-12.7" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-8.89" x2="-12.7" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-12.7" y="-8.89"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="-41.656" y1="-8.89" x2="-12.7" y2="-8.89" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="-16.51" y1="-24.13" x2="-12.7" y2="-24.13" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-24.13" x2="-12.7" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-26.67" x2="-12.7" y2="-30.48" width="0.1524" layer="91"/>
<junction x="-12.7" y="-26.67"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="-42.418" y1="-26.67" x2="-12.7" y2="-26.67" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="-16.51" y1="-41.91" x2="-12.7" y2="-41.91" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-41.91" x2="-12.7" y2="-44.45" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-44.45" x2="-12.7" y2="-48.26" width="0.1524" layer="91"/>
<junction x="-12.7" y="-44.45"/>
<pinref part="J8" gate="G$1" pin="1"/>
<wire x1="-41.91" y1="-44.45" x2="-12.7" y2="-44.45" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="53.34" y1="19.05" x2="63.5" y2="19.05" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-91.44" y1="-45.72" x2="-91.44" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<wire x1="-86.36" y1="-46.99" x2="-86.36" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="-45.72" x2="-91.44" y2="-45.72" width="0.1524" layer="91"/>
<junction x="-86.36" y="-45.72"/>
<pinref part="U4" gate="G$1" pin="VSS"/>
<pinref part="U4" gate="G$1" pin="RS"/>
</segment>
<segment>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<wire x1="-76.2" y1="-11.43" x2="-76.2" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="43.942" y1="1.778" x2="40.64" y2="-1.27" width="0.1524" layer="91"/>
<pinref part="U2" gate="P" pin="V-"/>
</segment>
<segment>
<pinref part="ESP8266" gate="G$1" pin="GND"/>
<pinref part="ESPFLASH" gate="A" pin="3"/>
<wire x1="-55.88" y1="54.864" x2="-63.5" y2="54.864" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="54.864" x2="-63.5" y2="47.244" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="47.244" x2="-48.26" y2="47.244" width="0.1524" layer="91"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<wire x1="-48.26" y1="47.244" x2="-40.64" y2="47.244" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="44.704" x2="-48.26" y2="47.244" width="0.1524" layer="91"/>
<junction x="-48.26" y="47.244"/>
</segment>
<segment>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="54.61" y1="92.456" x2="54.61" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="27.94" y1="92.964" x2="27.94" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="GND"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="99.06" y1="86.36" x2="115.57" y2="86.36" width="0.1524" layer="91"/>
<wire x1="115.57" y1="86.36" x2="115.57" y2="82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-80.518" y1="13.462" x2="-92.964" y2="13.462" width="0.1524" layer="91"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="-92.964" y1="13.462" x2="-104.394" y2="13.462" width="0.1524" layer="91"/>
<wire x1="-80.518" y1="11.176" x2="-80.518" y2="13.462" width="0.1524" layer="91"/>
<pinref part="GND2" gate="G$1" pin="TP"/>
<wire x1="-104.394" y1="14.478" x2="-104.394" y2="13.462" width="0.1524" layer="91"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<wire x1="-104.394" y1="11.938" x2="-104.394" y2="13.462" width="0.1524" layer="91"/>
<junction x="-104.394" y="13.462"/>
<pinref part="J13" gate="G$1" pin="1"/>
<wire x1="-115.824" y1="28.448" x2="-113.284" y2="28.448" width="0.1524" layer="91"/>
<wire x1="-113.284" y1="28.448" x2="-113.284" y2="13.462" width="0.1524" layer="91"/>
<wire x1="-113.284" y1="13.462" x2="-104.394" y2="13.462" width="0.1524" layer="91"/>
<junction x="-80.518" y="13.462"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="-61.468" y1="13.462" x2="-80.518" y2="13.462" width="0.1524" layer="91"/>
<wire x1="-80.518" y1="23.368" x2="-80.518" y2="13.462" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="-"/>
<wire x1="-92.964" y1="19.05" x2="-92.964" y2="13.462" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="-"/>
<wire x1="-61.468" y1="18.796" x2="-61.468" y2="13.462" width="0.1524" layer="91"/>
<junction x="-92.964" y="13.462"/>
</segment>
<segment>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
<wire x1="16.002" y1="-20.32" x2="19.05" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
<wire x1="15.748" y1="-38.1" x2="19.05" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
<wire x1="16.002" y1="-54.864" x2="19.05" y2="-57.15" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<wire x1="17.018" y1="-2.54" x2="19.05" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
<pinref part="LED3" gate="G$1" pin="C"/>
<wire x1="57.15" y1="48.514" x2="57.15" y2="49.53" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="P" pin="V-"/>
<wire x1="42.164" y1="-38.862" x2="42.164" y2="-41.148" width="0.1524" layer="91"/>
<wire x1="42.164" y1="-41.148" x2="61.722" y2="-41.148" width="0.1524" layer="91"/>
<wire x1="66.802" y1="-29.21" x2="66.802" y2="-41.148" width="0.1524" layer="91"/>
<wire x1="66.802" y1="-41.148" x2="61.722" y2="-41.148" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<junction x="61.722" y="-41.148"/>
<pinref part="C16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VLED" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<wire x1="24.13" y1="57.912" x2="24.13" y2="58.674" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="A_PE1" class="0">
<segment>
<wire x1="50.8" y1="-31.75" x2="53.34" y2="-31.75" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-31.75" x2="55.88" y2="-31.75" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-31.75" x2="58.42" y2="-31.75" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-34.29" x2="33.02" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-34.29" x2="33.02" y2="-39.878" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-39.878" x2="53.34" y2="-39.878" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-39.878" x2="53.34" y2="-31.75" width="0.1524" layer="91"/>
<junction x="53.34" y="-31.75"/>
<label x="58.42" y="-31.75" size="1.27" layer="95" xref="yes"/>
<wire x1="53.34" y1="-31.75" x2="53.34" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-25.4" x2="33.02" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="A1" gate="G$1" pin="TP"/>
<wire x1="55.88" y1="-30.48" x2="55.88" y2="-31.75" width="0.1524" layer="91"/>
<junction x="55.88" y="-31.75"/>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="U3" gate="A" pin="IN-"/>
<pinref part="U3" gate="A" pin="OUT"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PE1"/>
<wire x1="-83.82" y1="85.09" x2="-78.74" y2="85.09" width="0.1524" layer="91"/>
<label x="-78.74" y="85.09" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PE0" class="0">
<segment>
<wire x1="50.8" y1="-49.53" x2="53.34" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-49.53" x2="55.88" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-49.53" x2="58.42" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-52.07" x2="33.02" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-52.07" x2="33.02" y2="-57.15" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-57.15" x2="53.34" y2="-57.15" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-57.15" x2="53.34" y2="-49.53" width="0.1524" layer="91"/>
<junction x="53.34" y="-49.53"/>
<label x="58.42" y="-49.53" size="1.27" layer="95" xref="yes"/>
<wire x1="53.34" y1="-49.53" x2="53.34" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-43.18" x2="33.02" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="A0" gate="G$1" pin="TP"/>
<wire x1="55.88" y1="-48.26" x2="55.88" y2="-49.53" width="0.1524" layer="91"/>
<junction x="55.88" y="-49.53"/>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="U3" gate="B" pin="IN-"/>
<pinref part="U3" gate="B" pin="OUT"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PE0"/>
<wire x1="-22.86" y1="95.25" x2="-17.78" y2="95.25" width="0.1524" layer="91"/>
<label x="-17.78" y="95.25" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="-72.39" y1="100.33" x2="-64.77" y2="100.33" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="100.33" x2="-64.77" y2="104.14" width="0.1524" layer="91"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<pinref part="+5V" gate="G$1" pin="TP"/>
<wire x1="-72.39" y1="101.6" x2="-72.39" y2="100.33" width="0.1524" layer="91"/>
<pinref part="LAUNCHPAD" gate="G$1" pin="+5V"/>
<wire x1="-83.82" y1="100.33" x2="-72.39" y2="100.33" width="0.1524" layer="91"/>
<junction x="-72.39" y="100.33"/>
</segment>
<segment>
<pinref part="J9Y" gate="A" pin="2"/>
<wire x1="101.6" y1="13.97" x2="109.22" y2="13.97" width="0.1524" layer="91"/>
<wire x1="109.22" y1="13.97" x2="109.22" y2="21.59" width="0.1524" layer="91"/>
<wire x1="93.98" y1="17.78" x2="93.98" y2="21.59" width="0.1524" layer="91"/>
<wire x1="109.22" y1="21.59" x2="93.98" y2="21.59" width="0.1524" layer="91"/>
<wire x1="93.98" y1="21.59" x2="93.98" y2="22.86" width="0.1524" layer="91"/>
<pinref part="J9X" gate="A" pin="4"/>
<wire x1="86.36" y1="17.78" x2="93.98" y2="17.78" width="0.1524" layer="91"/>
<junction x="93.98" y="21.59"/>
<pinref part="P+8" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J10Y" gate="A" pin="2"/>
<wire x1="93.98" y1="2.54" x2="93.98" y2="3.81" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-5.08" x2="109.22" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-5.08" x2="109.22" y2="2.54" width="0.1524" layer="91"/>
<wire x1="109.22" y1="2.54" x2="93.98" y2="2.54" width="0.1524" layer="91"/>
<pinref part="J10X" gate="A" pin="4"/>
<wire x1="86.36" y1="-1.27" x2="93.98" y2="-1.27" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-1.27" x2="93.98" y2="2.54" width="0.1524" layer="91"/>
<junction x="93.98" y="2.54"/>
<pinref part="P+9" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J11X" gate="A" pin="4"/>
<wire x1="86.36" y1="-22.86" x2="92.71" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="92.71" y1="-22.86" x2="92.71" y2="-19.05" width="0.1524" layer="91"/>
<pinref part="J11Y" gate="A" pin="2"/>
<wire x1="92.71" y1="-19.05" x2="92.71" y2="-16.51" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-26.67" x2="109.22" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-26.67" x2="109.22" y2="-19.05" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-19.05" x2="92.71" y2="-19.05" width="0.1524" layer="91"/>
<junction x="92.71" y="-19.05"/>
<pinref part="P+10" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J12X" gate="A" pin="4"/>
<wire x1="86.36" y1="-44.45" x2="93.98" y2="-44.45" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-44.45" x2="93.98" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="J12Y" gate="A" pin="2"/>
<wire x1="93.98" y1="-40.64" x2="93.98" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-48.26" x2="109.22" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-48.26" x2="109.22" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-40.64" x2="93.98" y2="-40.64" width="0.1524" layer="91"/>
<junction x="93.98" y="-40.64"/>
<pinref part="P+11" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="-26.67" y1="11.43" x2="-26.67" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-24.13" y1="11.43" x2="-26.67" y2="11.43" width="0.1524" layer="91"/>
<junction x="-26.67" y="11.43"/>
<pinref part="P+15" gate="1" pin="+5V"/>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="-41.656" y1="11.43" x2="-26.67" y2="11.43" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="+"/>
</segment>
<segment>
<wire x1="-26.67" y1="-6.35" x2="-26.67" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="-6.35" x2="-24.13" y2="-6.35" width="0.1524" layer="91"/>
<junction x="-26.67" y="-6.35"/>
<pinref part="P+14" gate="1" pin="+5V"/>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="-41.656" y1="-6.35" x2="-26.67" y2="-6.35" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="+"/>
</segment>
<segment>
<wire x1="-26.67" y1="-24.13" x2="-26.67" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="-24.13" x2="-24.13" y2="-24.13" width="0.1524" layer="91"/>
<junction x="-26.67" y="-24.13"/>
<pinref part="P+12" gate="1" pin="+5V"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="-42.418" y1="-24.13" x2="-26.67" y2="-24.13" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="+"/>
</segment>
<segment>
<wire x1="-26.67" y1="-41.91" x2="-26.67" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="-41.91" x2="-24.13" y2="-41.91" width="0.1524" layer="91"/>
<junction x="-26.67" y="-41.91"/>
<pinref part="P+13" gate="1" pin="+5V"/>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="-41.91" y1="-41.91" x2="-26.67" y2="-41.91" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="+"/>
</segment>
<segment>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="-86.36" y1="-15.24" x2="-86.36" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="-11.43" x2="-86.36" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="-11.43" x2="-86.36" y2="-11.43" width="0.1524" layer="91"/>
<junction x="-86.36" y="-11.43"/>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="P+16" gate="1" pin="+5V"/>
<wire x1="40.64" y1="70.358" x2="40.64" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-92.964" y1="37.592" x2="-92.964" y2="30.988" width="0.1524" layer="91"/>
<junction x="-92.964" y="30.988"/>
<pinref part="P+20" gate="1" pin="+5V"/>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="-103.124" y1="28.448" x2="-103.124" y2="30.988" width="0.1524" layer="91"/>
<wire x1="-103.124" y1="30.988" x2="-92.964" y2="30.988" width="0.1524" layer="91"/>
<label x="-101.854" y="31.242" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="IN"/>
<wire x1="-88.138" y1="30.988" x2="-92.964" y2="30.988" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="+"/>
<wire x1="-92.964" y1="26.67" x2="-92.964" y2="30.988" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VLED2" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<wire x1="40.64" y1="59.69" x2="40.64" y2="60.198" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="A_PB5" class="0">
<segment>
<pinref part="J10X" gate="A" pin="3"/>
<wire x1="86.36" y1="-3.81" x2="95.25" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="95.25" y1="-3.81" x2="95.25" y2="1.27" width="0.1524" layer="91"/>
<wire x1="95.25" y1="1.27" x2="115.57" y2="1.27" width="0.1524" layer="91"/>
<label x="115.57" y="1.27" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB5"/>
<wire x1="-106.68" y1="97.79" x2="-111.76" y2="97.79" width="0.1524" layer="91"/>
<label x="-111.76" y="97.79" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PB5" gate="G$1" pin="TP"/>
<wire x1="83.82" y1="48.26" x2="96.52" y2="48.26" width="0.1524" layer="91"/>
<label x="88.9" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_PB4" class="0">
<segment>
<pinref part="J10Y" gate="A" pin="3"/>
<wire x1="101.6" y1="-2.54" x2="113.03" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="J10X" gate="A" pin="2"/>
<wire x1="113.03" y1="-2.54" x2="115.57" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-6.35" x2="96.52" y2="-6.35" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-6.35" x2="96.52" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-3.81" x2="113.03" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="113.03" y1="-3.81" x2="113.03" y2="-2.54" width="0.1524" layer="91"/>
<junction x="113.03" y="-2.54"/>
<label x="115.57" y="-2.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB4"/>
<wire x1="-106.68" y1="85.09" x2="-111.76" y2="85.09" width="0.1524" layer="91"/>
<label x="-111.76" y="85.09" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PB4" gate="G$1" pin="TP"/>
<wire x1="91.44" y1="40.64" x2="96.52" y2="40.64" width="0.1524" layer="91"/>
<label x="93.98" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_PB3" class="0">
<segment>
<wire x1="86.36" y1="-25.4" x2="95.25" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="95.25" y1="-25.4" x2="95.25" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="95.25" y1="-20.32" x2="115.57" y2="-20.32" width="0.1524" layer="91"/>
<label x="115.57" y="-20.32" size="1.27" layer="95" xref="yes"/>
<pinref part="J11X" gate="A" pin="3"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB3"/>
<wire x1="-45.72" y1="95.25" x2="-50.8" y2="95.25" width="0.1524" layer="91"/>
<label x="-50.8" y="95.25" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PB3" gate="G$1" pin="TP"/>
<wire x1="104.14" y1="60.96" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<label x="106.68" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_PB7" class="0">
<segment>
<pinref part="J9X" gate="A" pin="3"/>
<wire x1="86.36" y1="15.24" x2="95.25" y2="15.24" width="0.1524" layer="91"/>
<wire x1="95.25" y1="15.24" x2="95.25" y2="20.32" width="0.1524" layer="91"/>
<wire x1="95.25" y1="20.32" x2="115.57" y2="20.32" width="0.1524" layer="91"/>
<label x="115.57" y="20.32" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB7"/>
<wire x1="-22.86" y1="87.63" x2="-17.78" y2="87.63" width="0.1524" layer="91"/>
<label x="-17.78" y="87.63" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB7" gate="G$1" pin="TP"/>
<wire x1="83.82" y1="60.96" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
<label x="88.9" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_PB6" class="0">
<segment>
<pinref part="J9X" gate="A" pin="2"/>
<wire x1="86.36" y1="12.7" x2="96.52" y2="12.7" width="0.1524" layer="91"/>
<wire x1="96.52" y1="12.7" x2="96.52" y2="15.24" width="0.1524" layer="91"/>
<wire x1="96.52" y1="15.24" x2="113.03" y2="15.24" width="0.1524" layer="91"/>
<pinref part="J9Y" gate="A" pin="3"/>
<wire x1="101.6" y1="16.51" x2="113.03" y2="16.51" width="0.1524" layer="91"/>
<wire x1="113.03" y1="16.51" x2="115.57" y2="16.51" width="0.1524" layer="91"/>
<wire x1="113.03" y1="15.24" x2="113.03" y2="16.51" width="0.1524" layer="91"/>
<junction x="113.03" y="16.51"/>
<label x="115.57" y="16.51" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB6"/>
<wire x1="-22.86" y1="85.09" x2="-17.78" y2="85.09" width="0.1524" layer="91"/>
<label x="-17.78" y="85.09" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB6" gate="G$1" pin="TP"/>
<wire x1="91.44" y1="53.34" x2="96.52" y2="53.34" width="0.1524" layer="91"/>
<label x="93.98" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_PB2" class="0">
<segment>
<pinref part="J11Y" gate="A" pin="3"/>
<wire x1="101.6" y1="-24.13" x2="113.03" y2="-24.13" width="0.1524" layer="91"/>
<pinref part="J11X" gate="A" pin="2"/>
<wire x1="113.03" y1="-24.13" x2="115.57" y2="-24.13" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-27.94" x2="96.52" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-27.94" x2="96.52" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-25.4" x2="113.03" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="113.03" y1="-25.4" x2="113.03" y2="-24.13" width="0.1524" layer="91"/>
<junction x="113.03" y="-24.13"/>
<label x="115.57" y="-24.13" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB2"/>
<wire x1="-22.86" y1="97.79" x2="-17.78" y2="97.79" width="0.1524" layer="91"/>
<label x="-17.78" y="97.79" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PB2" gate="G$1" pin="TP"/>
<wire x1="104.14" y1="48.26" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<label x="106.68" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_PE2" class="0">
<segment>
<wire x1="50.8" y1="-13.97" x2="53.34" y2="-13.97" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-13.97" x2="55.88" y2="-13.97" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-13.97" x2="58.42" y2="-13.97" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-16.51" x2="33.02" y2="-16.51" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-16.51" x2="33.02" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-21.59" x2="53.34" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-21.59" x2="53.34" y2="-13.97" width="0.1524" layer="91"/>
<junction x="53.34" y="-13.97"/>
<label x="58.42" y="-13.97" size="1.27" layer="95" xref="yes"/>
<wire x1="53.34" y1="-13.97" x2="53.34" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-7.62" x2="33.02" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="A2" gate="G$1" pin="TP"/>
<wire x1="55.88" y1="-12.7" x2="55.88" y2="-13.97" width="0.1524" layer="91"/>
<junction x="55.88" y="-13.97"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="U2" gate="B" pin="IN-"/>
<pinref part="U2" gate="B" pin="OUT"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PE2"/>
<wire x1="-83.82" y1="82.55" x2="-78.74" y2="82.55" width="0.1524" layer="91"/>
<label x="-78.74" y="82.55" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PE3" class="0">
<segment>
<wire x1="50.8" y1="6.35" x2="53.34" y2="6.35" width="0.1524" layer="91"/>
<wire x1="53.34" y1="6.35" x2="55.88" y2="6.35" width="0.1524" layer="91"/>
<wire x1="55.88" y1="6.35" x2="58.42" y2="6.35" width="0.1524" layer="91"/>
<wire x1="35.56" y1="3.81" x2="33.02" y2="3.81" width="0.1524" layer="91"/>
<wire x1="33.02" y1="3.81" x2="33.02" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-3.81" x2="53.34" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-3.81" x2="53.34" y2="6.35" width="0.1524" layer="91"/>
<junction x="53.34" y="6.35"/>
<label x="58.42" y="6.35" size="1.27" layer="95" xref="yes"/>
<wire x1="53.34" y1="6.35" x2="53.34" y2="14.478" width="0.1524" layer="91"/>
<wire x1="53.34" y1="14.478" x2="33.02" y2="14.478" width="0.1524" layer="91"/>
<pinref part="A3" gate="G$1" pin="TP"/>
<wire x1="55.88" y1="7.62" x2="55.88" y2="6.35" width="0.1524" layer="91"/>
<junction x="55.88" y="6.35"/>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="U2" gate="A" pin="IN-"/>
<pinref part="U2" gate="A" pin="OUT"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PE3"/>
<wire x1="-83.82" y1="80.01" x2="-78.74" y2="80.01" width="0.1524" layer="91"/>
<label x="-78.74" y="80.01" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ESP_PD" class="0">
<segment>
<pinref part="ESP8266" gate="G$1" pin="CH_PD"/>
<wire x1="-5.08" y1="52.324" x2="-2.54" y2="52.324" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ESP_0" class="0">
<segment>
<pinref part="ESP8266" gate="G$1" pin="GPIO_0"/>
<wire x1="-40.64" y1="57.404" x2="-55.88" y2="57.404" width="0.1524" layer="91"/>
<pinref part="ESPFLASH" gate="A" pin="2"/>
</segment>
</net>
<net name="A_PD1" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PD1"/>
<wire x1="-83.82" y1="92.71" x2="-78.74" y2="92.71" width="0.1524" layer="91"/>
<label x="-78.74" y="92.71" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-42.418" y1="-19.05" x2="-36.068" y2="-19.05" width="0.1524" layer="91"/>
<wire x1="-36.068" y1="-19.05" x2="-36.068" y2="-17.272" width="0.1524" layer="91"/>
<label x="-36.068" y="-17.272" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="J7" gate="G$1" pin="4"/>
</segment>
</net>
<net name="A_PD0" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PD0"/>
<wire x1="-83.82" y1="95.25" x2="-78.74" y2="95.25" width="0.1524" layer="91"/>
<label x="-78.74" y="95.25" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="4"/>
<wire x1="-41.91" y1="-36.83" x2="-35.56" y2="-36.83" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-36.83" x2="-35.56" y2="-34.544" width="0.1524" layer="91"/>
<label x="-35.56" y="-34.544" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="A_PE4" class="0">
<segment>
<wire x1="-78.74" y1="-25.4" x2="-72.39" y2="-25.4" width="0.1524" layer="91"/>
<label x="-72.39" y="-25.4" size="1.27" layer="95" xref="yes"/>
<pinref part="U4" gate="G$1" pin="RXD"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PE4"/>
<wire x1="-106.68" y1="90.17" x2="-111.76" y2="90.17" width="0.1524" layer="91"/>
<label x="-111.76" y="90.17" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A_PE5" class="0">
<segment>
<wire x1="-78.74" y1="-30.48" x2="-72.39" y2="-30.48" width="0.1524" layer="91"/>
<label x="-72.39" y="-30.48" size="1.27" layer="95" xref="yes"/>
<pinref part="U4" gate="G$1" pin="TXD"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PE5"/>
<wire x1="-106.68" y1="87.63" x2="-111.76" y2="87.63" width="0.1524" layer="91"/>
<label x="-111.76" y="87.63" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A_PA4" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PA4"/>
<wire x1="-22.86" y1="82.55" x2="-17.78" y2="82.55" width="0.1524" layer="91"/>
<label x="-17.78" y="82.55" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="MISO"/>
<wire x1="99.06" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<label x="104.14" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PA5" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PA5"/>
<wire x1="-106.68" y1="82.55" x2="-111.76" y2="82.55" width="0.1524" layer="91"/>
<label x="-111.76" y="82.55" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="MOSI"/>
<wire x1="99.06" y1="101.6" x2="104.14" y2="101.6" width="0.1524" layer="91"/>
<label x="104.14" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PA6" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PA6"/>
<wire x1="-106.68" y1="80.01" x2="-111.76" y2="80.01" width="0.1524" layer="91"/>
<label x="-111.76" y="80.01" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="C/D"/>
<wire x1="99.06" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<label x="104.14" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PA7" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PA7"/>
<wire x1="-106.68" y1="77.47" x2="-111.76" y2="77.47" width="0.1524" layer="91"/>
<label x="-111.76" y="77.47" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="RESET"/>
<wire x1="99.06" y1="91.44" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<label x="104.14" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PD6" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PD6"/>
<wire x1="-45.72" y1="82.55" x2="-50.8" y2="82.55" width="0.1524" layer="91"/>
<label x="-50.8" y="82.55" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ESP8266" gate="G$1" pin="TX"/>
<wire x1="-5.08" y1="47.244" x2="-2.54" y2="47.244" width="0.1524" layer="91"/>
<label x="-2.54" y="47.244" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PD7" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PD7"/>
<wire x1="-45.72" y1="80.01" x2="-50.8" y2="80.01" width="0.1524" layer="91"/>
<label x="-50.8" y="80.01" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ESP8266" gate="G$1" pin="RX"/>
<wire x1="-40.64" y1="62.484" x2="-43.18" y2="62.484" width="0.1524" layer="91"/>
<label x="-43.18" y="62.484" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CANH" class="0">
<segment>
<wire x1="-96.52" y1="-27.94" x2="-99.06" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-27.94" x2="-99.06" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-25.4" x2="-102.87" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-102.87" y="-25.4"/>
<label x="-104.14" y="-22.86" size="1.778" layer="95" rot="R180"/>
<wire x1="-111.76" y1="-25.4" x2="-102.87" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-33.02" x2="-111.76" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="-33.02" x2="-111.76" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="2"/>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="U4" gate="G$1" pin="CANH"/>
</segment>
</net>
<net name="CANL" class="0">
<segment>
<wire x1="-96.52" y1="-33.02" x2="-99.06" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-33.02" x2="-99.06" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-35.56" x2="-102.87" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-102.87" y="-35.56"/>
<label x="-105.41" y="-36.83" size="1.778" layer="95" rot="R180"/>
<wire x1="-114.3" y1="-35.56" x2="-102.87" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="1"/>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="CANL"/>
</segment>
</net>
<net name="A_PD2" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PD2"/>
<wire x1="-83.82" y1="90.17" x2="-78.74" y2="90.17" width="0.1524" layer="91"/>
<label x="-78.74" y="90.17" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="4"/>
<wire x1="-41.656" y1="-1.27" x2="-35.306" y2="-1.27" width="0.1524" layer="91"/>
<wire x1="-35.306" y1="-1.27" x2="-35.306" y2="0.508" width="0.1524" layer="91"/>
<label x="-35.052" y="0.762" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="A_PD3" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PD3"/>
<wire x1="-83.82" y1="87.63" x2="-78.74" y2="87.63" width="0.1524" layer="91"/>
<label x="-78.74" y="87.63" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="4"/>
<wire x1="-41.656" y1="16.51" x2="-34.544" y2="16.51" width="0.1524" layer="91"/>
<wire x1="-34.544" y1="16.51" x2="-34.544" y2="20.32" width="0.1524" layer="91"/>
<label x="-34.544" y="20.32" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="A_PA3" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PA3"/>
<wire x1="-22.86" y1="80.01" x2="-17.78" y2="80.01" width="0.1524" layer="91"/>
<label x="-17.78" y="80.01" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="TFT_CS"/>
<wire x1="99.06" y1="99.06" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<label x="104.14" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PA2" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PA2"/>
<wire x1="-22.86" y1="77.47" x2="-17.78" y2="77.47" width="0.1524" layer="91"/>
<label x="-17.78" y="77.47" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="SCLK"/>
<wire x1="99.06" y1="104.14" x2="104.14" y2="104.14" width="0.1524" layer="91"/>
<label x="104.14" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="X3" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="3"/>
<wire x1="-30.988" y1="6.35" x2="-10.16" y2="6.35" width="0.1524" layer="91"/>
<wire x1="-41.656" y1="13.97" x2="-30.988" y2="13.97" width="0.1524" layer="91"/>
<wire x1="-30.988" y1="13.97" x2="-30.988" y2="6.35" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="X2" class="0">
<segment>
<wire x1="-10.16" y1="-11.43" x2="-30.48" y2="-11.43" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="3"/>
<wire x1="-41.656" y1="-3.81" x2="-30.48" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-3.81" x2="-30.48" y2="-11.43" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="X1" class="0">
<segment>
<wire x1="-10.16" y1="-29.21" x2="-30.48" y2="-29.21" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="3"/>
<wire x1="-42.418" y1="-21.59" x2="-30.48" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-21.59" x2="-30.48" y2="-29.21" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="X0" class="0">
<segment>
<wire x1="-10.16" y1="-46.99" x2="-30.48" y2="-46.99" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="3"/>
<wire x1="-41.91" y1="-39.37" x2="-30.48" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-39.37" x2="-30.48" y2="-46.99" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="Y1" class="0">
<segment>
<wire x1="0" y1="-29.21" x2="2.54" y2="-29.21" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-29.21" x2="5.08" y2="-29.21" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-25.4" x2="2.54" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-25.4" x2="2.54" y2="-29.21" width="0.1524" layer="91"/>
<junction x="2.54" y="-29.21"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="5.334" y1="-29.21" x2="5.08" y2="-29.21" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="C12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="Y2" class="0">
<segment>
<wire x1="0" y1="-11.43" x2="2.54" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-11.43" x2="5.08" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-7.62" x2="2.54" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-11.43" width="0.1524" layer="91"/>
<junction x="2.54" y="-11.43"/>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="C10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="Y3" class="0">
<segment>
<wire x1="0" y1="6.35" x2="2.54" y2="6.35" width="0.1524" layer="91"/>
<wire x1="2.54" y1="6.35" x2="5.08" y2="6.35" width="0.1524" layer="91"/>
<wire x1="25.4" y1="14.478" x2="2.54" y2="14.478" width="0.1524" layer="91"/>
<wire x1="2.54" y1="14.478" x2="2.54" y2="6.35" width="0.1524" layer="91"/>
<junction x="2.54" y="6.35"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="C8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="U2PIN10" class="0">
<segment>
<wire x1="15.24" y1="-46.99" x2="19.05" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-49.53" x2="19.05" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-46.99" x2="35.56" y2="-46.99" width="0.1524" layer="91"/>
<junction x="19.05" y="-46.99"/>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="U3" gate="B" pin="IN+"/>
</segment>
</net>
<net name="Y0" class="0">
<segment>
<wire x1="0" y1="-46.99" x2="2.54" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-46.99" x2="5.08" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-43.18" x2="2.54" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-43.18" x2="2.54" y2="-46.99" width="0.1524" layer="91"/>
<junction x="2.54" y="-46.99"/>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="C14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="U2PIN12" class="0">
<segment>
<wire x1="35.56" y1="-29.21" x2="19.05" y2="-29.21" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-29.21" x2="15.24" y2="-29.21" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-30.48" x2="19.05" y2="-29.21" width="0.1524" layer="91"/>
<junction x="19.05" y="-29.21"/>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="U3" gate="A" pin="IN+"/>
</segment>
</net>
<net name="U2PIN5" class="0">
<segment>
<wire x1="15.24" y1="-11.43" x2="19.05" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-11.43" x2="35.56" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-12.7" x2="19.05" y2="-11.43" width="0.1524" layer="91"/>
<junction x="19.05" y="-11.43"/>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="U2" gate="B" pin="IN+"/>
</segment>
</net>
<net name="U2PIN3" class="0">
<segment>
<wire x1="15.24" y1="6.35" x2="19.05" y2="6.35" width="0.1524" layer="91"/>
<wire x1="19.05" y1="6.35" x2="30.48" y2="6.35" width="0.1524" layer="91"/>
<wire x1="30.48" y1="6.35" x2="30.48" y2="8.89" width="0.1524" layer="91"/>
<wire x1="30.48" y1="8.89" x2="35.56" y2="8.89" width="0.1524" layer="91"/>
<wire x1="19.05" y1="5.08" x2="19.05" y2="6.35" width="0.1524" layer="91"/>
<junction x="19.05" y="6.35"/>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="U2" gate="A" pin="IN+"/>
</segment>
</net>
<net name="ESP_3V3" class="0">
<segment>
<wire x1="-61.468" y1="26.416" x2="-61.468" y2="30.988" width="0.1524" layer="91"/>
<junction x="-61.468" y="30.988"/>
<pinref part="+3.3V2" gate="G$1" pin="TP"/>
<wire x1="-61.468" y1="34.036" x2="-61.468" y2="30.988" width="0.1524" layer="91"/>
<label x="-74.168" y="32.004" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="OUT"/>
<wire x1="-72.898" y1="30.988" x2="-61.468" y2="30.988" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="+"/>
<wire x1="-61.468" y1="26.416" x2="-61.468" y2="25.146" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ESPFLASH" gate="A" pin="1"/>
<wire x1="-55.88" y1="59.944" x2="-63.5" y2="59.944" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="59.944" x2="-63.5" y2="65.024" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="65.024" x2="-48.26" y2="65.024" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="65.024" x2="-48.26" y2="70.104" width="0.1524" layer="91"/>
<pinref part="ESP8266" gate="G$1" pin="GPIO_2"/>
<wire x1="-40.64" y1="52.324" x2="-48.26" y2="52.324" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="52.324" x2="-48.26" y2="65.024" width="0.1524" layer="91"/>
<junction x="-48.26" y="65.024"/>
<label x="-52.07" y="71.374" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ESP8266" gate="G$1" pin="VCC"/>
<wire x1="-5.08" y1="62.484" x2="10.16" y2="62.484" width="0.1524" layer="91"/>
<wire x1="10.16" y1="62.484" x2="10.16" y2="70.104" width="0.1524" layer="91"/>
<wire x1="7.62" y1="52.324" x2="10.16" y2="52.324" width="0.1524" layer="91"/>
<wire x1="10.16" y1="52.324" x2="10.16" y2="62.484" width="0.1524" layer="91"/>
<junction x="10.16" y="62.484"/>
<label x="3.81" y="71.374" size="1.778" layer="95"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="57.15" y1="68.58" x2="57.15" y2="72.39" width="0.1524" layer="91"/>
<label x="55.88" y="73.152" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="A_PC5" class="0">
<segment>
<wire x1="-45.72" y1="90.17" x2="-50.8" y2="90.17" width="0.1524" layer="91"/>
<label x="-50.8" y="90.17" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="LAUNCHPAD" gate="G$1" pin="PC5"/>
</segment>
<segment>
<pinref part="J12X" gate="A" pin="3"/>
<wire x1="86.36" y1="-46.99" x2="95.25" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="95.25" y1="-46.99" x2="95.25" y2="-41.91" width="0.1524" layer="91"/>
<wire x1="95.25" y1="-41.91" x2="115.57" y2="-41.91" width="0.1524" layer="91"/>
<label x="115.57" y="-41.91" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PC5" gate="G$1" pin="TP"/>
<wire x1="116.84" y1="55.88" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<label x="119.38" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_PC4" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PC4"/>
<wire x1="-45.72" y1="92.71" x2="-50.8" y2="92.71" width="0.1524" layer="91"/>
<label x="-50.8" y="92.71" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PC4" gate="G$1" pin="TP"/>
<wire x1="116.84" y1="40.64" x2="121.92" y2="40.64" width="0.1524" layer="91"/>
<label x="119.38" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_PB0" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB0"/>
<wire x1="-106.68" y1="95.25" x2="-111.76" y2="95.25" width="0.1524" layer="91"/>
<label x="-111.76" y="95.25" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="A" pin="CARD_CS"/>
<wire x1="99.06" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<label x="104.14" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_PB1" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB1"/>
<wire x1="-106.68" y1="92.71" x2="-111.76" y2="92.71" width="0.1524" layer="91"/>
<label x="-111.76" y="92.71" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ESP8266" gate="G$1" pin="RESET"/>
<wire x1="-5.08" y1="57.404" x2="-2.54" y2="57.404" width="0.1524" layer="91"/>
<label x="-2.54" y="57.404" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="2"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="-115.824" y1="30.988" x2="-105.664" y2="30.988" width="0.1524" layer="91"/>
<wire x1="-105.664" y1="30.988" x2="-105.664" y2="28.448" width="0.1524" layer="91"/>
<label x="-115.824" y="30.988" size="1.778" layer="95"/>
</segment>
</net>
<net name="VLED3" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="A"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="57.15" y1="57.15" x2="57.15" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A_PC6" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PC6"/>
<wire x1="-45.72" y1="87.63" x2="-50.8" y2="87.63" width="0.1524" layer="91"/>
<label x="-50.8" y="87.63" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="39.37" y1="106.68" x2="27.94" y2="106.68" width="0.1524" layer="91"/>
<wire x1="27.94" y1="106.68" x2="27.94" y2="103.124" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="27.94" y1="106.68" x2="24.892" y2="106.68" width="0.1524" layer="91"/>
<junction x="27.94" y="106.68"/>
<label x="24.892" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A_PC7" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PC7"/>
<wire x1="-45.72" y1="85.09" x2="-50.8" y2="85.09" width="0.1524" layer="91"/>
<label x="-50.8" y="85.09" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="60.96" y1="106.68" x2="54.61" y2="106.68" width="0.1524" layer="91"/>
<wire x1="54.61" y1="106.68" x2="54.61" y2="102.616" width="0.1524" layer="91"/>
<wire x1="54.61" y1="106.68" x2="52.324" y2="106.68" width="0.1524" layer="91"/>
<junction x="54.61" y="106.68"/>
<label x="52.324" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A_PF4" class="0">
<segment>
<pinref part="J12Y" gate="A" pin="3"/>
<wire x1="101.6" y1="-45.72" x2="111.76" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="J12X" gate="A" pin="2"/>
<wire x1="111.76" y1="-45.72" x2="115.57" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-49.53" x2="96.52" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-49.53" x2="96.52" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-46.99" x2="111.76" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-46.99" x2="111.76" y2="-45.72" width="0.1524" layer="91"/>
<junction x="111.76" y="-45.72"/>
<label x="115.57" y="-45.72" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PF4"/>
<wire x1="-45.72" y1="77.47" x2="-50.8" y2="77.47" width="0.1524" layer="91"/>
<label x="-50.8" y="77.47" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="86.36" y1="-50.8" x2="96.52" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
