// Retarget.h
// Runs on TM4C123 
// Retargets printf statments .
// Brandon Boesch
// Ce Wei
// March 15th, 2016


// ************* Stucts ****************************************************
struct __FILE{ 
  int handle; /* Add whatever you need here */ 
};
// **************************************************************************


//********************Externs***********************************************
extern int StreamToFile; // redirects printf statments. 0=UART, 1=stream to file
//**************************************************************************
