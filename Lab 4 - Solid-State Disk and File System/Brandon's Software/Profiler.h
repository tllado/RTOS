// Profiler.h
// Runs on TM4C123 
// Provides digital output pins on PortF that can be  
// attached to a scope to create a profile of the system.
// Brandon Boesch
// Ce Wei
// March 19th, 2016


//******** PortF_Init **************
// Initalize port F for profiling pins
// inputs:  none
// outputs: none
void PortF_Init(void);
