// Lab4.c
// Runs on TM4C123
// Real Time Operating System for Lab 4
// Brandon Boesch
// Ce Wei
// March 13th, 2016

// Jonathan W. Valvano 1/31/14, valvano@mail.utexas.edu
// EE445M/EE380L.6 
// You may use, edit, run or distribute this file 
// You are free to change the syntax/organization of this file

// ************ ST7735's microSD card interface*******************
// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground
// **************************************************************


//************ Timer Resources *******************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - OS_AddPeriodicThread(), Os_AddPeriodicThread() ; priority = 0
//  Timer2A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//  Timer3A - OS_Sleep() ; priority = 3
//  Timer5A - eDisk.c ; priority = 2
//************************************************


#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab4.h"
#include "OS.h"
#include "eFile.h"
#include "ADC.h"
#include "eDisk.h"
#include "UART.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"

//******************** Globals ***********************************************
unsigned long NumSamples;   // incremented every sample
unsigned long DataLost;     // data sent by Producer, but not received by Consumer
int Running;                // true while robot is running
unsigned long Idlecount=0;  // counts how many times IdleTask() is ran
//**************************************************************************


//******** Robot *************** 
// foreground thread, accepts data from producer
// inputs:  none
// outputs: none
void Robot(void){   
unsigned long data;        // ADC sample(12bit), 0 to 4096
unsigned long voltage;     // in mV,             0 to 3000
unsigned long startTime;   // in 1msec,          0 to 10000 
unsigned long currentTime; // in 1msec,          0 to 10000 	
unsigned long t=0;  
	DataLost = 0;          // new run with no lost data 
	printf("\n\rRobot running...");
	eFile_RedirectToFile("robot");
	printf("time(sec)\tdata(volts)\n\r");
	OS_ClearMsTime(); 
	startTime = OS_MsTime();       // 1ms resolution
	do{
		t++;
		currentTime = OS_MsTime();   // 1ms resolution
		data = OS_Fifo_Get();        // 1000 Hz sampling get from producer
		voltage = (3000*data)/4096;  // in mV. 12 bit ADC, so divide by 4096
		printf("%u.%03u\t%0u.%03u\n\r",currentTime/1000,currentTime%1000,voltage/1000,voltage%1000);
	}
	while(OS_TimeDifference(currentTime,startTime) < 2000); // change this to mean 10 seconds
	if(StreamToFile){              // if disk became full during robot, the file will already be closed from Retarget.c
		eFile_EndRedirectToFile();
	}
	printf("done.\n\n\r");
	printf("\n\r--Enter Command--> ");
	Running = 0;                // robot no longer running
	OS_Kill();
}


void DiskFullErrorPrint(void){
  printf("\n\rCannot run robot, Disk is full.\n\n\r");
  Running = 0; 
	OS_Kill();
}


//************ButtonPush*************
// Called when Select Button pushed
// background threads execute once and return
void ButtonPush(void){
  if(Running==0){
    Running = 1;  // prevents you from starting two robot threads
		if(eFile_DiskFull()){
      OS_AddThread(&DiskFullErrorPrint,128,1);   
	  }
		else{
      OS_AddThread(&Robot,128,1);  // start a 10 second run
    }
	}
}

//******** ButtonSPICommand **************
// use eDisk to read one block. 
// inputs:  none
// outputs: none
void ButtonSPICommand(void){  
	bool errorFound = false;
  printf("\n\r* SPI Command & Response----");
	
	OS_bWait(&FileSystemMutex);                       // aquire FileSystem mutex

	 
	// write dummy data to local block
	for(int i = 0; i < 512; i++){
		Block[i] = 'a'+i%26;                           
	}
	
	PF1 ^= 0x02; 
	// write block
	if(eDisk_WriteBlock(Block,3000)){
		if(!errorFound) printf("failed\n\r");
	  printf("    -eDisk_WriteBlock failure.\n\r");
	  errorFound = true;
	}	
	PF1 ^= 0x02; 
	// read block 
  
	if(eDisk_ReadBlock(Block,3000)){
		if(!errorFound) printf("failed\n\r");
	  printf("    -eDisk_ReadBlock failure.\n\r");
	  errorFound = true;
	}
	PF1 ^= 0x02; 	   
 
	// finished eDisk read/write
	OS_bSignal(&FileSystemMutex);                   // release FileSystem mutex
	if(errorFound == false){
	  printf("finished\n\r");
	}
	printf("\n\r--Enter Command--> ");
	Running = 0;
	OS_Kill();
}


//************DownPush*************
// Called when Down Button pushed
// background threads execute once and return
void DownPush(void){
  if(Running==0){
    Running = 1;  // prevents you from starting two threads
    OS_AddThread(&ButtonSPICommand,128,1);  // start a 10 second run
	}
}


//******** Producer *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 1 kHz, started by your ADC_Collect
// The timer triggers the ADC, creating the 1 kHz sampling
// Your ADC ISR runs when ADC data is ready
// Your ADC ISR calls this function with a 10-bit sample 
// sends data to the Robot, runs periodically at 1 kHz
// inputs:  none
// outputs: none
void Producer(unsigned short data){  
  if(Running){
    if(OS_Fifo_Put(data)){     // send to Robot
      NumSamples++;
    } else{ 
      DataLost++;
    } 
  }
}


//******** IdleTask  *************** 
// foreground thread, runs when no other work needed
// never blocks, never sleeps, never dies
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
    Idlecount++;        // debugging 
  }
}


//******** Interpreter **************
// foreground thread, accepts input from serial port, outputs to serial port
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		if(!StreamToFile){
		  Interpreter_Parse();
		}
	}
}


//******************* Lab4 MAIN *******************************************
#ifdef MAIN
int main(void){  
  // initalize globals	
  Running = 0;                           // robot not running
  DataLost = 0;                          // lost data between producer and consumer
  NumSamples = 0;
	
  // initalize modules	
	OS_Init();                             // initialize OS, disable interrupts
  eFile_Init();                          // initialize file system
	PortF_Init();                          // initialize Port F profiling

  // initialize communication channels
  ADC_InitHWTrigger(0, 1000, &Producer); // start ADC sampling, channel 0(PE3), 1000 Hz

  // attach background tasks
  OS_AddButtonTask(&ButtonPush, PF0, 2);
  OS_AddButtonTask(&DownPush, PF4, 3);

  // create initial foreground threads
  OS_AddThread(&Interpreter, 128, 2); 
  OS_AddThread(&IdleTask, 128, 7);       // runs when nothing useful to do
 
  OS_Launch(TIMESLICE);                  // doesn't return, interrupts enabled in here
  return 0;                              // this never executes
}
#endif  
//******************* end MAIN **********************************************


//**************** TESTMAIN *************************************************
#ifdef TESTMAIN
unsigned char buffer[512];
#define MAXBLOCKS 100

//******** TestDisk **************
// Simple test of eDisk reading and writing blocks
// inputs:  none
// outputs: none
void TestDisk(void){  
	DSTATUS result;  
	unsigned short block; 
	int i; 
	unsigned long n;
  printf("\n\rEE345M/EE380L, Lab 4 eDisk test\n\r");
	
	// initialize disk                          
  result = eDisk_Init(0);
	if(result){
		eFile_Error("eDisk_Init",result);
	}

	// test writing to blocks
	printf("Writing blocks\n\r");
  n = 1;                                            // seed
  for(block = 0; block < MAXBLOCKS; block++){
    for(i=0;i<512;i++){
      n = (16807*n)%2147483647;                     // pseudo random sequence
      buffer[i] = 0xFF&n;        
    }
    PF3 = 0x08;                                     // PF3 high for 100 block writes
    if(eDisk_WriteBlock(buffer,block)){
			eFile_Error("eDisk_WriteBlock",block);        // save to disk
		}
    PF3 = 0x00;     
  }  
	
	PF1 = 0x02;
	
	// test reading from blocks.  Should match what was written
  printf("Reading blocks\n\r");
  n = 1;                                            // reseed, start over to get the same sequence
  for(block = 0; block < MAXBLOCKS; block++){
    PF2 = 0x04;                                     // PF2 high for 100 block read
    if(eDisk_ReadBlock(buffer,block)){
			eFile_Error("eDisk_ReadBlock",block); // read from disk
		}
		PF2 = 0x00;
    for(i=0;i<512;i++){
      n = (16807*n)%2147483647;                     // pseudo random sequence
      if(buffer[i] != (0xFF&n)){
        printf("Read data not correct, block=%u, i=%u, expected %u, read %u\n\r",block,i,(0xFF&n),buffer[i]);
        OS_Kill();
      }      
    }
  }  
	
	PF1 = 0x00;
	// finished eDisk testing
  printf("Successful test of %u blocks\n\r",MAXBLOCKS);
  OS_Kill();
}


//******** RunTest **************
// Aperiodic background tasks which adds a foreground thread to  
// run eDisk test. Runs whenever the designated switch is pressed
// inputs:  none
// outputs: none
void RunTest(void){
  OS_AddThread(&TestDisk,128,1);  
}


//******** TestFile **************
// Simple test of eFile
// inputs:  none
// outputs: none
void TestFile(void){   
	int i; 
	char data; 
  printf("\n\rEE345M/EE380L, Lab 4 eFile test\n\r");
	// format disk
	if(eFile_Format()){
		eFile_Error("eFile_Format",0); 
  }
	// display the directory with filenames and sizes
	eFile_Directory();
	// create a file
  if(eFile_Create("file1")){
		eFile_Error("eFile_Create",0);
  }
	// open file for writing
  if(eFile_WOpen("file1")){
		eFile_Error("eFile_WOpen",0);
	}
	// write data to new file
	for(i=0;i<1000;i++){
    if(eFile_Write('a'+i%26)){
			eFile_Error("eFile_Write",i);
		}
		if(i%52==51){
      if(eFile_Write('\n')){
				eFile_Error("eFile_Write",i);
			}				
      if(eFile_Write('\r')){
				eFile_Error("eFile_Write",i);
			}
		}
  }
	// close file for writing
  if(eFile_WClose()){
		eFile_Error("eFile_WClose",0);
	}
	// display the directory with filenames and sizes
	eFile_Directory();
	// open file for reading
  if(eFile_ROpen("file1")){
		eFile_Error("eFile_ROpen",0);
	}
	// read data from file and display to UART
	for(i=0;i<1000;i++){
    if(eFile_ReadNext(&data)){
			eFile_Error("eFile_ReadNext",i);
    }
		UART_OutChar(data);
  }
	// close file for reading
	if(eFile_RClose()){
		eFile_Error("eFile_RClose",0);
	}
	// delete file
  if(eFile_Delete("file1")){
		eFile_Error("eFile_Delete",0);
  }
	// display the directory with filenames and sizes
	eFile_Directory(&UART_OutChar);
  printf("Successful test of creating a file\n\r");
  OS_Kill();
}


//******** main **************
// SYSTICK interrupts, period established by OS_Launch
// Timer interrupts, period established by first call to OS_AddPeriodicThread
int main(void){                                       
  OS_Init();                               // initialize OS, disable interrupts
  eFile_Init();                            // initialize file system
	PortF_Init();                            // initialize Port F profiling
	
// attach background tasks
  OS_AddButtonTask(&RunTest,PF0,2);
  
// create initial foreground threads
  OS_AddThread(&TestFile,128,1);  
	OS_AddThread(&IdleTask,128,3); 
 
  OS_Launch(TIME_10MS);                    // doesn't return, interrupts enabled in here
  return 0;                                // this never executes
}
#endif
//**************** End TESTMAIN **********************************************



//******** BandwidthTest **************
// Test eDisk bandwidth reading and writing blocks 
// and looking at pins PF1-3 on logic analyzer. 
// inputs:  none
// outputs: none
void BandwidthTest(void){  
	bool errorFound = false;
  printf("\n\r* Bandwidth test----");
	
	// write test
	OS_bWait(&FileSystemMutex);                       // aquire FileSystem mutex
  for(int i = 0; i < 100; i++){
	  PF3 = 0x08;                                     // PF3 toggles for 100 block writes
	  if(eDisk_WriteBlock(Block,i)){
		  if(!errorFound) printf("failed\n\r");
		  printf("    -eDisk_WriteBlock failure.\n\r");
		  errorFound = true;
		}
    PF3 = 0x00;    
  }
	PF1 = 0x02;
	
	// read test
  for(int i = 0; i < 100; i++){
	  PF2 = 0x04;                                     // PF3 toggles for 100 block writes
	  if(eDisk_ReadBlock(Block,i)){
		  if(!errorFound) printf("failed\n\r");
		  printf("    -eDisk_ReadBlock failure.\n\r");
		  errorFound = true;
		}
    PF2 = 0x00;    
  }
	PF1 = 0x00;
	
	// finished eDisk testing
	OS_bSignal(&FileSystemMutex);                   // release FileSystem mutex
	if(errorFound == false){
	  printf("passed\n\r");
	}
}


//******** FormatTest **************
// Test of eFile_Format
// inputs:  none
// outputs: none
void FormatTest(void){
	printf("\n\r* Format test-------");
	bool errorFound = false;

	// write pattern into local copy of Block, and write into all Blocks on Disk
	OS_bWait(&FileSystemMutex);               // aquire FileSystem mutex
	for(int i = 0; i < 512; i++){
		Block[i] = i;                           // add pattern to local Block before writing to Disk
	}
	for(int j = 0; j < MAX_NUM_BLOCKS; j++){  // zero out all Blocks
		if(eDisk_WriteBlock((BYTE*)&Block, j)){
		  if(!errorFound) printf("failed\n\r");
		  printf("    -FormatTest did not write data while writing pattern on Disk.\n\r");
		  errorFound = true;
		}
	}	
	OS_bSignal(&FileSystemMutex);             // release FileSystem mutex
	
	// format Disk to verify that FreeSpace manager, Directory, and FAT are being initalized properly
	eFile_Format();  
	
	// check if formating created the FreeSpace manager accurately
	eDisk_ReadBlock((BYTE*)&Directory, 0);    // reload the FreeSpace manager's Directory from Disk to RAM
	if(strcmp(Directory[0].fileName, "*")){   // check if formating created the FreeSpace manager's fileName
		if(!errorFound) printf("failed\n\r");
		printf("    -Format did not initialize FreeSpace manager's fileName.\n\r");
		errorFound = true;
	}
	
	if(Directory[0].startIndex != FIRST_UNRESERVED_FAT_BLOCK){   
		if(!errorFound) printf("failed\n\r");
		printf("    -Format did not initialize FreeSpace startIndex.\n\r");
		errorFound = true;
	}
	
	if(Directory[0].fileSize != MAX_NUM_FILES){ 
		if(!errorFound) printf("failed\n\r");
		printf("    -Format did not initialize FreeSpace fileSize.\n\r");
		errorFound = true;
	}
	
	// make sure Directory entries are all blank on Disk
	OS_bWait(&FileSystemMutex);               // aquire FileSystem mutex
  eDisk_ReadBlock((BYTE*)&Directory, 0);    // make sure first Directory Block on Disk is blank
  if(strcmp(Directory[1].fileName, "")){    // check if first entry after FreeSpace manager is blank
		if(!errorFound) printf("failed\n\r");
		printf("    -Format did not erase the first Directory entry after FreeSpace manager ).\n\r");
		errorFound = true;
	}
	
	 eDisk_ReadBlock((BYTE*)&Directory,NUM_DIR_BLOCKS-1);              // Load last Directory Block on Disk
  if(strcmp(Directory[ENTRIES_PER_DIRECTORY_BLOCK-1].fileName, "")){ // check if last entry in last Directory Block is blank
		if(!errorFound) printf("failed\n\r");
		printf("    -Format did not erase the last Directory entry).\n\r");
		errorFound = true;
	}
	
	// check if FAT is initialized
	eDisk_ReadBlock((BYTE*)&FAT, FIRST_RESERVED_FAT_BLOCK);         
	for(int i = 0; i < NUM_FAT_BLOCKS; i++){                                   
		for(int j = 0; j < ENTRIES_PER_FAT_BLOCK; j++){                            
			if((i == (NUM_FAT_BLOCKS-1)) && (j == ENTRIES_PER_FAT_BLOCK-1)){
				if(FAT[j] != 0){
					if(!errorFound) printf("failed\n\r");
					printf("    -Final entry of FAT's linked list is not null pointer.\n\r");
					errorFound = true;
				}                                                          
			}
			else{
				if(FAT[j] != (ENTRIES_PER_FAT_BLOCK*i)+j+1){
					if(!errorFound) printf("failed\n\r");
					printf("    -entries of FAT are not linked properly.\n\r");
					errorFound = true;
				}
				// if(at last entry in Block), store current FAT Block to Disk, and load next Block
				if(j == ENTRIES_PER_FAT_BLOCK-1){
					eDisk_ReadBlock((BYTE*)&FAT, (FIRST_RESERVED_FAT_BLOCK+i)+1);
				}
			}
		}
	}
  
	// finished format test
	OS_bSignal(&FileSystemMutex);                   // release FileSystem mutex
	if(errorFound == false){
	  printf("passed\n\r");
	}
}


//******** CreateTest **************
// Test of eFile_Create
// inputs:  none
// outputs: none
void CreateTest(void){
	printf("\n\r* Create test-------");
	eFile_Format();                                 // format before beginning test
	bool errorFound = false;
	
	// create a file.  Should be added to slot 72 = Directory[8] in Block 2 and FAT[72] in Block 64.
	eFile_Create("file1");
	OS_bWait(&FileSystemMutex);                     // aquire FileSystem mutex
	
	// check if FreeSpace manager is accurate
  eDisk_ReadBlock((BYTE*)&Directory, 0);          // reload FreeSpace manager's Block into RAM
	if(Directory[0].fileSize != (MAX_NUM_FILES-1)){ 
		if(!errorFound) printf("failed\n\r");
		printf("    -FreeSpace fileSize not accurate.\n\r");
		errorFound = true;
	}
	
	if(Directory[0].startIndex != 73){               // new file should have taken FAT slot 72, so first free is 73
		if(!errorFound) printf("failed\n\r");
		printf("    -FreeSpace startIndex of %u not accurate.\n\r",Directory[0].startIndex);
		errorFound = true;
	}
	
	// check if Directory is accurate. 
	eDisk_ReadBlock((BYTE*)&Directory, 2);          // reload FreeSpace manager's Block into RAM
	if(strcmp(Directory[8].fileName, "file1")){     // check if Directory name is accurate
		if(!errorFound) printf("failed\n\r");
		printf("    -Directory entry for new fileName is not accurate.\n\r");
		errorFound = true;
	}
		
	if(Directory[8].startIndex != 72){              // check if Directory startIndex is accurate. Should be linked to slot 72
		if(!errorFound) printf("failed\n\r");
		printf("    -Directory entry for new startIndex is not accurate.\n\r");
		errorFound = true;
	}
	
	if(Directory[8].fileSize != 0){                // check if Directory fileSize is accurate. Should be 0 bytes
		if(!errorFound) printf("failed\n\r");
		printf("    -Directory entry for new fileSize is not accurate.\n\r");
		errorFound = true;
	}
	
	// check if FAT is accurate
	eDisk_ReadBlock((BYTE*)&FAT, 64);              // load file's FAT Block into RAM
  if(FAT[72] != 0){                              // should be only entry in linked list
		if(!errorFound) printf("failed\n\r");
		printf("    -FAT entry for new file is not accurate.\n\r");
		errorFound = true;
	}
	
	// finished Create test
	OS_bSignal(&FileSystemMutex);                  // release FileSystem mutex
	if(errorFound == false){
	  printf("passed\n\r");
	}
}


//******** WriteTest **************
// Test of eFile_Write
// inputs:  none
// outputs: none
void WriteTest(void){
	printf("\n\r* Write test--------");
  bool errorFound = false;
	eFile_Format();                                 // format before beginning test
	
	// create and open a file for writing.  Should be added to Directory[2] and FAT[2].
	eFile_Create("file1");
	eFile_WOpen("file1");
	
	// write data to new file
	for(int i=0;i<1000;i++){
    if(eFile_Write('a'+i%26)){
			eFile_Error("eFile_Write",i);
		}
		if(i%52==51){
      if(eFile_Write('\n')){
				eFile_Error("eFile_Write",i);
			}				
      if(eFile_Write('\r')){
				eFile_Error("eFile_Write",i);
			}
		}
  }
	eFile_WClose();
	
	// Check results.  File's first Block should be in Block 72
	OS_bWait(&FileSystemMutex);              // aquire FileSystem mutex
	eDisk_ReadBlock((BYTE*)&Block, 72);      // reload Block from Disk
	if(Block[0] != 'a'){      // check if first elemenet in file's first block is accurate
		if(!errorFound) printf("failed\n\r");
		printf("    -File's first block not written correctly.\n\r");
		errorFound = true;
	}
	
	if(Block[52] != '\n'){    // check if 52nd elemenet in file's first block is accurate
		if(!errorFound) printf("failed\n\r");
		printf("    -File's first block not written correctly.\n\r");
		errorFound = true;
	}
	
	// The last byte of last file should be a z
	eDisk_ReadBlock((BYTE*)&Block, 72+2);   // reload file's last Block from Disk should be in Block 74
	if(Block[511] != 'z'){                  // check if last elemenet in file's second block is accurate
		if(!errorFound) printf("failed\n\r");
		printf("    -File's third block not written correctly.\n\r");
		errorFound = true;
	}
	
	 // block 5 should be empty
	eDisk_ReadBlock((BYTE*)&Block, 72+3);   // reload Block past file from Disk. Should be in Blcok 75
	if(Block[0] != 0){                      // check if first elemenet after file's last Block is accurate
		if(!errorFound) printf("failed\n\r");
		printf("    -Block 5 should be empty.\n\r");
		errorFound = true;
	}
	
	// finished Write test
	OS_bSignal(&FileSystemMutex);           // release FileSystem mutex
	if(errorFound == false){
	  printf("passed\n\r");
	}
}


//******** ReadTest **************
// Test of eFile_Read
// inputs:  none
// outputs: none
void ReadTest(void){
	printf("\n\rRead test begin:\n\r");
  bool errorFound = false;
	eFile_Format();                                 // format before beginning test
	
	// create and open a file for writing.  Should be added to Directory[2] and FAT[2].
	eFile_Create("file1");
	eFile_WOpen("file1");
	
	// write data to new file
	for(int i=0;i<1000;i++){
    if(eFile_Write('a'+i%26)){
			eFile_Error("eFile_Write",i);
		}
		if(i%52==51){
      if(eFile_Write('\n')){
				eFile_Error("eFile_Write",i);
			}				
      if(eFile_Write('\r')){
				eFile_Error("eFile_Write",i);
			}
		}
  }
	// close file for writing
	eFile_WClose();

	// open file for reading and read entirety of file
	eFile_ROpen("file1");
	char data;                                                       // stores read data
	while(CurrentReadIndex < Directory[FileEntryInDir].fileSize){
		eFile_ReadNext(&data);                                         // read next byte
		// if(last data is not an 'l'), then an error.(think about 1000/26)
		if((CurrentReadIndex == Directory[FileEntryInDir].fileSize) && (data != 'l')){ //CurrentReadIndex is incremented in ReadNext
		  errorFound = true;                                       
		}
		UART_OutChar(data);                                            // output read result
	}
		 
	// finished Read test. Close file and print results.
	eFile_RClose();
	if(errorFound == false){
	  printf("\n\n\r* Read test---------passed\n\r");
	}
	else{
		printf("\n\n\r* Read test---------failed\n\r");
	  printf("    -Last letter printed should be an 'l'.\n\r");
	}
}

//******** DeleteTest **************
// Test of eFile_Delete
// inputs:  none
// outputs: none
void DeleteTest(void){
	printf("\n\rDelete test begin:\n\r");
  bool error1Found = false;
	bool error2Found = false;
	eFile_Format();                           // format before beginning test
	
  // create and open a file for writing.  Should be added to Directory[2] and FAT[2].
	eFile_Create("file1");
	eFile_WOpen("file1");
	
	// write data to new file
	for(int i=0;i<1000;i++){
    if(eFile_Write('a'+i%26)){
			eFile_Error("eFile_Write",i);
		}
		if(i%52==51){
      if(eFile_Write('\n')){
				eFile_Error("eFile_Write",i);
			}				
      if(eFile_Write('\r')){
				eFile_Error("eFile_Write",i);
			}
		}
  }
	
	// close file for writing
	eFile_WClose();
	
	// create and open a second file for writing.  Should be added to Directory[5] and FAT[5].
	eFile_Create("file2");
	eFile_WOpen("file2");
	
	// write data to new file
	for(int i=0;i<1000;i++){
    if(eFile_Write('a'+i%26)){
			eFile_Error("eFile_Write",i);
		}
		if(i%52==51){
      if(eFile_Write('\n')){
				eFile_Error("eFile_Write",i);
			}				
      if(eFile_Write('\r')){
				eFile_Error("eFile_Write",i);
			}
		}
  }
	
	// close file for writing
	eFile_WClose();
	
	// print out Directory
	eFile_Directory();
	
	// delete file 1
	eFile_Delete("file1");
	
  // check if fileName was deleted
	if(strcmp(Directory[FileEntryInDir].fileName, "")){
		error1Found = true;  
	}
	
	// print out Directory
	eFile_Directory();
	
	// delete file 2
	eFile_Delete("file2");
	
  // check if fileName was deleted
	if(strcmp(Directory[FileEntryInDir].fileName, "")){
		error2Found = true;  
	}
	
	// print out Directory
	eFile_Directory();
	
	// finished Delete test. Print results.
	if(error1Found == true){
    printf("\n\n\r* Delete test---------failed\n\r");
	  printf("    -file1 not removed from directory\n\r");
	}
	if(error2Found == true){
		if(!error1Found)printf("\n\n\r* Delete test---------failed\n\r");
	  printf("    -file2 not removed from directory\n\r");
	}
	else{
	  printf("\n\n\r* Delete test---------passed\n\r");
	}
}


//******** RedirectToFileTest **************
// Test of eFile_RedirectToFile and eFile_EndRedirectToFile()
// inputs:  none
// outputs: none
void RedirectToFileTest(void){
	printf("\n\rRedirectToFile test begin:\n\n\r");
  bool errorFound = false;
	eFile_Format();                           // format before beginning test
	
  // redirect IO to file. Creates and opens a file for writing. 
  eFile_RedirectToFile("file1");
	
	// printf to file
	printf("This was written to Disk from printf.\n\rIf you can read this, then success!");
	
	// redirect IO to UART.
	eFile_EndRedirectToFile();
	
	// open file for reading
	eFile_ROpen("file1");
	char data;                                                       // stores read data
	while(CurrentReadIndex < Directory[FileEntryInDir].fileSize){
		eFile_ReadNext(&data);                                         // read next byte
		// if(second letter is not an "e"), then error.
		if((CurrentReadIndex == 1) && (data != 'T')){                  //CurrentReadIndex is incremented in ReadNext
		  errorFound = true;                                       
		}
		printf("%c",data);                                             // output read result
	}
	eFile_RClose();
	
	if(errorFound == false){
	  printf("\n\n\r* RedirectToFile test-passed\n\r");
	}
	else{
		printf("\n\n\r* RedirectToFile test-failed\n\r");
	  printf("    -IO stream did not write to file properly.\n\r");
	}
}
	

//******** eFileTest **************
// Test of eFile system.
// inputs:  none
// outputs: none
void eFileTest(void){
	printf("\n\n\r******************************************************\n\r");
	printf("                 eFile Testing Begins\n\r");
	printf("******************************************************\n\r");

//  begin testbench
	BandwidthTest();
	FormatTest();
	CreateTest();
	WriteTest();
	ReadTest();
	DeleteTest();
	RedirectToFileTest();
	
	printf("\n\r******************************************************\n\r");
	printf("                eFile Testing Complete\n\r");
	printf("******************************************************\n\r");
	OS_Kill();
}

//**************** DEBUG *************************************************
#ifdef DEBUG


//******** main **************
int main(void){ 
	OS_Init();                               // initialize OS, disable interrupts
  PortF_Init();                            // initialize Port F profiling
	eFile_Init();                            // initialize file system

	OS_AddThread(&eFileTest, 128, 2);      
//	OS_AddThread(&Interpreter, 128, 2);      // adds interpreter
//	OS_AddButtonTask(&eFileTest, PF4, 2);    
//	OS_AddButtonTask(&eFileTest, PF0, 2);
  
	OS_Launch(TIME_10MS);                    // doesn't return, interrupts enabled in here
  return 0;                                // this never executes
}
#endif 
//**************** End DEBUG **********************************************

