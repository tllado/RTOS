// Lab4.h
// Runs on TM4C123
// Real Time Operating System for Labs 4
// Brandon Boesch
// Ce Wei
// March 13th, 2016


//**********************Compilter Directives********************************
#define MAIN          //<-
//#define TESTMAIN      //<- Only one of these with an arrow should               
//#define DEBUG         //<-  be uncommented at any given time  
// *************************************************************************


// ******************** Constants ******************************************
#define PF1  (*((volatile unsigned long *)0x40025008))
#define PF2  (*((volatile unsigned long *)0x40025010))
#define PF3  (*((volatile unsigned long *)0x40025020))
//**************************************************************************


//********************Prototypes********************************************

//******** BandwidthTest **************
// Test eDisk bandwidth reading and writing blocks 
// and looking at pins PF1-3 on logic analyzer. 
// inputs:  none
// outputs: none
void BandwidthTest(void);

//******** FormatTest **************
// Test of eFile_Format
// inputs:  none
// outputs: none
void FormatTest(void);

//******** CreateTest **************
// Test of eFile_Create
// inputs:  none
// outputs: none
void CreateTest(void);

//******** WriteTest **************
// Test of eFile_Write
// inputs:  none
// outputs: none
void WriteTest(void);

//******** ReadTest **************
// Test of eFile_Read
// inputs:  none
// outputs: none
void ReadTest(void);

//******** DeleteTest **************
// Test of eFile_Delete
// inputs:  none
// outputs: none
void DeleteTest(void);

//******** RedirectToFileTest **************
// Test of eFile_RedirectToFile and eFile_EndRedirectToFile()
// inputs:  none
// outputs: none
void RedirectToFileTest(void);
	
//**************************************************************************


//********************Externs***********************************************

//**************************************************************************
