// eFile.c
// Runs on TM4C123
// Middle-level routines to implement a solid-state disk 
// Brandon Boesch
// Ce Wei
// March 13th, 2016


#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "eFile.h"
#include "Retarget.h"
#include "OS.h"
#include "eDisk.h"


// *****************Globals**************************
DirEntryType Directory[ENTRIES_PER_DIRECTORY_BLOCK];  // Local copy of a Directory Block. The Directory lives in Blocks 0-63 on disk
uint16_t FAT[ENTRIES_PER_FAT_BLOCK];                  // Local copy of a Block in the File Allocation Table(FAT). The FAT lives in Blocks 64-71 on Disk
uint8_t Block[512];                   // Local copy of a Block with 512 bytes.
uint16_t CurrentFATBlock;             // File's current Block in FAT
uint16_t CurrentFATIndex;             // Open file's index to current block
uint16_t FileEntryInDir;               // Index into open file's Directory entry
uint16_t FileDirBlock;                 // file's Directory entry Block
uint32_t CurrentReadIndex;            // Current byte index into File's Block that the function eFile_ReadNext is accessing            
bool ReassignStartIndex;              // if overflow, need to manually set FreeSpace managers startIndex in eFile_Delete

// **************************************************


//---------- eFile_Init-----------------
// Activate the file system, without formating. Since this function
// initializes the disk, it must run with the disk periodic task operating.
// Should be called before OS_Launch is called, while interupts are disabled.
// Calls OS_Error() on failure(already initialized, no disk, write protected, etc...)
// Input: none
// Output: none 
void eFile_Init(void){
	// add related periodic tasks
	OS_AddPeriodicThread(&disk_timerproc,TIME_10MS,5);  // time out routines for Disk
	
	// initalize semaphores and globals
  OS_InitSemaphore(&FileSystemMutex, 1);              // initialize FileSystemMutex semaphore as available
	OS_InitSemaphore(&NoFileOpenMutex, 1);              // initialize NoFileOpenMutex semaphore as available.
	ReassignStartIndex = false;                         // no startIndex overflow has occured in FreeSpace manager yet
	
	// check for disk errors
	DSTATUS result = eDisk_Status(0);                   // check status of Disk
	if(result != 0x01){                                 // error
		eFile_Error("eDisk_Init",result);                 // print error
		OS_Error(EDISK_INIT);  
	}
	
	// initialize Disk
	else{
		result = eDisk_Init(0);                           // initialize Disk  
		if(result){                                       // if true, then error
		  eFile_Error("eDisk_Init",result);               // print error
		  OS_Error(EDISK_INIT);                           // failure
		}	
	}
}	


//---------- eFile_Format-----------------
// Delete all files by erasing blocks, initializing FreeSpace manager,
// creating a blank directory, and initializing FAT into a linked list.
// Input: none
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash)
int eFile_Format(void){	
	DRESULT result = RES_OK;                  // return result of eDisk's read/write functions. Initailed to 0.
	
	// erase all blocks
	OS_bWait(&FileSystemMutex);               // aquire FileSystem mutex
	for(int i = 0; i < 512; i++){
		Block[i] = 0;                           // zero out Block before writing to disk
	}
	for(int j = 0; j < MAX_NUM_BLOCKS; j++){  // zero out all blocks
		result |= eDisk_WriteBlock((BYTE*)&Block, j);
	}	
	OS_bSignal(&FileSystemMutex);             // release FileSystem mutex
	
	// Create free space manager
	result |= eFile_FreeSpaceInit();
	
	// finished
  return result;
}
	

//---------- eFile_FreeSpaceInit-----------------
// Create the free space manager by allocating the first element
// in the directory to hold the startIndex of free blocks and the number
// of free blocks available(fileSize). Also initalizes all the FAT entries 
// (except for the reserved entries 0 thru 71).
// Input: none
// Output: 0 if successful and 1 on failure (already initialized)
int eFile_FreeSpaceInit(void){
	DRESULT result = RES_OK;                                  // return result of eDisk's read/write functions. Initailed to 0.
	
	// copy Directory[0] and FAT[FIRST_RESERVED_FAT_BLOCK] from disk into RAM
	OS_bWait(&FileSystemMutex);                               // aquire FileSystem mutex
	eDisk_ReadBlock((BYTE*)&Directory, 0);                    // load FreeSpace manager's Block in Directory into RAM
  eDisk_ReadBlock((BYTE*)&FAT, FIRST_RESERVED_FAT_BLOCK);   // load first reserved FAT Block into RAM
	
	// create FreeSpace entry into index 0 of Directory
	strcpy(Directory[0].fileName, "*");                       // the FreeSpace manager will have a file name of "*"
	Directory[0].startIndex = FIRST_UNRESERVED_FAT_BLOCK;     // the first free block will be indexed after reserved Directory and FAT entries.
	Directory[0].fileSize = MAX_NUM_FILES;                    // file size will for FreeSpace manager will indicate the number of free blocks available.
  result |= eDisk_WriteBlock((BYTE*)&Directory, 0);         // write FreeSpace into disk block 0
	
	// create linked list for all FAT Blocks and store into Disk
	for(int i = 0; i < NUM_FAT_BLOCKS; i++){                                     // initialize all blocks for FAT
		for(int j = 0; j < ENTRIES_PER_FAT_BLOCK; j++){                            // initialize all elements in current FAT Block
		  // if(at last entry in last FAT Block), null terminate list, and store FAT Block to Disk
			if((i == (NUM_FAT_BLOCKS-1)) && (j == ENTRIES_PER_FAT_BLOCK-1)){
				FAT[j] = 0;                                                            // last FAT element in last FAT Block is null terminated
			  result |= eDisk_WriteBlock((BYTE*)&FAT, FIRST_RESERVED_FAT_BLOCK+i);   // write final FAT Block into Disk 
			}
			// else, make FreeSpace list in FAT sequential
			else{
				FAT[j] = (ENTRIES_PER_FAT_BLOCK*i)+j+1;
				// if(at last entry in Block), store current FAT Block to Disk, and load next Block
				if(j == ENTRIES_PER_FAT_BLOCK-1){
					result |= eDisk_WriteBlock((BYTE*)&FAT, FIRST_RESERVED_FAT_BLOCK+i);
					result |= eDisk_ReadBlock((BYTE*)&FAT, (FIRST_RESERVED_FAT_BLOCK+i)+1);
				}
			}
		}
	}
	
  // finsihed 
	OS_bSignal(&FileSystemMutex);                      // release FileSystem mutex
	return result;
}


//---------- eFile_Create-----------------
// Create a new, empty file with one allocated block
// Input: name[] - file name is an ASCII string up to 9 characters + 1 null terminator 
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash, 
//         directory is full, or if file already exists)
int eFile_Create(char name[]){ 
	DRESULT result = RES_OK;                            // return result of eDisk's read/write functions. Initailed to 0.
  
	// check if desired name is too large
	if(strlen(name) > MAX_FILE_NAME - 1){              
		printf("\n\rError in eFile_Create. Max file name is 9 characters.\n\r");
		return 1; 
	}
	
	// copy first Directory Block from disk into RAM
	OS_bWait(&FileSystemMutex);                         // aquire FileSystem mutex
	result |= eDisk_ReadBlock((BYTE*)&Directory, 0);    // reload first Directory Block from disk
	
	// if(Disk is full), then error
	if(Directory[0].fileSize == 0){                     // the FreeSpace manager's filesize tells how many blocks are available
		printf("\n\rError in eFile_Create. Disk is full.\n\r");
		OS_bSignal(&FileSystemMutex);                     // release FileSystemMutex
		return 1;                                         // no more free space available
	}
	
	// if(new file name already exists), then error
	for(int i = 0; i < NUM_DIR_BLOCKS; i++){
		for(int j = 0; j < ENTRIES_PER_DIRECTORY_BLOCK; j++){
      if(strcmp(Directory[j].fileName, name) == 0){
			  printf("\n\rError in eFile_Create. File name already exists.\n\r");
			  OS_bSignal(&FileSystemMutex);                       // release FileSystem mutex
			  return 1;
			}
			else if(j == ENTRIES_PER_DIRECTORY_BLOCK-1){
				result |= eDisk_ReadBlock((BYTE*)&Directory, i+1);  // reload the next Directory Block from Disk
			}
		}
	}
	
	// Create file. 
	// Load FreeSpace manager Block, and find location for new file from the FreeSpace manager. 
	result |= eDisk_ReadBlock((BYTE*)&Directory, 0);   // load FreeSpace's Directory Block from disk
	uint16_t newFATIndex = Directory[0].startIndex;    // find the new file's startIndex from FreeSpace manager
	
	// find and load new file's FAT Block 
	uint16_t newFATBlock = (newFATIndex / ENTRIES_PER_FAT_BLOCK) + FIRST_RESERVED_FAT_BLOCK;
	uint16_t  newFileFATEntry = newFATIndex % ENTRIES_PER_FAT_BLOCK;
	result |= eDisk_ReadBlock((BYTE*)&FAT, newFATBlock); 
	
	// update FreeSpace manager's number of free blocks(fileSize) and startIndex
	(Directory[0].fileSize)--;                         // use up 1 free block
	Directory[0].startIndex = FAT[newFileFATEntry];    // startIndex is now the next in line
	result |= eDisk_WriteBlock((BYTE*)&Directory, 0);  // reload first Directory Block from disk

	// find Directory Block and entry that will hold new file
	uint16_t newFileDirectoryBlock = newFATIndex / ENTRIES_PER_DIRECTORY_BLOCK;
	uint16_t  newFileDirectoryEntry = newFATIndex % ENTRIES_PER_DIRECTORY_BLOCK;
	
	// load new file's Directory Block, and update it's directory entry
  result |= eDisk_ReadBlock((BYTE*)&Directory, newFileDirectoryBlock);  
	strcpy(Directory[newFileDirectoryEntry].fileName, name);
	Directory[newFileDirectoryEntry].startIndex = newFATIndex;
	Directory[newFileDirectoryEntry].fileSize = 0;
	
	// udate FAT's entry
	FAT[newFileFATEntry] = 0;                          // only one block is being created, so null link pointer
	
	// Store upadates onto Disk
	result |= eDisk_WriteBlock((BYTE*)&Directory, newFileDirectoryBlock);  // store Directory to disk
	result |= eDisk_WriteBlock((BYTE*)&FAT, newFATBlock);                  // store new FAT Block to disk
	
	// finished
  OS_bSignal(&FileSystemMutex);                        // release FileSystemMutex
  return result;
}


//---------- eFile_WOpen-----------------
// Open the file for writing, by read the file's last Block into RAM
// Input: name[] - file name to open is an ASCII string.
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash, 
//         more than one file open, file does not exist)
int eFile_WOpen(char name[]){    
	DRESULT result = RES_OK;                              // return result of eDisk's read/write functions. Initailed to 0.
	
	// if(another file is already open), then error.
	if(NoFileOpenMutex.Value == 0){
		printf("\n\rError in eFile_WOpen. Another file is currently open already.\n\r");
		return 1; 
	}
	
	OS_bWait(&NoFileOpenMutex);                           // do not open file until all other files are closed
  OS_bWait(&FileSystemMutex);                           // aquire FileSystem mutex
	        
	// load first Directory Block from Disk and find requested file in directory
	result |= eDisk_ReadBlock((BYTE*)&Directory, 0);      // reload first Directory Block from Disk
	for(int i = 0; i < NUM_DIR_BLOCKS; i++){
		for(FileEntryInDir = 0; FileEntryInDir < ENTRIES_PER_DIRECTORY_BLOCK; FileEntryInDir++){
      if(strcmp(Directory[FileEntryInDir].fileName, name) == 0){
				FileDirBlock = i;                                       // file's Directory entry Block 
				goto FileFound;                                         //file found, break out of nested loops
			}
			else if(FileEntryInDir == ENTRIES_PER_DIRECTORY_BLOCK-1){
				result |= eDisk_ReadBlock((BYTE*)&Directory, i+1);      // reload the next Directory Block from Disk
			}
		}
	}
	// will only reach this point if file was not found, due to go goto instruction
	printf("\n\rError in eFile_WOpen. File name does not exist.\n\r");
	OS_bSignal(&FileSystemMutex);                         // release FileSystemMutex
	OS_bSignal(&NoFileOpenMutex);                         // release NoFileOpenMutex
	return 1;                                             // no more free space available
	
	//jump to here once FileEntryInDir is found in nested for loops above.
	FileFound:            
	
	// find and load file's FAT Block
	CurrentFATIndex = Directory[FileEntryInDir].startIndex;    // beggining of linked list.
	CurrentFATBlock = (CurrentFATIndex / ENTRIES_PER_FAT_BLOCK) + FIRST_RESERVED_FAT_BLOCK;
	uint16_t  currentFATEntry = CurrentFATIndex % ENTRIES_PER_FAT_BLOCK;
	result |= eDisk_ReadBlock((BYTE*)&FAT, CurrentFATBlock); 
	
	// cycle through FAT Blocks and entries until final entry is found.
	while(FAT[currentFATEntry] != 0){  
		CurrentFATIndex = FAT[currentFATEntry];
		currentFATEntry = CurrentFATIndex % ENTRIES_PER_FAT_BLOCK;
		uint16_t newFATBlock = (CurrentFATIndex / ENTRIES_PER_FAT_BLOCK) + FIRST_RESERVED_FAT_BLOCK;
		if(newFATBlock != CurrentFATBlock){
			CurrentFATBlock = newFATBlock;
			result |= eDisk_ReadBlock((BYTE*)&FAT, newFATBlock);   // load in newFATBlock
		}
	}
	
	// copy requested file's last Block from Disk into local Block so it can be written to in eFile_Write
	result |= eDisk_ReadBlock((BYTE*)&Block, CurrentFATIndex); // load Block from disk into RAM
	
  // finished
  OS_bSignal(&FileSystemMutex);                           // release FileSysteMutex
  return result;
}
	

//---------- eFile_Write-----------------
// Save a byte of data to the end of the open file's Block buffer stored in RAM.
// If the Block buffer becomes full, write the buffer to Disk. A copy of the files
// last Block should be stored in RAM from eFile_WOpen().
// Input: byte of data to be saved
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash,
//         disk is full, no file is open)
int eFile_Write(char data){
	DRESULT result = RES_OK;                                 // return result of eDisk's read/write functions. Initailed to 0.
  OS_bWait(&FileSystemMutex);                              // aquire FileSystem mutex
	
	// find where the data needs to be insterted in file
	uint16_t blockOffset = BLOCK_OFFSET(Directory[FileEntryInDir].fileSize);
	
	// put new data into local copy of Block
	Block[blockOffset]= data;
	
	// if (block was full),store new data into Block on Disk,
  // allocate new block from FAT and update FreeSpace manager.
	if((BLOCK_OFFSET(Directory[FileEntryInDir].fileSize) == 0) && (Directory[FileEntryInDir].fileSize > 0)){
		
		// Store cahnges to file's Directory Block and Reload FreeSpace manager's Directory Block 
		result |= eDisk_WriteBlock((BYTE*)&Directory, FileDirBlock);   // load FreeSpace's Directory Block from disk
    result |= eDisk_ReadBlock((BYTE*)&Directory, 0);               // load FreeSpace's Directory Block from disk
	
		// If(disk is full), then error
		if(Directory[0].fileSize == 0){
      if(!StreamToFile){
		    printf("\n\rError in eFile_Write. Disk is full.\n\r");
		  }
			OS_bSignal(&FileSystemMutex);                         // release FileSystemMutex
      return 1;
		}

	  // Find location for new Block in FreeSpace manager
    uint16_t newFATIndex = Directory[0].startIndex;     // find the file's new Block startIndex from FreeSpace manager
    
	  // Update file's data and FAT entry to Block,
		FAT[CurrentFATIndex] = newFATIndex;                         // old last now points to new
		result |= eDisk_WriteBlock((BYTE*)&FAT, CurrentFATBlock); // store Block to Disk
	  result |= eDisk_WriteBlock((BYTE*)&Block, CurrentFATIndex); // store Block to Disk
		
    // find and load file's new FAT Block and entry
	  uint16_t newFATBlock = (newFATIndex / ENTRIES_PER_FAT_BLOCK) + FIRST_RESERVED_FAT_BLOCK;
	  uint16_t  newFileFATEntry = newFATIndex % ENTRIES_PER_FAT_BLOCK;
	  result |= eDisk_ReadBlock((BYTE*)&FAT, newFATBlock); 
		
		// update FreeSpace manager's number of free blocks(fileSize) and startIndex
		(Directory[0].fileSize)--;
		Directory[0].startIndex = FAT[newFileFATEntry];
		
		// if the startIndex just overflowed (due to the new file using up the last free block, 
		// the startIndex will need to be manually assigned again in eFile_Delete.
		if(Directory[0].startIndex == 0){
			ReassignStartIndex = true;
		}
		
		// Update FAT's new Block with null terminator, and update CurrentFATIndex and CurrenFileFatBlock
		FAT[newFileFATEntry] = 0;                              // new FAT entry now has null terminator 
    CurrentFATBlock	= newFATBlock;                         // update CurrentFATBlock
		CurrentFATIndex = newFATIndex;                         // update CurrentFATIndex
		
		// Store updates onto Disk
	  result |= eDisk_WriteBlock((BYTE*)&Directory, 0);      // store FreeSpace manager's Directory Block to Disk
	  result |= eDisk_WriteBlock((BYTE*)&FAT, newFATBlock);  // store file's new FAT Block to Disk

		// reload file's Directory Block
	  result |= eDisk_ReadBlock((BYTE*)&Directory, FileDirBlock);      
	}
	
	// increment file's Directory entry fileSize
	(Directory[FileEntryInDir].fileSize)++;
	
	// finished
	OS_bSignal(&FileSystemMutex);                            // release FileSystem mutex
  return result;
}  


//---------- eFile_WClose-----------------
// close the file for writing. Leave disk in a state so that
// power can be removed.
// Input: none
// Output: 0 if successful and 1 on failure (e.g., trouble writing 
//         to flash, no file currently open)
int eFile_WClose(void){ 
	DRESULT result = RES_OK;                                   // return result of eDisk's read/write functions. Initailed to 0.
	
	// if(no files are currently open), then error
	if(NoFileOpenMutex.Value){                                      
		printf("\n\rError in eFile_WClose. No file is currently open.\n\r");
		return 1;                                                 
	}
	
	// Store updates onto Disk.  FAT and FreeSpace manager should already be updated in eFile_Write
	result |= eDisk_WriteBlock((BYTE*)&Block, CurrentFATIndex);   // store file's data Block to Disk
  result |= eDisk_WriteBlock((BYTE*)&Directory, FileDirBlock);  // store file's Directory entry to Disk
	
	OS_bSignal(&NoFileOpenMutex);                               // release NoFileOpenMutex
  return result;
}
	

//---------- eFile_ROpen-----------------
// Open the file for reading. Read first block into RAM 
// Input: name[] - file name to open is an ASCII string.
// Output: 0 if successful and 1 on failure (e.g., trouble read to flash,
//         file does not exist, another file is already open)
int eFile_ROpen(char name[]){    
	// if(another file is already open), then error.
	if(NoFileOpenMutex.Value == 0){
		printf("\n\rError in eFile_ROpen. Another file is currently open already.\n\r");
		return 1; 
	}
	
	DRESULT result = RES_OK;                              // return result of eDisk's read/write functions. Initailed to 0.
	OS_bWait(&NoFileOpenMutex);                           // do not open file until all other files are closed
  OS_bWait(&FileSystemMutex);                           // aquire FileSystem mutex
	        
	// load first Directory Block from Disk and find requested file in directory
	result |= eDisk_ReadBlock((BYTE*)&Directory, 0);      // reload first Directory Block from Disk
	for(int i = 0; i < NUM_DIR_BLOCKS; i++){
		for(FileEntryInDir = 0; FileEntryInDir < ENTRIES_PER_DIRECTORY_BLOCK; FileEntryInDir++){
      if(strcmp(Directory[FileEntryInDir].fileName, name) == 0){
				FileDirBlock = i;                                       // file's Directory entry Block 
				goto FileFound;                                         //file found, break out of nested loops
			}
			else if(FileEntryInDir == ENTRIES_PER_DIRECTORY_BLOCK-1){
				result |= eDisk_ReadBlock((BYTE*)&Directory, i+1);      // reload the next Directory Block from Disk
			}
		}
	}
	// will only reach this point if file was not found, due to go goto instruction
	printf("\n\rError in eFile_ROpen. File name does not exist.\n\r");
	OS_bSignal(&FileSystemMutex);                         // release FileSystemMutex
	OS_bSignal(&NoFileOpenMutex);                         // release NoFileOpenMutex
	return 1;                                             // no more free space available
	
	//jump to here once FileEntryInDir is found in nested for loops above.
	FileFound:  
	
	// find requested file's first entry in FAT
	CurrentFATIndex = Directory[FileEntryInDir].startIndex;    // beggining of linked list
	
	// copy requested file's first Block from Disk into RAM
	result |= eDisk_ReadBlock((BYTE*)&Block, CurrentFATIndex); // load Block from disk into RAM
	
	// update and load CurrentFATBlock into local copy of FAT for use in eFile_ReadNext
	CurrentFATBlock = (CurrentFATIndex / ENTRIES_PER_FAT_BLOCK) + FIRST_RESERVED_FAT_BLOCK;
	result |= eDisk_ReadBlock((BYTE*)&FAT, CurrentFATBlock); 
	
	// Reset CurrentReadIndex. 
  CurrentReadIndex = 0;                                      // reset CurrentReadIndex 	
	
  // finished
  OS_bSignal(&FileSystemMutex);                              // release FileSysteMutex
  return result;
}
	

//---------- eFile_ReadNext-----------------
// Retreive byte of data from open file. A copy of the file's Directory 
// Block, FAT Block, and the file's first data Block should be stored in  
// RAM initally after eFile_WOpen().
// Input: *pt - pointer which data will stored a byte to
// Output: return by reference of data. 0 if successful 
//         and 1 on failure (e.g., end of file)
int eFile_ReadNext(char *pt){       
	DRESULT result = RES_OK;                                 // return result of eDisk's read/write functions. Initailed to 0.
  OS_bWait(&FileSystemMutex);                              // aquire FileSystem mutex	
  
	// if(trying to read from end of file), then error
	if(CurrentReadIndex == Directory[FileEntryInDir].fileSize){
		printf("\n\rError in eFile_ReadNext. Trying to read past end of file.\n\r");
		OS_bSignal(&FileSystemMutex);                          // release FileSystemMutex
		return 1;                                           
	}

  // update refence pointer with read data
  *pt = Block[BLOCK_OFFSET(CurrentReadIndex)];

	// update read index
	CurrentReadIndex++;

	// if(at the end of the current Block), then reload the next Block for reading
	if(BLOCK_OFFSET(CurrentReadIndex) == 0){
		// find the index for the next Block of data
		uint16_t  newFileFATEntry = CurrentFATIndex % ENTRIES_PER_FAT_BLOCK;
		CurrentFATIndex = FAT[newFileFATEntry];
		
		if(CurrentFATIndex % ENTRIES_PER_FAT_BLOCK == 0){
			// find and load file's new FAT Block and entry
	    CurrentFATBlock = (CurrentFATIndex / ENTRIES_PER_FAT_BLOCK) + FIRST_RESERVED_FAT_BLOCK;
			result |= eDisk_ReadBlock((BYTE*)&FAT, CurrentFATBlock); 
		}
		
		// load the next data Block for reading
		result |= eDisk_ReadBlock((BYTE*)&Block, CurrentFATIndex); // read Block from Disk

	}
	
	// finished
	OS_bSignal(&FileSystemMutex);                            // release FileSystemMutex
	return result;
}
	

//---------- eFile_RClose-----------------
// close the reading file
// Input: none
// Output: 0 if successful and 1 on failure (e.g., wasn't open)
int eFile_RClose(void){ 
	// if(no files are currently open), then error
	if(NoFileOpenMutex.Value){                                      
		printf("\n\rError in eFile_RClose. No file is currently open.\n\r");
		return 1;                                                 
	}
	OS_bSignal(&NoFileOpenMutex);                    // release NoFileOpenMutex mutex
  return 0;
}

	
//---------- eFile_Directory-----------------
// Display the directory with filenames and sizes
// Input: none
// Output: characters returned by reference
//         0 if successful and 1 on failure (e.g., trouble reading from flash)
int eFile_Directory(void){
  DRESULT result = RES_OK;                              // return result of eDisk's read/write functions. Initailed to 0.
  OS_bWait(&FileSystemMutex);                           // aquire FileSystemMutex

	// load first Directory Block from Disk
	result |= eDisk_ReadBlock((BYTE*)&Directory, 0);      // reload first Directory Block from Disk
	
	// print directory table
	printf("\n\r---------------------------\n\r");
	printf("         DIRECTORY       \n\r");
	printf("---------------------------\n\r");
	printf("Blocks available:\t%d\n\n\r", Directory[0].fileSize);
	bool noFileInDirectory = true;
	
	// find all files in directory
	for(int i = 0; i < NUM_DIR_BLOCKS; i++){
		for(FileEntryInDir = 0; FileEntryInDir < ENTRIES_PER_DIRECTORY_BLOCK; FileEntryInDir++){
      if(i == 0 && FileEntryInDir == 0){
				// do nothing, this is the FreeSpace manager's entry in the Directory
			}
			else if(strcmp(Directory[FileEntryInDir].fileName, "")){
	      noFileInDirectory = false;
				// print file name
				printf("%s",Directory[FileEntryInDir].fileName);
				// print clean formatting spaces
			  int len = strlen(Directory[FileEntryInDir].fileName);
				int numSpace = (MAX_FILE_NAME) - len;
				while(numSpace > 0){
				  printf(" ");
				  numSpace--;
				}
				// print file size
			  printf("%i ",Directory[FileEntryInDir].fileSize);
			  if(Directory[FileEntryInDir].fileSize <10){
				  printf("   bytes\n\r");
			  }
				else if(Directory[FileEntryInDir].fileSize <100){
				  printf("  bytes\n\r");
			  }
			  else if(Directory[FileEntryInDir].fileSize <1000){
				  printf(" bytes\n\r");
			  }
			  else{
				  printf("bytes\n\r");
			  }	
			}
			else if(FileEntryInDir == ENTRIES_PER_DIRECTORY_BLOCK-1){
				result |= eDisk_ReadBlock((BYTE*)&Directory, i+1);      // reload the next Directory Block from Disk
			}
		}
	}
	
	if(noFileInDirectory){
		printf("No files in Directory\n\r");
	}
	
	// finished
	OS_bSignal(&FileSystemMutex);                            // release FileSystemMutex
  return result;
}	


//---------- eFile_Delete-----------------
// Delete this file.
// Input: name[] - file name to open is an ASCII string.
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash, 
//         file does not exist, file is currently open)
int eFile_Delete(char name[]){ 
	// if(file is currently open), then error.
	if(NoFileOpenMutex.Value == 0){
		printf("\n\rError in eFile_Delete. Close file before deleting it.\n\r");
		return 1;    
	}
	DRESULT result = RES_OK;                              // return result of eDisk's read/write functions. Initailed to 0.
	OS_bWait(&FileSystemMutex);                           // aquire FileSystem mutex
	
	// load first Directory Block from Disk and find file you are deleting in Directory
	// Store file's Directory Block in FileDirBlock.
	// Store file's entry in Directory Block in FileEntryInDir
  result |= eDisk_ReadBlock((BYTE*)&Directory, 0);      // reload Directory from Disk
	for(int i = 0; i < NUM_DIR_BLOCKS; i++){
		for(FileEntryInDir = 0; FileEntryInDir < ENTRIES_PER_DIRECTORY_BLOCK; FileEntryInDir++){
			if(strcmp(Directory[FileEntryInDir].fileName, name) == 0){
				FileDirBlock = i;                                       // file's Directory entry Block 
				goto FileFound;                                         //file found, break out of nested loops
			}
			else if(FileEntryInDir == ENTRIES_PER_DIRECTORY_BLOCK-1){
				result |= eDisk_ReadBlock((BYTE*)&Directory, i+1);      // reload the next Directory Block from Disk
			}
		}
	}
	// will only reach this point if file was not found, due to go goto instruction
	printf("\n\rError in eFile_Delete. File name does not exist.\n\r");
	OS_bSignal(&FileSystemMutex);                         // release FileSystemMutex
	return 1;                                             // no more free space available
	
	//jump to here once FileEntryInDir is found in nested for loops above.
	FileFound:   
	
	// load FreeSpace manager's Directory Block, and find its startIndex in FAT
	result |= eDisk_ReadBlock((BYTE*)&Directory, 0);      // reload Directory from Disk
	uint16_t freeSpaceIndex = Directory[0].startIndex;
	
	// if there was a FreeSpace startIndex overflow in eFile_Write, 
	// need to manually assign startIndex to file being deleted.
	if(ReassignStartIndex){
		ReassignStartIndex = false;
		Directory[0].startIndex = FileEntryInDir;
		result |= eDisk_WriteBlock((BYTE*)&Directory, 0);      // store FreeSpace manager's Directory Block to Disk
	}
	
	// find and load file's FreeSpace managers' FAT Block and entry
	uint16_t freeSpaceFATBlock = (freeSpaceIndex / ENTRIES_PER_FAT_BLOCK) + FIRST_RESERVED_FAT_BLOCK;
	uint16_t  freeSpaceFATEntry = freeSpaceIndex % ENTRIES_PER_FAT_BLOCK;
	result |= eDisk_ReadBlock((BYTE*)&FAT, freeSpaceFATBlock); 
	
	// Cycle through FreeSpace manager's FAT Blocks until last entry is found.
	// Store FreeSpace manager's last index in freeSpaceFATIndex
  // Store FreeSpace manager's last FAT Block in freeSpaceFATBlock.
	// Store FreeSpace manager's last entry in last Block in freeSpaceFATEntry
	while(FAT[freeSpaceFATEntry] != 0){
		freeSpaceIndex = FAT[freeSpaceFATEntry];
		uint16_t newFATBlock = (freeSpaceIndex / ENTRIES_PER_FAT_BLOCK) + FIRST_RESERVED_FAT_BLOCK;			                
		if(newFATBlock != freeSpaceFATBlock){
			freeSpaceFATBlock = newFATBlock;
			result |= eDisk_ReadBlock((BYTE*)&FAT, newFATBlock);   // load in newFATBlock
			freeSpaceFATEntry = freeSpaceIndex % ENTRIES_PER_FAT_BLOCK;	
		}
		else{
      freeSpaceFATEntry = freeSpaceIndex % ENTRIES_PER_FAT_BLOCK;			
		}
	}
	
  // Reload File's Directory Block.
	// FreeSpace manager's last entry now points to the startIndex of the deleted file.
	result |= eDisk_ReadBlock((BYTE*)&Directory, FileDirBlock);      // reload Directory from Disk
	FAT[freeSpaceFATEntry] = Directory[FileEntryInDir].startIndex;
	
	// remove file's entry from Directory
	strcpy(Directory[FileEntryInDir].fileName, "");
	uint32_t fileSize = Directory[FileEntryInDir].fileSize;       // save fileSize before erasing
  Directory[FileEntryInDir].fileSize = 0;
	Directory[FileEntryInDir].startIndex = 0;
	
	// store updateded Directory and FAT to disk
	result |= eDisk_WriteBlock((BYTE*)&Directory, FileDirBlock);  // store Directory to disk
	result |= eDisk_WriteBlock((BYTE*)&FAT, freeSpaceFATBlock);   // store FAT to disk
	
	// Determine how many blocks the deleting file consumes.
	// Update FreeSpace manager's fileSize (number of free entries).
	result |= eDisk_ReadBlock((BYTE*)&Directory, 0);  // load FreeSpace manager's Directory Block from Disk
	if(fileSize == 0){
		(Directory[0].fileSize) += 1;
	}
	else if((fileSize % BLOCKSIZE) == 0){
		(Directory[0].fileSize) += fileSize / BLOCKSIZE;
	}
	else{
		(Directory[0].fileSize)+= (fileSize / BLOCKSIZE) + 1;
	}
	
	// store updated FreeSpace manager's Directory Block to Disk
  result |= eDisk_WriteBlock((BYTE*)&Directory, 0);  // store Directory to disk
	
	// finished
	OS_bSignal(&FileSystemMutex);                       // release FileSystemMutex
  return result;
}
	

//---------- eFile_RedirectToFile-----------------
// open a file for writing. Redirect IO stream to file.
// Input: name[] - file name to open is an ASCII string.
// Output: 0 if successful and 1 on failure (e.g., trouble read/write to flash)
int eFile_RedirectToFile(char name[]){
	OS_bWait(&FileSystemMutex);                 // aquire FileSystemMutex
		// load first Directory Block from Disk and find requested file in directory
  eDisk_ReadBlock((BYTE*)&Directory, 0);      // reload first Directory Block from Disk
	for(int i = 0; i < NUM_DIR_BLOCKS; i++){
		for(FileEntryInDir = 0; FileEntryInDir < ENTRIES_PER_DIRECTORY_BLOCK; FileEntryInDir++){
      if(strcmp(Directory[FileEntryInDir].fileName, name) == 0){
				FileDirBlock = i;                                       // file's Directory entry Block 
				goto FileFound;                                         //file found, break out of nested loops
			}
			else if(FileEntryInDir == ENTRIES_PER_DIRECTORY_BLOCK-1){
				eDisk_ReadBlock((BYTE*)&Directory, i+1);      // reload the next Directory Block from Disk
			}
		}
	}
	// will only reach this point if file was not found, due to go goto instruction
	OS_bSignal(&FileSystemMutex);               // release FileSystemMutex	
  eFile_Create(name);                         // ignore error if file already exists
	
	//jump to here once FileEntryInDir is found in nested for loops above.
	FileFound:  
	
	OS_bSignal(&FileSystemMutex);               // release FileSystemMutex	
  if(eFile_WOpen(name)){
		return 1;                                 // cannot open file
	}
	OS_bWait(&FileSystemMutex);                 // aquire FileSystemMutex
	StreamToFile = 1;                           // IO now streams to file
	OS_bSignal(&FileSystemMutex);               // release FileSystemMutex
  return 0;
}


//---------- eFile_EndRedirectToFile-----------------
// close the previously open file. Redirect IO stream back to UART
// Input: none
// Output: 0 if successful and 1 on failure (e.g., wasn't open)
int eFile_EndRedirectToFile(void){
	OS_bWait(&FileSystemMutex);                 // aquire FileSystemMutex
  StreamToFile = 0;                           // IO now streams to UART
	OS_bSignal(&FileSystemMutex);               // release FileSystemMutex
  if(eFile_WClose()){
		return 1;                                 // cannot close file
	}
  return 0;
}


//******** eFile_Error **************
// Called whenever an error is found during eFile or 
// eDisk testing. Kills task after printing results.
// inputs:  *errtpye - string pointer containing the error enum 
//          block - block number in which error occured
// outputs: none
void eFile_Error(char* errtype, unsigned long block){
  printf(errtype);
  printf(" disk error %u",block);
  OS_Kill();
}

//******** eFile_DiskFull **************
// Returns whether the Disk is full or not.
// inputs:  none
// outputs: 0 = Disk is not full. 1 = Disk is full
uint8_t eFile_DiskFull(void){
	OS_bWait(&FileSystemMutex);                 // aquire FileSystemMutex
  eDisk_ReadBlock((BYTE*)&Directory, 0);      // reload Directory from Disk
	OS_bSignal(&FileSystemMutex);               // release FileSystemMutex
	if(Directory[0].fileSize == 0){             
		return 1;
	}
	else{
		return 0;
	}
}
