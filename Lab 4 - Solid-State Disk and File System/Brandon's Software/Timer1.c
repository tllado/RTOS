// Timer1.c
// Runs on TM4C123 
// Use Timer3 in 32-bit periodic mode to request interrupts at a periodic rate.
// Timer1 is in charge of running background threads in the OS.
// Brandon Boesch
// Ce Wei
// February 21, 2016

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2013
  Program 7.5, example 7.6

 Copyright 2013 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Interrupts.h"
#include "Timer1.h"
#include "Lab4.h"
#include "OS.h"


//***********Global Variables******************
void (*BackgroundTask)(void);   // background function called by Timer1 ISR handler
//*********************************************

// ***************** Timer3_Init ****************
// Activate Timer1 interrupts periodically.  Timer1 is in charge of running
// background threads in the OS.  Timer1 has highest priority
// Inputs:  period in units (1/clockfreq)
// Outputs: none
void Timer1_Init(unsigned long period){
  SYSCTL_RCGCTIMER_R |= 0x02;   // 0) activate TIMER1
	while((SYSCTL_PRTIMER_R&0x02) == 0){}; // allow time for clock to stabilize
  TIMER1_CTL_R = 0x00000000;    // 1) disable TIMER1A during setup
  TIMER1_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER1_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER1_TAILR_R = period-1;    // 4) reload value
  TIMER1_TAPR_R = 0;            // 5) bus clock resolution
  TIMER1_ICR_R = 0x00000001;    // 6) clear TIMER1A timeout flag
  TIMER1_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|(0 << 13); // 8) priority 0 
//  NVIC_EN0_R = 1<<21;           // 9) enable IRQ 21 in NVIC
//  TIMER1_CTL_R = 0x00000001;    // 10) enable TIMER1A
}

// ***************** Timer1A_Handler ******************
// Timer1 is in charge of running background threads in the OS.
void Timer1A_Handler(void){
  TIMER1_ICR_R = TIMER_ICR_TATOCINT;                  // acknowledge TIMER1A timeout
	int32_t status = StartCritical();
	TCBTypeBGround *tempTCBPt = FirstBackgroundPt;      // tempTCBPt used to cycle through TCB list. Start at front of line
  do{                                                 // cycle through all backgroundTCBs
		TCBTypeBGround *nextTCBPt = tempTCBPt->next;      // Save the next backgroundTCB to check, incase it is lost in OS_KillAperiodic
		BackgroundTCBs[tempTCBPt->id].sleepCnt -= 1;      // decrement backgroundTCB's sleep counter
		if(BackgroundTCBs[tempTCBPt->id].sleepCnt == 0){  // execute background task
			BackgroundTask = tempTCBPt->task;
			BackgroundTask();
			if(tempTCBPt->aperiodic){                            // if true, thread is aperiodic
				OS_KillAperiodic(tempTCBPt->id);                   // kill aperidic thread. Remove it from backgroundTCB list and decrement NumBGround
		  }
		  else{                                                // else, thread is periodic
				BackgroundTCBs[tempTCBPt->id].sleepCnt = SLEEPCNT_CONVERT(BackgroundTCBs[tempTCBPt->id].period);  // reset the background threads sleepcnt after execution
		  }
		}
		tempTCBPt = nextTCBPt;                                 // access next TCB in list
	}while(tempTCBPt != FirstBackgroundPt);
	EndCritical(status);
}
	

// ***************** Timer1A_Enable ******************
void Timer1A_Enable(void){
	NVIC_EN0_R = 1<<21;                // enable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000001;         // enable TIMER1A
}


// ***************** Timer1A_Disable ******************
void Timer1A_Disable(void){
	NVIC_DIS0_R = 1<<21;               // disable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000000;         // disable TIMER1A
}


