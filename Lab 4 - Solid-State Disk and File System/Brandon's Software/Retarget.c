// Retarget.c
// Runs on TM4C123 
// Retargets printf statments. 
// Brandon Boesch
// Ce Wei
// March 15th, 2016

#include <stdio.h>
#include <stdint.h>
#include "Retarget.h"
#include "eFile.h"
#include "UART.h"


// *****************Globals**************************
int StreamToFile=0;   // redirects printf statments. 0=UART, 1=stream to file
FILE __stdout;
FILE __stdin;
// **************************************************


//---------- fputc-----------------
// redirects printf statments to SD card or UART 
// depending on value of global StreamToFile.  
// StreamToFile = 0, print to UART.
// StreamToFile = 1, print to SDC. 
int fputc (int ch, FILE *f) {
	// if true, output to SD card
  if(StreamToFile){
    if(eFile_Write(ch)){          // close file on error
      eFile_EndRedirectToFile();  // cannot write to file
      return 1;                   // failure
    }
    return 0;                     // success writing
  }
  // else, regular UART output
  UART_OutChar(ch);
  return 0;
}


//---------- fgetc-----------------
// Returns the character currently pointed by the internal file 
// position indicator of the specified by FILE
int fgetc (FILE *f){
  return (UART_InChar());
}
