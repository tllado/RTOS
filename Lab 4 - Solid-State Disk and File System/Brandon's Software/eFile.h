// eFile.c
// Runs on TM4C123
// Middle-level routines to implement a solid-state disk 
// Brandon Boesch
// Ce Wei
// March 13th, 2016


// ******************** Constants *******************************************************
#define MAX_NUM_DIR    32     // Max number of directory entries
#define MAX_NUM_FAT    32     // Max number of File Allocation Table(FAT) entries
//#define MAX_NUM_BLOCKS 32     // Max number of blocks on disk
#define BLOCKSIZE      512    // Blocks on disk are 512 bytes each

#define MAX_FILE_NAME  10                    // fileNames can have a max of 9 characters + 1 null terminator.
#define MAX_NUM_BLOCKS              2048     // Max number of Blocks on Disk
#define NUM_DIR_BLOCKS              64       // Number of Blocks needed to hold Directory
#define NUM_FAT_BLOCKS              8        // Number of Blocks needed to hold FAT
#define ENTRIES_PER_DIRECTORY_BLOCK 32       // Numer of entries in 1 Directory Block
#define ENTRIES_PER_FAT_BLOCK       256      // Number of entries an 1 FAT Block.

#define FIRST_RESERVED_FAT_BLOCK    NUM_DIR_BLOCKS                     // Reserved FAT Blocks begin after the directory
#define FIRST_UNRESERVED_FAT_BLOCK  NUM_DIR_BLOCKS+NUM_FAT_BLOCKS      // The first available FAT entry after format
#define MAX_NUM_FILES               MAX_NUM_BLOCKS-(NUM_DIR_BLOCKS+NUM_FAT_BLOCKS) 
// **************************************************************************


// ******************** Macros *********************************************************
#define BLOCK_OFFSET(x) x % BLOCKSIZE    // used to determine which byte to access in a open file
// *************************************************************************************


// ************* Stucts ****************************************************
typedef struct DirEntry{
  char fileName[MAX_FILE_NAME];     // name of file. The largest name = (MAX_FILE_NAME - 1). Subtract 1 because of null terminator 
  uint16_t startIndex;              // index into file's first entry in the File Allocation Table(FAT)
	uint32_t fileSize;                // the size of the file (in bytes)
}DirEntryType;
// **************************************************************************


// ************* Externs ****************************************************
extern DirEntryType Directory[ENTRIES_PER_DIRECTORY_BLOCK];  // Local copy of a Directory Block. The Directory lives in Blocks 0-63 on disk
extern uint16_t FAT[ENTRIES_PER_FAT_BLOCK];                  // Local copy of a Block in the File Allocation Table(FAT). The FAT lives in Blocks 64-71 on Disk
extern uint8_t Block[512];                   // Local copy of a block with 512 bytes.
extern uint16_t FileEntryInDir;               // Index into open file's Directory entry
extern uint32_t CurrentReadIndex;            // Current byte index into File's Block that the function eFile_ReadNext is accessing            
// **************************************************************************


//********************Prototypes********************************************

//---------- eFile_Init-----------------
// Activate the file system, without formating. Since this function
// initializes the disk, it must run with the disk periodic task operating.
// Calls OS_Error() on failure(already initialized, no disk, write protected, etc...)
// Input: none
// Output: none 
void eFile_Init(void); 

//---------- eFile_Format-----------------
// Delete all files by erasing blocks, initializing FreeSpace manager,
// creating a blank directory, and initializing FAT into a linked list.
// Input: none
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash)
int eFile_Format(void); 

//---------- eFile_FreeSpaceInit-----------------
// Create the free space manager by allocating the first element
// in the directory to hold the startIndex of free blocks and the number
// of free blocks available(fileSize). Also initalizes all the FAT entries 
// (except for the reserved entries 0 thru 71).
// Input: none
// Output: 0 if successful and 1 on failure (already initialized)
int eFile_FreeSpaceInit(void);

//---------- eFile_Create-----------------
// Create a new, empty file with one allocated block
// Input: name[] - file name is an ASCII string up to 9 characters + 1 null terminator 
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash, 
//         directory is full, or if file already exists)
int eFile_Create(char name[]);

//---------- eFile_WOpen-----------------
// Open the file for writing, by read the file's last Blcok into RAM
// Input: name[] - file name to open is an ASCII string.
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash, 
//         more than one file open, file does not exist)
int eFile_WOpen(char name[]);     

//---------- eFile_Write-----------------
// Save a byte of data to the end of the open file's Block buffer stored in RAM.
// If the Block buffer becomes full, write the buffer to Disk. A copy of the files
// last Block should be stored in RAM from eFile_WOpen().
// Input: byte of data to be saved
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash,
//         disk is full, no file is open)
int eFile_Write( char data);  

//---------- eFile_WClose-----------------
// close the file for writing. Leave disk in a state so that
// power can be removed.
// Input: none
// Output: 0 if successful and 1 on failure (e.g., trouble writing 
//         to flash, no file currently open)
int eFile_WClose(void); 

//---------- eFile_ROpen-----------------
// Open the file for reading. Read first block into RAM 
// Input: name[] - file name to open is an ASCII string.
// Output: 0 if successful and 1 on failure (e.g., trouble read to flash,
//         file does not exist)
int eFile_ROpen( char name[]);  
   
//---------- eFile_ReadNext-----------------
// Retreive byte of data from open file. A copy of the file's Directory 
// Block, FAT Block, and the file's first data Block should be stored in  
// RAM initally after eFile_WOpen().
// Input: *pt - pointer which data will stored a byte to
// Output: return by reference of data. 0 if successful 
//         and 1 on failure (e.g., end of file)
int eFile_ReadNext( char *pt);     
                              
//---------- eFile_RClose-----------------
// close the reading file
// Input: none
// Output: 0 if successful and 1 on failure (e.g., wasn't open)
int eFile_RClose(void); 

//---------- eFile_Directory-----------------
// Display the directory with filenames and sizes
// Input: none
// Output: characters returned by reference
//         0 if successful and 1 on failure (e.g., trouble reading from flash)
int eFile_Directory(void);

//---------- eFile_Delete-----------------
// Delete this file.
// Input: name[] - file name to open is an ASCII string.
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash, 
//         file does not exist)
int eFile_Delete( char name[]); 

//---------- eFile_RedirectToFile-----------------
// open a file for writing. Redirect IO stream to file.
// Input: file name is a single ASCII letter
// stream printf data into file
// Output: 0 if successful and 1 on failure (e.g., trouble read/write to flash)
int eFile_RedirectToFile(char *name);

//---------- eFile_EndRedirectToFile-----------------
// close the previously open file. Redirect IO stream back to UART
// Input: none
// Output: 0 if successful and 1 on failure (e.g., wasn't open)
int eFile_EndRedirectToFile(void);

//******** eFile_Error **************
// Called whenever an error is found during eFile or 
// eDisk testing. Kills task after printing results.
// inputs:  *errtpye - string pointer containing the error enum 
//          block - block number in which error occured
// outputs: none
void eFile_Error(char* errtype, unsigned long block);

//******** eFile_DiskFull **************
// Returns weather the Disk is full or not.
// inputs:  none
// outputs: 0 = Disk is not full. 1 = Disk is full
uint8_t eFile_DiskFull(void);
