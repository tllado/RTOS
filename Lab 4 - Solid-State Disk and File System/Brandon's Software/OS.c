// OS.c
// Runs on TM4C123 
// Create and manage threads.
// Brandon Boesch
// Ce Wei
// January 25, 2016

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "tm4c123gh6pm.h"
#include "Lab4.h"
#include "Interrupts.h"
#include "OS.h"
#include "PLL.h"
#include "Timer1.h"
#include "Timer2.h"
#include "Timer3.h"
#include "UART.h"
#include "Buttons.h"
#include "Retarget.h"


// *****************Globals**************************
unsigned long Num4Ground;                        // number of foreground threads active
unsigned long NumBGround;                        // number of background threads active
unsigned long NumBlocked;                        // number of blocked foreground threads
unsigned long NumActive;                         // number of active foreground threads
unsigned long MaxNumPri = MAX_NUM_PRI;           // maximum number of priority levels.  Passed into OSasm.s
TCBType4Ground ForegroundTCBs[MAX_NUM_4GROUND];  // Foreground Thread Control Blocks (TCB)
TCBType4Ground *RunPt;                           // pointer to running ForegroundTCB
TCBTypeBGround BackgroundTCBs[MAX_NUM_BGROUND];  // Background Thread Control Blocks (TCB)
TCBTypeBGround *FirstBackgroundPt;               // points to the BackgroundTCB with the highest priority 
int32_t Stacks[MAX_NUM_4GROUND][STACKSIZE];      // stacks for active foreground threads
void (*PeriodicTask)(void);                      // pointer to user function found in OS_AddPeriodicThread
static uint32_t Fifo[FIFOSIZE];
volatile uint32_t *PutPt;
volatile uint32_t *GetPt;
volatile uint32_t MailBoxData;                   // data in mailbox that is passed from producer to consumer
volatile uint32_t MsTime;                        // Current time(in ms) since last reset
#ifdef PRIORITY_SCHEDULER
  uint32_t Scheduler = PRIORITY;                 // Determines which scheduler to use.  Passed into OSasm.s
#endif
#ifdef ROUNDROBIN_SCHEDULER
  uint32_t Scheduler = ROUNDROBIN;               // Determines which scheduler to use.  Passed into OSasm.s
#endif
// **************************************************


// *****************Global Semephores****************
volatile Sema4Type CurrentSize;     // signaled in OS_Fifo_Put. Signifies data is in fifo
volatile Sema4Type FifoMutex;       // binary semaphore signaled in OS_Fifo_Put and OS_Fifo_Get. Allows mutual exclusion of R-W-M access of fifo
volatile Sema4Type DataValid;       // binary semaphore signaled in OS_MailBox_Send. 0 = no data available, 1 = data available.
volatile Sema4Type BoxFree;         // binary semaphore signaled in OS_MailBox_Recv. 0 = mail box not available, 1 = mailbox available
volatile Sema4Type RxDataAvailable; // signaled in UART's copyHardwareToSoftware. 0 = Rx Fifo is empty, else = data in Rx Fifo
volatile Sema4Type TxRoomLeft;      // signaled in UART's copySoftwareToHardware. 0 = Tx Fifo is full, else = room available in Tx Fifo
volatile Sema4Type FileSystemMutex; // binary semaphore. 0 = FileSystem globals not available, 1 = FileSystem globals available
volatile Sema4Type NoFileOpenMutex; // binary semaphore. Only one file should be open at any time. 0 = a file is currently open, 1 = no files are currently open.
// **************************************************

// *****************Global Linked Lists**************
volatile LinkedListType Sleeping;                     // holds list of all sleeping ForegroundTCBs. 
volatile LinkedListType PriorityLists[MAX_NUM_PRI];   // array of priority levels' values and linked lists 
// **************************************************


// ******** OS_Init ************
// initialize operating system, disable interrupts until OS_Launch
// initialize OS controlled I/O: serial, ADC, systick, LaunchPad I/O and timers 
// input:  none
// output: none
void OS_Init(void){
  DisableInterrupts();
	//initalize clock
	PLL_Init(Bus80MHz);             // set processor clock to 80 MHz 
	// initialize globals
	Num4Ground = 0;                 // active foreground thread count
	NumBGround = 0;                 // active background thread count
	Sleeping.Value = 0;             // no foreground threads are sleeping initially
	NumBlocked = 0;                 // no foreground threads are blocked initially
	NumActive = 0;                  // no active foreground threads initially
	TCBInit();                      // initialize foreground and background TCBs
  // initialize I/O
	UART_Init();                    // initialize UART
	// initialize communication channels
	OS_MailBox_Init();
  OS_Fifo_Init(512);              // *note* 4 is not big enough
	// initialize timers
	NVIC_ST_CTRL_R = 0;             // disable SysTick during setup
  NVIC_ST_CURRENT_R = 0;          // any write to current clears it
  NVIC_SYS_PRI3_R =(NVIC_SYS_PRI3_R&0x00FFFFFF)|0xE0000000; // priority 7
	Timer1_Init(BACKGROUND_PERIOD); // initialize timer1 to run background tasks.
	Timer2_Init(0xFFFFFFFF);        // intialize timer2 with max 32bit period. Used for OS_Time()
	Timer3_Init(TIME_1MS);          // interrupt every millisecond.  Used for OS_Sleep();
	// add scheduler dependent code
  if(Scheduler == PRIORITY){      // add aging background thread if Priority scheduler is used
	  OS_AddPeriodicThread(&OS_Aging,AGING_PERIOD,7);  // aging background task.  Lowest priority
	}
}


//***********SetInitialStack************
// Make the TCB looks like it was suspended from previous states
// R15 - PC will be set in OS_AddThread
// R13 - user stack will not be changed
void SetInitialStack(int i){
  ForegroundTCBs[i].sp = &Stacks[i][STACKSIZE-16]; // thread stack pointer														 
  Stacks[i][STACKSIZE-1] = 0x01000000;   // thumb bit, PSR, last one in the array and lowest in the memory							 
	Stacks[i][STACKSIZE-3] = 0x14141414;   // R14, LR
  Stacks[i][STACKSIZE-4] = 0x12121212;   // R12
  Stacks[i][STACKSIZE-5] = 0x03030303;   // R3
  Stacks[i][STACKSIZE-6] = 0x02020202;   // R2
  Stacks[i][STACKSIZE-7] = 0x01010101;   // R1
  Stacks[i][STACKSIZE-8] = 0x00000000;   // R0
  Stacks[i][STACKSIZE-9] = 0x11111111;   // R11
  Stacks[i][STACKSIZE-10] = 0x10101010;  // R10
  Stacks[i][STACKSIZE-11] = 0x09090909;  // R9
  Stacks[i][STACKSIZE-12] = 0x08080808;  // R8
  Stacks[i][STACKSIZE-13] = 0x07070707;  // R7
  Stacks[i][STACKSIZE-14] = 0x06060606;  // R6
  Stacks[i][STACKSIZE-15] = 0x05050505;  // R5
  Stacks[i][STACKSIZE-16] = 0x04040404;  // R4
}


//******** TCBInit *************** 
// Initialize all the TCBs. Called within OS_Init.
// Inputs: none
// Outputs: none
void TCBInit(void){
	// initialize foreground TCBs
	for(int i = 0; i < MAX_NUM_4GROUND; i++){
		ForegroundTCBs[i].id = i;           // set unique id for each tcb
		ForegroundTCBs[i].alive = false;    // all tcbs are initially inactive
	}
	// initialize background TCBs
	for(int i = 0; i <MAX_NUM_BGROUND; i++){
		BackgroundTCBs[i].id = i;           // set unique id for each tcb
		BackgroundTCBs[i].alive = false;    // all tcbs are initially inactive
	}
}


// ******** OS_InitSemaphore ************
// Initialize semaphore. A value >= 1 means semaphor is free for use 
// input:  pointer to a semaphore
// output: none
void OS_InitSemaphore(volatile Sema4Type *semaPt, long value){
	int32_t status = StartCritical();
  semaPt->Value = value;
	semaPt->list = 0;
	EndCritical(status);
}


//******** OS_Launch *************** 
// start the scheduler, enable interrupts
// Inputs: number of 12.5ns clock cycles for each time slice
//         you may select the units of this parameter
// Outputs: none (does not return)
// In Lab 3, you should implement the user-defined TimeSlice field
// It is ok to limit the range of theTimeSlice to match the 24-bit SysTick
void OS_Launch(unsigned long theTimeSlice){
	if(Num4Ground == 0){                 // prevent OS from crashing by adding a dummy thread
    OS_AddThread(&Dummy,128,7); 
  }
	if(NumBGround != 0){                 // enable timer1 to handle background tasks
		Timer1A_Enable();
	}
  NVIC_ST_RELOAD_R = theTimeSlice - 1; // reload value
  NVIC_ST_CTRL_R = 0x00000007;         // enable, core clock and interrupt arm
  StartOS_ASM();                       // start on the first task
}


//******** OS_AddPeriodicThread *************** 
// Adds a background periodic task.  See Timer1.c for background handler
// Inputs: *task - pointer to a void/void background function
//         period - given in system time units (12.5ns). Must be 
//                  greater than or equal to BACKGROUND_PERIOD
//         priority - 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// You are free to select the time resolution for this function
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal, OS_AddThread
// This task does not have a Thread ID
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddPeriodicThread(void(*task)(void), unsigned long period, unsigned long priority){
	int32_t status = StartCritical();
	if((period % BACKGROUND_PERIOD) != 0){  
		OS_Error(B_PERIOD); // do not allow a period smaller than predefined value or not a multiple of Timer1's period. change either 'period' or BACKGROUND_PERIOD.
	}
	
	// not allowed to create more background threads than allocated
	else if(NumBGround >= MAX_NUM_BGROUND){
		EndCritical(status);
		return 0;        
	}
	
	// else if NumBground == 0, create first background thread
	else if(NumBGround == 0){
		Timer1A_Enable();                            // turn on timer1 to handle background threads
		BackgroundTCBs[0].alive = true;              // bring TCB to life
		BackgroundTCBs[0].task = task;               // assign TCB its task
		BackgroundTCBs[0].next = &BackgroundTCBs[0]; // cycle back to self since the only backgroundTCB alive
	  BackgroundTCBs[0].previous = &BackgroundTCBs[0]; // cycle back to self since the only backgroundTCB alive
		BackgroundTCBs[0].period = period;           // assign period to TCB
		BackgroundTCBs[0].sleepCnt = SLEEPCNT_CONVERT(period); // assign sleepCnt to TCB based off timer1's period
	  BackgroundTCBs[0].priority = priority;       // assign priority to TCB
		BackgroundTCBs[0].aperiodic = false;         // create a periodic backgroundTCB
		FirstBackgroundPt = &BackgroundTCBs[0];      // TCB[0] will run first
	}
	
	// else , create new background thread
	else{
		// find dead backgroundTCB slot to bring alive. In worstcase O(n). Maybe change to FIFO list of all availavle dead slots to choose from
		int i;                                    // i holds the new TCB's index. 
		for(i = 0; i < MAX_NUM_BGROUND; i++){     
		  if(BackgroundTCBs[i].alive == false){
				BackgroundTCBs[i].alive = true;       // bring TCB to life
				break;                          
			}
		}
		// assign new TCB its characteristics
		BackgroundTCBs[i].task = task;            // assign TCB its task
		BackgroundTCBs[i].period = period;        // assign period to TCB
		BackgroundTCBs[i].sleepCnt = SLEEPCNT_CONVERT(period); // assign sleepCnt to TCB based off timer1's period
	  BackgroundTCBs[i].priority = priority;    // assign priority to TCB
		BackgroundTCBs[i].aperiodic = false;      // create a periodic backgroundTCB
		
		// put backgroundTCB into linked list based on priority. In worstcase O(n). Possibly add a seperate living list for each priority level to increase efficiency
		TCBTypeBGround *tempTCBPt = FirstBackgroundPt;          // tempTCBPt used to cycle through TCB list. Start at front of line
		while(1){
			// if new background TCB has higher priority
		    if(priority < tempTCBPt->priority){                 // put background TCB in front of TCB with less priority
				BackgroundTCBs[i].previous = tempTCBPt->previous;   // create new background TCB's previous pt
				BackgroundTCBs[i].next = tempTCBPt;                 // create new background TCB's next pt
				tempTCBPt->previous->next = &BackgroundTCBs[i];     // point previous TCB's next pointer to new background TCB
				tempTCBPt->previous = &BackgroundTCBs[i];           // point next TCB's previous pointer to new background TCB	    
				if(tempTCBPt == FirstBackgroundPt){                 // background TCB will now be first in background list
				  FirstBackgroundPt = &BackgroundTCBs[i];
				}
		    break;                                              // background TCB inserted in place. leave while loop
			}
			// else, if new background TCB has a priority less than or equal to
			else{                                                 // continue on, unless at end of the list. If so tack on after tempTCBPt 
				if(tempTCBPt->next == FirstBackgroundPt){
					BackgroundTCBs[i].previous = tempTCBPt;           // create new background TCB's previous pt
					BackgroundTCBs[i].next = tempTCBPt->next;         // create new background TCB's next pt
					tempTCBPt->next->previous = &BackgroundTCBs[i];   // point next TCB's previous pointer to new background TCB
					tempTCBPt->next = &BackgroundTCBs[i];             // point previous TCB's next pointer to new background TCB
					break;                                            // background TCB inserted in place. leave while loop
				}
				tempTCBPt = tempTCBPt->next;                        // try next location in list
			}
		}
	}
	NumBGround++; // increment background thread count
  EndCritical(status);
  return 1;     // successfully added backgroundTCB
}


//******** OS_AddAperiodicThread *************** 
// Adds a background aperiodic task.  See Timer1.c for background handler
// Inputs: *task - pointer to a void/void background function
//         priority - 0 is the highest, 7 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal, OS_AddThread
// This task does not have a Thread ID
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddAperiodicThread(void(*task)(void), uint8_t priority){
	int32_t status = StartCritical();
	// not allowed to create more background threads than allocated
	if(NumBGround >= MAX_NUM_BGROUND){
		EndCritical(status);
		return 0;        
	}
	
	// if NumBground == 0, create first background thread
	else if(NumBGround == 0){
		Timer1A_Enable();                                // turn on timer1 to handle background threads
		BackgroundTCBs[0].alive = true;                  // bring TCB to life
		BackgroundTCBs[0].task = task;                   // assign TCB its task
		BackgroundTCBs[0].next = &BackgroundTCBs[0];     // cycle back to self since the only backgroundTCB alive
	  BackgroundTCBs[0].previous = &BackgroundTCBs[0]; // cycle back to self since the only backgroundTCB alive
		BackgroundTCBs[0].sleepCnt = 1;                  // assign sleepCnt to TCB so that it runs immediately in Timer1 handler
	  BackgroundTCBs[0].priority = priority;           // assign priority to TCB
		BackgroundTCBs[0].aperiodic = true;              // create an aperiodic backgroundTCB
		FirstBackgroundPt = &BackgroundTCBs[0];          // TCB[0] will run first
	}
	
	// else, create new background thread
	else{
		// find dead backgroundTCB slot to bring alive. In worstcase O(n). Maybe change to FIFO list of all availavle dead slots to choose from
		int i;                                    // i holds the new TCB's index. 
		for(i = 0; i < MAX_NUM_BGROUND; i++){     
		  if(BackgroundTCBs[i].alive == false){
				BackgroundTCBs[i].alive = true;       // bring TCB to life
				break;                          
			}
		}
		// assign new TCB its characteristics
		BackgroundTCBs[i].task = task;            // assign TCB its task
		BackgroundTCBs[i].sleepCnt = 1;           // assign sleepCnt to TCB so that it runs immediately in Timer1 handler
	  BackgroundTCBs[i].priority = priority;    // assign priority to TCB
		BackgroundTCBs[i].aperiodic = true;       // create a periodic backgroundTCB

		// put backgroundTCB into linked list based on priority. In worstcase O(n). Possibly add a seperate living list for each priority level to increase efficiency
		TCBTypeBGround *tempTCBPt = FirstBackgroundPt;          // tempTCBPt used to cycle through TCB list. Start at front of line
		while(1){
			// if new background TCB has higher priority
		  if(priority < tempTCBPt->priority){                   // put background TCB in front of TCB with less priority
				BackgroundTCBs[i].previous = tempTCBPt->previous;   // create new background TCB's previous pt
				BackgroundTCBs[i].next = tempTCBPt;                 // create new background TCB's next pt
				tempTCBPt->previous->next = &BackgroundTCBs[i];     // point previous TCB's next pointer to new background TCB
				tempTCBPt->previous = &BackgroundTCBs[i];           // point next TCB's previous pointer to new background TCB	    
				if(tempTCBPt == FirstBackgroundPt){                 // background TCB will now be first in background list
					FirstBackgroundPt = &BackgroundTCBs[i];
				}
			  break;                                              // background TCB inserted in place. leave while loop
			}
			// else, if new background TCB has a priority less than or equal to
			else{                                                 // continue on, unless at end of the list. If so tack on after tempTCBPt 
				if(tempTCBPt->next == FirstBackgroundPt){
					BackgroundTCBs[i].previous = tempTCBPt;           // create new background TCB's previous pt
					BackgroundTCBs[i].next = tempTCBPt->next;         // create new background TCB's next pt
					tempTCBPt->next->previous = &BackgroundTCBs[i];   // point next TCB's previous pointer to new background TCB
					tempTCBPt->next = &BackgroundTCBs[i];             // point previous TCB's next pointer to new background TCB
					break;                                            // background TCB inserted in place. leave while loop
				}
				tempTCBPt = tempTCBPt->next;                        // try next location in list
			}
		}
	}
  // book keeping
	NumBGround++; // increment background thread count
	EndCritical(status);
	return 1; //success
}


// ******** OS_KillAperiodic ************
// Kill the currently running aperiodic background 
// thread, and release its TCB. 
// Only to be called from Timer1A_Handler.  Timer1A_Handler should
// have interrupts disabled already, so do not need to do it here.
// Decrements NumBGround.
// input:  index - index of TCB which you would like to remove from BackgroundTCBs
// output: none
void OS_KillAperiodic(uint32_t index){
	TCBTypeBGround *nextTCBPt = FirstBackgroundPt->next;                    // save next TCB in case FirstBackgroundPt is being killed 
	BackgroundTCBs[index].previous->next = BackgroundTCBs[index].next;      // the TCB before the dying TCB should now connect to the dying TCB's next TCB
	BackgroundTCBs[index].next->previous = BackgroundTCBs[index].previous;  // the TCB after the dying TCB should have a previous TCB that is the same as the dying TCB's previous
	BackgroundTCBs[index].alive = false;                                    // kill the TCB
	if(FirstBackgroundPt == &BackgroundTCBs[index]){                        // update FirstBackgroundPt if it is the dying TCB
	  FirstBackgroundPt = nextTCBPt;	
	}
	NumBGround--;                                                           // decrement the number of background threads
  if(NumBGround == 0){  // if there are no background tasks, kill Timer1 to prevent a crash
		Timer1A_Disable();
	}
}


// ******** OS_AddButtonTask************
// Adds an aperiodic background task to a specified button
// Inputs:  *task - pointer to TCB that needs to move
//          port - port number that button is assigned to
//          priority - 0 is the highest, 7 is the lowest
// Outputs: none
void OS_AddButtonTask(void(*task)(void), uint8_t port, uint8_t priority){
	switch (port){
		case PF0: 											// Initialize SW2
			SW2_Init(task,priority);
			break;
		
		case PF4:                       // Initialize SW1
			SW1_Init(task,priority);
			break;
		
		default:                        // do nothing
			break;
  }
}


//******** OS_AddThread *************** 
// Add a foregound thread to the scheduler.  Each thread
// is assigned a ForegroundTCB slot and is assigned to 
// its priority level's active list.
// Increments Num4Ground, NumActive, and PriorityLists[#]->Value.
// Inputs: pointer to a void/void foreground task
//         number of bytes allocated for its stack
//         priority, 0 is highest, 7 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// stack size must be divisable by 8 (aligned to double word boundary)
// In Lab 3, you can ignore the stackSize fields
int OS_AddThread(void(*task)(void), unsigned long stackSize, uint8_t priority){
	int32_t status = StartCritical();
	// not allowed to create more foreground threads than allocated
	if(Num4Ground >= MAX_NUM_4GROUND){ 
		EndCritical(status);
		return 0;                // failure
	}
	
	// if Num4Ground == 0, create first foreground thread
	else if(Num4Ground == 0){
		ForegroundTCBs[0].alive = true;
    ForegroundTCBs[0].sleepCnt = 0;                    // set sleepCnt to zero for new thread
		ForegroundTCBs[0].priority = priority;             // set priority for new thread
		ForegroundTCBs[0].tempPriority = priority;         // set tempPriority for new thread
		ForegroundTCBs[0].RunCnt = 0;                      // initalize RunCnt
		ForegroundTCBs[0].LastRunCnt = 0;                  // initalize LastRunCnt
		SetInitialStack(0);                                // initialize thread's stack
		Stacks[0][STACKSIZE-2] = (int32_t)(task);          // set PC Register for user task
    PriorityLists[priority].list = &ForegroundTCBs[0];           // assign thread to its priority list
	  PriorityLists[priority].list->next = &ForegroundTCBs[0];     // new thread points to itself since only thread
		PriorityLists[priority].list->previous = &ForegroundTCBs[0]; // new thread points to itself since only thread
	  RunPt = &ForegroundTCBs[0];                                  // will run first since currently the only thread
	}
	
	// else, create new foreground thread
	else{
		// find dead foregroundTCB slot to bring alive. In worstcase O(n). Maybe change to FIFO list of all availavle dead slots to choose from
		int i;                                    // i holds the new tcb index
		for(i = 0; i < MAX_NUM_4GROUND; i++){     // find dead TCB slot to bring alive
		  if(ForegroundTCBs[i].alive == false){
				ForegroundTCBs[i].alive = true;       // bring foregroundTCB to life
				break;                          
			}
		}
		// assign new foregroundTCB its characteristics
    ForegroundTCBs[i].sleepCnt = 0;                  // set sleepCnt to zero for new thread
		ForegroundTCBs[i].priority = priority;           // set priority for new thread
		ForegroundTCBs[i].tempPriority = priority;       // set tempPriority for new thread
		ForegroundTCBs[i].RunCnt = 0;                    // initalize RunCnt
		ForegroundTCBs[i].LastRunCnt = 0;                // initalize LastRunCnt
		SetInitialStack(i);                              // initalize thread's stack
		Stacks[i][STACKSIZE-2] = (int32_t)(task);        // set PC Register for user task
		
		// put foregroundTCB into its priority's linked list
		if(PriorityLists[priority].Value == 0){                        // no threads have been added to this priority level yet
			PriorityLists[priority].list = &ForegroundTCBs[i];           // assign thread to its priority list
	    PriorityLists[priority].list->next = &ForegroundTCBs[i];     // new thread points to itself since only thread
		  PriorityLists[priority].list->previous = &ForegroundTCBs[i]; // new thread points to itself since only thread
		}
		else{                                                                  // else, add TCB into the back of its priority's linked list
		  ForegroundTCBs[i].next = PriorityLists[priority].list;               // new TCB's next pt will point to the beginning of the list
			ForegroundTCBs[i].previous = PriorityLists[priority].list->previous; // new TCB's previous pt will point to the old last TCB in list
			PriorityLists[priority].list->previous->next = &ForegroundTCBs[i];   // old last TCB's next pt now points to new TCB
			PriorityLists[priority].list->previous = &ForegroundTCBs[i];         // Priority list's previous pt now points to new TCB
	  }
	}
	// book keeping
  PriorityLists[priority].Value++;     // increment priority level's list count 
	Num4Ground++;                        // increment foreground thread count
	NumActive++;                         // increment number of active foreground threads
	EndCritical(status);
	return 1;                            // successfully added foregroundTCB	
}


// ******** OS_Kill ************
// Kill the currently running foreground thread, and 
// release its TCB and stack. 
// Decrements Num4Ground.
// input:  none
// output: none
void OS_Kill(void){
  int32_t status = StartCritical();
	if(NumActive == 1){                                         // prevent OS from crashing by adding a dummy thread
	  OS_AddThread(&Dummy,128,7); 
	}
	// remove dying TCB from PriorityList's linked list
	TCBType4Ground *savedNextTCBPt = RunPt->next;               // Save next TCB before altering it below
	RunPt->previous->next = RunPt->next;                        // TCB before the dying TCB's next pt now points to the dying TCB's next TCB
	RunPt->next->previous = RunPt->previous;                    // TCB after the dying TCB's previous pt now points to the dying TCB's previous
	if(PriorityLists[RunPt->tempPriority].list == RunPt){       // if killing the PriorityList's list pt, then update to next in list
		PriorityLists[RunPt->tempPriority].list = savedNextTCBPt;
	}
	
	// book keeping
	ForegroundTCBs[OS_Id()].alive = false;
	Num4Ground--;
	NumActive--;
	(PriorityLists[RunPt->tempPriority].Value)--;
	EndCritical(status);
	OS_Suspend();
}


// ******** OS_Wait ************
// Decrement semaphore 
// Block if less than zero
// input:  pointer to a counting semaphore
// output: none
void OS_Wait(volatile Sema4Type *semaPt){
	int32_t status = StartCritical();
  (semaPt->Value)--;                  // decrement semaphore's value
	// wait for resource
	if(semaPt->Value < 0){
		OS_Block(semaPt);                 // add TCB to that semaphore's blocked list
		EndCritical(status);
		OS_Suspend();                     // run thread switcher
	}
	// resource available, continue on
	EndCritical(status);
} 

// ******** OS_bWait ************
// Block if Value is 0
// If Value is 1, set Value to 0 and continue
// input:  pointer to a binary semaphore
// output: none
void OS_bWait(volatile Sema4Type *semaPt){
	int32_t status = StartCritical();
	// wait for resource
	if(semaPt->Value == 0){
		OS_Block(semaPt);                 // add TCB to that semaphore's blocked list
		EndCritical(status); 
		OS_Suspend();                     // run thread switcher
	}
	semaPt->Value = 0;                  // decrement semaphore's value
	// resource available, continue on
	EndCritical(status);
} 
		

// ******** OS_Signal ************
// Increment semaphore 
// Wakeup blocked thread if appropriate 
// input:  pointer to a counting semaphore
// output: none
void OS_Signal(volatile Sema4Type *semaPt){
	int32_t status = StartCritical();
	bool suspend = false;
	(semaPt->Value)++;                                    // increment semaphore's value
	if(semaPt->numBlocked > 0){             
	  suspend = OS_Unblock(semaPt);                       // remove one thread from semaphore's blocked list
	}
	EndCritical(status);
	if(suspend && (Scheduler == PRIORITY)) OS_Suspend();  // suspend thread if a priority scheduler is used and a higher priority thread has been unblocked
}


// ******** OS_bSignal ************
// Set Value to 1.  Wake up one blocked thread 
// if any are in blocked list.
// input:  pointer to a binary semaphore
// output: none
void OS_bSignal(volatile Sema4Type *semaPt){
	int32_t status = StartCritical();
	bool suspend = false;
	                                    // increment semaphore's value
	if(semaPt->numBlocked > 0){               
	  suspend = OS_Unblock(semaPt);                       // remove one thread from semaphore's blocked list
	}
	else{
		semaPt->Value = 1;
	}
	EndCritical(status);
	if(suspend && (Scheduler == PRIORITY)) OS_Suspend();  // suspend thread if a priority scheduler is used and a higher priority thread has been unblocked
}


// ******** OS_Block *********
// Add TCB to a semaphore's blocked list, and removes from active priority list. 
// Called from OS_Wait while interrupts are disabled
// Increments NumBlocked and semaPt->value. 
// Decrements NumActive and PriorityLists[RunPt->tempPriority].Value
// Inputs: pointer to semaphore you wish to add blocked thread to
// Outputs: None
void OS_Block(volatile Sema4Type *semaPt){
	if(NumActive == 1){    // prevent OS from crashing by adding a dummy thread
    OS_AddThread(&Dummy,128,7); 
  }
	
	// patch up active foreground list before removing
  TCBType4Ground *nextTCBPt = RunPt->next;                  // save next pt in list before damaging 	
	RunPt->previous->next = RunPt->next;
  RunPt->next->previous = RunPt->previous;
	if(PriorityLists[RunPt->tempPriority].list == RunPt){     // update PriorityList's list pt, if it is the same as blocked TCB
		PriorityLists[RunPt->tempPriority].list = nextTCBPt;  
	}
	
  // put into semaphore's	list based on tempPriority
	if(semaPt->numBlocked == 0){    // if true, newly blocked TCB will be the only TCB in this list
		semaPt->list = RunPt;         // semaphore now points to newly blocked TCB
		RunPt->altNext = RunPt;       // newly blocked TCB's altNext pointer now points to self
		RunPt->altPrevious = RunPt;   // newly blocked TCB's previous pointer now points to self
	}
	else{                           // else, other threads are in list. Determine where to put in list
		TCBType4Ground *tempTCBPt = semaPt->list;                   // tempTCBPt used to cycle through semaphore's TCB list. Start at front of line 
		while(1){
			if(RunPt->tempPriority < tempTCBPt->tempPriority){        // put blocked TCB in front of TCB with < tempPriority
				 RunPt->altPrevious = tempTCBPt->altPrevious;           // create newly blocked TCB's previous pt
				 RunPt->altNext = tempTCBPt;                            // create newly blocked TCB's next pt
				 tempTCBPt->altPrevious->altNext = RunPt;               // point previous TCB's next pointer to newly blocked TCB
				 tempTCBPt->altPrevious = RunPt;                        // point next TCB's previous pointer to newly blocked TCB	    
				 if(tempTCBPt == semaPt->list){                         // blocked TCB will now be first in semaphore's list
					 semaPt->list = RunPt;                               
				 }
				 break;                                                 // blocked TCB inserted in place. leave while loop
			}
			else if(RunPt->tempPriority >= tempTCBPt->tempPriority){  // continue on, unless at end of the list, if so tack on after tempTCBPt
				if(tempTCBPt->altNext == semaPt->list){                 
					RunPt->altPrevious = tempTCBPt;                       // create newly blocked TCB's previous pt
					RunPt->altNext = tempTCBPt->altNext;                  // create newly blocked TCB's next pt
					tempTCBPt->altNext->altPrevious = RunPt;              // point next TCB's previous pointer to newly blocked TCB
					tempTCBPt->altNext = RunPt;                           // point previous TCB's next pointer to newly blocked TCB
					break;                                                // blocked TCB inserted in place. leave while loop
				}				
				tempTCBPt = tempTCBPt->altNext;                         // try next location in list
			}
		}
	}
		
	// book keeping
	NumBlocked++;
	(semaPt->numBlocked)++;
	NumActive--;
	(PriorityLists[RunPt->tempPriority].Value)--;
}


// ******** OS_Unblock *********
// Remove first TCB from a semaphore's blocked list.
// Increments NumActive and PriorityLists[semaPt->list->tempPriority].Value
// Decrements NumBlocked.
// Called from OS_Signal while interrupts are disabled
// Inputs: semaPt - pointer to semaphore you wish to remove a blocked thread from
// Outputs: true - true when removed TCB has a higher tempPriority than running TCB
//          false - false when removed TCB has a lower tempPriority than running TCB
int OS_Unblock(volatile Sema4Type *semaPt){
	// if unblocked TCB has higher tempPriority that RunPt, set true.  
	// if true, will cause a context switch in OS_Signal if a priority scheduler is being used.
  bool higherPriority =(semaPt->list->tempPriority < RunPt->tempPriority) ? true : false; 
		
	// put unblocked TCB into its priority's linked list
	if(PriorityLists[semaPt->list->tempPriority].Value == 0){                  // no threads have been added to this priority level yet
	  (PriorityLists[semaPt->list->tempPriority].Value)++;
		PriorityLists[semaPt->list->tempPriority].list = semaPt->list;           // assign thread to its priority list
		PriorityLists[semaPt->list->tempPriority].list->next = semaPt->list; 	   // unblocked thread next pt points to itself since only thread in its priority list
	  PriorityLists[semaPt->list->tempPriority].list->previous = semaPt->list; // unblocked thread previosu pt points to itself since only thread in its priority list
	  
	}
	else{   // add unblocked TCB into the back of its priority's linked list		
		(PriorityLists[semaPt->list->tempPriority].Value)++;
		semaPt->list->next = PriorityLists[semaPt->list->tempPriority].list;               // unblocled TCB's next pt will point to the beginning of the list
	  semaPt->list->previous = PriorityLists[semaPt->list->tempPriority].list->previous; // unblocked TCB's previous pt will point to the old last TCB in list
		PriorityLists[semaPt->list->tempPriority].list->previous->next = semaPt->list;     // old last TCB's next pt now points to unblocked TCB
		PriorityLists[semaPt->list->tempPriority].list->previous = semaPt->list;           // Priority list's previous pt now points to unblocked TCB
	}
	
	// patch up semaphore's blocked list after removing
	TCBType4Ground *savedNextTCBPt = semaPt->list->altNext;          // Save next TCB before altering it below
	semaPt->list->altPrevious->altNext = semaPt->list->altNext;      // unblocked TCB's previous TCB needs to update its next pt
  semaPt->list->altNext->altPrevious = semaPt->list->altPrevious;  // unblocked TCB's next TCB needs to update its previous pt
	semaPt->list = savedNextTCBPt;                                   // update the semaphore's list pointer
	
	// book keeping
	NumBlocked--;
	(semaPt->numBlocked)--;
	NumActive++;
  
	return higherPriority;   // if unblocked TCB has higher tempPriority, then return true
}


// ******** OS_Sleep ************
// place this thread into a dormant state. Always puts sleeping thread 
// in front of sleeping list. Background threads cannot call this function. 
// input:  number of msec to sleep
// output: none
// You are free to select the time resolution for this function
void OS_Sleep(unsigned long sleepTime){
	int32_t status = StartCritical();
	if(NumActive == 1){      // prevent OS from crashing by adding a dummy thread
		OS_AddThread(&Dummy,128,7); 
	}
  RunPt->sleepCnt = sleepTime;                             // assign sleep value to thread
	
	// patch up active foreground list before removing
	TCBType4Ground *nextTCBPt = RunPt->next;                 // save next TCB in case need to update and waking has changed
	RunPt->previous->next = RunPt->next;
	RunPt->next->previous = RunPt->previous;
	if(PriorityLists[RunPt->tempPriority].list == RunPt){    // update PriorityList's list pt, if it is the same as sleeping TCB
		PriorityLists[RunPt->tempPriority].list = nextTCBPt;
	}
	
	// put into sleeping list using alternate pointers
	if(Sleeping.Value == 0){               // new sleepingTCB will be the only one in sleeping list
		RunPt->altNext = RunPt;              // its altNext pointer will point to self
		RunPt->altPrevious = RunPt;          // its sleepPreviosu pointer will point to self
	}
	else{                                               // put new sleepingTCB in front of the current list since order does not matter
		RunPt->altPrevious = Sleeping.list->altPrevious;  // new sleeping TCB's previous pt now points to last TCB
		RunPt->altNext = Sleeping.list;                   // new sleeping TCB's next pt now points to old first TCB
		Sleeping.list->altPrevious->altNext = RunPt;      // last TCB's next pointer needs to point to new sleeping TCB
		Sleeping.list->altPrevious = RunPt;               // old first TCB's previous pointer needs to point to new sleeping TCB
	}
	
	// book keeping
	Sleeping.list = RunPt;	                            // list now points to this TCB
	Sleeping.Value++;
	NumActive--;
	PriorityLists[RunPt->tempPriority].Value--;
	
	// perform a context switch
	EndCritical(status);
	OS_Suspend();
}


// ******** OS_Wakeup ************
// Remove TCB from sleepingTCB list, and place back in the
// active foregroundTCB list.  This will only be called 
// from the Timer3A_Handler
// input:  pointer to foregroundTCB that is waking up
// output: none
void OS_Wakeup(TCBType4Ground *waking){
	int32_t status = StartCritical();
  // patch up sleeping list before removing
	TCBType4Ground *nextTCBPt = waking->altNext;           // save next TCB in case need to update and waking has changed
	waking->altPrevious->altNext = waking->altNext;        // the waking TCB's previous TCB needs to update its next pointer
	waking->altNext->altPrevious = waking->altPrevious;    // the waking TCB's next TCB needs to update its previous pointer
	if(waking == Sleeping.list){                           // if waking up first thread in list, need to update the sleeping list pointer
		Sleeping.list = nextTCBPt;  
	}
	
	// put waking TCB into its tempPriority's linked list
	if(PriorityLists[waking->tempPriority].Value == 0){             // no threads active in this priority level 
	  PriorityLists[waking->tempPriority].list = waking;            // assign thread to its priority list
	  PriorityLists[waking->tempPriority].list->next = waking;      // assign thread to its priority list
	  PriorityLists[waking->tempPriority].list->previous = waking;  // assign thread to its priority list        
	}
	else{                                                                    // add waking TCB into the back of its priority's linked list
		waking->next = PriorityLists[waking->tempPriority].list;               // waking TCB's next pt will point to the beginning of the list
		waking->previous = PriorityLists[waking->tempPriority].list->previous; // waking TCB's previous pt will point to the old last TCB in list
		PriorityLists[waking->tempPriority].list->previous->next = waking;     // old last TCB's next pt now points to waking TCB
		PriorityLists[waking->tempPriority].list->previous = waking;           // Priority list's previous pt now points to waking TCB			   
	}
	// book keeping
	PriorityLists[waking->tempPriority].Value++;                             // increment priority level's list count
  EndCritical(status);
}


// ******** OS_Suspend ************
// suspend execution of currently running thread
// scheduler will choose another thread to execute
// Can be used to implement cooperative multitasking 
// Same function as OS_Sleep(0)
// input:  none
// output: none
void OS_Suspend(void){
	NVIC_ST_CURRENT_R = 0;          // clear the counter
  NVIC_INT_CTRL_R = 0x04000000;		// manually triggers SysTick. SysTick handler performs thread switch
}	


// ******** OS_Aging ************
// Periodically cycle through active foreground TCB and determine
// if a threads tempPriority should be promoted or demoted. OS_Aging
// is added as a periodic task during OS_Init.
// Will be called from Timer1_Handler while interrupts are disabeld.
// Inputs:  none
// Outputs: none
void OS_Aging(void){
	// cycle through all priority levels
	for(int i = 0; i<MAX_NUM_PRI; i++){                           
		if(PriorityLists[i].Value > 0){                             // if > 0, then this priority level has active threads to check
			TCBType4Ground *tempTCBPt = PriorityLists[i].list;        // tempTCBPt used to cycle through TCB list.
			// cycle through active TCBs in prioriy level
			do{                                                    
				TCBType4Ground *savedNextTCBPt = tempTCBPt->next;       // save the next location to check before losing it in OS_MoveThread					
			  // check if TCB needs to be promoted to higher tempPriority
			  if((tempTCBPt->RunCnt == tempTCBPt->LastRunCnt) && (i != 0)){  // if run count has not changed since last check, need to increase tempPriority. Priority 0 never needs to be promoted
					OS_MoveThread(tempTCBPt, true);                       // remove from current priority list and add to new list
          tempTCBPt->RunCnt = 0;                                // reset TCB's run count
					tempTCBPt->LastRunCnt = 0;                            // reset TCB's last run count
				}
				// check if TCB needs to be demoted back to original priority
				else if((tempTCBPt->RunCnt > tempTCBPt->LastRunCnt) && (tempTCBPt->tempPriority != tempTCBPt->priority)){
					OS_MoveThread(tempTCBPt, false);                      // remove from current priority list and add to new list			
				}
				// else, thread does not need to change tempPriority
				else{                                                
			    tempTCBPt->LastRunCnt = tempTCBPt->RunCnt;            // update LastRunCnt after finshed checking it
				}
				// finished with this TCB. Move to next TCB in list
				tempTCBPt = savedNextTCBPt; 				
			}while(tempTCBPt != PriorityLists[i].list);               // tempTCBPt will equal PriorityLists[i].list only when it has cycled throught the list or when there is only one active TCB in priority list (do while works for this case)
		}
	}
}


// ******** OS_MoveThread ************
// Move foreground thread from one priority list to another.
// Will update the moving thread's tempPriority.
// Will be called from OS_Aging which has interrupts disabeld.
// Inputs:  *movingTCBPt - pointer to TCB that needs to move
//          promoting - boolean that says whether a thread is being promoted or 
//                      demoted in priority(true = promoting, false = demoting)
// Outputs: none
void OS_MoveThread(TCBType4Ground *movingTCBPt, uint8_t promoting){
	// remove from old priority list and update old priority list's count
	TCBType4Ground *savedTCBPt = movingTCBPt->next;                    // save the next pt, in case Priority list needs to be updated
	movingTCBPt->previous->next = movingTCBPt->next;                   // previous TCB before movingTCB needs its next pt to point to movingTCB's next pt.
	movingTCBPt->next->previous = movingTCBPt->previous;               // TCB after movingTCB needs its previous pt to point to movingTCB's previous pt
	if(movingTCBPt == PriorityLists[movingTCBPt->tempPriority].list){  // if moving first element in list, need to update list
		PriorityLists[movingTCBPt->tempPriority].list = savedTCBPt;
	}
	PriorityLists[movingTCBPt->tempPriority].Value--;                  // decrement the number of TCBs in old priority list

	// calculate and update movingTCB's new tempPriority level
	uint32_t targetPriority;
  if(promoting){                                                 // if promoting, increase tempPriority by amount = AGING_JUMP
    targetPriority = (movingTCBPt->tempPriority-AGING_JUMP <= 0) ? (0) : (movingTCBPt->tempPriority-=AGING_JUMP);
	}
  else{                                                          // else demoting, restore back to native priority
    targetPriority = movingTCBPt->priority; 
	}		
	movingTCBPt->tempPriority = targetPriority;                    //update tempPriority 
	
	// add to back of new priority list update new priority list's count
	if(PriorityLists[targetPriority].Value == 0){                  // no threads have been added to this priority level yet
	  PriorityLists[targetPriority].list = movingTCBPt;            // assign moving thread to its priority list
	  PriorityLists[targetPriority].list->next = movingTCBPt;      // moving thread points to itself since only thread
		PriorityLists[targetPriority].list->previous = movingTCBPt;  // moving thread points to itself since only thread
	}
	else{                                                                   // add moving TCB to the back of new priority's linked list
		movingTCBPt->next = PriorityLists[targetPriority].list;               // new TCB's next pt will point to the beginning of the list
		movingTCBPt->previous = PriorityLists[targetPriority].list->previous; // moving TCB's previous pt will point to the old last TCB in new priority's list
		PriorityLists[targetPriority].list->previous->next = movingTCBPt;     // old last TCB's next pt now points to moving TCB
		PriorityLists[targetPriority].list->previous = movingTCBPt;           // Priority list's previous pt now points to moving TCB
  }
	PriorityLists[targetPriority].Value++;                                  // increase the number of TCBs in new priority list	
}


//******** OS_Id *************** 
// returns the thread ID for the currently running foreground thread
// Inputs: none
// Outputs: Thread ID, number greater than zero 
unsigned long OS_Id(void){
  return RunPt->id;
}


// ******** OS_Fifo_Init ************
// Initialize the Fifo to be empty
// Inputs: size
// Outputs: none 
// In Lab 2, you can ignore the size field
// In Lab 3, you should implement the user-defined fifo size
// In Lab 3, you can put whatever restrictions you want on size
//    e.g., 4 to 64 elements
//    e.g., must be a power of 2,4,8,16,32,64,128
void OS_Fifo_Init(unsigned long size){
  PutPt = &Fifo[0];
	GetPt = &Fifo[0];
	OS_InitSemaphore(&CurrentSize, 0);  // Nothing in fifo initially
	OS_InitSemaphore(&FifoMutex, 1);    // Fifo is available initially
}
	
	
// ******** OS_Fifo_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting.
// This function cannot enable/disable intterupts 
// since it will be called from interrupts.
// Inputs:  data
// Outputs: true - when data is properly saved,
//          false - if data not saved, because fifo was full
int OS_Fifo_Put(unsigned long data){
  volatile uint32_t *nextPutPt;
	nextPutPt = PutPt + 1;
	if(nextPutPt == &Fifo[FIFOSIZE]){
		nextPutPt = &Fifo[0];		// wrap around
	}
	if(nextPutPt == GetPt){
		return 0;  // cannot put since fifo is full. Results in lost data
	}
	else{
		*(PutPt) = data;
		PutPt = nextPutPt;
		OS_Signal(&CurrentSize);
	}
  return 1;    // data put in fifo successfully
}


// ******** OS_Fifo_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will block if empty
// Inputs:  none
// Outputs: data 
unsigned long OS_Fifo_Get(void){
	int32_t returnValue;    // holds the value poped off of fifo.
	OS_Wait(&CurrentSize);  // block for data
	OS_bWait(&FifoMutex);   // only one thread can access the fifo at any given time
	returnValue = *GetPt;   // pop element off fifo
	GetPt++;
	if(GetPt == &Fifo[FIFOSIZE]){
		GetPt = &Fifo[0];     // wrap around
	}
	OS_bSignal(&FifoMutex); 	//release the fifo
  return returnValue;
}


// ******** OS_MailBox_Init ************
// Initialize communication channel
// Inputs:  none
// Outputs: none
void OS_MailBox_Init(void){
  OS_InitSemaphore(&DataValid, 0);  // Nothing in mailbox initially
	OS_InitSemaphore(&BoxFree, 1);    // Mailbox is available initially
}
	
	
// ******** OS_MailBox_Send ************
// enter mail into the MailBox
// Inputs:  data to be sent
// Outputs: none
// This function will be called from a foreground thread
// It will block if the MailBox contains data not yet received 
void OS_MailBox_Send(unsigned long data){
  OS_bWait(&BoxFree);
	MailBoxData = data;
	OS_bSignal(&DataValid);
}
	
	
// ******** OS_MailBox_Recv ************
// remove mail from the MailBox
// Inputs:  none
// Outputs: data received
// This function will be called from a foreground thread
// It will block if the MailBox is empty 	
unsigned long OS_MailBox_Recv(void){
  OS_bWait(&DataValid);
	uint32_t returnData = MailBoxData;
	OS_bSignal(&BoxFree);
  return returnData;
}


// ******** OS_Time ************
// Return the system time. Uses Timer2A 
// Inputs:  none
// Outputs: time in 12.5ns units, 0 to 4294967295
// The time resolution should be less than or equal to 1us, and the precision 32 bits.
// It is ok to change the resolution and precision of this function as long as 
// this function and OS_TimeDifference have the same resolution and precision. 
unsigned long OS_Time(void){
  return TIMER2_TAR_R;         // Timer2A is used to return time
}


// ******** OS_TimeDifference ************
// Calculates difference between two times
// Inputs:  two times measured with OS_Time
// Outputs: time difference in 12.5ns units 
// The time resolution should be less than or equal to 1us, and the precision at least 12 bits.
// It is ok to change the resolution and precision of this function as long as 
// this function and OS_Time have the same resolution and precision 
unsigned long OS_TimeDifference(unsigned long start, unsigned long stop){
  return start-stop;           
}


// ******** OS_ClearMsTime ************
// sets the system time to zero (Timer2A)
// Inputs:  none
// Outputs: none
void OS_ClearMsTime(void){
	long sr = StartCritical();
  MsTime = 0;
	TIMER2_TAR_R = 0xFFFFFFFF;
	EndCritical(sr);
}
	
	
// ******** OS_MsTime ************
// Reads the current time (Timer2A) in ms units (assuming 80MHz clock)
// Inputs:  none
// Outputs: time in ms units since last clear
// You are free to select the time resolution for this function
// It is ok to make the resolution to match the first call to OS_AddPeriodicThread
unsigned long OS_MsTime(void){
	long sr = StartCritical();
	MsTime = (0xFFFFFFFF - TIMER2_TAR_R)/80000;
	EndCritical(sr);
  return MsTime;
}


// ******** OS_Error ************
// called when there is an error 
// preventing the system from running	
// Input: errorCode - enumeration for type of error. See OS.h for enum definitions
// Output: none
void OS_Error(int errorCode){
	DisableInterrupts();
  UART_Init();                    // initialize UART
	StreamToFile = 0;               // retarget IO stream to UART
	switch (errorCode){
		case B_PERIOD:
			printf("\n\rError in adding periodic thread");
			break;
		case EDISK_INIT:
			printf("\n\rError in initalizing disk");
			break;
	}
	while(1){}                       // do not allow to run with error
}


// ******** OS_Dummy************
// Added to prevent OS from crashing when NumActive == 0.
// This can happen when functions like OS_Sleep, OS_Block,
// or OS_Kill are called while there is only one active thread
void Dummy(void){
	uint32_t dummyCnt = 0;
	while(1){
    dummyCnt++;
//		if(NumActive > 1){
//			OS_Kill();
//		}
	}
}
