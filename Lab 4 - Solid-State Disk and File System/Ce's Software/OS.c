// os.c for Lab3
// Ce Wei
 //1st - include
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Lab4.h"
#include "PLL.h"
#include "OS.h"
#include "Interrupts.h"
#include "Timer2.h"
#include "Timer3.h"
#include "Buttons.h"
#include "UART.h"

//**************syntax of function pointer, user function********************
void (*PeriodicTask)(void);   //user function 
void (*PF4Task)(void);				//PF4 use task
void (*PF0Task)(void);				//PF0 use task
//***************************************************************************

//*******2nd - extern PUBLIC variable and functions used in this c file********
void EnableInterrupts(void);  // Enable interrupts
void DisableInterrupts(void); // Disable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void PLL_Init(uint32_t freq); 
void StartOS(void);						//in OSasm
void OS_Wait_ASM(void);				//normal spin lock semaphore
void OS_Signal_ASM(void);
void OS_bWait_ASM(void);			//binary version spin lock for LAB 2
void OS_bSignal_ASM(void);
//*****************************************************************************

//**************3rd - define constant**************
#define NUMTHREADS  10       // maximum number of threads
#define STACKSIZE   100      // number of 32-bit words in stack
#define FIFOSIZE		128       // size of the FIFOs (must be power of 2)
#define FIFOSUCCESS 1         // return value on success
#define FIFOFAIL    0         // return value on failure, create index implementation FIFO (see FIFO.h)
//*************************************************
 
//**************4th - struct, union, enum**********
struct TCB{
	int32_t *sp;       // pointer to stack (valid for threads not running
  struct TCB *next;  // linked-list pointer to next TCB block
	uint32_t sleepCount;
	uint32_t tcbIndex;
	struct TCB *previous;
	uint32_t priority;
};
typedef struct TCB TCBType;
//*************************************************

//***************5th - global variable and constants, might be public or private***********
TCBType tcbs[NUMTHREADS];		//the linked list will be more this time, for blocking semaphore
uint8_t tcbStatus[NUMTHREADS];
TCBType *RunPt;
uint32_t ThreadCounter;
static uint32_t FirstThread;
static uint32_t LastThread;
static uint32_t NextThreadPosition;
int32_t Stacks[NUMTHREADS][STACKSIZE];
static uint32_t Fifo[FIFOSIZE];
volatile uint32_t *PutPt;		//pointer to volatile data
volatile uint32_t *GetPt;
volatile uint32_t MailBoxValue;
volatile uint32_t MsTime;           // Current time(in ms) since reset
//*****************************************************************************************

//**************Global Semaphores*********************
volatile Sema4Type LCDFree;       // ST7735 binary semephore. 0=busy, 1=free
volatile Sema4Type CurrentSize;		// FIFO current size, normal semaphore
volatile Sema4Type FifoMutex;			// FIFO b semaphore
volatile Sema4Type BoxFree;				// MailBox b semaphore - free to put new value
volatile Sema4Type DataValid;			// MailBox b semaphore - Data is valid
//****************************************************


//***************6th - private functions(static), only define and prototype**
void SetInitialStack(int i);	
//*************************************************************************** 


void SetInitialStack(int i){
  tcbs[i].sp = &Stacks[i][STACKSIZE-16]; // thread stack pointer
																				 // MAKE the TCB looks like be suspend from previous states
  Stacks[i][STACKSIZE-1] = 0x01000000;   // thumb bit, PSR, last one in the array and lowest in the memory
																				 // The PC R15 will be set out of this FUNCTION
																				 // The R13 user stack will not be changed???
	Stacks[i][STACKSIZE-3] = 0x14141414;   // R14, LR
  Stacks[i][STACKSIZE-4] = 0x12121212;   // R12
  Stacks[i][STACKSIZE-5] = 0x03030303;   // R3
  Stacks[i][STACKSIZE-6] = 0x02020202;   // R2
  Stacks[i][STACKSIZE-7] = 0x01010101;   // R1
  Stacks[i][STACKSIZE-8] = 0x00000000;   // R0
  Stacks[i][STACKSIZE-9] = 0x11111111;   // R11
  Stacks[i][STACKSIZE-10] = 0x10101010;  // R10
  Stacks[i][STACKSIZE-11] = 0x09090909;  // R9
  Stacks[i][STACKSIZE-12] = 0x08080808;  // R8
  Stacks[i][STACKSIZE-13] = 0x07070707;  // R7
  Stacks[i][STACKSIZE-14] = 0x06060606;  // R6
  Stacks[i][STACKSIZE-15] = 0x05050505;  // R5
  Stacks[i][STACKSIZE-16] = 0x04040404;  // R4
}

// ******** OS_Init ************
// initialize operating system, disable interrupts until OS_Launch
// initialize OS controlled I/O: serial, ADC, systick, LaunchPad I/O and timers 
// input:  none
// output: none
//set up the systick timer, BUT not let it run in periodic mode
//disable interrupt
//OS_Launch will enable the core interrupt
void OS_Init(void){
	DisableInterrupts();								// disable interrupt during initialization
	PLL_Init(Bus80MHz);                 // set processor clock to 80 MHz
  // initialize I/O
	UART_Init();                    // initialize UART
	//systick timer
	NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
  NVIC_SYS_PRI3_R =(NVIC_SYS_PRI3_R&0x00FFFFFF)|0xE0000000; // priority 7, least priority
	ThreadCounter = 0;					//reset counter to 0, no threads at beginning
	FirstThread = 0;						//start thread index is 0, ALTHOUGH it has not been added yet
	LastThread = 0;							//default setting, means the last added thread
	NextThreadPosition = 0;			//next thread will be added to the position 0
	//initial the TCBs
	for(int i=0; i<NUMTHREADS; i++){
		tcbStatus[i] = 0;	//0 means the corresponding place is not occupied
		tcbs[i].sleepCount = 0;	//sleep counter set to 0
		tcbs[i].tcbIndex = i;	//index set to corresponding index for runPt to set the status
	}
	//For system time
	Timer2_Init(0xFFFFFFFF);    // intialize timer2 with max 32bit period. Used for OS_Time()
	//10ms period for wake up threads
	Timer3_Init(800000);		//0.01 seconds, deduct 10 ms per time 
}



// ******** OS_InitSemaphore ************
// initialize semaphore 
// input:  pointer to a semaphore
// output: none
void OS_InitSemaphore(volatile Sema4Type *semaPt, long value){
	(semaPt->Value) = value;
	(semaPt->blockingCounter) = 0;
	semaPt->first = 0;  // 0 is null pointer
}



// ******** OS_Wait ************
// decrement semaphore 
// Lab2 spinlock
// Lab3 block if less than zero
// input:  pointer to a counting semaphore
// output: none
// 4.11, P181
void OS_Wait(volatile Sema4Type *semaPt){
	//Need to make sure that the samaphore is not smaller than 0!!! For the FIFO usage
	long status;
	status = StartCritical();
	//judge if there are something could use
	if((semaPt->Value) == 0){
		//No value could be used, Need block this thread
		//SET the semaphore single linked list
		//block this thread, get out of the active circle
		if((semaPt->blockingCounter) == 0){
			//no block now, set to initial one
			semaPt->first = RunPt;
		}else{
			//add to the end of the single linked list
			TCBType *temp = semaPt->first;
			for(int i=1; i<(semaPt->blockingCounter); i++){
				temp = temp->next;
			}
			temp->next = RunPt;
		}
		//special case to handle delete the first or last node in the TCB
		if((RunPt->tcbIndex) == FirstThread){
			//block the first one in the circle, 
			FirstThread =(RunPt->next->tcbIndex);;
		}else if((RunPt->tcbIndex) == LastThread){
			//block the last one in the circle
			LastThread = (RunPt->previous->tcbIndex);
		}
		//RunPt->sp = &Stacks[RunPt->tcbIndex][STACKSIZE-16]; // thread stack pointer
		//connect previous.next to next
		//next.previous to previous
		//current will be a new TCB, and its next and previous will be set during the AddThread
		(RunPt->previous)->next = (RunPt->next);
		(RunPt->next)->previous = (RunPt->previous);
		//No decreament ThreadCounter
		//No reset next thread position activity
		//set status to 2, means block, for debug
		tcbStatus[RunPt->tcbIndex] = 2;				//set to blocked state
		//increase the semaphore blocking counter
		semaPt->blockingCounter += 1;
		//suspend the current blocked TCB
		EndCritical(status);
		OS_Suspend();						//context switch to make more efficient
	}else{
		//use one semaphore value, and continue
		semaPt->Value -=1;
		EndCritical(status);
	}
	//EnableInterrupts();
}

// ******** OS_Signal ************
// increment semaphore 
// Lab2 spinlock
// Lab3 wakeup blocked thread if appropriate 
// input:  pointer to a counting semaphore
// output: none
// 4.11, P181
void OS_Signal(volatile Sema4Type *semaPt){
	long status;
	status = StartCritical();
	//DESIGN for the FIFO use semaphore
	if(semaPt->blockingCounter > 0){
		//could unblock and have thread to block
		//priority one, choose the highest priority thread, and treat it as the unblock thread.
		TCBType *unblockTCB;
		if((semaPt->blockingCounter) == 1){
			//only one blocking thread and take it directly
			unblockTCB = semaPt->first;
			semaPt->first = 0;		//set to null pt
		}else{
			//more than one thread
			//go through the whole single linked list to find the highest priorty and reset the list edge
			unblockTCB = semaPt->first;
			int highestIndex = 0;
			int highestPriority = semaPt->first->priority;
			//go through until the last TCB in the semaphore blocking list
			TCBType *temp = semaPt->first;
			for(int i=1; i<(semaPt->blockingCounter); i++){
				temp = temp->next;
				//set the priorty and switch index if necessary
				if(temp->priority < unblockTCB->priority){
					//a higher priority TCB found
					unblockTCB = temp;
					highestPriority = temp->priority;
					highestIndex = i;				//i will between 0 and counter-1 inclusive
				}
			}
			//in more than one thread situation...
			if(highestIndex == 0){
				//the first one is highest priority
				semaPt->first = semaPt->first->next;	//set the initial
			}else if(highestIndex == semaPt->blockingCounter - 1){
				//the last one is highest priority
				temp = semaPt->first;
				for(int i=2; i<(semaPt->blockingCounter); i++){//set second last's next to null
					temp = temp->next;
				}
				temp->next = 0;		//set second last's next to null
			}else{
				//some internal one is highest priority
				temp = semaPt->first;
				//find the previous of the chosen TCB
				for(int i=1; i<highestIndex; i++){//highest will between 1 and counter-2 inclusive after trim the counter-1
					//i should between 0 and counter-3 inclusive, second last
					temp = temp->next;
				}
				temp->next = unblockTCB->next;
			}
		}
		//add the unblocked TCB back to the active circle
		//depends on the priority to choose the different position and whether call OS_Suspend
		if(unblockTCB->priority >= RunPt->priority){
			//unblock is lower, add to the last position of the list
			tcbs[LastThread].next = &tcbs[unblockTCB->tcbIndex]; // break old cycle, last point to next. it should original pointed to first
			tcbs[unblockTCB->tcbIndex].previous = &tcbs[LastThread];
			tcbs[unblockTCB->tcbIndex].next = &tcbs[FirstThread]; // next point to the initial
			tcbs[FirstThread].previous = &tcbs[unblockTCB->tcbIndex];
			LastThread = (unblockTCB->tcbIndex);
			//set back to active status 1
			tcbStatus[unblockTCB->tcbIndex] = 1;				//unBlock the thread
			//decrement the block counter
			semaPt->blockingCounter -= 1;		
		}else{
			//unblock is higher priority, add to the next position of the RunPt and call OS_Suspend
			TCBType *temp = RunPt->next;
			RunPt->next = unblockTCB;
			unblockTCB->previous = RunPt;
			unblockTCB->next = temp;
			temp->previous = unblockTCB;
			if(RunPt->tcbIndex == LastThread){
				LastThread = unblockTCB->tcbIndex;
			}
			//set back to active status 1
			tcbStatus[unblockTCB->tcbIndex] = 1;				//unBlock the thread
			//decrement the block counter
			semaPt->blockingCounter -= 1;
			EndCritical(status);
			OS_Suspend();
		}
	}else{
		//no blocking now
		semaPt->Value +=1;
	}
	EndCritical(status);
}


// ******** OS_bWait ************
// Lab2 spinlock, set to 0
// Lab3 block if less than zero
// input:  pointer to a binary semaphore
// output: none
void OS_bWait(volatile Sema4Type *semaPt){
	long status;
	status = StartCritical();
	//1st, detect if the signal is available
	if((semaPt->Value) == 0){
		//Need block
		//SET the semaphore single linked list
		if((semaPt->blockingCounter) == 0){
			//no block now, set to initial one
			semaPt->first = RunPt;
		}else{
			//add to the end of the single linked list
			TCBType *temp = semaPt->first;
			for(int i=1; i<(semaPt->blockingCounter); i++){
				temp = temp->next;
			}
			temp->next = RunPt;
		}
		//special case to handle delete the first or last node in the TCB
		if((RunPt->tcbIndex) == FirstThread){
			//block the first one in the circle, 
			FirstThread =(RunPt->next->tcbIndex);;
		}else if((RunPt->tcbIndex) == LastThread){
			//block the last one in the circle
			LastThread = (RunPt->previous->tcbIndex);
		}
		//RunPt->sp = &Stacks[RunPt->tcbIndex][STACKSIZE-16]; // thread stack pointer
		//connect previous.next to next
		//next.previous to previous
		//current will be a new TCB, and its next and previous will be set during the AddThread
		(RunPt->previous)->next = (RunPt->next);
		(RunPt->next)->previous = (RunPt->previous);
		//No decreament ThreadCounter
		//No reset next thread position activity
		//set status to 2, means block, for debug
		tcbStatus[RunPt->tcbIndex] = 2;				//set to blocked state
		//increase the semaphore
		semaPt->blockingCounter += 1;
		//suspend the current blocked TCB
		EndCritical(status);
		OS_Suspend();						//context switch to make more efficient
	}else{
		//value is 1, just use it and set it back to 0, not block it
		(semaPt->Value) = 0;
		EndCritical(status);
	}
}

// ******** OS_bSignal ************
// Lab2 spinlock, set to 1
// Lab3 wakeup blocked thread if appropriate 
// input:  pointer to a binary semaphore
// output: none
void OS_bSignal(volatile Sema4Type *semaPt){
	long status;
	status = StartCritical();
	//1st, judge if some is blocking
	if(semaPt->blockingCounter > 0){
		//could unblock and have thread to block
		//priority one, choose the highest priority thread, and treat it as the unblock thread.
		TCBType *unblockTCB;
		if((semaPt->blockingCounter) == 1){
			//only one blocking thread and take it directly
			unblockTCB = semaPt->first;
			semaPt->first = 0;		//set to null pt
		}else{
			//more than one thread
			//go through the whole single linked list to find the highest priorty and reset the list edge
			unblockTCB = semaPt->first;
			int highestIndex = 0;
			int highestPriority = semaPt->first->priority;
			//go through until the last TCB in the semaphore blocking list
			TCBType *temp = semaPt->first;
			for(int i=1; i<(semaPt->blockingCounter); i++){
				temp = temp->next;
				//set the priorty and switch index if necessary
				if(temp->priority < unblockTCB->priority){
					//a higher priority TCB found
					unblockTCB = temp;
					highestPriority = temp->priority;
					highestIndex = i;				//i will between 0 and counter-1 inclusive
				}
			}
			//in more than one thread situation...
			if(highestIndex == 0){
				//the first one is highest priority
				semaPt->first = semaPt->first->next;	//set the initial
			}else if(highestIndex == semaPt->blockingCounter - 1){
				//the last one is highest priority
				temp = semaPt->first;
				for(int i=2; i<(semaPt->blockingCounter); i++){//set second last's next to null
					temp = temp->next;
				}
				temp->next = 0;		//set second last's next to null
			}else{
				//some internal one is highest priority
				temp = semaPt->first;
				//find the previous of the chosen TCB
				for(int i=1; i<highestIndex; i++){//highest will between 1 and counter-2 inclusive after trim the counter-1
					//i should between 0 and counter-3 inclusive, second last
					temp = temp->next;
				}
				temp->next = unblockTCB->next;
			}
		}
		//add the unblocked TCB back to the active circle
		//depends on the priority to choose the different position and whether call OS_Suspend
		if(unblockTCB->priority >= RunPt->priority){
			//unblock is lower, add to the last position of the list
			tcbs[LastThread].next = &tcbs[unblockTCB->tcbIndex]; // break old cycle, last point to next. it should original pointed to first
			tcbs[unblockTCB->tcbIndex].previous = &tcbs[LastThread];
			tcbs[unblockTCB->tcbIndex].next = &tcbs[FirstThread]; // next point to the initial
			tcbs[FirstThread].previous = &tcbs[unblockTCB->tcbIndex];
			LastThread = (unblockTCB->tcbIndex);
			//set back to active status 1
			tcbStatus[unblockTCB->tcbIndex] = 1;				//unBlock the thread
			//decrement the block counter
			semaPt->blockingCounter -= 1;		
		}else{
			//unblock is higher priority, add to the next position of the RunPt and call OS_Suspend
			TCBType *temp = RunPt->next;
			RunPt->next = unblockTCB;
			unblockTCB->previous = RunPt;
			unblockTCB->next = temp;
			temp->previous = unblockTCB;
			if(RunPt->tcbIndex == LastThread){
				LastThread = unblockTCB->tcbIndex;
			}
			//set back to active status 1
			tcbStatus[unblockTCB->tcbIndex] = 1;				//unBlock the thread
			//decrement the block counter
			semaPt->blockingCounter -= 1;
			EndCritical(status);
			OS_Suspend();
		}
	}else{
		//no blocking thread, set flag to 1 for future use
		(semaPt->Value) = 1;
	}
	EndCritical(status);
}


//******** OS_AddThread *************** 
// add a FOREgound thread to the scheduler
// Inputs: pointer to a void/void foreground task
//         number of BYTES allocated for its stack
//         priority, 0 is highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// STACK SIZE must be divisable by 8 (aligned to double word boundary)
// In Lab 2, you can IGNORE both the stackSize and priority fields
// In Lab 3, you can IGNORE the stackSize fields
int OS_AddThread(void(*task)(void), 
   unsigned long stackSize, unsigned long priority){ 
	int32_t status;
	status = StartCritical();
	// add the threads to the TCB arrays
	if(ThreadCounter == 0){
		//no threads now
		tcbs[0].priority = priority;
		//put the thread into the first place in the TCB array
		tcbs[0].next = &tcbs[0]; // 0 points to 0, self cycle
		//set previous of tcb[0]
		tcbs[0].previous = &tcbs[0];	//0's previous is also 0
		SetInitialStack(0); Stacks[0][STACKSIZE-2] = (int32_t)(task); // initial for thread 0 and its PC
		tcbs[0].sleepCount = 0;	//set sleepCount not sleep
		//set tcb[0] to be occupied now
		tcbStatus[0] = 1;				
		RunPt = &tcbs[0];       // thread 0 will run first, IMPORTANT here!!!
		ThreadCounter++;				//should be 1
		NextThreadPosition++;		//should be 1, next will be added to the index 1
														//FirstThread still be 0, it only changed when it is removed
														//LastThread still be 0, 
		EndCritical(status);
		return 1;               // successful
	}else{
		//there are some threads now
		//choose the correct place
		//link them
		if(ThreadCounter >= NUMTHREADS){
			//TOO much threads than the max possible value
			EndCritical(status);
			return 0;
		}
		//still could hold, should add the new one in the cycle, break the cycle first(last point to first cycle)
	  tcbs[LastThread].next = &tcbs[NextThreadPosition]; // break old cycle, last point to next. it should original pointed to first
		tcbs[NextThreadPosition].previous = &tcbs[LastThread];
		tcbs[NextThreadPosition].next = &tcbs[FirstThread]; // next point to the initial
		tcbs[FirstThread].previous = &tcbs[NextThreadPosition];
		//set next one's stack and sleep counter
		SetInitialStack(NextThreadPosition); Stacks[NextThreadPosition][STACKSIZE-2] = (int32_t)(task); // initial for thread x and its PC
		tcbs[NextThreadPosition].sleepCount = 0;	//set sleepCount not sleep
		tcbs[NextThreadPosition].priority = priority;
		ThreadCounter++;				//increament the counter of threads
		//set thread to be occupied
		tcbStatus[NextThreadPosition] = 1;
		//set the last added thread to the new added one
		LastThread = NextThreadPosition;
		//the next TCB position need to be handled, choose the smallest blank place in the TCB array
		if(ThreadCounter == NUMTHREADS){
			NextThreadPosition = NUMTHREADS;	//ERROR, should give some signal
		}else{
			//choose the smallest blank place in the TCB array
			int i;
			for(i=0; i<NUMTHREADS; i++){
				if(tcbStatus[i] == 0){
					//choose the smallest possible one, if all occupied, the kill might release
					NextThreadPosition = i;
					break;
				}
			}
		}
														//FirstThread still be 0, it only changed when the first one is removed
		EndCritical(status);
		return 1;               // successful		
	}
}




//******** OS_Id *************** 
// returns the thread ID for the currently running thread
// Inputs: none
// Outputs: Thread ID, number greater than zero 
unsigned long OS_Id(void){
	return (RunPt->tcbIndex);
}


//******** OS_AddPeriodicThread *************** 
// add a BACKground periodic task
// typically this function receives the highest priority
// Inputs: pointer to a void/void BACKGROUND function
//         period given in system time units (12.5ns)--80Mhz frequency
//         priority 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// You are free to select the time resolution for this function
// It is assumed that the user task will run to completion and return
// This task CAN NOT spin, block, loop, sleep, or kill
// This task CAN call OS_Signal  OS_bSignal	 OS_AddThread
// This task does NOT have a Thread ID
// In lab 2, this command will be called 0 or 1 times
// In lab 2, the priority field can be ignored
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be UP TO FOUR background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddPeriodicThread(void(*task)(void), 
   unsigned long period, unsigned long priority){
	//long sr = StartCritical();
	 //initialized the timer 1A and pass the function pointer to the initialization
  SYSCTL_RCGCTIMER_R |= 0x02;   // 0) activate TIMER1
//set the function pointer to the parameter function pointer
  PeriodicTask = task;          // user function
  TIMER1_CTL_R = 0x00000000;    // 1) disable TIMER1A during setup
  TIMER1_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER1_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER1_TAILR_R = period-1;    // 4) reload value
  TIMER1_TAPR_R = 0;            // 5) bus clock resolution
  TIMER1_ICR_R = 0x00000001;    // 6) clear TIMER1A timeout flag
  TIMER1_IMR_R = 0x00000001;    // 7) arm timeout interrupt
	NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|(priority << 13); // 8) priority 1
  //NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|0x0000E000; // 8) priority 7
// interrupts enabled in the main program after all devices initialized
// vector number 37, interrupt number 21
  NVIC_EN0_R = 1<<21;           // 9) enable IRQ 21 in NVIC  (TIMER 1A)
  TIMER1_CTL_R = 0x00000001;    // 10) enable TIMER1A	 
	return 1;
}
// The TIMER1A handler for the background task
// call signal here???????????????????????????????????????
void Timer1A_Handler(void){
	TIMER1_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER1A timeout
	(*PeriodicTask)();                // execute user task
}
// ***************** Timer1A_Enable ******************
void Timer1A_Enable(void){
	NVIC_EN0_R = 1<<21;                // enable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000001;         // enable TIMER1A
}
// ***************** Timer1A_Disable ******************
void Timer1A_Disable(void){
	NVIC_DIS0_R = 1<<21;               // disable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000000;         // disable TIMER1A
}

//for OS_Sleep
void Timer3A_Handler(void){
	//For OS_Sleep
	//acknowledge
	TIMER3_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER3A timeout
	//Go through the current counter
	TCBType *temp = RunPt;
	//decrement the counter of sleep
	for(int i=0; i<ThreadCounter; i++){
		if((temp->sleepCount) > 0){
			temp->sleepCount = temp->sleepCount - 10;
		}
		temp = temp->next;
	}
}




//******** OS_AddSW1Task *************** 
//PF4 button task, negative logic!!!
// add a background task to run whenever the SW1 (PF4) button is pushed
// Inputs: pointer to a void/void BACKground function
//         priority 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal  OS_bSignal	 OS_AddThread
// This task does not have a Thread ID
// In labs 2 and 3, this command will be called 0 or 1 times
// In lab 2, the priority field can be ignored
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddSW1Task(void(*task)(void), unsigned long priority){
	volatile uint32_t delay;
	SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F
	delay = SYSCTL_RCGCGPIO_R;        // allow time for clock to start
	PF4Task = task;								// b) SET function pointer
	GPIO_PORTF_LOCK_R = 0x4C4F434B;   // 2) unlock GPIO Port F
  GPIO_PORTF_CR_R |= 0x10;           // allow changes to PF4
  GPIO_PORTF_DIR_R &= ~0x10;    // (c) make PF4 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x10;  //     disable alt funct on PF4
  GPIO_PORTF_DEN_R |= 0x10;     //     enable digital I/O on PF4   
  GPIO_PORTF_PCTL_R = 0x00000000; // configure PF4 as GPIO??????????????
  GPIO_PORTF_AMSEL_R = 0;       //     disable analog functionality on PF
  GPIO_PORTF_PUR_R |= 0x10;     //     enable weak pull-up on PF4, normal 3.3, when pressed is 0, and falling edge event
  GPIO_PORTF_IS_R &= ~0x10;     // (d) PF4 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~0x10;    //     PF4 is not both edges
  GPIO_PORTF_IEV_R &= ~0x10;    //     PF4 falling edge event
  GPIO_PORTF_ICR_R = 0x10;      // (e) clear flag4
  GPIO_PORTF_IM_R |= 0x10;      // (f) arm interrupt on PF4 *** No IME bit as mentioned in Book ***
	//don't care about the priority in LAB 2
	NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|(priority << 21); // priority 2
  //NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|0x00A00000; // (g) priority 5 hard code, bit 23-21
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
	return 1;	//in lab 2 always success
}
//******** OS_AddSW2Task *************** 
//LAB 3 ONLY
// add a background task to run whenever the SW2 (PF0) button is pushed
// Inputs: pointer to a void/void background function
//         priority 0 is highest, 5 is lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed user task will run to completion and return
// This task can not spin block loop sleep or kill
// This task can call issue OS_Signal, it can call OS_AddThread
// This task does not have a Thread ID
// In lab 2, this function can be ignored
// In lab 3, this command will be called will be called 0 or 1 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddSW2Task(void(*task)(void), unsigned long priority){
	volatile uint32_t delay;
	SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F
	delay = SYSCTL_RCGCGPIO_R;        // allow time for clock to start
	PF0Task = task;								// b) SET function pointer
	GPIO_PORTF_LOCK_R = 0x4C4F434B;   // 2) unlock GPIO Port F
  GPIO_PORTF_CR_R |= 0x01;           // allow changes to PF0
  GPIO_PORTF_DIR_R &= ~0x01;    // (c) make PF0 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x01;  //     disable alt funct on PF0
  GPIO_PORTF_DEN_R |= 0x01;     //     digital input enable
  GPIO_PORTF_PCTL_R = 0x00000000; // configure PF0 as GPIO(SPECIAL HERE!!!)
  GPIO_PORTF_AMSEL_R = 0;       //     disable analog functionality on PF
  GPIO_PORTF_PUR_R |= 0x01;     //    make pull-up with PF0, normal 3.3, when pressed is 0, and falling edge event
  GPIO_PORTF_IS_R &= ~0x01;     // (d) PF0 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~0x01;    //     PF0 is not both edges
  GPIO_PORTF_IEV_R &= ~0x01;    //     PF0 falling edge event
  GPIO_PORTF_ICR_R = 0x01;      // (e) clear flag0
  GPIO_PORTF_IM_R |= 0x01;      // (f) arm interrupt on PF0 *** No IME bit as mentioned in Book ***
	//don't care about the priority in LAB 2
	NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|(priority << 21); // priority 2
  //NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|0x00A00000; // (g) priority 5 hard code, bit 23-21
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
	return 1;	//in lab3 this will be called by 0 or 1 time.
}
// The GPIO handler for the background task
void GPIOPortF_Handler(void){
//both buttons are only falling edge, the task itself handle the debounce, just make sure disarm the interrupt on PF4 at beginning
	//judge which one is pressed(what about both)???
	/*
	GPIO_PORTF_ICR_R = 0x10;      // acknowledge flag4
	(*PF4Task)(); 								// it should be button task
	*/
		if(GPIO_PORTF_RIS_R & 0x01){  // poll PF0(SW2)
    GPIO_PORTF_ICR_R = 0x01;    // acknowledge flag0
		(*PF0Task)();
	  //OS_AddAperiodicThread(SW2Task, SW2Priority);
	}
	if(GPIO_PORTF_RIS_R & 0x10){  // poll PF4(SW1)
    GPIO_PORTF_ICR_R = 0x10;    // acknowledge flag4
		(*PF4Task)();
	  //OS_AddAperiodicThread(SW1Task, SW1Priority);
	}
}


// ******** OS_Sleep ************
// place this thread into a dormant state
// input:  number of msec to sleep --- # of 0.001 seconds to sleep
// output: none
// You are free to select the time resolution for this function
// OS_Sleep(0) implements cooperative multitasking
void OS_Sleep(unsigned long sleepTime){
//set the sleepCount in the TCB of the thread
//Then call the OS_Suspend
	(RunPt->sleepCount) = sleepTime;	//sleep 0 is also in this case, just cooperative, then next time it is active
	OS_Suspend();
}

// ******** OS_Kill ************
// kill the currently running thread, release its TCB and stack
// input:  none
// output: none
//1:release stack		--- set tcbStatus
//2:release TCB 		--- set tcbStatus
//3:decrease the threaCounter
//4:context switch by OS_Suspend
void OS_Kill(void){int32_t status;
	int i;
	status = StartCritical();
//release TCB, stack, and remove from the circle linked list
	//change to double linkedlist to help connect TCB
	if(ThreadCounter <= 1){
		//could not kill if thread # is less or equals to 1
		return;
	}
	//set magic to runPt, then context switch
	tcbStatus[RunPt->tcbIndex] = 0;				//release the stack and TCB
	//special case to handle delete the first or last node in the TCB
	if((RunPt->tcbIndex) == FirstThread){
		//kill the first one in the circle, 
		FirstThread =(RunPt->next->tcbIndex);;
	}else if((RunPt->tcbIndex) == LastThread){
		//kill the last one in the circle
		LastThread = (RunPt->previous->tcbIndex);
	}
	//RunPt->sp = &Stacks[RunPt->tcbIndex][STACKSIZE-16]; // thread stack pointer
	//connect previous.next to next
	//next.previous to previous
	//current will be a new TCB, and its next and previous will be set during the AddThread
	(RunPt->previous)->next = (RunPt->next);
	(RunPt->next)->previous = (RunPt->previous);
	//decrement the thread counter, might be less than 10
	ThreadCounter--;
	//reset next thread positionand call OS_Suspend must be done anyway
	//set the NextThreadPosition to the smallest possible one TCB
	for(i=0; i<NUMTHREADS; i++){
		if(tcbStatus[i] == 0){
			NextThreadPosition = i;
			break;
		}
	}
	EndCritical(status);
	OS_Suspend();
}


// ******** OS_Suspend ************
// suspend execution of currently running thread
// scheduler will CHOOSE NEXT THREAD to execute
// Can be used to implement cooperative multitasking 
// SAME FUNCTION as OS_Sleep(0)
// input:  none
// output: none
//trigger the systick handler but using the INT register
void OS_Suspend(void){
	NVIC_ST_CURRENT_R = 0;					//clear counter, also clear the COUNT bit in CTRL R, NOT trigger
	NVIC_INT_CTRL_R = 0x04000000;		//trigger SysTick by using the INT
}



// ******** OS_Fifo_Init ************
// Initialize the Fifo to be empty
// Inputs: size
// Outputs: none 
// In Lab 2, you can ignore the size field
// In Lab 3, you should implement the user-defined fifo size
// In Lab 3, you can put whatever restrictions you want on size
//    e.g., 4 to 64 elements
//    e.g., must be a power of 2,4,8,16,32,64,128
void OS_Fifo_Init(unsigned long size){
	//long sr;
  //sr = StartCritical();
  PutPt = &Fifo[0];
	GetPt = &Fifo[0];
	OS_InitSemaphore(&CurrentSize, 0);
	OS_InitSemaphore(&FifoMutex, 1);
	//EndCritical(sr);
}

// ******** OS_Fifo_Put ************
// Enter one data sample into the Fifo
// Called from the background, so NO waiting 
// Inputs:  data
// Outputs: true if data is properly saved,
//          false if data not saved, because it was FULL
// Since this is called by interrupt handlers 
//  this function can not disable or enable interrupts, will not be blocked because it is background
int OS_Fifo_Put(unsigned long data){
	volatile uint32_t *nextPutPt;
	nextPutPt = PutPt + 1;
	//try increment nextPutPt to see if any error happen
	if(nextPutPt == &Fifo[FIFOSIZE]){
		nextPutPt = &Fifo[0];		//swap
	}
	if(nextPutPt == GetPt){
		return(FIFOFAIL);
	}else{
		*(PutPt) = data;
		PutPt = nextPutPt;
		OS_Signal(&CurrentSize);
		return(FIFOSUCCESS);			//Success is 1
	}
}


// ******** OS_Fifo_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will spin/block if empty
// Inputs:  none
// Outputs: data 
unsigned long OS_Fifo_Get(void){
	uint32_t returnValue;
	// consumer might be blocked when the ADC is not available,
	// the take off and put back should work because the ADC result,
	// but when press button get some big trouble
	OS_Wait(&CurrentSize);			//see if I can get something
	//make sure only one fore thread could access it
	OS_bWait(&FifoMutex);
	returnValue = *GetPt;
	GetPt++;
	if(GetPt == &Fifo[FIFOSIZE]){
		GetPt = &Fifo[0];
	}
	//release the mutex
	OS_bSignal(&FifoMutex);
	return returnValue;
}


// ******** OS_Fifo_Size ************
// Check the status of the Fifo
// Inputs: none
// Outputs: returns the number of elements in the Fifo
//          greater than zero if a call to OS_Fifo_Get will return right away
//          zero or less than zero if the Fifo is empty 
//          zero or less than zero if a call to OS_Fifo_Get will spin or block
long OS_Fifo_Size(void){
//not use fifo size, use the semaphore CurrentSize
	return CurrentSize.Value;
}


//NEED semaphore version of mailbox???
// ******** OS_MailBox_Init ************
// Initialize communication channel
// Inputs:  none
// Outputs: none
void OS_MailBox_Init(void){
	OS_InitSemaphore(&BoxFree, 1);
	OS_InitSemaphore(&DataValid, 0);
	MailBoxValue = 0;
}


// ******** OS_MailBox_Send ************
// enter mail into the MailBox
// Inputs:  data to be sent
// Outputs: none
// This function will be called from a foreground thread
// It will spin/block if the MailBox contains data not yet received 
void OS_MailBox_Send(unsigned long data){
	OS_bWait(&BoxFree);
	MailBoxValue = data;
	OS_bSignal(&DataValid);
}


// ******** OS_MailBox_Recv ************
// remove mail from the MailBox
// Inputs:  none
// Outputs: data received
// This function will be called from a foreground thread
// It will spin/block if the MailBox is empty 
unsigned long OS_MailBox_Recv(void){
	OS_bWait(&DataValid);
	unsigned long returnValue = MailBoxValue;
	OS_bSignal(&BoxFree);
	return returnValue;
}


// ******** OS_Time ************
//80000 is 0.001 s
// return the system time 
// Inputs:  none
// Outputs: time in 12.5ns units, 0 to 4294967295
// The time resolution should be less than or equal to 1us, and the precision 32 bits
// It is ok to change the resolution and precision of this function as long as 
//   this function and OS_TimeDifference have the same resolution and precision 
unsigned long OS_Time(void){
	return TIMER2_TAR_R;         // Timer2A is used to return time, TAILR is the reload value
}



// ******** OS_TimeDifference ************
// Calculates difference between two times
// Inputs:  two times measured with OS_Time
// Outputs: time difference in 12.5ns units 
// The time resolution should be less than or equal to 1us, and the precision at least 12 bits
// It is ok to change the resolution and precision of this function as long as 
//   this function and OS_Time have the same resolution and precision 
//should always return a positive number, some time the stop might be bigger due to the reload, HANDLE it
unsigned long OS_TimeDifference(unsigned long start, unsigned long stop){
	return (start - stop);			//start should be big, the Timer will count down to stop
}


//The periodic function should increase the global counter???
// ******** OS_ClearMsTime ************
// sets the system time to zero (from Lab 1)
// Inputs:  none
// Outputs: none
// You are free to change how this works
void OS_ClearMsTime(void){
	long sr = StartCritical();
	MsTime = 0;
	EndCritical(sr);
}

//simply return the global counter???
// ******** OS_MsTime ************
// reads the current time in msec (from Lab 1)
// Inputs:  none
// Outputs: time in ms units ---- want time in ms units!!!
// You are free to select the time resolution for this function
// It is ok to make the resolution to match the first call to OS_AddPeriodicThread
//return the global 32-bit counter(system time)
//53687 to 0
//this function will only be called for debounce, when added a new thread, 
unsigned long OS_MsTime(void){
	long sr = StartCritical();
	MsTime = (0xFFFFFFFF - TIMER2_TAR_R)/80000;
	EndCritical(sr);
  return MsTime;
}

//***NEW added self function***
unsigned long OS_MsTimeDifference(unsigned long MsStart, unsigned long MsStop){
	if(MsStart > MsStop){
		return (MsStart - MsStop);			//start should be big, the Timer will count down to stop
	}else{
		// the timer was round back
		return (0xFFFFFFFF/80000 - MsStop + MsStart);
	}
}
	
//******** OS_Launch *************** 
// start the SCHEDULER, ENABLE interrupts
// Inputs: number of 12.5ns clock cycles for each time slice
//         you may select the units of this parameter
// Outputs: none (does not return)
// In Lab 2, you can ignore the theTimeSlice field
// In Lab 3, you should implement the user-defined TimeSlice field
// It is ok to limit the range of theTimeSlice to match the 24-bit SysTick
void OS_Launch(unsigned long theTimeSlice){
	NVIC_ST_RELOAD_R = theTimeSlice - 1; // reload value for systick interrupt, 2ms
  NVIC_ST_CTRL_R = 0x00000007; // enable, core clock and interrupt arm
  StartOS();                   // start on the first task, push things on main stack???
}


//************************TO DO******************
// OS_AddSW2Task --- Lab 3




