;/*****************************************************************************/
; OSasm.s: low-level OS commands, written in assembly                       */
; Runs on LM4F120/TM4C123
; A very simple real time operating system with minimal features.
; Daniel Valvano
; January 29, 2015
; Brandon Boesch
; Ce Wei Januaray 29, 2016
;
; This example accompanies the book
;  "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
;  ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015
;
;  Programs 4.4 through 4.12, section 4.2
;
;Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
;    You may use, edit, run or distribute this file
;    as long as the above copyright notice remains
; THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
; OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
; VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
; OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
; For more information about my classes, my research, and my books, see
; http://users.ece.utexas.edu/~valvano/
; */

        AREA |.text|, CODE, READONLY, ALIGN=2
        THUMB
        REQUIRE8
        PRESERVE8

        EXTERN  RunPt            ; currently running thread, it is a pointer!!!
		EXTERN	OtherTrigger	 ; OS_Suspend, OS_Sleep, OS_Kill will set it to 1
		EXTERN	ThreadCounter	 ; use it when OtherTrigger is NOT set to go through the circle to decrease the sleepCounter
        EXPORT  StartOS
        EXPORT  SysTick_Handler
		EXPORT	OS_Wait_ASM		 ; semaphore
		EXPORT	OS_Signal_ASM
		EXPORT	OS_bWait_ASM	 ; binary semaphore
		EXPORT	OS_bSignal_ASM

;The systick handler just do the normal context switch or jump the sleep threads
; GO through the whole TCBs to 
SysTick_Handler                ; 1) Saves R0-R3,R12,LR,PC,PSR ------> C will handle it? ---- handler mode VS thread mode
    CPSID   I                  ; 2) Prevent interrupt during switch
    PUSH    {R4-R11}           ; 3) Save remaining regs r4-11
							   ;The sp will be stored in the TCB.... 
    LDR     R0, =RunPt         ; 4) R0=pointer to RunPt, old thread
    LDR     R1, [R0]           ;    R1 = RunPt, the actual TCB address
    STR     SP, [R1]           ; 5) Save SP into TCB(pointed by the RunPt)
; skip the sleeping threads, must not all are sleeping!!!!!
Sleeping
	LDR		R1, [R1, #4]	   ; 6) sleep, continue to find next possible runnable TCB, R1 is the pointer's value(address)
	LDR		R2, [R1, #8]	   ;see if next's sleepCounter is not zero
	CMP		R2, #0			   ;
	BNE		Sleeping		   ;
							   ; come out when runnable TCB
    STR     R1, [R0]           ;    RunPt = R1
							   ;	set the stack pointer to the sp of next TCB
    LDR     SP, [R1]           ; 7) new thread SP; SP = RunPt->sp;
    POP     {R4-R11}           ; 8) restore regs r4-11
    CPSIE   I                  ; 9) tasks run with interrupts enabled
    BX      LR                 ; 10) restore R0-R3,R12,LR,PC,PSR ------> the C will handle it? --- handler mode to thread mode


StartOS
    LDR     R0, =RunPt         ; currently running thread
    LDR     R2, [R0]           ; R2 = value of RunPt
    LDR     SP, [R2]           ; new thread SP; SP = RunPt->stackPointer;
    POP     {R4-R11}           ; restore regs r4-11
    POP     {R0-R3}            ; restore regs r0-3
    POP     {R12}			   ; restore reg r12, make sure r0-r3 and r12 will not effect stack
    POP     {LR}               ; discard LR from initial stack
    POP     {LR}               ; start location, PC
    POP     {R1}               ; discard PSR
    CPSIE   I                  ; Enable interrupts at processor level
	; thread mode, works fine............
    BX      LR                 ; start first thread, IF BX will restore some wrong things??? we have already pop r0-r3,r12 and lr things


OS_Wait_ASM
	LDREX	R1, [R0]		;Counter
	SUBS	R1, #1			;Counter - 1
	ITT		PL				;OK if >= 0
	STREXPL	R2, R1, [R0]	;try update
	CMPPL	R2, #0			;succeed
	BNE		OS_Wait_ASM		;no, try again
	BX		LR				
	
	
OS_Signal_ASM
	LDREX	R1, [R0]		;Counter
	ADD		R1, #1			;Counter + 1
	STREX	R2, R1, [R0]	;try update
	CMP		R2, #0			;succeed
	BNE		OS_Signal_ASM	;try again
	BX		LR
	
OS_bWait_ASM	
	LDREX	R1, [R0]		;Counter
	SUBS	R1, #1			;Counter - 1, counter will be no bigger than 1 according to signal
	ITT		PL				;OK if >= 0, 
	STREXPL	R2, R1, [R0]	;try update
	CMPPL	R2, #0			;succeed?
	BNE		OS_bWait_ASM		;no, try again
	BX		LR	


OS_bSignal_ASM
	LDREX	R1, [R0]		;Counter
	MOV		R1, #1			;Counter = 1
	STREX	R2, R1, [R0]	;try update
	CMP		R2, #0			;succeed?
	BNE		OS_bSignal_ASM	;try again
	BX		LR


    ALIGN
    END
