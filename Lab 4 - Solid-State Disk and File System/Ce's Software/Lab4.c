// Lab4.c
// Runs on TM4C123
// Real Time Operating System for Lab 4
// Brandon Boesch
// Ce Wei
// March 13th, 2016

// Jonathan W. Valvano 1/31/14, valvano@mail.utexas.edu
// EE445M/EE380L.6 
// You may use, edit, run or distribute this file 
// You are free to change the syntax/organization of this file

// ************ ST7735's microSD card interface*******************
// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground
// **************************************************************


//************ Timer Resources *******************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - OS_AddPeriodicThread(), Os_AddPeriodicThread() ; priority = 0
//  Timer2A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//  Timer3A - OS_Sleep() ; priority = 3
//  Timer5A - eDisk.c ; priority = 2
//************************************************


#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab4.h"
#include "OS.h"
#include "eFile.h"
#include "ADC.h"
#include "eDisk.h"
#include "UART.h"
#include "Profiler.h"
#include "Interpreter.h"


//******************** Globals ***********************************************
unsigned long NumCreated;   // number of foreground threads created
unsigned long NumSamples;   // incremented every sample
unsigned long DataLost;     // data sent by Producer, but not received by Consumer
int Running;                // true while robot is running
unsigned long Idlecount=0;  // counts how many times IdleTask() is ran
//**************************************************************************

#define TIMESLICE 2*TIME_1MS  // thread switch time in system time units

//******** Robot *************** 
// foreground thread, accepts data from producer
// inputs:  none
// outputs: none
void Robot(void){   
unsigned long data;        // ADC sample(12bit), 0 to 4096
unsigned long voltage;     // in mV,             0 to 3000
unsigned long startTime;   // in 1msec,          0 to 10000 
unsigned long currentTime; // in 1msec,          0 to 10000 	
unsigned long t=0;  
	DataLost = 0;          // new run with no lost data 
	printf("\n\rRobot running...");
	eFile_RedirectToFile("robot");
	printf("time(sec)\tdata(volts)\n\r");
	OS_ClearMsTime(); 
	startTime = OS_MsTime();       // 1ms resolution
	do{
		t++;
		currentTime = OS_MsTime();   // 1ms resolution
		data = OS_Fifo_Get();        // 1000 Hz sampling get from producer
		voltage = (3000*data)/4096;  // in mV. 12 bit ADC, so divide by 4096
		printf("%u.%03u\t%0u.%03u\n\r",currentTime/1000,currentTime%1000,voltage/1000,voltage%1000);
	}
	while(OS_TimeDifference(currentTime,startTime) < 2000); // change this to mean 10 seconds
	if(streamToFile()){              // if disk became full during robot, the file will already be closed from Retarget.c
		eFile_EndRedirectToFile();
	}
	printf("done.\n\n\r");
	printf("\n\r--Enter Command--> ");
	Running = 0;                // robot no longer running
	OS_Kill();
}


void DiskFullErrorPrint(void){
  printf("\n\rCannot run robot, Disk is full.\n\n\r");
  Running = 0; 
	OS_Kill();
}


//************ButtonPush*************
// Called when Select Button pushed
// background threads execute once and return
void ButtonPush(void){
  if(Running==0){
    Running = 1;  // prevents you from starting two robot threads
    NumCreated += OS_AddThread(&Robot,128,1);  // start a 10 second run
	}
}

//******** ButtonSPICommand **************
// use eDisk to read one block. 
// inputs:  none
// outputs: none
void ButtonSPICommand(void){  
	bool errorFound = false;
  printf("\n\r* SPI Command & Response----");
	
	// write dummy data to local block
	for(int i = 0; i < 512; i++){
		blockBuffer[i] = 'a'+i%26;                           
	}
	
	PF1 ^= 0x02; 
	// write block
	if(eDisk_WriteBlock(blockBuffer,3000)){
		if(!errorFound) printf("failed\n\r");
	  printf("    -eDisk_WriteBlock failure.\n\r");
	  errorFound = true;
	}	
	PF1 ^= 0x02; 
	// read block 
  
	if(eDisk_ReadBlock(blockBuffer,3000)){
		if(!errorFound) printf("failed\n\r");
	  printf("    -eDisk_ReadBlock failure.\n\r");
	  errorFound = true;
	}
	PF1 ^= 0x02; 	   
 
	// finished eDisk read/write
	if(errorFound == false){
	  printf("finished\n\r");
	}
	printf("\n\r--Enter Command--> ");
	Running = 0;
	OS_Kill();
}


//************DownPush*************
// Called when Down Button pushed
// background threads execute once and return
void DownPush(void){
  if(Running==0){
    Running = 1;  // prevents you from starting two threads
    OS_AddThread(&ButtonSPICommand,128,1);  // start a 10 second run, read One block
	}
}


//******** Producer *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 1 kHz, started by your ADC_Collect
// The timer triggers the ADC, creating the 1 kHz sampling
// Your ADC ISR runs when ADC data is ready
// Your ADC ISR calls this function with a 10-bit sample 
// sends data to the Robot, runs periodically at 1 kHz
// inputs:  none
// outputs: none
void Producer(unsigned long data){  
  if(Running){
    if(OS_Fifo_Put(data)){     // send to Robot
      NumSamples++;
    } else{ 
      DataLost++;
    } 
  }
}


//******** IdleTask  *************** 
// foreground thread, runs when no other work needed
// never blocks, never sleeps, never dies
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
    Idlecount++;        // debugging 
  }
}


//******** Interpreter **************
// foreground thread, accepts input from serial port, outputs to serial port
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		if(!streamToFile()){
		  Interpreter_Parse();
		}
	}
}


//******************* Lab4 MAIN *******************************************
#ifdef MAIN
int main(void){  
  // initalize globals	
  Running = 0;                           // robot not running
  DataLost = 0;                          // lost data between producer and consumer
  NumSamples = 0;
	
  // initalize OS	
	OS_Init();                             // initialize OS, disable interrupts
  // initialize communication channels 
	OS_MailBox_Init();
  OS_Fifo_Init(512);              // *note* 4 is not big enough
  ADC_Collect(0, 1000, &Producer); // start ADC sampling, channel 0, 1000 Hz
	//ADC_InitHWTrigger(0, 1000, &Producer); // start ADC sampling, channel 0(PE3), 1000 Hz

  // attach background tasks
  OS_AddSW2Task(&ButtonPush, 2);	// PF0
  OS_AddSW1Task(&DownPush, 3);		// PF4

  NumCreated = 0 ;
	// create initial foreground threads
  NumCreated += OS_AddThread(&Interpreter, 128, 2); 
  NumCreated += OS_AddThread(&IdleTask, 128, 7);       // runs when nothing useful to do
	
	// File system and profiler
 	eFile_Init();                          // initialize file system
	PortF_Init();                          // initialize Port F profiling
  OS_Launch(TIMESLICE);                  // doesn't return, interrupts enabled in here
  return 0;                              // this never executes
}
#endif  
//******************* end MAIN **********************************************


