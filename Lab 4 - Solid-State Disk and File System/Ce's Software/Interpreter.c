// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// March 20th, 2016

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "Interpreter.h"
#include "UART.h"
#include "Lab4.h"
#include "eFile.h"
#include <ctype.h>

// *************Global Variables*****************************

cmdTable Commands[] = {        // CMD_LEN defines max command name length
	{"help",     cmdHelp,        "Displays all available commands."},
	{"dir",      cmdDirectory,   "Displays the directory of files on disk."},
	{"format",   cmdFormat,      "Formats the disk, and erases all files."},
	{"create",   cmdCreate,      "Creates a new blank file."},
	{"read",     cmdRead,        "Reads the contents of a file."},
	{"write",    cmdWrite,       "Writes data to a file."},
	{"delete",   cmdDelete,      "Deletes a file from disk."},
	{"test",     cmdTest,        "Run FileSystem testbench."},
	{"demo",     cmdDemo,        "Write a variable number of blocks to disk"},
};

char cmd[CMD_LEN+1];   // string to store command line inputs. +1 for null.
// *********************************************************************


void Interpreter_Init(void){
	printf("\n\n\r******************************************************\n\r");
	printf("                  Welcome to CeOS.\n\r");
	printf("         Type \"help\" for a list of commands.\n\r");
	printf("******************************************************\n\r");
}


//---------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void){
	printf("\n\r--Enter Command--> ");
	UART_InStringNL(cmd, CMD_LEN);  
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(cmd,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
	printf("Command not recognized.\n\r");
}

	
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
	printf("Below is a list of availble commands:\n\r");
	// output formating and display command name
	for(int i = 0; i < NUM_OF(Commands); i++){
		if(i+1 <10) printf("    ");                   
		else if(i+1 < 100) printf("   ");     
		printf("%d",i+1);                             
		printf(") ");                         
		// display command name
		printf("%s",(char*)Commands[i].name);  
    // output formating		
		for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
		  printf("-");                             
		}
		// display command tag
    printf("%s\n\r",(char*)Commands[i].tag);     
	}
}


//------------cmdDirectory-----------------
// Displays the directory of files on disk
void cmdDirectory(void){
	eFile_Directory(&UART_OutChar);
}


//------------cmdFormat-----------------
// Formats the disk, and erases all files.
void cmdFormat(void){
	eFile_Format();
	printf("Formating of disk complete.\n\r"); 
}


//------------cmdCreate-----------------
// Creates a new blank file.
void cmdCreate(void){
	printf("Enter new file's name (6 characters max): ");
	char fileName[MAX_FILE_NAME];            // string to store command line inputs.
	UART_InStringNL(fileName, MAX_FILE_NAME); 
	int result = eFile_Create(fileName);
	if(!result){
		printf("New file created.\n\r"); 
	}
}


//------------cmdRead-----------------
// Reads the contents of a file.
void cmdRead(void){
  printf("Enter name of file to read (6 characters max): ");
	char fileName[MAX_FILE_NAME];            // string to store command line inputs.
	UART_InStringNL(fileName, MAX_FILE_NAME); //get the file name that wants to read
	
	// open file for reading and read entirety of file
	int result = eFile_ROpen(fileName);
	if(!result){
		//have this file
		printf("\n\r-----------------\n\r");                             // add divider;                                                  
		char data;                                                       // stores read data
		while(!eFile_ReadNext(&data)){                                  
			printf("%c",data);                                             // output read result

		}
		eFile_RClose();                                                  // close file for reading
	  printf("\n\r-----------------\n\n\r");                           // add divider
		printf("End of file.\n\r"); 
  }
}


//------------cmdWrite-----------------
// Writes data to a file.
void cmdWrite(void){
  printf("Enter name of file to write to (6 characters max): ");
	char fileName[MAX_FILE_NAME];            // string to store command line inputs.
	UART_InStringNL(fileName, MAX_FILE_NAME); 
	
  // open file for reading and read entirety of file
	int result = eFile_ROpen(fileName);
	if(!result){
		printf("\n\r");                                                  // print newline
		printf("-----------------\n\r");                                 // add divider
		char data;                                                       // stores read data
		while(eFile_ReadNext(&data)){                                         // read next byte                                     
		  printf("%c",data);                                             // output read result
		}
		eFile_RClose();                                                  // close file for reading
	  printf("\n\r-----------------\n\n\r");                           // add divider
		printf("File printed above. Add new text and press enter when complete:\n\r ");
		
		// redirect IO stream	to Disk
		result = eFile_RedirectToFile(fileName);
	  if(!result){
			char writeData[BLOCK_SIZE];                                   // string to store command line inputs.
			for(int i = 0; i < BLOCK_SIZE; i++){
				writeData[i] = 0;
			}
			UART_InStringNL(writeData, BLOCK_SIZE);                       // command line input
			printf("%s", writeData);                                     // write data to disk
			eFile_EndRedirectToFile();                                   // redirect IO stream to UART
		  printf("\n\rWrite complete.\n\r"); 
		}
	} 
}


//------------cmdDelete-----------------
// Deletes a file from disk.
void cmdDelete(void){
	printf("Enter name of file to delete (6 characters max): ");
	char fileName[MAX_FILE_NAME];            // string to store command line inputs.
	UART_InStringNL(fileName, MAX_FILE_NAME); 
	int result = eFile_Delete(fileName);
	if(!result){
		printf("File deleted.\n\r"); 
	}
}


//------------cmdTest-----------------
// Run FileSystem testbench.
void cmdTest(void){
	printf("\n\n\r******************************************************\n\r");
	printf("                 eFile Testing Begins\n\r");
	printf("******************************************************\n\r");
	
	// begin testbench
	/*
	BandwidthTest();
	FormatTest();
	CreateTest();
	WriteTest();
	ReadTest();
	DeleteTest();
	RedirectToFileTest();
	*/
	printf("                No cmdTest\n\r");
	printf("                No cmdTest\n\r");
	printf("                No cmdTest\n\r");
	//***************
	printf("\n\r******************************************************\n\r");
	printf("                eFile Testing Complete\n\r");
	printf("******************************************************\n\r");
}


//------------cmdDemo-----------------
// Create a demo file and write a variable number of Blocks.  
// Used for demo purposes.
void cmdDemo(void){
	int result;                                             // stores error rresults from using eFile functions
  printf("Choose the number of blocks to write (1-3): ");
	uint32_t num = UART_InUDec();
	switch (num){
	  case(1): 
			printf("\n\rWriting 1 block to disk");
		  	result = eFile_Create("demo");
		    result = eFile_WOpen("demo");
		    for(int i = 0; i < BLOCK_SIZE; i++){
		      eFile_Write('1');
				}
		  break;
		case(2): 
			printf("\n\rWriting 2 blocks to disk");
		  	result = eFile_Create("demo");
		    result = eFile_WOpen("demo");
		    for(int i = 0; i < BLOCK_SIZE; i++){
		      eFile_Write('1');
				}
			  for(int i = 0; i < BLOCK_SIZE; i++){
		      eFile_Write('2');
				}
			break;
		case(3): 
			printf("\n\rWriting 3 blocks to disk");
		  	result = eFile_Create("demo");
		    result = eFile_WOpen("demo");
		    for(int i = 0; i < BLOCK_SIZE; i++){
		      eFile_Write('1');
				}
			  for(int i = 0; i < BLOCK_SIZE; i++){
		      eFile_Write('2');
				}
				for(int i = 0; i < BLOCK_SIZE; i++){
		      eFile_Write('3');
				}
			break;
		default: 
			printf("Improper selection. Must choose 1, 2, or 3."); 
		  return;
	}		
	eFile_WClose();
	if(!result){
		printf("\n\rDemo writing complete.\n\r"); 
	}
	else{
		printf("Error in creating or writing to files");
	}
}
