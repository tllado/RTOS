// Interpreter.h
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// March 20th, 2016

#define CMD_LEN 10                           // maximum number of letters allowed for command names.
#define NUM_OF(x) (sizeof(x)/sizeof(x[0]))   // counts the # of available commands.


// ************* Stucts ****************************************************
typedef struct{
  const char *name;
  void (*func)(void);
	const char *tag;
}cmdTable;
// **************************************************************************


//------------Interpreter_Init-------
// Initialize interpreter
void Interpreter_Init(void);

//------------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void);

//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void);

//------------cmdDirectory-----------------
// Displays the directory of files on disk
void cmdDirectory(void);

//------------cmdFormat-----------------
// Formats the disk, and erases all files.
void cmdFormat(void);

//------------cmdCreate-----------------
// Creates a new blank file.
void cmdCreate(void);

//------------cmdRead-----------------
// Reads the contents of a file.
void cmdRead(void);

//------------cmdWrite-----------------
// Writes data to a file.
void cmdWrite(void);

//------------cmdDelete-----------------
// Deletes a file from disk.
void cmdDelete(void);

//------------cmdTest-----------------
// Run FileSystem testbench.
void cmdTest(void);

//------------cmdDemo-----------------
// Create a demo file and write a variable number of Blocks.  
// Used for demo purposes.
void cmdDemo(void);

