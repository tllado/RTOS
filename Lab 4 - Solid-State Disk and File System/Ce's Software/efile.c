// efile.c
// Runs on TM4C123
// Lab4 for RTOS, logic level to physical level
// Brandon Boesch, Ce Wei
// March 8, 2016
// Middle-level routines to implement a solid-state disk 

//*****************READ_ME*******************
// Use 256 blocks for this program
//
// Block 0 is reserved for root directory
// Block 255 is reserved for Allocation table
//
// Allocation table will contain address of next block
//
// Special cases: 
//	0xFFFF indicates end of file
//	0x00FF to 0xFFFE reserved; not to be used
//	0x0000 indicates unallocated space
//	0x0001 to 0x00FE store address of next block
//******************************************

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "edisk.h"
#include "efile.h"


//***** UART/FILE *****
int StreamToFile=0; // 0=UART, 1=stream to file

//***** Logic Level Buffers Variables *****
//typedef unsigned char	BYTE;
BYTE blockBuffer[BLOCK_SIZE];					//The buffer for one data block in logic level, initialization
BYTE directoryBuffer[BLOCK_SIZE];			//The buffer for the directory in logic level
BYTE fileBuffer[BLOCK_SIZE];					//The buffer for the file usage, one block size
//***** Logic Level Variables For Read Write Usage *****
int fileDirectoryIndex;								//directory index of file CURRENTLY open
int directoryCacheStatus;							//1 = directory buffer is current, 0 = needs to be reloaded
int fileBlockIndex;										//the block Index for the file in the physical level
int fileStatus;												//0 = a file is not open, 1 = a file is open
int writeStatus;											//0 = read mode; 1 = write mode;
int readIndex;												//The position that is going to be read
//***** the struct for the directoryEntryStruct *****
struct directoryEntryStruct{
	BYTE fileName;
	BYTE startIndex;
	BYTE size;
};
typedef struct directoryEntryStruct dEntry_t;
//***** the data block arrangement *****
// index 0 is the next block for this file, is NULL if it is free
// index 1 is current data size used in this block 
// index 2 is current data size used in this block
// other positions are for the real data
//**************************************

//************** getFreeBlock ***************
//Returns 0 if NO free block available
//or the index of a free block block returned is NOT initialized
int getFreeBlock(void){
	int blockIndex;
	//read directory
	if(eDisk_ReadBlock(directoryBuffer, 0)){
		//Error occurred
		return 1;
	}
	blockIndex = directoryBuffer[FREE_BLOCK_INDEX];
	if(blockIndex == 0){
		//no free blocks
		return 0;
	}
	//Update free block linked list
	if(eDisk_ReadBlock(blockBuffer, blockIndex)){
		//Error occurred
		return 1;
	}
	//next free block is now first element of linked list
	directoryBuffer[FREE_BLOCK_INDEX] = blockBuffer[0];

	if(eDisk_WriteBlock(directoryBuffer, 0)){
		//Error occurred
		return 1;
	}
	return blockIndex;
}

//************* eFile_distError **************
//turn off the LED to indicate the error
void eFile_diskError(char* errtype, unsigned long n){
  PF2 = 0x00;      // turn LED off to indicate error
  while(1){};
}

//---------- streamToFile-----------------
//Returns 1 if Stream To File variable is set, 0 otherwise
int streamToFile(void){
	return StreamToFile;

}

// ***************.h public functions************
// ***************.h public functions************
// ***************.h public functions************
//---------- eFile_Init-----------------
// Activate the file system, WITHOUT formating
// Input: none
// Output: 0 if successful and 1 on failure (already initialized)
// since this program initializes the disk, it must run with 
//    the disk periodic task operating
// 		initialize file system
int eFile_Init(void){
	DSTATUS result;
  // simple test of eDisk
  result = eDisk_Init(0);  // initialize disk
  if(result) eFile_diskError("eDisk_Init",result);
	return result;
}

//---------- eFile_Format-----------------
// Erase all files, create blank directory, initialize free space manager
// Input: none
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash)
//		 erase disk, add format
int eFile_Format(void){
	int directoryPtr;
	int blockIndex;
	BYTE writeByte;
	dEntry_t directoryEntry;			
	//initialization of the temp Entry, will be used for all of files
	directoryEntry.fileName = 0;
	directoryEntry.startIndex = 0;
	directoryEntry.size = 0;
	//****************create a blank directory
	//All files in directory are set to free
	for(directoryPtr = 0; directoryPtr < BLOCK_SIZE - 1; directoryPtr+=DIRECTORY_FILE_SIZE){
		directoryBuffer[directoryPtr] = directoryEntry.fileName;
		directoryBuffer[directoryPtr+1] = directoryEntry.startIndex;
		directoryBuffer[directoryPtr+2] = directoryEntry.size;
	}
	//****************initialize free space manager
	//LAST byte of directory contains starting INDEX of linked list of free blocks
	directoryBuffer[FREE_BLOCK_INDEX] = 1;	//All DATA SECTORS are set to free so the starting index is block 1
	//Write directory; directory is in SECTION 0!!!
	if(eDisk_WriteBlock(directoryBuffer, 0)){
		//Error occured
		return 1;
	}
	//****************erase all files
	//Directory is in sector 0; Data Blocks start at sector 1; LAST one is the free manager
	for(blockIndex = 1; blockIndex < MAX_BLOCK_INDEX - 1; blockIndex++){
		//initialize all blocks to free
		//first byte in block is index of next block
		writeByte = blockIndex + 1;	//form free data blocks linked list --- the first BYTE of a block is the index of the next free block
		blockBuffer[0] = writeByte;
		if(eDisk_WriteBlock(blockBuffer, blockIndex)){
			//Error occured
			return 1;
		}
	}
	//LAST free block has a NULL pointer as its next block pointer
	blockIndex++;
	writeByte = 0;
	blockBuffer[0] = writeByte;	//last block point to nothing
	if(eDisk_WriteBlock(blockBuffer, blockIndex)){
		//Error occured
		return 1;
	}
	return 0; //Formatting operation was successful
}


//---------- eFile_Create-----------------
// Create a new, empty file with ONE allocated block
// Input: file name is an ASCII string up to seven characters 
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash)
// 		create new file, make it empty 
int eFile_Create( char name[]){
	//name[] is the file name that will be created
	int directoryPtr;
	BYTE blockIndex;
	dEntry_t directoryEntry;
	//check if the current logic level directory buffer is the real one from the SD card
	if(directoryCacheStatus == 0){
		//directoryBuffer is not current, need to be reloaded from the physical level SD card
		if(eDisk_ReadBlock(directoryBuffer, 0)){
			//Error occured
			return 1;
		}
	}
	//Build directory entry
	directoryEntry.fileName = name[0];	//set the parameter to fileName
	directoryEntry.size = 1;

	//find free block to allocate to file, directyoryBuffer[511](last one is the )
	blockIndex = directoryBuffer[FREE_BLOCK_INDEX];	//first time the blockIndex is 1
	if(!(blockIndex == NULL)){
		//A block is available; use it
		directoryEntry.startIndex = blockIndex;
	}

	//Search if file of SAME name already exists
	for(directoryPtr = 0; directoryPtr < BLOCK_SIZE - 1; directoryPtr += DIRECTORY_FILE_SIZE){
		if(directoryBuffer[directoryPtr] == name[0]){
			//Error: file already exists
			return 1;
		}
	}

	//Search for empty directory file from very beginning
	for(directoryPtr = 0; directoryPtr < BLOCK_SIZE - 1; directoryPtr += DIRECTORY_FILE_SIZE){
		//NULL file name means file is empty; file name is first byte
		if(directoryBuffer[directoryPtr] == 0){
			//File is empty and could be use
			directoryBuffer[directoryPtr] = directoryEntry.fileName;
			directoryBuffer[directoryPtr+1] = directoryEntry.startIndex;
			directoryBuffer[directoryPtr+2] = directoryEntry.size;
			//***********
			//Free block will be used; Update free block linked list
			//read the free one into blockBuffer
			if(eDisk_ReadBlock(blockBuffer, blockIndex)){
				//Error occurred
				return 1;
			}
			//first available free block is now next free block in linked list
			//UPDATE position 511 in the directory buffer
			directoryBuffer[FREE_BLOCK_INDEX] = blockBuffer[0];
			//***********
			//update allocated block
			//it is not free block anymore so that position 0 should be reset to 0 (NULL)
			blockBuffer[0] = NULL;	//only 1 block for new file
			blockBuffer[1] = blockBuffer[2] = NULL; //file size is 0
			//***********
			//Write blocks to disk, physical level
			if(eDisk_WriteBlock(directoryBuffer, 0) | eDisk_WriteBlock(blockBuffer, blockIndex)){
				return 1;
			}
			return 0;	//File created
		}
	}
	//the for loop fail and NO empty place
	return 1;
}


//---------- eFile_WOpen-----------------
// Open the file, read into RAM LAST block---- READ last block because the last block is the place that could be write
// Input: file name is a single ASCII letter
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash)
//      OPEN a file For WRITING 
int eFile_WOpen(char name[]){
	int directoryPtr;
	int nextIndex;
	dEntry_t directoryEntry;
	//***********
	//check file open status
	if(fileStatus){
		//Error: A file is already open, fileStatus is originally 0
		return 1;
	}
	//Update file status
	fileStatus = 1;	//File open
	writeStatus = 1;	//Write mode, open for write
	//***********
	//Load directory into RAM
	if(eDisk_ReadBlock(directoryBuffer, 0)){
		//Error occured
		return 1;
	}
	//***********
	//Search for file
	for(directoryPtr = 0; directoryPtr < BLOCK_SIZE - 1; directoryPtr += DIRECTORY_FILE_SIZE){
		if(directoryBuffer[directoryPtr] == name[0]){
			//names are the same; file found
			directoryEntry.fileName = directoryBuffer[directoryPtr];
			directoryEntry.startIndex = directoryBuffer[directoryPtr+1];
			directoryEntry.size = directoryBuffer[directoryPtr+2];
			fileDirectoryIndex = directoryPtr;
		}
	}
	//Entry buffer is originally NULL
	if(directoryEntry.fileName == NULL){
		//file was never found
		return 1;
	}
	//***********
	nextIndex = directoryEntry.startIndex;
	//find LAST block of file
	while(nextIndex != NULL){
		//First byte of file is index to next block
		if(eDisk_ReadBlock(blockBuffer, nextIndex)){
			//Error occurred
			return 1;
		}
		//Byte will be NULL if it's the last block of linked list
		if(blockBuffer[0] == NULL){
			break;
		}
		nextIndex = blockBuffer[0];	//continue searching for the last block of the file
	}
	//set the fileBlockIndex to the last block index of the file
	fileBlockIndex = nextIndex;
	//***********
	//Load last block of file into RAM
	if(eDisk_ReadBlock(fileBuffer, fileBlockIndex)){
		//error occurred
		return 1;
	}
	//file was opened successfully
	return 0;
}


//---------- eFile_Write-----------------
// save at end of the open file--- save the data into at the open file
// Input: data to be saved
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash)
int eFile_Write( char data){
	BYTE writeByte;
	int freeBlockIndex;
	int blockSize;
	int dataIndex;
	//***********
	//check that a file is open and in write mode
	if(!(fileStatus & writeStatus)){
		//Error: No file open or file open in read mode
		return 1;
	}
	//read from SDC
	if(eDisk_ReadBlock(fileBuffer, fileBlockIndex)){
		//Error occurred
		return 1;
	}
	//***********
	//the Byte that is going to be written to the SDC
	writeByte = data;
	//Get size of block
	//Size of block is in 2nd (high byte in index 1) and 3rd (low byte in index 2) of data block
	//blockSize is the current used place in this block for data
	blockSize = (((int) fileBuffer[1]) << 8) + (fileBuffer[2]);
	//set write index, 3 is for the next, the size high byte, the size low byte information
	dataIndex = blockSize + 3;
	//check if the current block is FULL
	if(dataIndex == (BLOCK_SIZE)){
		//FULL and need the get a new block for the writting
		freeBlockIndex = getFreeBlock();
		if(freeBlockIndex == 0){
			//file system full, cannot write
			return 1;
		}
		//file Buffer position 0 is the next block for this file
		fileBuffer[0] = freeBlockIndex;
		//Write modified block back
		if(eDisk_WriteBlock(fileBuffer, fileBlockIndex)){
			//Error occurred
			return 1;
		}
		//Load free block for the writting, for consistency always use fileBlockIndex for the readBlock function
		fileBlockIndex = freeBlockIndex;
		if(eDisk_ReadBlock(fileBuffer, fileBlockIndex)){
			//Error occurred
			return 1;
		}
		//Initialize new block
		fileBuffer[0] = NULL;	// not free and it is the last block for the file
		fileBuffer[1] = fileBuffer[2] = 0;
		dataIndex = 3;
		blockSize = 0;
		//Increase size of file in directory
		if(eDisk_ReadBlock(directoryBuffer, 0)){
			//error occurred
			return 1;
		}
		directoryBuffer[fileDirectoryIndex + 2] += 1;
		if(eDisk_WriteBlock(directoryBuffer, 0)){
			//Error occurred
			return 1;
		}
	}
	//***********
	//write data to fileBuffer
	fileBuffer[dataIndex] = writeByte;	//write data
	blockSize++;
	//Update blockSize for the block
	fileBuffer[1] = (blockSize & 0xFF00) >> 8;
	fileBuffer[2] = blockSize & 0x00FF;
	//***********
	//write blockBuffer to disk
	if(eDisk_WriteBlock(fileBuffer, fileBlockIndex)){
		//Error occurred
		return 1;
	}
	//Data successfully written to file
	return 0;
}


//---------- eFile_Close-----------------
// Deactivate the file system
// Input: none
// Output: 0 if successful and 1 on failure (not currently open)
int eFile_Close(void){
	if(fileStatus){
		//A file is open; close it
		fileStatus = 0;
	}else{
		//Error: no file open
		return 1;
	}
	return 0;
}


//---------- eFile_WClose-----------------
// close the file, left disk in a state that power can be removed
// Input: none
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash)
//	 close the file for writing
int eFile_WClose(void){
	//check that a file is open and in write mode
	if(!(fileStatus & writeStatus)){
		//Error: No file open or file open in read mode
		return 1;
	}
	fileStatus = 0;
	writeStatus = 0;
	return 0; //File successfuly closed
}


//---------- eFile_ROpen------------------
// Open the file, READ FIRST block into RAM 
// Input: file name is a single ASCII letter
// Output: 0 if successful and 1 on failure (e.g., trouble read to flash)
//		open a file for reading
int eFile_ROpen( char name[]){
	int directoryPtr;
	dEntry_t directoryEntry;
	//***********
	if(fileStatus){
		//Error: A file is already open
		return 1;
	}
	//Update file status
	fileStatus = 1;	//File open
	writeStatus = 0;	//read mode
	//***********
	//Load directory into RAM
	if(eDisk_ReadBlock(directoryBuffer, 0)){
		//Error occured
		return 1;
	}
	//Search for file
	for(directoryPtr = 0; directoryPtr < BLOCK_SIZE - 1; directoryPtr += DIRECTORY_FILE_SIZE){
		if(directoryBuffer[directoryPtr] == name[0]){
			//names are the same; file found
			directoryEntry.fileName = directoryBuffer[directoryPtr];
			directoryEntry.startIndex = directoryBuffer[directoryPtr+1];
			directoryEntry.size = directoryBuffer[directoryPtr+2];
			fileDirectoryIndex = directoryPtr;
		}
	}
	//nothing found
	if(directoryEntry.fileName == NULL){
		//file was never found
		return 1;
	}
	fileBlockIndex = directoryEntry.startIndex;
	//Load FIRST block of file into RAM
	if(eDisk_ReadBlock(fileBuffer, fileBlockIndex)){
		//error occurred
		return 1;
	}
	//set readIndex to 0, global
	readIndex = 0;
	//file was opened successfully
	return 0;
}
   

//---------- eFile_ReadNext-----------------
// retreive data from open file
// Input: none
// Output: return by reference data
//         0 if successful and 1 on failure (e.g., end of file)
//		get next byte 
int eFile_ReadNext( char *pt){
	int blockSize;
	//**********
	//Get block size
	blockSize = (((int)fileBuffer[1]) << 8) + fileBuffer[2];
	//check if the readIndex is too big and need to go to next block
	if(readIndex < blockSize){
		//still in the current block
		*pt = fileBuffer[readIndex + 3];	//first 3 bytes of block are block parameters
		readIndex++;
	}else{
		//need to go to the next block
		if(fileBuffer[0]){
			//still have next block
			//Go to next block
			fileBlockIndex = fileBuffer[0];	//always use this variable to interface with physical
			if(eDisk_ReadBlock(fileBuffer, fileBlockIndex)){
				//Error occurred
				return 1;
			}
			//Read next byte
			if(fileBuffer[2]){
				//Block not empty
				readIndex = 1;	//Set read index for next read call
				*pt = fileBuffer[readIndex + 2];	//read first data byte of block
			}else{
				//End of file
				return 1;
			}
		}else{
			//no next block could go, and readNext fail
			//End of file
			return 1;
		}
	}

	return 0; //data successfully read
}


//---------- eFile_RClose-----------------
// close the reading file
// Input: none
// Output: 0 if successful and 1 on failure (e.g., wasn't open)
//		close the file for writing
int eFile_RClose(void){
	//check that a file is open and in write mode
	if(!(fileStatus & !writeStatus)){
		//Error: No file open or file open in read mode
		return 1;
	}
	fileStatus = 0;
	writeStatus = 0;

	return 0; //File successfuly closed
}


//---------- eFile_Directory-----------------
// Display the directory with filenames and sizes
// Input: pointer to a function that outputs ASCII characters to display
// Output: characters returned by reference
//         0 if successful and 1 on failure (e.g., trouble reading from flash)
int eFile_Directory(void(*fp)(char)){
	// input is the pointer to the UART display???
	// show the directry to the display...
	int directoryPtr;
	//char buffer[10];
	//char *data;
	if(eDisk_ReadBlock(directoryBuffer, 0)){
			//Error occured
			return 1;
		}
	//**********
	//display all of the data in directory with the function
	for(directoryPtr = 0; directoryPtr < BLOCK_SIZE - 1; directoryPtr+=DIRECTORY_FILE_SIZE){
		if (directoryBuffer[directoryPtr]){ // if the file isn't null
			fp(directoryBuffer[directoryPtr]); // name
																					//start Index
			fp(' ');
			//sprintf(buffer, "%d", directoryBuffer[directoryPtr+2]);	//size, how many blocks
			fp(directoryBuffer[directoryPtr+2]); // size
			fp(' ');
			fp('b');
			fp('l');
			fp('o');
			fp('c');
			fp('k');
			fp('s');
			fp('\n');
			fp('\r');
			//data = buffer;
			//while(*data != 0){
				//fp(*data);
				//data++;
			//}
		}
	}
	return 0;
}


//---------- eFile_Delete-----------------
// delete this file
// Input: file name is a SINGLE ASCII letter
// Output: 0 if successful and 1 on failure (e.g., trouble writing to flash)
//		remove this file 
int eFile_Delete( char name[]){
	//delete the file from SD card
	//manage the free space
	int directoryPtr;
	dEntry_t directoryEntry;
	if(directoryCacheStatus == 0){
		//directoryBuffer is not current, reload
		if(eDisk_ReadBlock(directoryBuffer, 0)){
			//Error occured
			return 1;
		}
	}
	//Set directory entry
	directoryEntry.fileName = name[0];
	//Search for the directory file
	for(directoryPtr = 0; directoryPtr < BLOCK_SIZE - 1; directoryPtr += DIRECTORY_FILE_SIZE){
		//NULL file name means file is empty; file name is first byte
		if(directoryBuffer[directoryPtr] == name[0]){ //FOUND the file
			int i, blockPtr;
			//**********
			//free the blocks
			blockPtr = directoryBuffer[directoryPtr+1];	// start address
			for (i=0; i< directoryBuffer[directoryPtr+2] - 1; i++){ // For the SIZE of the file
				//free the blocks of the file
				if(eDisk_ReadBlock(blockBuffer, blockPtr)){
					//Error occurred
					return 1;
				}
				blockPtr = blockBuffer[0];	//free the next block with corresponding index
			}
			BYTE temp = directoryBuffer[FREE_BLOCK_INDEX];
			blockBuffer[0] = directoryBuffer[directoryPtr+1];
			if(eDisk_WriteBlock(blockBuffer, directoryBuffer[FREE_BLOCK_INDEX])){
				//Error occurred
				return 1;
			}
			blockBuffer[0] = temp;
			if(eDisk_WriteBlock(blockBuffer, blockPtr)){
				//Error occurred
				return 1;
			}
			//**********
			// removing the file from DIRECTORY
			// set the next to zero, the size to zero, then write back
			directoryBuffer[directoryPtr] = 0;
			directoryBuffer[directoryPtr+1] = 0;
			directoryBuffer[directoryPtr+2] = 0;
			if(eDisk_WriteBlock(directoryBuffer, 0)){
				//Error occurred
				return 1;
			}
			return 0;	//File deleted successfully
		}
	}
	//NOT found the file with that name
	return 1;
}


//---------- eFile_RedirectToFile-----------------
// open a file for writing !!!
// Input: file name is a single ASCII letter
// stream printf data into file
// Output: 0 if successful and 1 on failure (e.g., trouble read/write to flash)
int eFile_RedirectToFile(char *name){
	eFile_Create(name); // ignore error if file already exists
	if(eFile_WOpen(name)){
		return 1; // cannot open file
	}
	StreamToFile = 1;
	return 0;
}


//---------- eFile_EndRedirectToFile-----------------
// close the previously open file
// redirect printf data back to UART
// Output: 0 if successful and 1 on failure (e.g., wasn't open)
int eFile_EndRedirectToFile(void){
	StreamToFile = 0;
	if(eFile_WClose()){
		return 1; // cannot close file
	}
	return 0;
}


