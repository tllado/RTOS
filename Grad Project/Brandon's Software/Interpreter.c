// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// March 20th, 2016

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "Interpreter.h"
#include "UART.h"
#include "GradProject.h"
#include "Interrupts.h"
#include "OS.h"



// *************Global Variables*****************************

cmdTable Commands[] = {        // CMD_LEN defines max command name length
	{"help",        cmdHelp,        "Displays all available commands."},
	{"bground",     cmdBackground,  "Runs Background Thread Manager tests."},
};

char cmd[CMD_LEN+1];   // string to store command line inputs. +1 for null.
// *********************************************************************


void Interpreter_Init(void){
	printf("\n\n\r******************************************************\n\r");
	printf("                  Welcome to bOS.\n\r");
	printf("          Running software for motor board\n\r");
	printf("         Type \"help\" for a list of commands.\n\r");
	printf("******************************************************\n\r");
}


//---------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void){
	printf("\n\r--Enter Command--> ");
	UART_InStringNL(cmd, CMD_LEN);  
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(cmd,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
	printf("Command not recognized.\n\r");
}

	
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
	printf("Below is a list of availble commands:\n\r");
	// output formating and display command name
	for(int i = 0; i < NUM_OF(Commands); i++){
		if(i+1 <10) printf("    ");                   
		else if(i+1 < 100) printf("   ");     
		printf("%d",i+1);                             
		printf(") ");                         
		// display command name
		printf("%s",(char*)Commands[i].name);  
    // output formating		
		for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
		  printf("-");                             
		}
		// display command tag
    printf("%s\n\r",(char*)Commands[i].tag);     
	}
}


//------------cmdBackground-----------------
// Runs Background Thread Manager tests
void cmdBackground(void){
	printf("Choose a command for Background Thread Manager:\n\r");
	printf("    (1) Array Contents - Prints the contents of the test array.\n\r");
	printf("    (2) Start Test - Add a large number of background threads.\n\r");

	printf("--Enter Command #--> ");
	uint32_t selection = UART_InUDec();
	printf("\n\r");
	
	switch(selection){
		case 1:{
			for(int i = 0; i < 100; i++){
				printf("TestArray[%d] = %d\n\r" , i, TestArray[i]);
			}
			printf("\n\rFinished printing TestArray.\n\r");
			break;
		}
	  case 2:{
			OS_AddThread(&BackgroundTest, 128, 1);          // test the background manager. Set MAX_NUM_BGROUND in OS.h. Set stacksize to 64	
			printf("Finished adding background thread manager test.\n\r");
			break;
		}		
	}
}
