// GradProject.h
// Runs on TM4C123
// Real Time Operating System for Graduate Project
// Brandon Boesch
// Ce Wei
// April 17th, 2016


//**********************Compilter Directives********************************
          
// *************************************************************************


//********************Constants*********************************************

//**************************************************************************


//******************** Externs *********************************************
extern uint8_t TestArray[100];   // holds the results from the background thread manager test
extern uint32_t IdleCount;       // counts how many iterations IdleTask() is ran.

//**************************************************************************


//********************Prototypes********************************************

void BackgroundTest(void);
void IdleTask(void);
//**************************************************************************


