// Buttons.h
// Runs on LM4F120 or TM4C123
// Request an interrupt on the falling edge of PF4 (when the user
// button is pressed) and increment a counter in the interrupt.  Note
// that button bouncing is not addressed.
// Daniel Valvano
// May 3, 2015


#define SW1       0x10                      // on the left side of the Launchpad board
#define SW2       0x01                      // on the right side of the Launchpad board


// ******** SW1_Init ************
// Initialize Port F4 to handle edge triggered interrupt when SW1 is pressed. 
// Store SW1's task and priority.
// input:  *task - pointer to SW1's user task
//         priority - SW1's task priority
// output: none
void SW1_Init(void(*task)(void), uint8_t priority);

// ******** SW2_Init ************
// Initialize Port F0 to handle edge triggered interrupt when SW2 is pressed. 
// Store SW2's task and priority.
// input:  *task - pointer to SW2's user task
//         priority - SW1's task priority
// output: none
void SW2_Init(void(*task)(void), uint8_t priority);



// ***************** Switch_Enable ******************
void Switch_Enable(void);


// ***************** Switch_Disable ******************
void Switch_Disable(void);
