;/*****************************************************************************/
; OSasm.s: low-level OS commands, written in assembly                       */
; Runs on LM4F120/TM4C123
; A very simple real time operating system with minimal features.
; Daniel Valvano
; January 29, 2015
;
; This example accompanies the book
;  "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
;  ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015
;
;  Programs 4.4 through 4.12, section 4.2
;
;Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
;    You may use, edit, run or distribute this file
;    as long as the above copyright notice remains
; THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
; OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
; VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
; OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
; For more information about my classes, my research, and my books, see
; http://users.ece.utexas.edu/~valvano/
; */

PE3 EQU 0x40024020   ; access PE3 for profiling

        AREA |.text|, CODE, READONLY, ALIGN=2
        THUMB
        REQUIRE8
        PRESERVE8

        EXTERN  RunPt              ; currently running thread
        EXTERN  First4groundPt     ; first active thread in foreground
		EXTERN  NumActive          ; number of active foreground threads
		EXPORT  StartOS_ASM
        EXPORT  SysTick_Handler
		EXPORT  OS_Wait_ASM
		EXPORT	OS_Signal_ASM
			
	    

SysTick_Handler                ; 1)   Saves R0-R3,R12,LR,PC,PSR
    CPSID   I                  ; 2)   Prevent interrupt during switch
    PUSH    {R4-R11}           ; 3)   Save remaining regs r4-11
    ;LDR     R4, =PE3           ; 4.1) Toggle PE3 twice. R4 = &PE3
    ;LDR     R5, [R4]           ; 4.2) R5 = PE3
    ;EOR     R5, #0x08		   ; 4.3) R5 ^= 0x8
	;STR     R5, [R4]           ; 4.4) PE3 ^= 0x8
	;EOR     R5, #0x08          ; 4.5) R5 ^= 0x8
	;STR     R5, [R4]           ; 4.6) PE3 ^= 0x8
    LDR     R0, =RunPt         ; 5.1) R0=pointer to RunPt, old thread
    LDR     R1, [R0]           ; 5.2) R1 = RunPt
    STR     SP, [R1]           ; 6)   Save SP into TCB
    LDR     R1, [R1, #4]       ; 7.1) R1 = RunPt->next
    STR     R1, [R0]           ; 7.2) RunPt = R1
    LDR     SP, [R1]           ; 8)   new thread SP; SP = RunPt->sp;
	;EOR     R5, #0x08		    ; 9.1) Toggle PE3. R5 ^= 0x8
	;STR     R5, [R4]           ; 9.2) PE3 ^= 0x8
    POP     {R4-R11}           ; 10)  restore regs r4-11
    CPSIE   I                  ; 11)  tasks run with interrupts enabled
    BX      LR                 ; 12)  restore R0-R3,R12,LR,PC,PSR

;------SysTick_Handler----------
; A priority schedular that performs a context switch on the thread
; with the highest priority.  If multiple threads have the same priority
; then a round robin algorithm is used to schedule the next thread. 
SysTick_Handler2                ; 1)    saves R0-R3,R12,LR,PC,PSR
    CPSID   I                  ; 2)    prevent interrupt during switch
	PUSH    {R4-R11}           ; 3)    save remaining regs r4-11
	                           
Mismatch                       ; 4)    if(RunPt != First4groundPt), save RunPt stack and switch to First4groundPt
    LDR     R7, =RunPt         ; 4.1)  R0=pointer to RunPt, old thread
    LDR     R0, [R7]           ; 4.2)  R0 = RunPt
	LDR     R6, =First4groundPt; 4.3)  R6 = pointer to First4groundPt
	LDR     R6, [R6]           ; 4.4)  R6 = First4groundPt, thread with highest priority
	CMP     R0, R6             ; 4.5)  check if (RunPt != First4groundPt)
    BNE     ContextSwitch      ; 4.6)  if(RunPt != First4groundPt), perform context switch

OneThread                      ; 5)    if(NumActive == 1), do nothing
    LDR     R4, =NumActive     ; 5.1)  R4 = pointer to NumActive
	LDR     R4, [R4]           ; 5.2)  R4 = NumActive
	SUBS    R4, #1             ; 5.3)  check if NumActive == 1
	BEQ     Finish             ; 5.4)  if equal, do nothing
		
PriorityCheck                  ; 6)    if(RunPt->Priority != RunPt->Next->Priority), do nothing since current thread has higher priority
	LDR     R1, [R0, #4]       ; 6.1)  R1 = RunPt->next
	LDR     R2, [R0, #12]      ; 6.2)  R2 = RunPt->priority
	LDR     R3, [R1, #12]      ; 6.3)  R3 = RunPt->next->priority						   
	CMP     R2, R3			   ; 6.4)  check if (RunPt->Priority != RunPt->Next_>Priority)			   
	BNE     Finish   		   ; 6.5)  if not equal, do nothing. Branch to Finish                              						   
  
NextSpot	                   ; 7)    else, roundrobin. while(RunPt->priority == tempPt->Next->priority), keep looking location to put TCB
	LDR     R3, [R1, #12]      ; 7.1)  R3 = tempPt->next->priority
	CMP     R2, R3             ; 7.2)  check if (RunPt->priority == tempPt->Next->priority)
	BNE     Reorder            ; 7.3)  if not equal, then location has been found
	SUBS    R4, #1             ; 7.4)  R4 = NumActive - 1  
	BEQ     Reorder            ; 7.5)  if (NumActive == 0), then all priorities are the same in list. place at end	
	LDR     R1, [R1, #4]       ; 7.6)  else continue looking. R1 = tempPt->next
	B       NextSpot           ; 7.7)  not finished, continue looking

Reorder	                       ; 8)    put RunPt at end of current priority's list
    LDR     R2, [R0, #4]       ; 8.3)  R2 = RunPt->next
    LDR     R3, [R0, #8]       ; 8.4)  R3 = RunPt->previous
    LDR     R4, [R1, #4]       ; 8.5)  R4 = tempPt->next
    LDR     R5, [R4, #8]       ; 8.6)  R5 = tempPt->next->previous
    STR     R4, [R2, #4]       ; 8.7)  RunPt->next = tempPt-next	
	STR     R1, [R3, #8]       ; 8.8)  RunPt->previous = tempPt
	STR     R0, [R5, #8]       ; 8.9)  tempPt->next->previous = RunPt
	STR     R0, [R4, #4]       ; 8.10) tempPt->next = RunPt
	MOV     R6, R1             ; 8.11) R6 = R1, used in context switch
	
ContextSwitch                  ; 9) Store RunPt's SP, update RunPt, and Load in new SP
    STR     SP, [R0]           ; 9.1) Save RunPt SP into TCB
    STR     R6, [R7]           ; 9.2) RunPt = First4groundPt [or] tempPt, depending if Reorder ran
	LDR     SP, [R6]           ; 9.3) new thread SP; SP = RunPt->sp;
    LDR     R6, =First4groundPt; 9.4) R6 = pointer to First4groundPt
    STR     R1, [R6]   		   ; 9.6) First4groundPt = RunPt

Finish
    POP     {R4-R11}           ; 10)  restore regs r4-11
    CPSIE   I                  ; 11)  tasks run with interrupts enabled
    BX      LR                 ; 12)  restore R0-R3,R12,LR,PC,PSR




StartOS_ASM
    LDR     R0, =RunPt         ; currently running thread
    LDR     R2, [R0]           ; R2 = value of RunPt
    LDR     SP, [R2]           ; new thread SP; SP = RunPt->stackPointer;
    POP     {R4-R11}           ; restore regs r4-11
    POP     {R0-R3}            ; restore regs r0-3
    POP     {R12}
    POP     {LR}               ; discard LR from initial stack
    POP     {LR}               ; start location
    POP     {R1}               ; discard PSR
    CPSIE   I                  ; Enable interrupts at processor level
    BX      LR                 ; start first thread


OS_Wait_ASM
	LDREX	R1, [R0]		;Counter
	SUBS	R1, #1			;Counter - 1
	ITT		PL				;OK if >= 0
	STREXPL	R2, R1, [R0]	;try update
	CMPPL	R2, #0			;succeed?
	BNE		OS_Wait_ASM		;no, try again
	BX		LR				
	
	
OS_Signal_ASM
	LDREX	R1, [R0]		;Counter
	ADD		R1, #1			;Counter + 1
	STREX	R2, R1, [R0]	;try update
	CMP		R2, #0			;succeed?
	BNE		OS_Signal_ASM	;try again
	BX		LR
	

    ALIGN
    END

