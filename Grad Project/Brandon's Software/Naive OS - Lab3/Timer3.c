// Timer3.c
// Runs on LM4F120/TM4C123
// Use Timer3 in 32-bit periodic mode to request interrupts at a periodic rate
// Timer3 will periodically decrement all sleeping foregroundTCBs' sleep counters.
// Daniel Valvano
// March 20, 2014

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2013
  Program 7.5, example 7.6

 Copyright 2013 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Timer3.h"
#include "Lab3.h"
#include "OS.h"
#include "Interrupts.h"

// ***************** Timer3_Init ****************
// Activate Timer3 interrupts periodically
// Inputs:  period in units (1/clockfreq)
// Outputs: none
void Timer3_Init(unsigned long period){
  SYSCTL_RCGCTIMER_R |= 0x08;   // 0) activate TIMER3
	while((SYSCTL_PRTIMER_R&0x08) == 0){}; // allow time for clock to stabilize
  TIMER3_CTL_R = 0x00000000;    // 1) disable TIMER3A during setup
  TIMER3_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER3_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER3_TAILR_R = period-1;    // 4) reload value
  TIMER3_TAPR_R = 0;            // 5) bus clock resolution
  TIMER3_ICR_R = 0x00000001;    // 6) clear TIMER3A timeout flag
  TIMER3_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI8_R = (NVIC_PRI8_R&0x00FFFFFF)|0x60000000; // 8) priority 3
  NVIC_EN1_R = 1<<(35-32);      // 9) enable IRQ 35 in NVIC
  TIMER3_CTL_R = 0x00000001;    // 10) enable TIMER3A
}

// ***************** Timer3A_Handler ****************
// Periodically decrement all sleeping foregroundTCBs' sleep counters.
// Wakes up threads that are finished sleeping.
void Timer3A_Handler(void){
  TIMER3_ICR_R = TIMER_ICR_TATOCINT;          // acknowledge TIMER3A timeout
	int wakeupCnt = 0;                          // number of threads that will be woken up this ISR
	TCBType4Ground *tempTCBPt = Sleeping.list;  // temp pointer used for cycling through living TCBs
	TCBType4Ground *tempTCBPt2;                 // temp pointer used in case a thread is removed from sleeping list
	for(int i = 0; i<Sleeping.Value; i++){
		tempTCBPt->sleepCnt = tempTCBPt->sleepCnt - 1;  // decrement sleepCnt
		if(tempTCBPt->sleepCnt == 0) {                  // thread needs to wake up
			tempTCBPt2 = tempTCBPt->altNext;              // store the next TCB that needs to be checked before it is lost in OS_Wakeup
			OS_Wakeup(tempTCBPt);                         // wake up TCB
		  wakeupCnt++;                                  // keep track of how many threads wakeup
			tempTCBPt = tempTCBPt2;
		}
		else{                                           // thread did not need to wake up
		  tempTCBPt = tempTCBPt->altNext;               // move to next sleeping thread
		}
	}
	int32_t status = StartCritical();
	Sleeping.Value = Sleeping.Value - wakeupCnt;      // decrment Sleeping.Value by how many threads woke up
  NumActive = NumActive + wakeupCnt;
	EndCritical(status);
}


// ***************** Timer3A_Enable ******************
void Timer3A_Enable(void){
	NVIC_EN1_R = 1<<(35-32);     // enable IRQ 35 in NVIC
	TIMER3_CTL_R = 0x00000001;   // enable TIMER3A
}


// ***************** Timer3A_Disable ******************
void Timer3A_Disable(void){
	NVIC_DIS1_R = 1<<(35-32);    // disable IRQ 35 in NVIC
	TIMER3_CTL_R = 0x00000000;   // disable TIMER3A
}

