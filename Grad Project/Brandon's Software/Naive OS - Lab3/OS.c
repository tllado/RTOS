// OS.c
// Runs on TM4C123 
// Create and manage threads.
// Brandon Boesch
// Ce Wei
// January 25, 2016

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "tm4c123gh6pm.h"
#include "Lab3.h"
#include "Interrupts.h"
#include "OS.h"
#include "PLL.h"
#include "Timer1.h"
#include "Timer2.h"
#include "Timer3.h"
#include "buttons.h"
#include "UART.h"
#include "ST7735.h"
#include "Profiler.h"



// *****************Globals**************************
unsigned long Num4Ground;                        // number of foreground threads active
unsigned long NumBGround;                        // number of background threads active
unsigned long NumBlocked;                        // number of blocked foreground threads
unsigned long NumActive;                         // number of active foreground threads
TCBType4Ground ForegroundTCBs[MAX_NUM_4GROUND];  // Foreground Thread Control Blocks (TCB)
TCBType4Ground *RunPt;                           // pointer to running ForegroundTCB
TCBType4Ground *First4groundPt;                  // points to the ForegroundTCB with the highest priority 
TCBTypeBGround BackgroundTCBs[MAX_NUM_BGROUND];  // Background Thread Control Blocks (TCB)
TCBTypeBGround *FirstBackgroundPt;               // points to the BackgroundTCB with the highest priority 
int32_t Stacks[MAX_NUM_4GROUND][STACKSIZE];      // stacks for active foreground threads
void (*PeriodicTask)(void);                      // pointer to user function found in OS_AddPeriodicThread
static uint32_t Fifo[FIFOSIZE];
volatile uint32_t *PutPt;
volatile uint32_t *GetPt;
volatile uint32_t MailBoxData;                   // data in mailbox that is passed from producer to consumer
volatile uint32_t MsTime;                        // Current time(in ms) since last reset
bool Launched;                                   // true if OS_Launch has run.
// **************************************************

// *****************Global Semephores****************
volatile Sema4Type LCDFree;         // ST7735 binary semephore. 0=busy, 1=free
volatile Sema4Type CurrentSize;     // signaled in OS_Fifo_Put. Signifies data is in fifo
volatile Sema4Type FifoMutex;       // signaled in OS_Fifo_Put and OS_Fifo_Get. Allows mutual exclusion of R-W-M access of fifo
volatile Sema4Type DataValid;       // signaled in OS_MailBox_Send. 0 = no data available, 1 = data available.
volatile Sema4Type BoxFree;         // signaled in OS_MailBox_Recv. 0 = mail box not available, 1 = mailbox available
volatile Sema4Type RxDataAvailable; // signaled in UART's copyHardwareToSoftware. 0 = Rx Fifo is empty, else = data in Rx Fifo
volatile Sema4Type TxRoomLeft;      // signaled in UART's copySoftwareToHardware. 0 = Tx Fifo is full, else = room available in Tx Fifo
volatile Sema4Type Sleeping;        // holds list of all sleeping ForegroundTCBs. 
// **************************************************


//******** OS_AddPeriodicThread *************** 
// Adds a background periodic task.  See Timer1.c for background handler
// Inputs: *task - pointer to a void/void background function
//         period - given in system time units (12.5ns). Must be 
//                  greater than or equal to BACKGROUND_PERIOD
//         priority - 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// You are free to select the time resolution for this function
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal, OS_AddThread
// This task does not have a Thread ID
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddPeriodicThread(void(*task)(void), unsigned long period, unsigned long priority){
	int32_t status = StartCritical();
	if((period % BACKGROUND_PERIOD) != 0){  
		OS_Error(); // do not allow a period smaller than predefined value or not a multiple of Timer1's period. change either 'period' or BACKGROUND_PERIOD.
	}
	// not allowed to create more background threads than allocated
	else if(NumBGround >= MAX_NUM_BGROUND){
		EndCritical(status);
		return 0;        
	}
	// create first background thread
	else if(NumBGround == 0){
		BackgroundTCBs[0].task = task;               // assign TCB its task
		BackgroundTCBs[0].next = &BackgroundTCBs[0]; // cycle back to self since the only backgroundTCB alive
	  BackgroundTCBs[0].previous = &BackgroundTCBs[0]; // cycle back to self since the only backgroundTCB alive
		BackgroundTCBs[0].alive = true;              // bring TCB to life
		BackgroundTCBs[0].period = period;           // assign period to TCB
		BackgroundTCBs[0].sleepCnt = SLEEPCNT_CONVERT(period); // assign sleepCnt to TCB based off timer1's period
	  BackgroundTCBs[0].priority = priority;       // assign priority to TCB
		BackgroundTCBs[0].aperiodic = false;         // create a periodic backgroundTCB
		FirstBackgroundPt = &BackgroundTCBs[0];      // TCB[0] will run first
	}
	// create new background thread
	else{
		// find dead backgroundTCB slot to bring alive. In worstcase O(n). Maybe change to FIFO list of all availavle dead slots to choose from
		int i;                                    // i holds the new TCB's index. 
		for(i = 0; i < MAX_NUM_BGROUND; i++){     
		  if(BackgroundTCBs[i].alive == false){
				BackgroundTCBs[i].alive = true;       // bring TCB to life
				break;                          
			}
		}
		// assign new TCB its characteristics
		BackgroundTCBs[i].task = task;           // assign TCB its task
		BackgroundTCBs[i].period = period;        // assign period to TCB
		BackgroundTCBs[i].sleepCnt = SLEEPCNT_CONVERT(period); // assign sleepCnt to TCB based off timer1's period
	  BackgroundTCBs[i].priority = priority;    // assign priority to TCB
		BackgroundTCBs[i].aperiodic = false;      // create a periodic backgroundTCB
		
		// put backgroundTCB into linked list, order based on priority. In worstcase O(n). Possibly add a seperate living list for each priority level
		int j;                               // j is order of the current BackgroundTCB looking at in list
		TCBTypeBGround *tempTCBPt = FirstBackgroundPt;          // tempTCBPt used to cycle through TCB list. Start at front of line
		for(j = 0; j < NumBGround; j++){                        // cycle through all active backgroundTCBs
      if(priority <= tempTCBPt->priority){                  // place new TCB in line before this TCB
				if(j == 0) FirstBackgroundPt = &BackgroundTCBs[i];  // new TCB will now be first in line
				BackgroundTCBs[i].previous = tempTCBPt->previous;   // create new TCB's previous pt
				BackgroundTCBs[i].next = tempTCBPt;                 // create new TCB's next pt
				tempTCBPt->previous->next = &BackgroundTCBs[i];     // point previous TCB to new TCB
				tempTCBPt->previous = &BackgroundTCBs[i];           // old TCB's previous now points to new TCB
				break;
			}
			else{
				tempTCBPt = tempTCBPt->next;                        // try next location in list
			}
		}
		// check if new thread has lowest priority. If so, place new thread in back of line
		if(j == NumBGround){
			FirstBackgroundPt->previous->next = &BackgroundTCBs[i];   // The old last TCB now points the new last TCB
      BackgroundTCBs[i].previous = FirstBackgroundPt->previous; // create new TCB's previous pt
			FirstBackgroundPt->previous = &BackgroundTCBs[i];         // First TCB's previous now points to new TCB
			BackgroundTCBs[i].next = FirstBackgroundPt;               // New TCB's next pt now points to First TCB
    }
	}
	NumBGround++; // increment background thread count
  EndCritical(status);
  return 1;     // successfully added backgroundTCB
}


//******** OS_AddAperiodicThread *************** 
// Adds a background aperiodic task.  See Timer1.c for background handler
// Inputs: *task - pointer to a void/void background function
//         priority - 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal, OS_AddThread
// This task does not have a Thread ID
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddAperiodicThread(void(*task)(void), uint8_t priority){
	// not allowed to create more background threads than allocated
	if(NumBGround >= MAX_NUM_BGROUND){
		return 0;        
	}
		// create first background thread
	else if(NumBGround == 0){
		BackgroundTCBs[0].task = task;               // assign TCB its task
		BackgroundTCBs[0].next = &BackgroundTCBs[0]; // cycle back to self since the only backgroundTCB alive
	  BackgroundTCBs[0].previous = &BackgroundTCBs[0]; // cycle back to self since the only backgroundTCB alive
		BackgroundTCBs[0].alive = true;              // bring TCB to life
		BackgroundTCBs[0].sleepCnt = 1;              // assign sleepCnt to TCB so that it runs immediately in Timer1 handler
	  BackgroundTCBs[0].priority = priority;       // assign priority to TCB
		BackgroundTCBs[0].aperiodic = true;          // create an aperiodic backgroundTCB
		FirstBackgroundPt = &BackgroundTCBs[0];      // TCB[0] will run first
	}
		// create new background thread
	else{
		// find dead backgroundTCB slot to bring alive. In worstcase O(n). Maybe change to FIFO list of all availavle dead slots to choose from
		int i;                                    // i holds the new TCB's index. 
		for(i = 0; i < MAX_NUM_BGROUND; i++){     
		  if(BackgroundTCBs[i].alive == false){
				BackgroundTCBs[i].alive = true;       // bring TCB to life
				break;                          
			}
		}
		// assign new TCB its characteristics
		BackgroundTCBs[i].task = task;            // assign TCB its task
		BackgroundTCBs[i].sleepCnt = 1;           // assign sleepCnt to TCB so that it runs immediately in Timer1 handler
	  BackgroundTCBs[i].priority = priority;    // assign priority to TCB
		BackgroundTCBs[i].aperiodic = true;       // create a periodic backgroundTCB

		// put backgroundTCB into linked list, order based on priority. In worstcase O(n). Possibly add a seperate living list for each priority level
		int j;                               // j is order of the current BackgroundTCB looking at in list
		TCBTypeBGround *tempTCBPt = FirstBackgroundPt;          // tempTCBPt used to cycle through TCB list. Start at front of line
		for(j = 0; j < NumBGround; j++){                        // cycle through all active backgroundTCBs
      if(priority <= tempTCBPt->priority){                  // place new TCB in line before this TCB
				if(j == 0) FirstBackgroundPt = &BackgroundTCBs[i];  // new TCB will now be first in line
				BackgroundTCBs[i].previous = tempTCBPt->previous;   // create new TCB's previous pt
				BackgroundTCBs[i].next = tempTCBPt;                 // create new TCB's next pt
				tempTCBPt->previous->next = &BackgroundTCBs[i];     // point previous TCB to new TCB
				tempTCBPt->previous = &BackgroundTCBs[i];           // old TCB's previous now points to new TCB
				break;
			}
			else{
				tempTCBPt = tempTCBPt->next;                        // try next location in list
			}
		}
		// check if new thread has lowest priority. If so, place new thread in back of line
		if(j == NumBGround){
			FirstBackgroundPt->previous->next = &BackgroundTCBs[i];   // The old last TCB now points the new last TCB
      BackgroundTCBs[i].previous = FirstBackgroundPt->previous; // create new TCB's previous pt
			FirstBackgroundPt->previous = &BackgroundTCBs[i];         // First TCB's previous now points to new TCB
			BackgroundTCBs[i].next = FirstBackgroundPt;               // New TCB's next pt now points to First TCB
    }
	}
	NumBGround++; // increment background thread count
	return 1; //success
}


// ******** OS_Init ************
// initialize operating system, disable interrupts until OS_Launch
// initialize OS controlled I/O: serial, ADC, systick, LaunchPad I/O and timers 
// input:  none
// output: none
void OS_Init(void){
  DisableInterrupts();
	PLL_Init(Bus80MHz);             // set processor clock to 80 MHz 
	TCBInit();                      // initialize foreground and background TCBs
	UART_Init();                    // initialize UART
	ST7735_InitR(INITR_REDTAB);     // initialize ST7735 LCD
	ST7735_DrawFastHLine(0, 79, 127, ST7735_WHITE);  // draw divider for two devices
	NVIC_ST_CTRL_R = 0;             // disable SysTick during setup
  NVIC_ST_CURRENT_R = 0;          // any write to current clears it
  NVIC_SYS_PRI3_R =(NVIC_SYS_PRI3_R&0x00FFFFFF)|0xE0000000; // priority 7
	Timer1_Init(BACKGROUND_PERIOD); // initialize timer1 to run background tasks.
	Timer2_Init(0xFFFFFFFF);        // intialize timer2 with max 32bit period. Used for OS_Time()
	Timer3_Init(TIME_1MS);          // interrupt every millisecond.  Used for OS_Sleep();
	Num4Ground = 0;                 // active foreground thread count
	NumBGround = 0;                 // active background thread count
	Sleeping.Value = 0;             // no foreground threads are sleeping initially
	NumBlocked = 0;                 // no foreground threads are blocked initially
	NumActive = 0;                  // no active foreground threads initially
	Launched = false;               // OS has not launched yet
}


//******** TCBInit *************** 
// Initialize all the TCBs. Called within OS_Init.
// Inputs: none
// Outputs: none
void TCBInit(void){
	for(int i = 0; i < MAX_NUM_4GROUND; i++){
		ForegroundTCBs[i].id = i;           // set unique id for each tcb
		ForegroundTCBs[i].alive = false;    // all tcbs are initially inactive
	}
	for(int i = 0; i <MAX_NUM_BGROUND; i++){
		BackgroundTCBs[i].id = i;           // set unique id for each tcb
		BackgroundTCBs[i].alive = false;    // all tcbs are initially inactive
	}
}


//***********SetInitialStack************
// Make the TCB looks like it was suspended from previous states
// R15 - PC will be set in OS_AddThread
// R13 - user stack will not be changed
void SetInitialStack(int i){
  ForegroundTCBs[i].sp = &Stacks[i][STACKSIZE-16]; // thread stack pointer														 
  Stacks[i][STACKSIZE-1] = 0x01000000;   // thumb bit, PSR, last one in the array and lowest in the memory							 
	Stacks[i][STACKSIZE-3] = 0x14141414;   // R14, LR
  Stacks[i][STACKSIZE-4] = 0x12121212;   // R12
  Stacks[i][STACKSIZE-5] = 0x03030303;   // R3
  Stacks[i][STACKSIZE-6] = 0x02020202;   // R2
  Stacks[i][STACKSIZE-7] = 0x01010101;   // R1
  Stacks[i][STACKSIZE-8] = 0x00000000;   // R0
  Stacks[i][STACKSIZE-9] = 0x11111111;   // R11
  Stacks[i][STACKSIZE-10] = 0x10101010;  // R10
  Stacks[i][STACKSIZE-11] = 0x09090909;  // R9
  Stacks[i][STACKSIZE-12] = 0x08080808;  // R8
  Stacks[i][STACKSIZE-13] = 0x07070707;  // R7
  Stacks[i][STACKSIZE-14] = 0x06060606;  // R6
  Stacks[i][STACKSIZE-15] = 0x05050505;  // R5
  Stacks[i][STACKSIZE-16] = 0x04040404;  // R4
}


//******** OS_AddThread *************** 
// Add a foregound thread to the scheduler in priority order.
// Increments Num4Ground.
// Inputs: pointer to a void/void foreground task
//         number of bytes allocated for its stack
//         priority, 0 is highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// stack size must be divisable by 8 (aligned to double word boundary)
// In Lab 3, you can ignore the stackSize fields
int OS_AddThread(void(*task)(void), unsigned long stackSize, uint8_t priority){
	int32_t status = StartCritical();
	// not allowed to create more foreground threads than allocated
	if(Num4Ground >= MAX_NUM_4GROUND){ 
		EndCritical(status);
		return 0;                // failure
	}
	
	// create first foreground thread
	else if(Num4Ground == 0){
		ForegroundTCBs[0].alive = true;
		ForegroundTCBs[0].next = &ForegroundTCBs[0];     // cycle back to self since the only foregroundTCB alive
	  ForegroundTCBs[0].previous = &ForegroundTCBs[0]; // cycle back to self since the only foregroundTCB alive
	  ForegroundTCBs[0].sleepCnt = 0;                  // set sleepCnt to zero for new thread
		ForegroundTCBs[0].priority = priority;           // set priority for new thread
		SetInitialStack(0);
		Stacks[0][STACKSIZE-2] = (int32_t)(task);        // set PC Register for user task
		RunPt = &ForegroundTCBs[0];                      // thread 0 will run first
		First4groundPt = &ForegroundTCBs[0];             // thread 0 has highest priority
	}
	
	// else create new foreground thread
	else{
		// find dead foregroundTCB slot to bring alive. In worstcase O(n). Maybe change to FIFO list of all availavle dead slots to choose from
		int i;                                    // i holds the new tcb index
		for(i = 0; i < MAX_NUM_4GROUND; i++){     // find dead TCB slot to bring alive
		  if(ForegroundTCBs[i].alive == false){
				ForegroundTCBs[i].alive = true;       // bring foregroundTCB to life
				break;                          
			}
		}
		// assign new foregroundTCB its characteristics
    ForegroundTCBs[i].sleepCnt = 0;                  // set sleepCnt to zero for new thread
		ForegroundTCBs[i].priority = priority;           // set priority for new thread
		SetInitialStack(i);
		Stacks[i][STACKSIZE-2] = (int32_t)(task);        // set PC Register for user task
		
		// put foregroundTCB into linked list, order based on priority. In worstcase O(n). Possibly add a seperate living list for each priority level
		int j;                                                 // j is order of the current foregroundTCB looking at in list
	  TCBType4Ground *tempTCBPt = First4groundPt;            // tempTCBPt used to cycle through foregroundTCB list. Start at front of line
	  for(j = 0; j < NumActive; j++){                        // cycle through all active foregroundTCBs
		  if(priority <= tempTCBPt->priority){                 // place new TCB in line before this TCB
        if(j == 0){                                        // new TCB will now be first in line
					First4groundPt = &ForegroundTCBs[i]; 
          if(!Launched) RunPt = First4groundPt;            // update RunPt if OS hasn't launched yet					
        }
				ForegroundTCBs[i].previous = tempTCBPt->previous;  // create new TCB's previous pt
		    ForegroundTCBs[i].next = tempTCBPt;                // create new TCB's next pt
		    tempTCBPt->previous->next = &ForegroundTCBs[i];    // point previous TCB's next pointer to new TCB
		    tempTCBPt->previous = &ForegroundTCBs[i];          // old TCB's previous now points to new TCB
		    break;
			}
		  else{
		    tempTCBPt = tempTCBPt->next;                       // try next location in list
		  }
		}
		// check if foregroundTCB has lowest priority. If so, place new foregroundTCB in back of line
	  if(j == NumActive){
			First4groundPt->previous->next = &ForegroundTCBs[i];   // The old last TCB's next pointer now points the new last TCB
		  ForegroundTCBs[i].previous = First4groundPt->previous; // new TCB's previous pointer now points to the old last TCB
		  First4groundPt->previous = &ForegroundTCBs[i];         // First TCB's previous now points to new TCB
			ForegroundTCBs[i].next = First4groundPt;               // New TCB's next pt now points to First TCB
		}
	}
	Num4Ground++; // increment foreground thread count
	NumActive++;  // increment number of active foreground threads
	EndCritical(status);
	return 1;     // successfully added foregroundTCB
}

	
//******** OS_Launch *************** 
// start the scheduler, enable interrupts
// Inputs: number of 12.5ns clock cycles for each time slice
//         you may select the units of this parameter
// Outputs: none (does not return)
// In Lab 2, you can ignore the theTimeSlice field
// In Lab 3, you should implement the user-defined TimeSlice field
// It is ok to limit the range of theTimeSlice to match the 24-bit SysTick
void OS_Launch(unsigned long theTimeSlice){
	Launched = true;                     // the system is about to launch
  NVIC_ST_RELOAD_R = theTimeSlice - 1; // reload value
  NVIC_ST_CTRL_R = 0x00000007;         // enable, core clock and interrupt arm
  StartOS_ASM();                       // start on the first task
}


// ******** OS_Suspend ************
// suspend execution of currently running thread
// scheduler will choose another thread to execute
// Can be used to implement cooperative multitasking 
// Same function as OS_Sleep(0)
// input:  none
// output: none
void OS_Suspend(void){
	NVIC_ST_CURRENT_R = 0;          // clear the counter
  NVIC_INT_CTRL_R = 0x04000000;		// manually triggers SysTick. SysTick handler performs thread switch
}	


// ******** OS_InitSemaphore ************
// Initialize semaphore. A value >= 1 means semaphor is free for use 
// input:  pointer to a semaphore
// output: none
void OS_InitSemaphore(volatile Sema4Type *semaPt, long value){
  semaPt->Value = value;
}
	

// ******** OS_Wait ************
// decrement semaphore 
// Lab3 block if less than zero
// input:  pointer to a counting semaphore
// output: none
void OS_Wait(volatile Sema4Type *semaPt){
	int32_t status = StartCritical();
  semaPt->Value = semaPt->Value - 1;  // decrement semaphore's value
	if(semaPt->Value < 0){
		OS_Block(semaPt);            // add TCB to that semaphore's blocked list
		EndCritical(status);
		OS_Suspend();                     // run thread switcher
	}
	EndCritical(status);
} 
		

// ******** OS_Signal ************
// increment semaphore 
// Lab3 wakeup blocked thread if appropriate 
// input:  pointer to a counting semaphore
// output: none
void OS_Signal(volatile Sema4Type *semaPt){
	int32_t status = StartCritical();
	bool suspend = false;
	semaPt->Value = semaPt->Value + 1;  // increment semaphore's value
	if(semaPt->Value <= 0){             
	  suspend = OS_Unblock(semaPt); // remove one thread from semaphore's blocked list
	}
	EndCritical(status);
	if(suspend) OS_Suspend();
}
	

// ******** OS_Sleep ************
// place this thread into a dormant state. Always puts sleeping thread 
// in front of sleeping list. Background threads cannot call this function. 
// input:  number of msec to sleep
// output: none
// You are free to select the time resolution for this function
void OS_Sleep(unsigned long sleepTime){
	if(NumActive == 1){      // prevent OS from crashing by adding a dummy thread
		OS_AddThread(&Dummy,128,7); 
	}
	int32_t status = StartCritical();
  RunPt->sleepCnt = sleepTime;             // assign sleep value to thread
	
	// patch up active foreground list before removing
	if(First4groundPt == RunPt){             // if puting the active list's first foregroundTCB to sleep, update First4groundPt
		First4groundPt = First4groundPt->next;  
	}
	RunPt->previous->next = RunPt->next;
	RunPt->next->previous = RunPt->previous;
	
	// remove from active foregroung list, and put into sleeping list
	if(Sleeping.Value == 0){               // new sleepingTCB will be the only one in sleeping list
		RunPt->altNext = RunPt;              // its altNext pointer will point to self
		RunPt->altPrevious = RunPt;          // its sleepPreviosu pointer will point to self
	}
	else{                                  // put new sleepingTCB in front of the current list
		RunPt->altPrevious = Sleeping.list->altPrevious;
		RunPt->altNext = Sleeping.list;
		Sleeping.list->altPrevious->altNext = RunPt;
		Sleeping.list->altPrevious = RunPt;
	}
	Sleeping.list = RunPt;	               // list now points to this TCB
	Sleeping.Value++;
	NumActive--;
	// perform a context switch
	EndCritical(status);
	OS_Suspend();
}

// ******** OS_Wakeup ************
// Remove TCB from sleepingTCB list, and place back in the
// active foregroundTCB list.  This will only be called 
// from the Timer3A_Handler.
// input:  pointer to foregroundTCB that is waking up
// output: none
void OS_Wakeup(TCBType4Ground *waking){
	int32_t status = StartCritical();
	
  // patch up sleeping list before removing
	if(waking == Sleeping.list){                           // if waking up first thread in list, need to update the sleeping list pointer
		Sleeping.list = waking->altNext;  
	}
	waking->altPrevious->altNext = waking->altNext;        // the waking TCB's previous TCB needs to update its next pointer
	waking->altNext->altPrevious = waking->altPrevious;    // the waking TCB's next TCB needs to update its previous pointer
	
	// remove from sleeping list and place back into its slot in active foreground list. Connect list based on priority. In worstcase O(n). Possibly add a seperate living list for each priority level
  TCBType4Ground *tempTCBPt = First4groundPt;                     // tempTCBPt used to cycle through foregroundTCB list. Start at front of line
	int i;                                                          // i is order of the current foregroundTCB looking at in list
	for(i = 0; i < NumActive; i++){                                 // cycle through all active foregroundTCBs
	  if(waking->priority <= tempTCBPt->priority){                  // place waking TCB in line before this TCB
      if(i == 0){                                                 // waking TCB will now be first in line  
				First4groundPt = &ForegroundTCBs[waking->id];          
			}
			ForegroundTCBs[waking->id].previous = tempTCBPt->previous;  // update waking TCB's previous pt  
		  ForegroundTCBs[waking->id].next = tempTCBPt;                // update waking TCB's next pt
		  tempTCBPt->previous->next = &ForegroundTCBs[waking->id];    // point previous TCB's next pointer to waking TCB
		  tempTCBPt->previous = &ForegroundTCBs[waking->id];          // old TCB's previous now points to waking TCB
		  break;    
		}
		else{
		  tempTCBPt = tempTCBPt->next;                                // try next location in list
		}
	}
	// check if waking TCB has lowest priority. If so, place waking TCB in back of line
	if(i == NumActive){
	  First4groundPt->previous->next = &ForegroundTCBs[waking->id];   // The old last TCB now points the new last TCB
		ForegroundTCBs[waking->id].previous = First4groundPt->previous; // update waking TCB's previous pt
		First4groundPt->previous = &ForegroundTCBs[waking->id];         // First TCB's previous now points to waking TCB
		ForegroundTCBs[waking->id].next = First4groundPt;               // waking TCB's next pt now points to First TCB
	}
	
	EndCritical(status);
}


// ******** OS_Kill ************
// Kill the currently running foreground thread, and 
// release its TCB and stack. 
// Decrements Num4Ground.
// input:  none
// output: none
void OS_Kill(void){
  int32_t status = StartCritical();
	if(NumActive == 1){          // prevent OS from crashing by adding a dummy thread
	  OS_AddThread(&Dummy,128,7); 
	}
	// determine if the TCB you are killing is first in the list (highest priority)
	if(First4groundPt == &ForegroundTCBs[OS_Id()]){
		First4groundPt = ForegroundTCBs[OS_Id()].next;
	}
	// remove dying TCB from linked list
	ForegroundTCBs[OS_Id()].previous->next = ForegroundTCBs[OS_Id()].next;  // the TCB before the dying TCB should now connect to the dying TCB's next TCB
	ForegroundTCBs[OS_Id()].next->previous = ForegroundTCBs[OS_Id()].previous;  // the TCB after the dying TCB should have a previous TCB that is the same as the dying TCB's previous
	ForegroundTCBs[OS_Id()].alive = false;
	Num4Ground--;
	NumActive--;
	EndCritical(status);
	OS_Suspend();
}


// ******** OS_KillAperiodic ************
// Kill the currently running aperiodic background 
// thread, and release its TCB. 
// Decrements NumBGround.
// input:  index - index of TCB which you would like to remove from BackgroundTCBs
// output: none
void OS_KillAperiodic(uint32_t index){
	BackgroundTCBs[index].previous->next = BackgroundTCBs[index].next;      // the TCB before the dying TCB should now connect to the dying TCB's next TCB
	BackgroundTCBs[index].next->previous = BackgroundTCBs[index].previous;  // the TCB after the dying TCB should have a previous TCB that is the same as the dying TCB's previous
	BackgroundTCBs[index].alive = false;    // kill the TCB
	NumBGround--;                           // decrement the number of background threads
}

//******** OS_Id *************** 
// returns the thread ID for the currently running foreground thread
// Inputs: none
// Outputs: Thread ID, number greater than zero 
unsigned long OS_Id(void){
  return RunPt->id;
}


//******** OS_AddSW1Task *************** 
// add a background task to run whenever the SW1 (PF4) button is pushed
// Inputs: pointer to a void/void background function
//         priority 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal OS_AddThread
// This task does not have a Thread ID
// In labs 2 and 3, this command will be called 0 or 1 times
// In lab 2, the priority field can be ignored
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddSW1Task(void(*task)(void), uint8_t priority){
	if(NumBGround == MAX_NUM_BGROUND) return 0;  // cannot make new task since max # of threads is already active
	SW1_Init(*task, priority);
  return 1;
}


//******** OS_AddSW2Task *************** 
// add a background task to run whenever the SW2 (PF0) button is pushed
// Inputs: pointer to a void/void background function
//         priority 0 is highest, 5 is lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed user task will run to completion and return
// This task can not spin block loop sleep or kill
// This task can call issue OS_Signal, it can call OS_AddThread
// This task does not have a Thread ID
// In lab 2, this function can be ignored
// In lab 3, this command will be called will be called 0 or 1 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddSW2Task(void(*task)(void), uint8_t priority){
	if(NumBGround == MAX_NUM_BGROUND) return 0;  // cannot make new task since max # of threads is already active
	SW2_Init(*task, priority);
  return 1;
}


// ******** OS_Time ************
// Return the system time. Uses Timer2A 
// Inputs:  none
// Outputs: time in 12.5ns units, 0 to 4294967295
// The time resolution should be less than or equal to 1us, and the precision 32 bits.
// It is ok to change the resolution and precision of this function as long as 
// this function and OS_TimeDifference have the same resolution and precision. 
unsigned long OS_Time(void){
  return TIMER2_TAR_R;         // Timer2A is used to return time
}


// ******** OS_TimeDifference ************
// Calculates difference between two times
// Inputs:  two times measured with OS_Time
// Outputs: time difference in 12.5ns units 
// The time resolution should be less than or equal to 1us, and the precision at least 12 bits.
// It is ok to change the resolution and precision of this function as long as 
// this function and OS_Time have the same resolution and precision 
unsigned long OS_TimeDifference(unsigned long start, unsigned long stop){
  return start-stop;           
}


// ******** OS_ClearMsTime ************
// sets the system time to zero (Timer2A)
// Inputs:  none
// Outputs: none
void OS_ClearMsTime(void){
	long sr = StartCritical();
  MsTime = 0;
	EndCritical(sr);
}
	
	
// ******** OS_MsTime ************
// Reads the current time (Timer2A) in ms units (assuming 80MHz clock)
// Inputs:  none
// Outputs: time in ms units since last clear
// You are free to select the time resolution for this function
// It is ok to make the resolution to match the first call to OS_AddPeriodicThread
unsigned long OS_MsTime(void){
	long sr = StartCritical();
	MsTime = (0xFFFFFFFF - TIMER2_TAR_R)/80000;
	EndCritical(sr);
  return MsTime;
}


// ******** OS_Fifo_Init ************
// Initialize the Fifo to be empty
// Inputs: size
// Outputs: none 
// In Lab 2, you can ignore the size field
// In Lab 3, you should implement the user-defined fifo size
// In Lab 3, you can put whatever restrictions you want on size
//    e.g., 4 to 64 elements
//    e.g., must be a power of 2,4,8,16,32,64,128
void OS_Fifo_Init(unsigned long size){
  PutPt = &Fifo[0];
	GetPt = &Fifo[0];
	OS_InitSemaphore(&CurrentSize, 0);  // Nothing in fifo initially
	OS_InitSemaphore(&FifoMutex, 1);    // Fifo is available initially
}
	
	
// ******** OS_Fifo_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting.
// This function cannot enable/disable intterupts 
// since it will be called from interrupts.
// Inputs:  data
// Outputs: true - when data is properly saved,
//          false - if data not saved, because fifo was full
int OS_Fifo_Put(unsigned long data){
  volatile uint32_t *nextPutPt;
	nextPutPt = PutPt + 1;
	if(nextPutPt == &Fifo[FIFOSIZE]){
		nextPutPt = &Fifo[0];		// wrap around
	}
	if(nextPutPt == GetPt){
		return 0;  // cannot put since fifo is full. Results in lost data
	}
	else{
		*(PutPt) = data;
		PutPt = nextPutPt;
		OS_Signal(&CurrentSize);
	}
  return 1;    // data put in fifo successfully
}


// ******** OS_Fifo_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will block if empty
// Inputs:  none
// Outputs: data 
unsigned long OS_Fifo_Get(void){
	int32_t returnValue;    // holds the value poped off of fifo.
	OS_Wait(&CurrentSize);  // spin/block for data
	OS_Wait(&FifoMutex);    // only one thread can access the fifo at any given time
	returnValue = *GetPt;   // pop element off fifo
	GetPt++;
	if(GetPt == &Fifo[FIFOSIZE]){
		GetPt = &Fifo[0];     // wrap around
	}
	OS_Signal(&FifoMutex); 	//release the fifo
  return returnValue;
}


// ******** OS_Fifo_Size ************
// Check the status of the Fifo
// Inputs: none
// Outputs: returns the number of elements in the Fifo
//          greater than zero if a call to OS_Fifo_Get will return right away
//          zero or less than zero if the Fifo is empty 
//          zero or less than zero if a call to OS_Fifo_Get will spin or block
long OS_Fifo_Size(void){
  return CurrentSize.Value;
}


// ******** OS_MailBox_Init ************
// Initialize communication channel
// Inputs:  none
// Outputs: none
void OS_MailBox_Init(void){
  OS_InitSemaphore(&DataValid, 0);  // Nothing in mailbox initially
	OS_InitSemaphore(&BoxFree, 1);    // Mailbox is available initially
}
	
	
// ******** OS_MailBox_Send ************
// enter mail into the MailBox
// Inputs:  data to be sent
// Outputs: none
// This function will be called from a foreground thread
// It will block if the MailBox contains data not yet received 
void OS_MailBox_Send(unsigned long data){
  OS_Wait(&BoxFree);
	MailBoxData = data;
	OS_Signal(&DataValid);
}
	
	
// ******** OS_MailBox_Recv ************
// remove mail from the MailBox
// Inputs:  none
// Outputs: data received
// This function will be called from a foreground thread
// It will block if the MailBox is empty 	
unsigned long OS_MailBox_Recv(void){
  OS_Wait(&DataValid);
	uint32_t returnData = MailBoxData;
	OS_Signal(&BoxFree);
  return returnData;
}


// ******** OS_Error ************
// called when there is an error 
// preventing the system from running	
void OS_Error(void){
	DisableInterrupts();
  ST7735_InitR(INITR_REDTAB);      // initialize ST7735 LCD
	ST7735_FillScreen(0);            // set screen to black
	ST7735_SetCursor(0,0);           // allign cursor to left
	ST7735_OutString("Error in adding");
	ST7735_SetCursor(0,1);           // allign cursor to left
	ST7735_OutString("periodic thread");
	while(1){}                       // do not allow to run with error
}


// ******** OS_Block *********
// Add TCB to a semaphore's blocked list. 
// Called from OS_Wait while interrupts are disabled
// Increments NumBlocked and semaPt->value. 
// Inputs: pointer to semaphore you wish to add blocked thread to
// Outputs: None
void OS_Block(volatile Sema4Type *semaPt){
	if(NumActive == 1){    // prevent OS from crashing by adding a dummy thread
    OS_AddThread(&Dummy,128,7); 
  }
	
	// patch up active foreground list before removing
	if(First4groundPt == RunPt){
 		First4groundPt = First4groundPt->next;  // if removing the active list's first foregroundTCB, update First4groundPt
  }
	RunPt->previous->next = RunPt->next;
  RunPt->next->previous = RunPt->previous;
	
  // put into semaphore's	list based on priority
	if(abs(semaPt->Value) == 1){  // newly blocked TCB will be the only TCB in this list
		semaPt->list = RunPt;       // semaphore now points to newly blocked TCB
		RunPt->altNext = RunPt;     // newly blocked TCB's altNext pointer now points to self
		RunPt->altPrevious = RunPt; // newly blocked TCB's previous pointer now points to self
	}
	else{                         // find where to insert newly blocked TCB into list, based on priority
    int i;                                           // i is order of the current blocked TCB looking at in semaphore's list
    TCBType4Ground *tempTCBPt = semaPt->list;        // tempTCBPt used to cycle through semaphore's TCB list. Start at front of line 
		for(i = 0; i < abs(semaPt->Value); i++){         // cycle through all TCB's in semaphore's list
		  if(RunPt->priority <= tempTCBPt->priority){
			  if(i == 0){                                  // new TCB will now be first in line
					semaPt->list = RunPt;                      // update semaPt->list with new first item
				}
			  RunPt->altPrevious = tempTCBPt->altPrevious; // create newly blocked TCB's previous pt
				RunPt->altNext = tempTCBPt;                  // create newly blocked TCB's next pt
				tempTCBPt->altPrevious->altNext = RunPt;     // point previous TCB's next pointer to newly blocked TCB
			  tempTCBPt->altPrevious = RunPt;              // old TCB's previous now points to newly blocked TCB
		    break;
			}
			else{
				tempTCBPt = tempTCBPt->altNext;              // try next location in list
			}
	  }
		// check if newly blocked has lowest priority. If so, place newly blocked TCB in back of line
	  if(i == NumActive){
			semaPt->list->altPrevious->altNext = RunPt;     // The old last TCB's next pointer now points the newly blocked TCB
		  RunPt->altPrevious = semaPt->list->altPrevious; // newly blocked TCB's previous pointer now points to the old last TCB
	  	semaPt->list->altPrevious = RunPt;              // First TCB's previous now points to newly blocked TCB
	    RunPt->altNext = semaPt->list;                  // Newly blocked TCB's next pt now points to First TCB
		}
  }
	NumBlocked++;
	NumActive--;
}




// ******** OS_Unblock *********
// Remove first TCB from a semaphore's blocked list.
// Decrements NumBlocked and semaPt->Value.
// Called from OS_Signal while interrupts are disabled
// Inputs: semaPt - pointer to semaphore you wish to remove a blocked thread from
// Outputs: true - true when removed TCB has a higher priority than running TCB
//          false - false when removed TCB has a lower priority than running TCB
int OS_Unblock(volatile Sema4Type *semaPt){
  bool highestPriority = false;     // will be set true if unblocked TCB will have the highest priority                        
	
	// remove from blocked list and place back into its slot in active foreground list. Connect list based on priority. In worstcase O(n). Possibly add a seperate living list for each priority level
	TCBType4Ground *tempTCBPt = First4groundPt;             // tempTCBPt used to cycle through foregroundTCB list. Start at front of line
  int i;                                                  // i is order of the current foregroundTCB looking at in list
	for(i = 0; i < NumActive; i++){                         // cycle through all active foregroundTCBs
	  if(semaPt->list->priority <= tempTCBPt->priority){    // if unblocked TCB has higher priority that current TCB in active list, place unblocked TCB in line before this TCB 
		  if(i == 0){                                         // if unblocked TCB has highest priority, will be first in line 
        First4groundPt = semaPt->list;                    // update First4groundPt with unblocked TCB
	      highestPriority = true;                           // unblocked TCB will has the highest priority
			}	
		  semaPt->list->previous = tempTCBPt->previous;       // update unblocked TCB's previous pt  
	    semaPt->list->next = tempTCBPt;                     // update unblocked TCB's next pt
	    tempTCBPt->previous->next = semaPt->list;           // point previous TCB's next pointer to unblocked TCB
	  	tempTCBPt->previous = semaPt->list;                 // old TCB's previous now points to unblocked TCB		 
	    break;                                              // found location in list, break out of search
		}
		else{                                                 // else unblocking TCB has lower priority that current TCB in active list 
	    tempTCBPt = tempTCBPt->next;                        // try next location in list
	  }
	}
	
	// check if waking TCB has lowest priority. If so, place waking TCB in back of line
  if(i == NumActive){
	  First4groundPt->previous->next = semaPt->list;      // The old last TCB next pointer now points to unblocked TCB
    semaPt->list->previous = First4groundPt->previous;  // unblocked TCB's previous pt now points to old last TCB
		First4groundPt->previous = semaPt->list;            // First TCB's previous now points to unblocked TCB
	  semaPt->list->next = First4groundPt;                // unblocked TCB's next pt now points to First TCB
	}
	
	// patch up semaphore's blocked list after removing
  semaPt->list->altPrevious->altNext = semaPt->list->altNext;      // unblocked TCB's previous TCB needs to update its next pointer
  semaPt->list->altNext->altPrevious = semaPt->list->altPrevious;  // unblocked TCB's next TCB needs to update its previous pointer
	semaPt->list = semaPt->list->altNext;                            // update the semaphore's list pointer
	NumBlocked--;
	NumActive++;
  return highestPriority;  // if unblocked TCB has highest priority than return true
}	


// ******** OS_Dummy************
// Added to prevent OS from crashing when NumActive == 0.
// This can happen when functions like OS_Sleep, OS_Block,
// or OS_Kill are called while there is only one active thread
void Dummy(void){
	uint32_t dummyCnt = 0;
	while(1){
    dummyCnt++;
	}
}

