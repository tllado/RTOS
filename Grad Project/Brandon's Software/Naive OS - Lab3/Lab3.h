// Lab3.h
// Runs on TM4C123
// Real Time Operating System for Labs 3
// Brandon Boesch
// Ce Wei
// February 21, 2016


//**********************Defines*********************************

//////////////////////////////////////////////////////////////////////////////
// Only one of the following defines in this block 
// should be uncommented at any given time:
//////////////////////////////////////////////////////////////////////////////
//#define MAIN      
//#define TESTMAIN5
//#define TESTMAIN6
//#define TESTMAIN7 
#define DEBUG
//////////////////////////////////////////////////////////////////////////////
// END BLOCK
//////////////////////////////////////////////////////////////////////////////

#define PROFILER                    // Adds profiler pins PE0-3 when defined

#define FS 400                      // producer/consumer sampling
#define RUNLENGTH (20*FS)           // display results and quit when NumSamples==RUNLENGTH
#define PERIOD TIME_500US           // DAS 2kHz sampling period in system time units
#define JITTERSIZE 64    

// edit these depending on your clock frequency       
#define TIME_1MS    80000          
#define TIME_2MS    (2*TIME_1MS)  
#define TIME_10MS   (10*TIME_1MS)
#define TIME_500US  (TIME_1MS/2)  
#define TIME_250US  (TIME_1MS/5) 
#define TIME_100US  (TIME_1MS/10)
#define TIME_10US   (TIME_1MS/100)
#define TIME_5US    (TIME_1MS/200) 
#define TIME_1US    (TIME_1MS/1000)
//**************************************************************************


//********************Prototypes********************************************

void cr4_fft_64_stm32(void *pssOUT, void *pssIN, unsigned short Nbin); // FFT in cr4_fft_64_stm32.s, STMicroelectronics
short PID_stm32(short Error, short *Coeff);  // PID in PID_stm32.s, STMicroelectronics
void Producer(unsigned long data);
void Display(void); 
//**************************************************************************


//********************Externs***********************************************
extern unsigned long NumSamples;    // incremented every ADC sample, in Producer
extern long MaxJitter;              // largest time jitter between interrupts in usec
extern unsigned long DataLost;      // data sent by Producer, but not received by Consumer
extern unsigned long FilterWork;    // number of digital filter calculations finished
extern unsigned long PIDWork;       // current number of PID calculations finished
extern long x[64],y[64];            // input and output arrays for FFT
//**************************************************************************
