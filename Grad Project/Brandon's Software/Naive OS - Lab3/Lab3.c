// Lab3.c
// Runs on TM4C123
// Real Time Operating System for Labs 3
// Brandon Boesch
// Ce Wei
// February 21, 2016


//************Timer Resources*******************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - OS_AddPeriodicThread(), Os_AddPeriodicThread() ; priority = 0
//  Timer2A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//  Timer3A - OS_Sleep() ; priority = 3
//**********************************************

#include <string.h> 
#include <stdint.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab3.h"
#include "ST7735.h"
#include "UART.h"
#include "OS.h"
#include "Profiler.h"
#include "Interpreter.h"


void LowerPriority (void){
	while(1){
	  OS_Sleep(100);
	}
}

void PriorityListsTest(void){
	for(int i = 0; i < 49; i++){
		OS_AddThread(&Dummy, 128, 1);
	}
	OS_AddThread(&LowerPriority, 128, 2); 
	OS_Kill();
}


int main(void){  
	// intialize globals
         
  // initalize modules	
	OS_Init();                                      // initialize OS, disable interrupts
	PortF_Init();                                   // initialize Port F profiling
	
  // create initial foreground threads
	OS_AddThread(&PriorityListsTest, 128, 1);
	
  OS_Launch(TIMESLICE);                           // doesn't return, interrupts enabled in here
  return 0;                                       // this never executes
}
