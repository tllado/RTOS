;/*****************************************************************************/
; OSasm.s: low-level OS commands, written in assembly                       */
; Runs on LM4F120/TM4C123
; A very simple real time operating system with minimal features.
; Daniel Valvano
; January 29, 2015
;
; This example accompanies the book
;  "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
;  ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015
;
;  Programs 4.4 through 4.12, section 4.2
;
;Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
;    You may use, edit, run or distribute this file
;    as long as the above copyright notice remains
; THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
; OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
; VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
; OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
; For more information about my classes, my research, and my books, see
; http://users.ece.utexas.edu/~valvano/
; */

PE3 EQU 0x40024020   ; access PE3 for profiling

        AREA |.text|, CODE, READONLY, ALIGN=2
        THUMB
        REQUIRE8
        PRESERVE8

        EXTERN  RunPt            ; currently running thread
        EXPORT  StartOS
        EXPORT  SysTick_Handler
		EXPORT  OS_Wait_ASM
		EXPORT	OS_Signal_ASM
		EXPORT  OS_bWait_ASM
		EXPORT	OS_bSignal_ASM

SysTick_Handler                ; 1)   Saves R0-R3,R12,LR,PC,PSR
    CPSID   I                  ; 2)   Prevent interrupt during switch
    PUSH    {R4-R11}           ; 3)   Save remaining regs r4-11
    ;LDR     R4, =PE3           ; 4.1) Toggle PE3 twice. R4 = &PE3
    ;LDR     R5, [R4]           ; 4.2) R5 = PE3
    ;EOR     R5, #0x08		   ; 4.3) R5 ^= 0x8
	;STR     R5, [R4]           ; 4.4) PE3 ^= 0x8
	;EOR     R5, #0x08          ; 4.5) R5 ^= 0x8
	;STR     R5, [R4]           ; 4.6) PE3 ^= 0x8
    LDR     R0, =RunPt         ; 5.1) R0=pointer to RunPt, old thread
    LDR     R1, [R0]           ; 5.2) R1 = RunPt
    STR     SP, [R1]           ; 6)   Save SP into TCB
sleeping                       ; 7.1) Change RunPt to next if not sleeping	
    LDR     R1, [R1, #4]       ; 7.2) R1 = RunPt->next
	LDR     R2, [R1, #8]       ; 7.3) R2 = RunPt->sleepCnt
	CMP     R2, #0             ; 7.4) check is sleepCnt == 0
	BNE     sleeping           ; 7.5) branch back if sleepCnt != 0
    STR     R1, [R0]           ; 7.6) RunPt = R1
    LDR     SP, [R1]           ; 8)   new thread SP; SP = RunPt->sp;
	;EOR     R5, #0x08		   ; 9.1) Toggle PE3. R5 ^= 0x8
	;STR     R5, [R4]           ; 9.2) PE3 ^= 0x8
    POP     {R4-R11}           ; 10)  restore regs r4-11
    CPSIE   I                  ; 11)  tasks run with interrupts enabled
    BX      LR                 ; 12)  restore R0-R3,R12,LR,PC,PSR


StartOS
    LDR     R0, =RunPt         ; currently running thread
    LDR     R2, [R0]           ; R2 = value of RunPt
    LDR     SP, [R2]           ; new thread SP; SP = RunPt->stackPointer;
    POP     {R4-R11}           ; restore regs r4-11
    POP     {R0-R3}            ; restore regs r0-3
    POP     {R12}
    POP     {LR}               ; discard LR from initial stack
    POP     {LR}               ; start location
    POP     {R1}               ; discard PSR
    CPSIE   I                  ; Enable interrupts at processor level
    BX      LR                 ; start first thread

OS_Wait_ASM
	LDREX	R1, [R0]		;Counter
	SUBS	R1, #1			;Counter - 1
	ITT		PL				;OK if >= 0
	STREXPL	R2, R1, [R0]	;try update
	CMPPL	R2, #0			;succeed?
	BNE		OS_Wait_ASM		;no, try again
	BX		LR				
	
	
OS_Signal_ASM
	LDREX	R1, [R0]		;Counter
	ADD		R1, #1			;Counter + 1
	STREX	R2, R1, [R0]	;try update
	CMP		R2, #0			;succeed?
	BNE		OS_Signal_ASM	;try again
	BX		LR
	
OS_bWait_ASM	
	LDREX	R1, [R0]		;Counter
	MOV 	R1, #0			;Counter = 0,
	ITT		PL				;OK if == 1, 
	STREXPL	R2, R1, [R0]	;try update
	CMPPL	R2, #0			;succeed?
	BNE		OS_bWait_ASM    ;no, try again
	BX		LR	


OS_bSignal_ASM
	LDREX	R1, [R0]		;Counter
	MOV		R1, #1			;Counter = 1
	STREX	R2, R1, [R0]	;try update
	CMP		R2, #0			;succeed?
	BNE		OS_bSignal_ASM	;try again
	BX		LR

    ALIGN
    END

