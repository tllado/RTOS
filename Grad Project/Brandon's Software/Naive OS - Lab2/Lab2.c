// Lab2.c
// Runs on LM4F120/TM4C123
// Real Time Operating System for Labs 2 and 3
// Lab2 Part 1: Testmain1 and Testmain2
// Lab2 Part 2: Testmain3 Testmain4  and main
// Lab3: Testmain5 Testmain6, Testmain7, and main (with SW2)

// Jonathan W. Valvano 1/31/14, valvano@mail.utexas.edu
// EE445M/EE380L.6 
// You may use, edit, run or distribute this file 
// You are free to change the syntax/organization of this file

// LED outputs to logic analyzer for OS profile 
// PF1 is preemptive thread switch
// PF2 is periodic task, samples PD3
// PF3 is SW1 task (touch PF4 button)

// Button inputs
// PF0 is SW2 task (Lab3)
// PF4 is SW1 button input

// Analog inputs
// PD3 Ain3 sampled at 2k, sequencer 2, by DAS software start in ISR
// PD2 Ain5 sampled at 250Hz, sequencer 3, by Producer, timer tigger

//************Timer Resources*******************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - OS_AddPeriodicThread() ; priority = variable
//  Timer2A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//  Timer3A - OS_Sleep() ; priority = 4
//**********************************************

#include <string.h> 
#include <stdint.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab2.h"
#include "UART.h"
#include "OS.h"
#include "Profiler.h"

#define TIME_1MS    80000   

//********************Globals***********************************************

//**************************************************************************


// foreground thread
// called from SleepTest()
void SleepingThreads(void){
	OS_Sleep(20000);
	while(1){}
}

// foreground thread
// called from SleepTest()
void ActiveThread(void){
	while(1){
	  PF1 ^= 0x02;
	  OS_Suspend();
	}
}


// foreground thread
// adds threads used in SleepTest()
void SleepTest(void){
	for(int i = 0; i<99; i++){
		OS_AddThread(&SleepingThreads,128,1);
	}
	OS_AddThread(&ActiveThread, 128, 1);
  OS_Kill();
}

	
int main(void){ 
  OS_Init();           // initialize, disable interrupts
	PortF_Init();        // initialize Profiler
  
	OS_AddThread(&SleepTest, 128, 1);               // tests optimized sleeping/blocking. Set MAX_NUM_4GROUND > 100. Set stack size to 32
  OS_Launch(TIME_1MS);                      // doesn't return, interrupts enabled in here
	return 0;            // this never executes
}
