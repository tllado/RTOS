// Lab2.h
// Runs on LM4F120/TM4C123
// Real Time Operating System for Labs 2 and 3
// Lab2 Part 1: Testmain1 and Testmain2
// Lab2 Part 2: Testmain3 Testmain4  and main
// Lab3: Testmain5 Testmain6, Testmain7, and main (with SW2)


//**********************Defines*********************************

//////////////////////////////////////////////////////////////////////////////
// Only one of the following defines in this block 
// should be uncommented at any given time:
//////////////////////////////////////////////////////////////////////////////
#define MAIN      
//#define TESTMAIN1 
//#define TESTMAIN2 
//#define TESTMAIN3 
//#define TESTMAIN4 
//#define TESTMAIN7 
//#define DEBUG1
//////////////////////////////////////////////////////////////////////////////
// END BLOCK
//////////////////////////////////////////////////////////////////////////////

#define PROFILER                    // Adds profiler pins PE0-3 when defined

#define FS 400                      // producer/consumer sampling
#define RUNLENGTH (20*FS)           // display results and quit when NumSamples==RUNLENGTH
#define PERIOD TIME_500US           // DAS 2kHz sampling period in system time units
#define JITTERSIZE 64    
//**************************************************************************


//********************Prototypes********************************************
// FFT in cr4_fft_64_stm32.s, STMicroelectronics
void cr4_fft_64_stm32(void *pssOUT, void *pssIN, unsigned short Nbin);
// PID in PID_stm32.s, STMicroelectronics
short PID_stm32(short Error, short *Coeff);

void Producer(unsigned long data);
void Display(void); 
//**************************************************************************


//********************Externs***********************************************
extern unsigned long NumCreated;    // number of foreground threads created
extern unsigned long NumSamples;    // incremented every ADC sample, in Producer
extern long MaxJitter;              // largest time jitter between interrupts in usec
extern unsigned long DataLost;      // data sent by Producer, but not received by Consumer
extern unsigned long FilterWork;    // number of digital filter calculations finished
extern unsigned long PIDWork;       // current number of PID calculations finished
extern long x[64],y[64];            // input and output arrays for FFT
//**************************************************************************
