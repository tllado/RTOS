// GradProject.c
// Runs on TM4C123
// Real Time Operating System for Graduate Project
// Brandon Boesch
// Ce Wei
// April 17th, 2016


//************ Timer Resources *************************************************
//  SysTick - OS_Launch() ; priority = 7
//  Timer4A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 0
//  Timer5A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//******************************************************************************


//************ OS's Backgroud Threads ******************************************
//  OS_Aging - priority = 7 (only added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 3
//******************************************************************************


//******************** Libraries ***********************************************
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "GradProject.h"
#include "OS.h"
#include "UART.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"
#include "Interrupts.h"
#include "Buttons.h"
#include "ST7735.h"
#include "DMASoftware.h"
//**************************************************************************


//******************** Globals ***********************************************
uint32_t IdleCount;       // counts how many iterations IdleTask() is ran.
uint8_t TestArray[100];   // holds the results from the background thread manager test
#define SIZE 128
//****************************************************************************


//******** IdleTask  *************** 
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
		PF1 ^= 0x02;        // debugging profiler 
    IdleCount++;        // debugging 
	//		if(DMA_Status() == 0){
//			OS_bSignal(&DMA_Soft_Sema4);
//		}
//		if(DMA_Status_Read() == 0){
//			OS_bSignal(&DMA_Read_Sema4);
//		}
//		if(DMA_Status_Write() == 0){
//			OS_bSignal(&DMA_Write_Sema4);
//		}
		WaitForInterrupt(); // low-power mode
  }
}


//******** Interpreter **************
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}


// called by PriorityListTest()
void LowerPriority (void){
	while(1){
	  OS_Sleep(100);
	}
}


// creates 99 higher priority threads,
// and 1 lower priority thread, to show
// how long it takes to re-inseart a waking
// thread back into the list of schedule-able threads
void PriorityListsTest(void){
	for(int i = 0; i < 99; i++){
		OS_AddThread(&Dummy, 128, 1);
	}
	OS_AddThread(&LowerPriority, 128, 2); 
	OS_Kill();
}


// 100 periodic background threads, used in
// BackgroundTest()
void Periodic0(void){
  TestArray[0] = 1;
}
void Periodic1(void){
  TestArray[1] = 1;
}
void Periodic2(void){
  TestArray[2] = 1;
}
void Periodic3(void){
  TestArray[3] = 1;
}
void Periodic4(void){
  TestArray[4] = 1;
}
void Periodic5(void){
  TestArray[5] = 1;
}
void Periodic6(void){
  TestArray[6] = 1;
}
void Periodic7(void){
  TestArray[7] = 1;
}
void Periodic8(void){
  TestArray[8] = 1;
}
void Periodic9(void){
  TestArray[9] = 1;
}
void Periodic10(void){
  TestArray[10] = 1;
}
void Periodic11(void){
  TestArray[11] = 1;
}
void Periodic12(void){
  TestArray[12] = 1;
}
void Periodic13(void){
  TestArray[13] = 1;
}
void Periodic14(void){
  TestArray[14] = 1;
}
void Periodic15(void){
  TestArray[15] = 1;
}
void Periodic16(void){
  TestArray[16] = 1;
}
void Periodic17(void){
  TestArray[17] = 1;
}
void Periodic18(void){
  TestArray[18] = 1;
}
void Periodic19(void){
  TestArray[19] = 1;
}
void Periodic20(void){
  TestArray[20] = 1;
}
void Periodic21(void){
  TestArray[21] = 1;
}
void Periodic22(void){
  TestArray[22] = 1;
}
void Periodic23(void){
  TestArray[23] = 1;
}
void Periodic24(void){
  TestArray[24] = 1;
}
void Periodic25(void){
  TestArray[25] = 1;
}
void Periodic26(void){
  TestArray[26] = 1;
}
void Periodic27(void){
  TestArray[27] = 1;
}
void Periodic28(void){
  TestArray[28] = 1;
}
void Periodic29(void){
  TestArray[29] = 1;
}
void Periodic30(void){
  TestArray[30] = 1;
}
void Periodic31(void){
  TestArray[31] = 1;
}
void Periodic32(void){
  TestArray[32] = 1;
}
void Periodic33(void){
  TestArray[33] = 1;
}
void Periodic34(void){
  TestArray[34] = 1;
}
void Periodic35(void){
  TestArray[35] = 1;
}
void Periodic36(void){
  TestArray[36] = 1;
}
void Periodic37(void){
  TestArray[37] = 1;
}
void Periodic38(void){
  TestArray[38] = 1;
}
void Periodic39(void){
  TestArray[39] = 1;
}
void Periodic40(void){
  TestArray[40] = 1;
}
void Periodic41(void){
  TestArray[41] = 1;
}
void Periodic42(void){
  TestArray[42] = 1;
}
void Periodic43(void){
  TestArray[43] = 1;
}
void Periodic44(void){
  TestArray[44] = 1;
}
void Periodic45(void){
  TestArray[45] = 1;
}
void Periodic46(void){
  TestArray[46] = 1;
}
void Periodic47(void){
  TestArray[47] = 1;
}
void Periodic48(void){
  TestArray[48] = 1;
}
void Periodic49(void){
  TestArray[49] = 1;
}
void Periodic50(void){
  TestArray[50] = 1;
}
void Periodic51(void){
  TestArray[51] = 1;
}
void Periodic52(void){
  TestArray[52] = 1;
}
void Periodic53(void){
  TestArray[53] = 1;
}
void Periodic54(void){
  TestArray[54] = 1;
}
void Periodic55(void){
  TestArray[55] = 1;
}
void Periodic56(void){
  TestArray[56] = 1;
}
void Periodic57(void){
  TestArray[57] = 1;
}
void Periodic58(void){
  TestArray[58] = 1;
}
void Periodic59(void){
  TestArray[59] = 1;
}
void Periodic60(void){
  TestArray[60] = 1;
}
void Periodic61(void){
  TestArray[61] = 1;
}
void Periodic62(void){
  TestArray[62] = 1;
}
void Periodic63(void){
  TestArray[63] = 1;
}
void Periodic64(void){
  TestArray[64] = 1;
}
void Periodic65(void){
  TestArray[65] = 1;
}
void Periodic66(void){
  TestArray[66] = 1;
}
void Periodic67(void){
  TestArray[67] = 1;
}
void Periodic68(void){
  TestArray[68] = 1;
}
void Periodic69(void){
  TestArray[69] = 1;
}
void Periodic70(void){
  TestArray[70] = 1;
}
void Periodic71(void){
  TestArray[71] = 1;
}
void Periodic72(void){
  TestArray[72] = 1;
}
void Periodic73(void){
  TestArray[73] = 1;
}
void Periodic74(void){
  TestArray[74] = 1;
}
void Periodic75(void){
  TestArray[75] = 1;
}
void Periodic76(void){
  TestArray[76] = 1;
}
void Periodic77(void){
  TestArray[77] = 1;
}
void Periodic78(void){
  TestArray[78] = 1;
}
void Periodic79(void){
  TestArray[79] = 1;
}
void Periodic80(void){
  TestArray[80] = 1;
}
void Periodic81(void){
  TestArray[81] = 1;
}
void Periodic82(void){
  TestArray[82] = 1;
}
void Periodic83(void){
  TestArray[83] = 1;
}
void Periodic84(void){
  TestArray[84] = 1;
}
void Periodic85(void){
  TestArray[85] = 1;
}
void Periodic86(void){
  TestArray[86] = 1;
}
void Periodic87(void){
  TestArray[87] = 1;
}
void Periodic88(void){
  TestArray[88] = 1;
}
void Periodic89(void){
  TestArray[89] = 1;
}
void Periodic90(void){
  TestArray[90] = 1;
}
void Periodic91(void){
  TestArray[91] = 1;
}
void Periodic92(void){
  TestArray[92] = 1;
}
void Periodic93(void){
  TestArray[93] = 1;
}
void Periodic94(void){
  TestArray[94] = 1;
}
void Periodic95(void){
  TestArray[95] = 1;
}
void Periodic96(void){
  TestArray[96] = 1;
}
void Periodic97(void){
  TestArray[97] = 1;
}
void Periodic98(void){
  TestArray[98] = 1;
}
void Periodic99(void){
  TestArray[99] = 1;
}

// adds 100 periodic background threads to test
// background manager.  Called from Interpreter.
void BackgroundTest(void){
	int32_t status = StartCritical();
	OS_AddPeriodicThread(&Periodic0, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic1, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic2, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic3, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic4, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic5, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic6, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic7, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic8, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic9, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic10, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic11, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic12, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic13, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic14, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic15, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic16, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic17, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic18, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic19, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic20, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic21, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic22, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic23, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic24, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic25, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic26, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic27, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic28, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic29, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic30, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic31, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic32, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic33, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic34, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic35, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic36, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic37, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic38, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic39, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic40, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic41, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic42, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic43, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic44, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic45, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic46, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic47, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic48, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic49, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic50, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic51, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic52, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic53, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic54, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic55, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic56, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic57, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic58, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic59, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic60, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic61, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic62, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic63, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic64, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic65, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic66, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic67, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic68, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic69, TIME_100MS, 1);
  OS_AddPeriodicThread(&Periodic70, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic71, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic72, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic73, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic74, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic75, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic76, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic77, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic78, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic79, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic80, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic81, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic82, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic83, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic84, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic85, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic86, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic87, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic88, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic89, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic90, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic91, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic92, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic93, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic94, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic95, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic96, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic97, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic98, TIME_100MS, 1);
	OS_AddPeriodicThread(&Periodic99, TIME_100MS, 1);
  EndCritical(status);
	OS_Kill();
}


// foreground thread
// added in AgingTest()
void AgingThread_High(void){
	while(1){
		PF2 ^= 0x04;
		// add a do-nothing delay
		for(int i = 0; i < 10000; i++){
			
		}
	}
}


// foreground thread
// added in AgingTest()
void AgingThread_Low(void){
	while(1){
		PF3 ^= 0x08;
		// add a do-nothing delay
		for(int i = 0; i < 10000; i++){
			
		}
	}
}

// foreground thread
// tests Aging.
void AgingTest(void){
	OS_AddThread(&AgingThread_High, 128, 1);
	OS_AddThread(&AgingThread_Low, 128, 2);
	OS_Kill();
}

// foreground thread
// added in AdjustableSchedulerTest
void AdjustableThread1(void){
	while(1){
	  PF1 ^= 0x02;
	  // add a do-nothing delay
	  for(int i = 0; i < 10000; i++){
		}
		OS_Suspend();		
	}
}


// foreground thread
// added in AdjustableSchedulerTest
void AdjustableThread2(void){
	while(1){
	  PF2 ^= 0x04;
	  // add a do-nothing delay
	  for(int i = 0; i < 10000; i++){
		}
		OS_Suspend();		
	}
}


// foreground thread
// added in AdjustableSchedulerTest
void AdjustableThread3(void){
	while(1){
	  PF3 ^= 0x08;
	  // add a do-nothing delay
	  for(int i = 0; i < 10000; i++){
		}
		OS_Suspend();		
	}
}




// foreground thread
// tests the adjustable scheduler
void AdjustableSchedulerTest(void){
	OS_AddThread(&AdjustableThread1, 128, 0);
	OS_AddThread(&AdjustableThread2, 128, 0);
	OS_AddThread(&AdjustableThread3, 128, 2);
	OS_Kill();
}

// periodic background task
// called from ErrorCheckTest()
void ErrorPeriodicTask(void){
	// do nothing
}

// foreground thread
// uncomment the line corrisponding to the error check you wish to test
void ErrorCheckTest(void){
//	OS_AddPeriodicThread(&ErrorPeriodicTask, TIME_500US, 1);
  OS_Sleep(1);	  // change both #define BACKGROUND_PERIOD and #define SLEEP_PERIOD to a value less than 1ms first
//  for(int i = 0; i <= MAX_NUM_4GROUND; i++){  // change both #define BACKGROUND_PERIOD and #define SLEEP_PERIOD back to 1MS
//		OS_AddThread(&Dummy, 128, 1);
//	}
	for(int i = 0; i <= MAX_NUM_BGROUND; i++){  // change both #define BACKGROUND_PERIOD and #define SLEEP_PERIOD back to 1MS
		OS_AddPeriodicThread(&ErrorPeriodicTask, TIME_1MS, 1);
	}
	
	OS_Kill();
}

// foreground thread
// called from SleepTest()
void SleepingThreads(void){
	OS_Sleep(20000);
	while(1){}
}

// foreground thread
// called from SleepTest()
void ActiveThread(void){
	while(1){
	  PF1 ^= 0x02;
	  OS_Suspend();
	}
}


// foreground thread
// adds threads used in SleepTest()
void SleepTest(void){
	for(int i = 0; i<99; i++){
		OS_AddThread(&SleepingThreads,128,1);
	}
	OS_AddThread(&ActiveThread, 128, 1);
  OS_Kill();
}



void DMATest(void){
	DMA_Init_Read();  // initialize DMA channel 30 for software transfer
	OS_AddThread(&IdleTask, 128, 7);                   // runs when nothing useful to do
	for(int i=0;i<SIZE;i++){
      SrcBuf[i] = i;
      DestBuf[i] = 0;
			ReadBuf[i] = 0;
			WriteBuf[i] = 0;
  }
	//OS_AddThread(&DMA_Soft, 128, 5);                   // runs when nothing useful to do
	//OS_AddThread(&Normal_Soft, 128, 5);
	
	//OS_AddThread(&DMA_Read, 128, 5);                   // runs when nothing useful to do
	//OS_AddThread(&Normal_Read, 128, 5);
	
	//OS_AddThread(&DMA_Write, 128, 5);                   // runs when nothing useful to do
	OS_AddThread(&Normal_Write, 128, 5);
	OS_Kill();
}


int main(void){  
	// intialize globals
	IdleCount = 0;
         
  // initalize modules	
	OS_Init();                                      // initialize OS, disable interrupts
	PortF_Init();                                   // initialize Port F profiling
	
  // create initial foreground threads
//  OS_AddThread(&Interpreter, 128, 6);             // add command line interpreter
//  OS_AddThread(&IdleTask, 128, 7);                // runs when nothing useful to do
//	OS_AddThread(&PriorityListsTest, 128, 1);       // tests tiered priority list.  Set MAX_NUM_4GROUND in OS.h. Set stacksize to 32
//  OS_AddThread(&AgingTest, 128, 1);               // tests aging. Run twice, once with aging removed, and once with aging added. Add profiling pins to choosen threads   
//	OS_AddThread(&AdjustableSchedulerTest, 128, 1);   // tests the adjustable scheduler.  Need to run two seperate times, switching the scheduler in OS.h each time
//	OS_AddThread(&ErrorCheckTest, 128 ,1);          // tests the automated error checker
	OS_AddThread(&SleepTest, 128, 1);               // tests optimized sleeping/blocking. Set MAX_NUM_4GROUND > 100. Set stack size to 32
//  OS_AddThread(&DMATest, 128, 1);  
	
  OS_Launch(TIMESLICE);                           // doesn't return, interrupts enabled in here
  return 0;                                       // this never executes
}
