// DMASoftware.c
// Runs on LM4F120/TM4C123
// Software triggered memory block transfer
// Jonathan Valvano
// September 11, 2013

/* This example accompanies the book
   "Embedded Systems: Real Time Operating Systems for ARM Cortex M Microcontrollers",
   ISBN: 978-1466468863, Jonathan Valvano, copyright (c) 2014
   Section 6.4.5, Program 6.1

 Copyright 2014 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "DMASoftware.h"
#include "ST7735.h"
#include "GradProject.h"
#include "OS.h"



// The control table used by the uDMA controller.  This table must be aligned to a 1024 byte boundary.
// each channel has source,destination,control,pad (pad word is ignored)
uint32_t ucControlTable[256] __attribute__ ((aligned(1024)));
// channel 30 is at indices 120,121,122 (primary source,destination,control) and
//               at indices 248,249,250 (alternate source,destination,control not used)
#define CH30 (30*4)
#define BIT30 0x40000000

// Software uses uDMA channel 8 encoding 3, Read from port B to RAM
#define CH8 (8*4)
#define BIT8 0x00000100


uint32_t DMACount;
uint32_t NormalCount;
uint32_t TEST = 100000;

uint32_t SrcBuf[SIZE],DestBuf[SIZE];
uint8_t  ReadBuf[SIZE];
uint8_t  WriteBuf[SIZE];

// ************DMA_Init*****************
// Initialize the memory to memory transfer
// This needs to be called once before requesting a transfer
// Inputs:  none
// Outputs: none
void DMA_Init(void){  int i;
  volatile uint32_t delay; 
  for(i=0; i<256; i++){
    ucControlTable[i] = 0;
  }
  SYSCTL_RCGCDMA_R = 0x01;    // �DMA Module Run Mode Clock Gating Control
  delay = SYSCTL_RCGCDMA_R;   // allow time to finish 
  UDMA_CFG_R = 0x01;          // MASTEN Controller Master Enable
  UDMA_CTLBASE_R = (uint32_t)ucControlTable;
  UDMA_PRIOCLR_R = BIT30;     // default, not high priority
  UDMA_ALTCLR_R = BIT30;      // use primary control
  UDMA_USEBURSTCLR_R = BIT30; // responds to both burst and single requests
  UDMA_REQMASKCLR_R = BIT30;  // allow the �DMA controller to recognize requests for this channel
}
// ************DMA_Transfer_RAM2RAM*****************
// Called to transfer 32-bit words from source to destination
// Inputs:  source is a pointer to the first 32-bit word of the original data
//          destination is a pointer to a place to put the copy
//          count is the number of words to transfer (max is 1024 words)
// Outputs: none
// This routine does not wait for completion
void DMA_Transfer_RAM2RAM(uint32_t *source, uint32_t *destination, uint32_t count){ 
  ucControlTable[CH30]   = (uint32_t)source+count*4-1;       // last address
  ucControlTable[CH30+1] = (uint32_t)destination+count*4-1;  // last address
  ucControlTable[CH30+2] = 0xAA00C002+((count-1)<<4);             // DMA Channel Control Word (DMACHCTL)
/* DMACHCTL          Bits    Value Description
   DSTINC            31:30   10    32-bit destination address increment
   DSTSIZE           29:28   10    32-bit destination data size
   SRCINC            27:26   10    32-bit source address increment
   SRCSIZE           25:24   10    32-bit source data size
   reserved          23:18   0     Reserved  
   ARBSIZE           17:14   0011  Arbitrates after 8 transfers
   XFERSIZE          13:4  count-1 Transfer count items
   NXTUSEBURST       3       0     N/A for this transfer type
   XFERMODE          2:0     010   Use Auto-request transfer mode
  */
  UDMA_ENASET_R = BIT30;  // �DMA Channel 30 is enabled.
  UDMA_SWREQ_R = BIT30;   // software start, 
  // bit 30 in UDMA_ENASET_R become clear when done
  // bits 2:0 ucControlTable[CH30+2] become clear when done
  // vector 62, NVIC interrupt 46, vector address 0x0000.00F8 could be armed or �DMA Software interrupt
}

// ************DMA_Status*****************
// Can be used to check the status of a previous request
// Inputs:  none
// Outputs: true if still active, false if complete
// This routine does not wait for completion
uint32_t DMA_Status(void){ 
  return (UDMA_ENASET_R&BIT30);  // �DMA Channel 30 enable nit is high if active
}

// ************PortB_Inite_Read*****************
// initialize the port B for the DMA Read testing
// Inputs:  none
// Outputs: none
void PortB_Init(void){
	int t;
  SYSCTL_RCGCGPIO_R |= 0x02;   // enable Port B clock
  t = 0;                       // allow time to finish, PB7-4 inputs
  GPIO_PORTB_DIR_R = 0xFF;     // make PB0-7 output
  GPIO_PORTB_AFSEL_R &= ~0xFF; // disable alt funct on PF7-0
  GPIO_PORTB_DEN_R |= 0xFF;    // enable digital I/O on PF7-0
  GPIO_PORTB_PCTL_R = 0;       // all pins are regular I/O
  GPIO_PORTB_AMSEL_R = 0;      // disable analog functionality on Port B
}



// ************DMA_Init_Read*****************
// Initialize the PortF to memory transfer, triggered by timer 5A
// This needs to be called once before requesting a transfer
// The source address is fixed, destination address incremented each byte
// Inputs:  period in usec
// Outputs: none
void DMA_Init_Read(void){  
	int i;
  volatile uint32_t delay; 
  for(i=0; i<256; i++){
    ucControlTable[i] = 0;
  }
  SYSCTL_RCGCDMA_R = 0x01;    // �DMA Module Run Mode Clock Gating Control
  delay = SYSCTL_RCGCDMA_R;   // allow time to finish 
  UDMA_CFG_R = 0x01;          // MASTEN Controller Master Enable
  UDMA_CTLBASE_R = (uint32_t)ucControlTable;
  UDMA_PRIOCLR_R = BIT30;     // default, not high priority
  UDMA_ALTCLR_R = BIT30;      // use primary control
  UDMA_USEBURSTCLR_R = BIT30; // responds to both burst and single requests
  UDMA_REQMASKCLR_R = BIT30;  // allow the �DMA controller to recognize requests for this channel	
}

// ************DMA_Transfer_Read*****************
// Called to transfer bytes from source to destination
// The source address is fixed, destination address incremented each byte
// Inputs:  source is a pointer to the 32-bit I/O, least significant byte to read
//          destination is a pointer to a place to put the copy
//          count is the number of bytes to transfer (max is 1024 words)
// Outputs: none
// This routine does not wait for completion
void DMA_Transfer_Read(volatile uint32_t *source, uint8_t *destination, uint32_t count){
  ucControlTable[CH30]   = (uint32_t)source+count*4-1;       // last address
  ucControlTable[CH30+1] = (uint32_t)destination+count*4-1;  // last address
  ucControlTable[CH30+2] = 0xAA00C002+((count-1)<<4);             // DMA Channel Control Word (DMACHCTL)
/* DMACHCTL          Bits    Value Description
   DSTINC            31:30   10    32-bit destination address increment
   DSTSIZE           29:28   10    32-bit destination data size
   SRCINC            27:26   10    32-bit source address increment
   SRCSIZE           25:24   10    32-bit source data size
   reserved          23:18   0     Reserved  
   ARBSIZE           17:14   0011  Arbitrates after 8 transfers
   XFERSIZE          13:4  count-1 Transfer count items
   NXTUSEBURST       3       0     N/A for this transfer type
   XFERMODE          2:0     010   Use Auto-request transfer mode
  */
  UDMA_ENASET_R = BIT30;  // �DMA Channel 30 is enabled.
  UDMA_SWREQ_R = BIT30;   // software start, 
}

// ************DMA_Status_Read*****************
// Can be used to check the status of a previous request
// Inputs:  none
// Outputs: true if still active, false if complete
uint32_t DMA_Status_Read(void){ 
  return (UDMA_ENASET_R&BIT30);  // �DMA Channel 30 enable nit is high if active	
  //return (UDMA_ENASET_R&BIT8);  // �DMA Channel 8 enable bit is high if active
}


// ************DMA_Init_Write*****************
// Initialize the PortF to memory transfer, triggered by timer 5A
// This needs to be called once before requesting a transfer
// The source address is fixed, destination address incremented each byte
// Inputs:  period in usec
// Outputs: none
void DMA_Init_Write(void){
	int i;
  volatile uint32_t delay; 
  for(i=0; i<256; i++){
    ucControlTable[i] = 0;
  }
  SYSCTL_RCGCDMA_R = 0x01;    // �DMA Module Run Mode Clock Gating Control
  delay = SYSCTL_RCGCDMA_R;   // allow time to finish 
  UDMA_CFG_R = 0x01;          // MASTEN Controller Master Enable
  UDMA_CTLBASE_R = (uint32_t)ucControlTable;
  UDMA_PRIOCLR_R = BIT30;     // default, not high priority
  UDMA_ALTCLR_R = BIT30;      // use primary control
  UDMA_USEBURSTCLR_R = BIT30; // responds to both burst and single requests
  UDMA_REQMASKCLR_R = BIT30;  // allow the �DMA controller to recognize requests for this channel	
}

// ************DMA_Transfer_Read*****************
// Called to transfer bytes from source to destination
// The source address is fixed, destination address incremented each byte
// Inputs:  source is a pointer to the 32-bit I/O, least significant byte to read
//          destination is a pointer to a place to put the copy
//          count is the number of bytes to transfer (max is 1024 words)
// Outputs: none
// This routine does not wait for completion
void DMA_Transfer_Write(uint8_t *source, volatile uint32_t *destination, uint32_t count){
  ucControlTable[CH30]   = (uint32_t)source+count*4-1;       // last address
  ucControlTable[CH30+1] = (uint32_t)destination+count*4-1;  // last address
  ucControlTable[CH30+2] = 0xAA00C002+((count-1)<<4);             // DMA Channel Control Word (DMACHCTL)
  UDMA_ENASET_R = BIT30;  // �DMA Channel 30 is enabled.
  UDMA_SWREQ_R = BIT30;   // software start, 
}

// ************DMA_Status_Read*****************
// Can be used to check the status of a previous request
// Inputs:  none
// Outputs: true if still active, false if complete
uint32_t DMA_Status_Write(void){ 
  return (UDMA_ENASET_R&BIT30);  // �DMA Channel 30 enable nit is high if active	
  //return (UDMA_ENASET_R&BIT8);  // �DMA Channel 8 enable bit is high if active
}



void DMA_Soft(void){  volatile uint32_t delay;
  while(1){
    while(DMA_Status()); // wait for idle
    DMA_Transfer_RAM2RAM(SrcBuf,DestBuf,SIZE);	//one 32 bit word
//    for(delay = 0; delay < 600000; delay++){
//    }		
		//ST7735_OutUDec(DMACount);
		//ST7735_SetCursor(0, 0);
		DMACount++;
		if(DMACount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "DMA Count is: ",DMACount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 423
			//200000 - 843
			//500000 - 2118
			//1000000 - 4221
			//2000000 - 8448
			OS_Kill();
		}
  }
}


void Normal_Soft(void){  volatile uint32_t delay; uint32_t i;
  while(1){
    for(i=0;i<SIZE;i++){
      DestBuf[i] = SrcBuf[i];
    }
		NormalCount++;
		if(NormalCount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "Normal Count: ",NormalCount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 1473
			//200000 - 2943
			//500000 - 7344
			//1000000 - 14688
			//2000000 - 29373
			OS_Kill();
		}
  }
}

void DMA_Read(void){
	PortB_Init();
  while(1){
    while(DMA_Status_Read()); // wait for idle
    DMA_Transfer_Read(&GPIO_PORTB_DATA_R, ReadBuf, SIZE);
//		ST7735_OutUDec(DMACount);
//		ST7735_SetCursor(0, 0);
		DMACount++;
		if(DMACount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "DMA Count is: ",DMACount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 490
			//200000 - 993
			//500000 - 2487
			//1000000 - 4968
			//2000000 - 9933
			OS_Kill();
		}
  }
}

void Normal_Read(void){
	PortB_Init();
  while(1){
    for(int i=0;i<SIZE;i++){
      ReadBuf[i] = GPIO_PORTB_DATA_R;
    }
		NormalCount++;
		if(NormalCount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "Normal Count: ",NormalCount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 1308
			//200000 - 2616
			//500000 - 6543
			//1000000 - 13083
			//2000000 - 26157
			OS_Kill();
		}
  }
}

void DMA_Write(void){
	PortB_Init();
  while(1){
    while(DMA_Status_Write()); // wait for idle
		DMA_Transfer_Write(WriteBuf, &GPIO_PORTB_DATA_R, SIZE);
//		ST7735_OutUDec(DMACount);
//		ST7735_SetCursor(0, 0);
		DMACount++;
		if(DMACount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "DMA Count is: ",DMACount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 396
			//200000 - 790
			//500000 - 1983
			//1000000 - 3963
			//2000000 - 7908
			OS_Kill();
		}
  }
}

void Normal_Write(void){
	PortB_Init();
  while(1){
    for(int i=0;i<SIZE;i++){
      GPIO_PORTB_DATA_R = WriteBuf[i];
    }
		NormalCount++;
		if(NormalCount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "Normal Count: ",NormalCount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 1308
			//200000 - 2616
			//500000 - 6543
			//1000000 - 13083
			//2000000 - 26154
			OS_Kill();
		}
  }
}
