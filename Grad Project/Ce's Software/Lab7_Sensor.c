// Lab7_Sensor.c
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016


// ************ ST7735's Interface*********************************************
// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground
// ****************************************************************************


//************ Timer Resources *************************************************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2, IR sensors
//  Timer1A - Ping1() ; priority = 1
//  Timer4A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 0
//  Timer5A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = 4
//******************************************************************************


//************ OS's Backgroud Threads ******************************************
//  PingCompute - priority = 2
//  CANProducer - priority = 3
//  OS_Aging - priority = 7 (only added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 1

//******************************************************************************


//******************** Libraries ***********************************************
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab7_Sensor.h"
#include "OS.h"
#include "UART.h"
#include "ST7735.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"
#include "Interrupts.h"
#include "ADC.h"
#include "Ping.h"
#include "SysTick.h"
#include "CAN_4C123\can0.h"
#include "Buttons.h"
#include "IR.h"
#include "DMASoftware.h"
//**************************************************************************


//******************** Globals ***********************************************
uint32_t DMACount;
uint32_t NormalCount;
uint32_t TEST = 1000;
uint32_t IdleCount;           // counts how many iterations IdleTask() is ran.
uint8_t FirstRun;             // boolean that only adds race threads once when button is pressed. 

#define SIZE 128
uint32_t SrcBuf[SIZE],DestBuf[SIZE];
uint8_t  ReadBuf[SIZE];
uint8_t  WriteBuf[SIZE];

uint8_t StartFrame[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};  // start frame sent from sensor board to motor board
//****************************************************************************


//******** IdleTask  *************** 
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
		PF1 ^= 0x02;        // debugging profiler 
    IdleCount++;        // debugging
//		if(DMA_Status_Soft() == 0){
//			OS_bSignal(&DMA_Soft_Sema4);
//		}
//		if(DMA_Status_Read() == 0){
//			OS_bSignal(&DMA_Read_Sema4);
//		}
//		if(DMA_Status_Write() == 0){
//			OS_bSignal(&DMA_Write_Sema4);
//		}
		WaitForInterrupt(); // low-power mode
  }
}


//******** Interpreter **************
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}


//******** CANProducer **************
// Periodic background thread.
// Sends sensor data over CAN.
// inputs:  none
// outputs: none
void CANProducer(void){ 
	uint8_t frame[8];                   // stores the CAN frame's message in four 8bit chuncks
	frame[0] = IR0_Dist;                // Byte 0
	frame[1] = IR1_Dist;                // Byte 1
	frame[2] = IR2_Dist;                // Byte 2
	frame[3] = IR3_Dist;                // Byte 3
	frame[4] = Ping1_Dist;              // Byte 4
//	frame[5] = Bumper0_Data;            // Byte 5
//	frame[6] = Bumper1_Data;            // Byte 6
//	frame[7] = RESERVED;               // Byte 7
	
	CAN0_SendData(frame);               // send 64 bits of sensor data		                      
}


//******** ButtonWorkRace *************** 
// Aperiodic Background thread.
// Begins race when pressed.
// Can only add threads once.
// inputs:  none
// outputs: none
void ButtonWorkRace(void){
	if(FirstRun){
		FirstRun = 0;                                      // Should only initalize threads once.
		
		// add threads needed for race
		OS_AddThread(&PingDisplay, 128, 6);                // Display ping results
		OS_AddThread(&IRDisplay, 128, 6);								   // display IR results
		OS_AddPeriodicThread(&CANProducer, TIME_100MS, 3); // send sensor information to motor board
		
		// send start signal to motor board
		CAN0_SendData(StartFrame);                         //send 64 bit start id
		
		// enable Timer5A interrupts so that robot stops after 180 seconds
	  NVIC_EN2_R = 1<<(92-(32*2));                       // Enable IRQ 92 in NVIC

	}
}


void DMA_Soft(void){  volatile uint32_t delay;
  while(1){
    //while(DMA_Status()); // wait for idle
		OS_bWait(&DMA_Soft_Sema4);
    DMA_Transfer_RAM2RAM(SrcBuf,DestBuf,SIZE);	//one 32 bit word
//    for(delay = 0; delay < 600000; delay++){
//    }		
		//ST7735_OutUDec(DMACount);
		//ST7735_SetCursor(0, 0);
		DMACount++;
		if(DMACount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "DMA Count is: ",DMACount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 423
			//200000 - 843
			//500000 - 2118
			//1000000 - 4221
			//2000000 - 8448
			OS_Kill();
		}
  }
}


void Normal_Soft(void){  volatile uint32_t delay; uint32_t i;
  while(1){
    for(i=0;i<SIZE;i++){
      DestBuf[i] = SrcBuf[i];
    }
		NormalCount++;
		if(NormalCount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "Normal Count: ",NormalCount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 1473
			//200000 - 2943
			//500000 - 7344
			//1000000 - 14688
			//2000000 - 29373
			OS_Kill();
		}
  }
}

void DMA_Read(void){
	PortB_Init();
  while(1){
    //while(DMA_Status_Read()); // wait for idle
		OS_bWait(&DMA_Read_Sema4);
    DMA_Transfer_Read(&GPIO_PORTB_DATA_R, ReadBuf, SIZE);
//		ST7735_OutUDec(DMACount);
//		ST7735_SetCursor(0, 0);
		DMACount++;
		if(DMACount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "DMA Count is: ",DMACount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 490
			//200000 - 993
			//500000 - 2487
			//1000000 - 4968
			//2000000 - 9933
			OS_Kill();
		}
  }
}

void Normal_Read(void){
	PortB_Init();
  while(1){
    for(int i=0;i<SIZE;i++){
      ReadBuf[i] = GPIO_PORTB_DATA_R;
    }
		NormalCount++;
		if(NormalCount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "Normal Count: ",NormalCount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 1308
			//200000 - 2616
			//500000 - 6543
			//1000000 - 13083
			//2000000 - 26157
			OS_Kill();
		}
  }
}

void DMA_Write(void){
	PortB_Init();
  while(1){
    //while(DMA_Status_Write()); // wait for idle, 0 is idle and 1 is busy, initially 0
		OS_bWait(&DMA_Write_Sema4);
		DMA_Transfer_Write(WriteBuf, &GPIO_PORTB_DATA_R, SIZE);
//		ST7735_OutUDec(DMACount);
//		ST7735_SetCursor(0, 0);
		DMACount++;
		if(DMACount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "DMA Count is: ",DMACount);
			ST7735_Message(1,0, "Idle is: ",IdleCount);
			//100000 - 396
			//200000 - 790
			//500000 - 1983
			//1000000 - 3963
			//2000000 - 7908
			OS_Kill();
		}
  }
}

void Normal_Write(void){
	PortB_Init();
  while(1){
    for(int i=0;i<SIZE;i++){
      GPIO_PORTB_DATA_R = WriteBuf[i];
    }
		NormalCount++;
		if(NormalCount==TEST){
			//uint8_t device, uint8_t line, char *string, uint32_t value
			ST7735_Message(0,0, "Normal Count: ",NormalCount);
			ST7735_Message(1,0, "Idle Count is: ",IdleCount);
			//100000 - 1308
			//200000 - 2616
			//500000 - 6543
			//1000000 - 13083
			//2000000 - 26154
			OS_Kill();
		}
  }
}

int main(void){  
	// intialize globals
	IdleCount = 0;                                     // number of times IdleTask() is ran
	FirstRun = 1;                                      // prevents ButtonWorkRace() from running more than once
                
  // initalize modules	
	OS_Init();                                         // initialize OS, disable interrupts
	PortF_Init();                                      // initialize Port F profiling
  //DMA_Init();  // initialize DMA channel 30 for software transfer
	DMA_Init_Read();  // initialize DMA channel 30 for software transfer
	

  // add initial foreground threads
  //OS_AddThread(&Interpreter, 128, 2);                // adds command line interpreter over UART
  OS_AddThread(&IdleTask, 128, 7);                   // runs when nothing useful to do
	
	for(int i=0;i<SIZE;i++){
      SrcBuf[i] = i;
      DestBuf[i] = 0;
			ReadBuf[i] = 0;
			WriteBuf[i] = 0;
  }
	//OS_AddThread(&DMA_Soft, 128, 5);                   // runs when nothing useful to do
	//OS_AddThread(&Normal_Soft, 128, 5);
	
	//OS_AddThread(&DMA_Read, 128, 5);                   // runs when nothing useful to do
	//OS_AddThread(&Normal_Read, 128, 5);
	
	//OS_AddThread(&DMA_Write, 128, 5);                   // runs when nothing useful to do
	//OS_AddThread(&Normal_Write, 128, 5);
	


//Testing for the CPU ability
//Testing for the CPU ability
//Testing for the CPU ability
	OS_InitSemaphore(&DMA_Soft_Sema4,1);               // initialize binary semaphore to 1, signifying resource is available	
	OS_InitSemaphore(&DMA_Read_Sema4,1);               // initialize binary semaphore to 1, signifying resource is available	
	OS_InitSemaphore(&DMA_Write_Sema4,1);               // initialize binary semaphore to 1, signifying resource is available	

	//OS_AddThread(&DMA_Soft, 128, 5);                   // runs when nothing useful to do	
	//1000: 1003
	//5000: 5003
	//10000: 10003
	//20000: 20003
	OS_AddThread(&Normal_Soft, 128, 5);						//set same priority as the idle task to see the idle count	
	//1000: 18
	//5000: 78
	//10000: 153
	//20000: 297
	//OS_AddThread(&DMA_Read, 128, 5);                   // runs when nothing useful to do	
	//1000: 1003
	//5000: 5003
	//10000: 10003
	//20000: 20003
	//OS_AddThread(&Normal_Read, 128, 5);						//set same priority as the idle task to see the idle count	
	//1000: 18
	//5000: 63
	//10000: 138
	//20000: 261
	//OS_AddThread(&DMA_Write, 128, 5);                   // runs when nothing useful to do	
	//1000: 1003
	//5000: 5003
	//10000: 10003
	//20000: 20003
	//OS_AddThread(&Normal_Write, 128, 5);						//set same priority as the idle task to see the idle count	
	//1000: 18
	//5000: 63
	//10000: 138
	//20000: 261
	
	
	//TESTING for round robin scheduler(same priority in priority scheduler)
	//OS_AddThread(&DMA_Write, 128, 7);                   // runs when nothing useful to do	
	//1000: 3032
	//5000: 15015
	//10000: 29998
	//20000: 59955
	//OS_AddThread(&Normal_Write, 128, 7);						//set same priority as the idle task to see the idle count	
	//1000: 78
	//5000: 234
	//10000: 432
	//20000: 825
	
	// finished initialization
  OS_Launch(TIMESLICE);                              // doesn't return, interrupts enabled in here
	return 0;                                          // this never executes
}
