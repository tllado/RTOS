// Ping.c
// Runs on TM4C123
// uses ping PB2, PB4, PB6, PC5, for falling edge triggered interrupts 
// to capture the pulse width for sonar Ping))) sensors.  Note, Port C handler 
// is defined in Buttons.c
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 4th, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "Ping.h"
#include "ST7735.h"
#include "OS.h"
#include "Interrupts.h"
#include "SysTick.h"
#include "Lab7_Sensor.h"
                           
//******************** Globals ***********************************************
volatile uint32_t StartTime1;          // Timer count on rising edge for Ping #1.
volatile uint32_t PulseTime1;          // Time of Ping #1's pulse (.1ms)
volatile uint32_t Ping1_Dist;          // calibrated ping1 sensor data

const uint8_t PingLookup[SIZE_OF_LOOKUP] = {     // lookup table for ping sensors
	4,5,6,7,8,9,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,31,
	32,33,34,35,36,37,38,39,40,41,42,44,45,46,47,48,49,50,51,52,53,54,55,56,58,59,
	60,61,62,63,64,65,66,67,68,69,71,72,73,74,75,76,77,78,79,80,81,82,83,85
};
//****************************************************************************


//******** Ping_Init *************** 
// Initializes ping timer for 24-bit edge time mode and request 
// interrupts on the rising and falling edge of specified CCPO pin.
// Max pulse measurement is (2^24-1)*12.5ns = 209.7151ms
// Min pulse measurement determined by time to run ISR, which is about 1us
// inputs:  none
// outputs: none
void Ping_Init(void){
	// Initalize PB4 as CCPO pin that causes interrupts
  SYSCTL_RCGCGPIO_R |= 0x02;             // activate clock for port B      
	while((SYSCTL_PRGPIO_R&0x02) == 0){};  // allow time for clock to stabilize	
  GPIO_PORTB_DIR_R &= ~0x10;             // make PB4 input
  GPIO_PORTB_AFSEL_R |= 0x10;            // enable alt funct on PB4
  GPIO_PORTB_DEN_R |= 0x10;              // enable digital I/O on PB4
  GPIO_PORTB_AMSEL_R &= ~0x10;           // disable analog functionality on PB4
	GPIO_PORTB_PDR_R |= 0x10;              // enable weak pull-down on PB4
  GPIO_PORTB_PCTL_R &= 0xFFF0FFFF;       // configure PB4 as T1CCP0
  GPIO_PORTB_PCTL_R |= 0x00070000;       // configure PB4 as T1CCP0
	
	// Initalize timer used for input capture
	// Timer1
	SYSCTL_RCGCTIMER_R |= 0x02;            // activate TIMER1  
	while((SYSCTL_PRTIMER_R&0x02) == 0){}; // allow time for clock to stabilize	
  TIMER1_CTL_R &= ~TIMER_CTL_TAEN;       // disable timer1A during setup
  TIMER1_CFG_R = TIMER_CFG_16_BIT;       // configure for 16-bit timer mode                                   
  TIMER1_TAMR_R = (TIMER_TAMR_TACMR|TIMER_TAMR_TAMR_CAP); // configure for 24-bit capture mode                                 
	TIMER1_CTL_R |= TIMER_CTL_TAEVENT_NEG; // configure as neg edge event
  TIMER1_TAILR_R = TIMER_TAILR_TAILRL_M; // start value
  TIMER1_TAPR_R = 0xFF;                  // activate prescale, creating 24-bit
  TIMER1_IMR_R |= TIMER_IMR_CAEIM;       // enable capture match interrupt
  TIMER1_ICR_R = TIMER_ICR_CAECINT;      // clear timer1A capture match flag
  TIMER1_CTL_R |= TIMER_CTL_TAEN;        // enable timer1A 16-b, +edge timing, interrupts                    
  NVIC_PRI5_R &= 0xFFFF00FF;             // Timer1A priority = 1, top 3 bits
	NVIC_PRI5_R |= 0x00002000;             // Timer1A priority = 1, top 3 bits
  NVIC_EN0_R = NVIC_EN0_INT21;           // enable interrupt 21 in NVIC. 
		
	// add Ping))) sensor's threads
	OS_AddThread(&Ping1, 128, 1);                      // Sends a pulse to the ping sensor #1, 10 times a second
	OS_AddPeriodicThread(&PingCompute, TIME_100MS, 2); // calculate ping's distance
}


// ******** Timer1A_Handler **************
// Captures pulse width on PB4.
void Timer1A_Handler(void){
	TIMER1_ICR_R = TIMER_ICR_CAECINT;                                   // acknowledge timer1A capture match
	int32_t status = StartCritical();

	// capture time on falling edge
	uint32_t endTime = OS_Time();
	PulseTime1 = OS_TimeDifference(StartTime1,endTime)/8000;            // how long the pulse was high (.1ms units)
	//PulseTime1 = OS_TimeDifference(StartTime1,endTime)/80000;         // how long the pulse was high (ms units)	
	
	// set pin back in default state for next signal pulse
	GPIO_PORTB_PDR_R |= 0x10;                                           // enable weak pull-down on PB4
	EndCritical(status);
}


//******** SendPulse *************** 
// Issue a 40kHz sound pulse by sending a short 
// logical high burst ~(5us) to the ping sensor.
// Called from PulsePing().
// inputs:  none
// outputs: none
void SendPulse(void){
	int32_t status = StartCritical();
	TIMER1_CTL_R &= ~TIMER_CTL_TAEN;    // disable timer1A during pulse
	
	// set PB4 as a GPIO output
	GPIO_PORTB_DIR_R |= 0x10;           // make PB4 output
	GPIO_PORTB_AFSEL_R &= ~0x10;        // disable alt funct on PB4
	GPIO_PORTB_PCTL_R &= 0xFFF0FFFF;    // configure PB4 as GPIO
	GPIO_PORTB_PCTL_R |= 0x00000000;    // configure PB4 as GPIO

	// create 5us pulse
	PB4 = 0x10;
	SysTick_Wait(400);                  // 5us / 12.5ns = 400
	PB4 = 0x00;	
	EndCritical(status);
}


//******** ConfigForCapture *************** 
// Configure pin for input capture using a pull up resistor
// Called from Ping#().
// inputs:  none
// outputs: none
void ConfigForCapture(void){
	int32_t status = StartCritical();
	GPIO_PORTB_DIR_R &= ~0x10;             // make PB4 input
	GPIO_PORTB_AFSEL_R |= 0x10;            // enable alt funct on PB4
	GPIO_PORTB_PUR_R |= 0x10;              // enable weak pull-up on PB4
	GPIO_PORTB_PCTL_R &= 0xFFF0FFFF;       // configure PB4 as T1CCP0
	GPIO_PORTB_PCTL_R |= 0x00070000;       // configure PB4 as T1CCP0
	TIMER1_ICR_R = TIMER_ICR_CAECINT;      // clear timer1A capture match flag
	TIMER1_CTL_R |= TIMER_CTL_TAEN;        // re-enable timer1A since SendPulse disabled it
	StartTime1 = OS_Time();             // set startTime for measuring Ping pulse
	EndCritical(status);
}


//******** Ping1 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping1(void){
	// send pulses 10 times a second
	while(1){
		SendPulse();         // send a single pulse to the Ping sensor
		SysTick_Wait(60000);       // wait for holdoff time = 750 us. 
		ConfigForCapture();  // configure pin to capture pulse width
		
		// sleep 100ms to accomplish 10Hz pinging.  
		OS_Sleep(100);   
	}
}


//******** PingCompute *************** 
// Periodic background thread.
// Translated measured ping values to actual 
// values using a lookup table
// inputs:  none
// outputs: none
void PingCompute(void){
	// calculate distance using formula from lecture 9 notes
	uint32_t ping1_measured = DIST(PulseTime1);
	
	// calibration with lookup table	
	// Ping1
	if(ping1_measured <= SIZE_OF_LOOKUP-1){
		Ping1_Dist = PingLookup[ping1_measured];
	}			
	else{
		Ping1_Dist = PingLookup[SIZE_OF_LOOKUP-1];   // ceiling
	}				
}


//******** PingDisplay *************** 
// Foreground thread.
// Display ping sensor readings 10 times a second
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void PingDisplay(void){
	while(1){                                               // never dies
		// Display results to ST7735
	
		if(Ping1_Dist >= PingLookup[SIZE_OF_LOOKUP-1]){       // unmeasureable distance ~ >= 85cm
			ST7735_SetTextColor(ST7735_RED);        
		}
		else if(Ping1_Dist < 10){                                  
      ST7735_SetTextColor(ST7735_BLUE);			
		}
		else{                                                 // 10cm < dist < 85cm 
      ST7735_SetTextColor(ST7735_YELLOW);			
		}
		ST7735_Message(0,2,"Ping1 dist(cm) = ",Ping1_Dist);   // display distance  	
		
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);  
	}
}
