// Config.h
// Runs on TM4C123
// Contains user settings for autonomous car
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.05.03

////////////////////////////////////////////////////////////////////////////////
// User Settings

// Debug mode?
//#define DEBUG

// Bumper.h
#define BACKUP_DIST     15   // corner IR distance
#define BACKUP_PAUSE    250 // ms
#define BACKUP_SPEED    60
#define BACKUP_DIR      127
#define SYMMETRIC       0   // 0 = right/left response, 1 = symmetric response

// Controller.h
#define SPEED_MIN 80
#define SPEED_MAX 100
// front IR sensors
#define KP_F 500    // default = 500
#define KI_F 0      // default = 0
#define KD_F 200    // default = 200
// corner IR sensors
#define KP_C 1500   // default = 1500
#define KI_C 0      // default = 0
#define KD_C 1000    // default = 500
// side IR sensors
#define KP_S 1500   // default = 1500
#define KI_S 0      // default = 0
#define KD_S 500    // default = 500

// Timer5.h
#define RACE_LENGTH 179000  // ms
