// Retarget.c
// Runs on TM4C123 
// Retargets printf statments. 
// Brandon Boesch
// Ce Wei
// March 29th, 2016

#include <stdio.h>
#include <stdint.h>
#include "Retarget.h"
#include "UART.h"
#include "Config.h"
#ifdef DEBUG
    #include "ST7735.h"
#endif

// *****************Globals**************************
FILE __stdout;
FILE __stdin;
// **************************************************


//---------- fputc-----------------
// redirects printf statments to UART 
int fputc (int ch, FILE *f) {
    UART_OutChar(ch);
    #ifdef DEBUG
        ST7735_OutChar(ch);   // OutChar currently does not have LCDFree semaphore protection.
    #endif
    return 0;
}


//---------- fgetc-----------------
// Returns the character currently pointed by the internal file 
// position indicator of the specified by FILE
int fgetc (FILE *f){
    return (UART_InChar());
}


// Function called when file error occurs.
int ferror(FILE *f){
    /* Your implementation of ferror */
    return EOF;
}
