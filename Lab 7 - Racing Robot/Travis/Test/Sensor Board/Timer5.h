// Timer5.h
// Runs on TM4C123 
// Use TIMER5 in 32-bit periodic mode.  Used for calculating time.  
// Does not trigger interrupts.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.05.03

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Config.h"
#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// System Settings
#define CLOCK_PERIOD  80000 // CPU ticks

////////////////////////////////////////////////////////////////////////////////
// Global Variables

extern volatile uint32_t Timer5Time;

// ***************** Timer5_Init ****************
// Activate Timer5A
// Inputs:  period in units (1/clockfreq)
// Outputs: none
void Timer5_Init(void);

// ***************** Timer5A_Enable ******************
void Timer5A_Enable(void);

// ***************** Timer5A_Disable ******************
void Timer5A_Disable(void);
