// PWM.c
// Runs on TM4C123 
// Initiates and updates PWM signals on PB4, PB5, PB6, PB7, PD0, and PD1 pins
//  for motor and servo control
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 19th, 2016

///////////////////////////////////////////////////////////////////////////////
// PWMInit()
// Initializes PB4,PB5,PB6,PB7 at motFrq frequency and 0 duty cycle.
//  Initializes PD0,PD1 at srvFrq frequency and 1.5ms duty cycle.
// Takes no input.
// Gives no output.

void PWMInit(void);

///////////////////////////////////////////////////////////////////////////////
// updateDuties()
// Converts two motor speeds to four PWM duty values, converts one servo
//  position into one PWM duty cycle, and updates six corresponding registries.
// Takes input of three uint8_t values representing two motor speeds and one
//  servo position.
// Gives no output.

void updateDuties(uint8_t speedL, uint8_t speedR, uint16_t servoPos);
