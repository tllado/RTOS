// Controller.h
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 23th, 2016

//******************** Constants *********************************************
#define speedDefault 30
#define KpSide 250      // Gains are multiplied by 100 here
#define KpFront 200
// *************************************************************************

void simpleController(void);
