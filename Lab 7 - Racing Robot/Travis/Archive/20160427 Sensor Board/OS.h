// OS.h
// Runs on TM4C123 
// Create and manage threads.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// March 29th, 2016

//**********************Compilter Directives*********************************
// Initializes code used by priority scheduler if PRIORITY is uncommented 
// ie) OS_Aging, OS_MoveThread, RunCt.
// Also changes out scheduler in OSasm.s  
	                                     //////////////////////////////////////////
#define PRIORITY_SCHEDULER             //<- Only one of these with arrows should 
//#define ROUNDROBIN_SCHEDULER         //<- be uncommented at any given time.
                                       //////////////////////////////////////////                                     
// ************************************************************************************


// ******************** Constants *******************************************************
#define MAX_NUM_4GROUND   15           // maximum number of fore ground threads
#define MAX_NUM_BGROUND   20           // maximum number of background threads
#define MAX_NUM_PRI       8            // maximum number of foreground priority levels
#define STACKSIZE         256          // number of 32-bit words in stack
#define FIFOSIZE          512    
#define PRIORITY          1            // scheduler enumeration
#define ROUNDROBIN        0            // scheduler enumeration
#define AGING_JUMP        2            // how many tempPriority levels a starved thread will jump through each time it is promoted 
#define AGING_PERIOD      10*TIMESLICE // how often OS_Aging background thread is ran
#define SLEEP_PERIOD      TIME_1MS     // how often the system checks if any threads need to wakeup.  This value determines the common denominator of all OS_Sleep calls. Must be a common multiple and >= BACKGROUND_PERIOD. Must be >= Time_1MS
#define BACKGROUND_PERIOD TIME_1MS     // interval in which timer1 checks if any background threads shoulds run. Set to common denominator of all background tasks' periods
#define TIMESLICE         TIME_2MS     // how often the Schedular is ran in Systick_Handler


// edit these depending on your clock frequency.  Assuming 80MHz clock       
#define TIME_1MS    80000          
#define TIME_2MS    (2*TIME_1MS)  
#define TIME_5MS    (5*TIME_1MS) 
#define TIME_10MS   (10*TIME_1MS)
#define TIME_100MS  (100*TIME_1MS)
#define TIME_200MS  (200*TIME_1MS)
#define TIME_500US  (TIME_1MS/2)  
#define TIME_250US  (TIME_1MS/5) 
#define TIME_100US  (TIME_1MS/10)
#define TIME_10US   (TIME_1MS/100)
#define TIME_5US    (TIME_1MS/200) 
#define TIME_1US    (TIME_1MS/1000)

// port enumerations for OS_AddButtonTask()
#define PF0_TASK 0
#define PF4_TASK 1
#define PC6_TASK 2
#define PC7_TASK 3

// enumeratoions for OS_Error()
#define B_PERIOD          0     // error in assigning BACKGROUND_PERIOD. Called from OS_AddPeriodicThread() 
#define S_PERIOD          1     // error in assigning SLEEP_PERIOD.  Called from OS_Init();
#define MAX_4GROUND       2     // error adding too many foreground threads. Called from OS_AddThread();
#define MAX_BGROUND       3     // error adding too many background threads. Called from OS_AddPeriodicThread() and OS_AddAperiodicThread();
// ************************************************************************************


// ******************** Macros *********************************************************
#define SLEEPCNT_CONVERT(x) x/BACKGROUND_PERIOD    // assign sleepCnt to TCB based off timer1's period
// *************************************************************************************


// ************* Stucts ****************************************************
typedef struct ForegroundTCB{
	int32_t *sp;                          // Pointer to stack (valid for threads not running)
  struct ForegroundTCB *next;           // Linked-list pointer to next foregroundTCB
  struct ForegroundTCB *previous;       // Linked-list pointer to previous foregroundTCB
	uint32_t priority;                    // prioritizes each thread
	int32_t tempPriority;                 // Used by schedular. Temporarily increases if needed to prevent starving.  Needs to be signed since checking if <= 0 in OS_MoveThreads
	uint32_t RunCnt;                      // Number of times that this thread has run
	uint32_t LastRunCnt;                  // Number of times that this thread has run at last check
	uint32_t id;                          // Id for unique identification
	struct ForegroundTCB *altNext;        // Linked-list pointer to alternate next foregroundTCB.  Used for Sleeping/Blocked lists
	struct ForegroundTCB *altPrevious;    // Linked-list pointer to alternate previous foregroundTCB.  Used for Sleeping/Blocked lists
	uint32_t sleepCnt;                    // Used for suspending execution
	uint8_t alive;                        // tcb is either alive or dead. Made alive in OS_AddThread(). Made dead in OS_Kill().
}TCBType4Ground;


typedef struct BackgroundTCB{
	void (*task)(void);              // Pointer to background thread's user task
  struct BackgroundTCB *next;      // Linked-list pointer to next backgroundTCB
	struct BackgroundTCB *previous;  // Linked-list pointer to previous backgroundTCB
	uint32_t sleepCnt;               // Used for suspending execution
	uint32_t period;                 // determines frequency of operation
	uint32_t id;                      // Id for unique identification
	uint8_t alive;                   // tcb is either alive or dead. Made alive in OS_AddPeriodicThread() Or OS_SW#Task().
	uint8_t priority;                // prioritizes each thread
	uint8_t aperiodic;               // determines the source of TCB (true = Aperiodic, false = Periodic)
}TCBTypeBGround;


typedef struct Sema4{
  long Value;                      // >=0 means free, otherwise means busy. The magnitude of a negative number represents how many threads are blocked on this semaphore        
	uint32_t numBlocked;             // Number of threads blocked on this semaphore
	struct ForegroundTCB *list;      // Linked-list pointer to first element in this semaphore's TCB list
}Sema4Type;


typedef struct LinkedList{
  uint32_t Value;                  // Number of threads in this linked list       
  struct ForegroundTCB *list;      // Linked-list pointer to first element in this semaphore's TCB list
}LinkedListType;
// **************************************************************************


// ************* Externs ****************************************************
extern unsigned long Num4Ground;           // number of foreground threads created
extern unsigned long NumBGround;           // number of background threads created
extern unsigned long NumBlocked;           // number of blocked foreground threads
extern unsigned long NumActive;            // number of active foreground threads
extern unsigned long MaxNumPri;            // maximum number of priority levels.  Passed into OSasm.s
extern TCBType4Ground ForegroundTCBs[MAX_NUM_4GROUND];
extern TCBTypeBGround BackgroundTCBs[MAX_NUM_BGROUND];
extern TCBType4Ground *RunPt;              // pointer to running TCB
extern TCBTypeBGround *FirstBackgroundPt;  // points to the BackgroundTCB with the highest priority 
extern volatile Sema4Type CAN0ReceiveFree;        // binary semephore for receiving on CAN0. 0=busy, 1=free
extern volatile Sema4Type LCDFree;         // binary semephore for ST7735. 0=busy, 1=free
extern volatile Sema4Type LCDColorFree;    // binary semephore for ST7735 dispaly color. 0=busy, 1=free
extern volatile Sema4Type RxDataAvailable; // signaled in UART's copyHardwareToSoftware. 0 = Rx Fifo is empty, else = data in Rx Fifo
extern volatile Sema4Type TxRoomLeft;      // signaled in UART's copySoftwareToHardware. 0 = Tx Fifo is full, else = room available in Tx Fifo
extern volatile Sema4Type FileSystemMutex; // binary semaphore. 0 = Disk not available, 1 = Disk available
extern volatile Sema4Type NoFileOpenMutex; // binary semaphore. Only one file should be open at any time. 0 = a file is currently open, 1 = no files are currently open.
extern volatile LinkedListType Sleeping;                     // holds list of all sleeping ForegroundTCBs. 
extern volatile LinkedListType PriorityLists[MAX_NUM_PRI];   // array of priority levels' values and linked lists 
extern uint32_t Scheduler;                 // Initializes and run different code depending on scheduler defined at top of OS.h 
// **************************************************************************


// ******** OS_Init ************
// initialize operating system, disable interrupts until OS_Launch
// initialize OS controlled I/O: serial, ADC, systick, LaunchPad I/O and timers 
// input:  none
// output: none
void OS_Init(void); 

//******** StartOS_ASM *************** 
// Start on the first OS task. Defined in OSasm.s
void StartOS_ASM(void); 

//***********SetInitialStack************
// Make the TCB looks like be suspend from previous states
// R15 - PC will be set in OS_AddThread
// R13 - user stack will not be changed
void SetInitialStack(int i);

//******** TCBInit *************** 
// Initialize all the TCBs. Called within OS_Init.
// Inputs: none
// Outputs: none
void TCBInit(void);

// ******** OS_InitSemaphore ************
// initialize semaphore 
// input:  pointer to a semaphore
// output: none
void OS_InitSemaphore(volatile Sema4Type *semaPt, long value); 

//******** OS_Launch *************** 
// start the scheduler, enable interrupts
// Inputs: number of 12.5ns clock cycles for each time slice
//         you may select the units of this parameter
// Outputs: none (does not return)
// In Lab 2, you can ignore the theTimeSlice field
// In Lab 3, you should implement the user-defined TimeSlice field
// It is ok to limit the range of theTimeSlice to match the 24-bit SysTick
void OS_Launch(unsigned long theTimeSlice);

//******** OS_AddPeriodicThread *************** 
// Adds a background periodic task.  See Timer1.c for background handler
// Inputs: *task - pointer to a void/void background function
//         period - given in system time units (12.5ns). Must be 
//                  greater than or equal to BACKGROUND_PERIOD
//         priority - 0 is the highest, 7 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// You are free to select the time resolution for this function
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal, OS_AddThread
// This task does not have a Thread ID
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddPeriodicThread(void(*task)(void), unsigned long period, unsigned long priority);
	
//******** OS_AddAperiodicThread *************** 
// Adds a background aperiodic task.  See Timer1.c for background handler
// Inputs: *task - pointer to a void/void background function
//         priority - 0 is the highest, 7 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal, OS_AddThread
// This task does not have a Thread ID
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddAperiodicThread(void(*task)(void), uint8_t priority);

// ******** OS_KillAperiodic ************
// Kill the currently running aperiodic background 
// thread, and release its TCB. 
// Decrements NumBGround.
// input:  index - index of TCB which you would like to remove from BackgroundTCBs
// output: none
void OS_KillAperiodic(uint32_t index);

// ******** OS_AddButtonTask************
// Adds an aperiodic background task to a specified button
// Inputs:  *task - pointer to TCB that needs to move
//          port - port number that button is assigned to
//          priority - 0 is the highest, 7 is the lowest
// Outputs: none
void OS_AddButtonTask(void(*task)(void), uint8_t port, uint8_t priority);

//******** OS_AddThread *************** 
// Add a foregound thread to the scheduler.  Each thread
// is assigned a ForegroundTCB slot and is assigned to 
// its priority level's active list.
// Increments Num4Ground, NumActive, and PriorityLists[#]->Value.
// Inputs: pointer to a void/void foreground task
//         number of bytes allocated for its stack
//         priority, 0 is highest, 7 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// stack size must be divisable by 8 (aligned to double word boundary)
// In Lab 3, you can ignore the stackSize fields
int OS_AddThread(void(*task)(void), unsigned long stackSize, uint8_t priority);

// ******** OS_Kill ************
// Kill the currently running thread, release its TCB and stack. 
// Decrements Num4Ground.
// input:  none
// output: none
void OS_Kill(void); 

// ******** OS_Wait ************
// decrement semaphore 
// block if less than zero
// input:  pointer to a counting semaphore
// output: none
void OS_Wait(volatile Sema4Type *semaPt); 

// ******** OS_Signal ************
// increment semaphore 
// wakeup blocked thread if appropriate 
// input:  pointer to a counting semaphore
// output: none
void OS_Signal(volatile Sema4Type *semaPt); 

// ******** OS_bWait ************
// Block if Value is 0
// If Value is 1, set Value to 0 and continue
// input:  pointer to a binary semaphore
// output: none
void OS_bWait(volatile Sema4Type *semaPt); 

// ******** OS_bSignal ************
// Set Value to 1.  Wake up one blocked thread 
// if any are in blocked list.
// input:  pointer to a binary semaphore
// output: none
void OS_bSignal(volatile Sema4Type *semaPt);

// ******** OS_Block *********
// Add TCB to a semaphore's blocked list, and removes from active priority list. 
// Called from OS_Wait while interrupts are disabled
// Increments NumBlocked and semaPt->value. 
// Decrements NumActive and PriorityLists[RunPt->tempPriority].Value
// Inputs: pointer to semaphore you wish to add blocked thread to
// Outputs: None
void OS_Block(volatile Sema4Type *semaPt); 

// ******** OS_Unblock *********
// Remove TCB from a semaphore's blocked list
// Inputs: semaPt - pointer to semaphore you wish to remove a blocked thread from
//         status - current saved status of interrupts
// Outputs: true - true when removed TCB has a higher tempPriority than running TCB
//          false - false when removed TCB has a lower tempPriority than running TCB
int OS_Unblock(volatile Sema4Type *semaPt);  

// ******** OS_Sleep ************
// Place currently running thread into a dormant state. Always puts sleeping 
// thread in front of sleeping list. Background threads cannot call this function. 
// input:  number of msec to sleep
// output: none
void OS_Sleep(unsigned long sleepTime); 

// ******** OS_SleepCheck ************
// Background thread
// Periodically decrements all sleeping foregroundTCBs' sleep counters.
// Wakes up threads that are finished sleeping.
// input:  none
// output: none
void OS_SleepCheck(void);

// ******** OS_Wakeup ************
// Remove TCB from sleepingTCB list, and place back in the
// active foregroundTCB list.  This will only be called 
// from OS_SleepCheck().
// input:  pointer to foregroundTCB that is waking up
// output: none
void OS_Wakeup(TCBType4Ground *waking);
	
// ******** OS_Suspend ************
// suspend execution of currently running thread
// scheduler will choose another thread to execute
// Can be used to implement cooperative multitasking 
// Same function as OS_Sleep(0)
// input:  none
// output: none
void OS_Suspend(void);

// ******** OS_Aging ************
// Periodic Background Thread
// Cycle through active foreground TCB and determine
// if a threads tempPriority should be promoted or demoted. OS_Aging
// is added as a periodic task during OS_Init.
// Inputs:  none
// Outputs: none
void OS_Aging(void);

// ******** OS_MoveThread ************
// Move foreground thread from one priority list to another.
// Will update the moving thread's tempPriority.
// Will be called from OS_Aging which has interrupts disabeld.
// Inputs:  *movingTCBPt - pointer to TCB that needs to move
//          promoting - boolean that says whether a thread is being promoted or 
//                      demoted in priority(true = promoting, false = demoting)
// Outputs: none
void OS_MoveThread(TCBType4Ground *movingTCBPt, uint8_t promoting);

//******** OS_Id *************** 
// returns the thread ID for the currently running thread
// Inputs: none
// Outputs: Thread ID, number greater than zero 
unsigned long OS_Id(void);
 
// ******** OS_Fifo1_Init ************
// Initialize the Fifo to be empty
// Inputs: size
//         e.g., 4 to 64 elements
//         e.g., must be a power of 2,4,8,16,32,64,128
// Outputs: none 
void OS_Fifo1_Init(unsigned long size);

// ******** OS_Fifo2_Init ************
// Initialize the Fifo to be empty
// Inputs: size
//         e.g., 4 to 64 elements
//         e.g., must be a power of 2,4,8,16,32,64,128
// Outputs: none 
void OS_Fifo2_Init(unsigned long size);

// ******** OS_Fifo3_Init ************
// Initialize the Fifo to be empty
// Inputs: size
//         e.g., 4 to 64 elements
//         e.g., must be a power of 2,4,8,16,32,64,128
// Outputs: none 
void OS_Fifo3_Init(unsigned long size);

// ******** OS_Fifo4_Init ************
// Initialize the Fifo to be empty
// Inputs: size
//         e.g., 4 to 64 elements
//         e.g., must be a power of 2,4,8,16,32,64,128
// Outputs: none 
void OS_Fifo4_Init(unsigned long size);

// ******** OS_Fifo1_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting.
// This function cannot enable/disable intterupts 
// since it will be called from interrupts.
// Inputs:  data
// Outputs: true - when data is properly saved,
//          false - if data not saved, because fifo was full
int OS_Fifo1_Put(unsigned long data);

// ******** OS_Fifo2_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting.
// This function cannot enable/disable intterupts 
// since it will be called from interrupts.
// Inputs:  data
// Outputs: true - when data is properly saved,
//          false - if data not saved, because fifo was full
int OS_Fifo2_Put(unsigned long data);

// ******** OS_Fifo3_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting.
// This function cannot enable/disable intterupts 
// since it will be called from interrupts.
// Inputs:  data
// Outputs: true - when data is properly saved,
//          false - if data not saved, because fifo was full
int OS_Fifo3_Put(unsigned long data);
	
// ******** OS_Fifo4_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting.
// This function cannot enable/disable intterupts 
// since it will be called from interrupts.
// Inputs:  data
// Outputs: true - when data is properly saved,
//          false - if data not saved, because fifo was full
int OS_Fifo4_Put(unsigned long data);

// ******** OS_Fifo1_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will block if empty
// Inputs:  none
// Outputs: data 
unsigned long OS_Fifo1_Get(void);
	
// ******** OS_Fifo2_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will block if empty
// Inputs:  none
// Outputs: data 
unsigned long OS_Fifo2_Get(void);
	
// ******** OS_Fifo3_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will block if empty
// Inputs:  none
// Outputs: data 
unsigned long OS_Fifo3_Get(void);

// ******** OS_Fifo4_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will block if empty
// Inputs:  none
// Outputs: data 
unsigned long OS_Fifo4_Get(void);

// ******** OS_MailBox_Init ************
// Initialize communication channel
// Inputs:  none
// Outputs: none
void OS_MailBox_Init(void);

// ******** OS_MailBox_Send ************
// enter mail into the MailBox
// Inputs:  data to be sent
// Outputs: none
// This function will be called from a foreground thread
// It will spin/block if the MailBox contains data not yet received 
void OS_MailBox_Send(unsigned long data);

// ******** OS_MailBox_Recv ************
// remove mail from the MailBox
// Inputs:  none
// Outputs: data received
// This function will be called from a foreground thread
// It will spin/block if the MailBox is empty 
unsigned long OS_MailBox_Recv(void);

// ******** OS_Time ************
// Return the system time. Uses Timer5A 
// Inputs:  none
// Outputs: time in 12.5ns units, 0 to 4294967295
// The time resolution should be less than or equal to 1us, and the precision 32 bits
// It is ok to change the resolution and precision of this function as long as 
//   this function and OS_TimeDifference have the same resolution and precision 
unsigned long OS_Time(void);

// ******** OS_TimeDifference ************
// Calculates difference between two times
// Inputs:  two times measured with OS_Time
// Outputs: time difference in 12.5ns units 
// The time resolution should be less than or equal to 1us, and the precision at least 12 bits
// It is ok to change the resolution and precision of this function as long as 
//   this function and OS_Time have the same resolution and precision 
unsigned long OS_TimeDifference(unsigned long start, unsigned long stop);

// ******** OS_ClearMsTime ************
// sets the system time to zero (from Lab 1)
// Inputs:  none
// Outputs: none
// You are free to change how this works
void OS_ClearMsTime(void);

// ******** OS_MsTime ************
// reads the current time in msec (from Lab 1)
// Inputs:  none
// Outputs: time in ms units
// You are free to select the time resolution for this function
// It is ok to make the resolution to match the first call to OS_AddPeriodicThread
unsigned long OS_MsTime(void);

	
// ******** OS_Error ************
// called when there is an error 
// preventing the system from running	
// Input: errorCode - enumeration for type of error. See OS.h for enum definitions
// Output: none
void OS_Error(int errorCode);

// ******** OS_Dummy************
// Added to prevent OS from crashing when NumActive == 0.
// This can happen when functions like OS_Sleep, OS_Block,
// or OS_Kill are called while there is only one active thread
void Dummy(void);


