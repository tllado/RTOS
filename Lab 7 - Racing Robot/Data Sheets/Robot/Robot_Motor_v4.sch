<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<description>&lt;b&gt;Robot Motor board  version 4&lt;/b&gt;
&lt;p&gt;
Jonathan Valvano / Daniel Valvano /
Jon Valvano / Steven Prickett / Brent Wylie&lt;br/&gt;
Feb 17, 2016&lt;br/&gt;
EE445M/EE380L.6&lt;br/&gt;
This PCB implements labs associated with the Embbeded Systems books&lt;br/&gt;
&lt;u&gt;Embedded Systems:
Introduction to ARM Cortex-M Microcontrollers&lt;/u&gt;, 2015, ISBN:
978-1477508992, &lt;a target="_blank"
href="http://users.ece.utexas.edu/%7Evalvano/arm/outline1.htm"&gt;&lt;b&gt;http://users.ece.utexas.edu/~valvano/arm/outline1.htm&lt;/b&gt;&lt;/a&gt;&lt;br&gt;
&lt;u&gt;Embedded Systems: Real-Time
Interfacing to ARM
Cortex-M Microcontrollers&lt;/u&gt;, 2015, ISBN: 978-1463590154, &lt;b&gt;&lt;a target="_blank"
href="http://users.ece.utexas.edu/%7Evalvano/arm/outline.htm"&gt;http://users.ece.utexas.edu/~valvano/arm/outline.htm&lt;/a&gt;&lt;/b&gt;&lt;br&gt;
&lt;u&gt;Embedded Systems: Real-Time Operating
Systems for the ARM Cortex-M Microcontrollers &lt;/u&gt;, 2015,
ISBN: 978-1466468863, &lt;b&gt;&lt;a target="_blank"
href="http://users.ece.utexas.edu/%7Evalvano/arm/outline3.htm"&gt;http://users.ece.utexas.edu/~valvano/arm/outline3.htm&lt;/a&gt;&lt;/b&gt;&lt;br&gt;</description>
<libraries>
<library name="EE445L">
<description>&lt;b&gt;EE445L Library&lt;/b&gt;
&lt;p&gt;
This library has been compiled by EE445L professors, TAs,
and students.  It is meant as a service to students,
simplifying the design, manufacturing, and testing of PCBs associated with the 
Cirqoid PCB milling machine in the College of Engineering &lt;b&gt;Makerspace&lt;/b&gt;.
For more information about the PCB mill see &lt;b&gt;http://cirqoid.com/&lt;/b&gt;.
There are a number of restrictions that we will need to follow to simplify the PCB construction.
&lt;p&gt;
&lt;b&gt;PCB size.&lt;/b&gt; There are two standard sizes of the PCB material one can order from Cirqoid: 100 by 75 mm, and 100 by 160 mm. The smaller size allows you to design the  PCB in the freeware version of Eagle. You can of course, set the board size to be less than 100 by 75 mm.
&lt;p&gt;
&lt;b&gt;Number of layers.&lt;/b&gt; You can create single or double layer PCBs with the Cirqoid PCB mill.
&lt;p&gt;
&lt;b&gt;Drill sizes.&lt;/b&gt; The Makerspace only has specific drill sizes. This library will restrict drill sizes to these four:
 0.0157in=0.4mm,
 0.0315in=0.8mm,
 0.0433in=1.1mm, and
 0.120in=3.0mm. You can only drill holes of a certain size, if you have a drill of that size. Second, the fewer drill sizes you use the faster it will be to manufacture the board.
&lt;p&gt;
&lt;b&gt;Placement of vias.&lt;/b&gt; The Cirqoid PCB mill will not pour copper into vias which run from the top to bottom layers. Therefore you will need to place wire into the vias and solder on both sides. This means one must be careful where the vias are located (e.g., not under chips_ so you will have access to solder both layers.&lt;p&gt;
Use the &lt;b&gt;ADD&lt;/b&gt; command in the Schematic Editor or Layout Editor
window to search for a certain device or package!
&lt;p&gt;
If you are going to create new parts, please do not add them to this library. This is because we routinely make updates to the this library, and if you add your part to this library, it will/may be lost when you install a newer version of the EE445L library.
&lt;p&gt;
Please let me know if you find any mistakes at &lt;b&gt;valvano@mail.utexas.edu&lt;/b&gt;. Thank you and have fun,&lt;p&gt;
Jonathan Valvano, University of Texas at Austin, 
UTA7.343, 1616 Guadalupe Street, Austin, TX, 78701</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="RES200MIL">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<pad name="2" x="2.54" y="0" drill="0.8"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="RES300MIL">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8"/>
<pad name="2" x="3.81" y="0" drill="0.8"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="RES400MIL">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8"/>
<pad name="2" x="5.08" y="0" drill="0.8"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="RES_VERTICAL">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8"/>
<pad name="2" x="1.27" y="0" drill="0.8"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round, , 0.8mm holes, 100 mil spacing</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="-2.5908" y1="1.7272" x2="-1.8542" y2="1.7272" width="0.127" layer="21"/>
<wire x1="-2.2352" y1="1.3208" x2="-2.2352" y2="2.1082" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8" diameter="1.6764"/>
<pad name="K" x="1.27" y="0" drill="0.8" diameter="1.6764" shape="square"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round, 0.8mm holes, 100 mil spacing</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8" diameter="1.6764"/>
<pad name="K" x="1.27" y="0" drill="0.8" diameter="1.6764" shape="square"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TM4C123LAUNCHPAD">
<description>&lt;B&gt;Two Dual In Line&lt;/B&gt;&lt;br&gt;
Samtec ESW-110-37-L-D&lt;br&gt;
or FCI 67997-210HLF, Digikey 609-3236-ND</description>
<wire x1="-19.05" y1="-11.303" x2="-19.05" y2="13.843" width="0.1524" layer="25"/>
<wire x1="-24.13" y1="13.843" x2="-19.05" y2="13.843" width="0.1524" layer="25"/>
<wire x1="-24.13" y1="-11.303" x2="-19.05" y2="-11.303" width="0.1524" layer="25"/>
<pad name="1" x="-22.86" y="12.7" drill="0.9" shape="square" rot="R90"/>
<pad name="2" x="-22.86" y="10.16" drill="0.9"/>
<pad name="3" x="-22.86" y="7.62" drill="0.9"/>
<pad name="4" x="-22.86" y="5.08" drill="0.9"/>
<pad name="5" x="-22.86" y="2.54" drill="0.9"/>
<pad name="6" x="-22.86" y="0" drill="0.9"/>
<pad name="7" x="-22.86" y="-2.54" drill="0.9"/>
<pad name="8" x="-22.86" y="-5.08" drill="0.9"/>
<pad name="9" x="-22.86" y="-7.62" drill="0.9"/>
<pad name="10" x="-22.86" y="-10.16" drill="0.9"/>
<pad name="11" x="22.86" y="-10.16" drill="0.9"/>
<pad name="12" x="22.86" y="-7.62" drill="0.9"/>
<pad name="13" x="22.86" y="-5.08" drill="0.9"/>
<pad name="14" x="22.86" y="-2.54" drill="0.9"/>
<pad name="15" x="22.86" y="0" drill="0.9"/>
<pad name="16" x="22.86" y="2.54" drill="0.9"/>
<pad name="17" x="22.86" y="5.08" drill="0.9"/>
<pad name="18" x="22.86" y="7.62" drill="0.9"/>
<pad name="19" x="22.86" y="10.16" drill="0.9"/>
<pad name="20" x="22.86" y="12.7" drill="0.9"/>
<pad name="21" x="-20.32" y="12.7" drill="0.9"/>
<pad name="22" x="-20.32" y="10.16" drill="0.9"/>
<pad name="23" x="-20.32" y="7.62" drill="0.9"/>
<pad name="24" x="-20.32" y="5.08" drill="0.9"/>
<pad name="25" x="-20.32" y="2.54" drill="0.9"/>
<pad name="26" x="-20.32" y="0" drill="0.9"/>
<pad name="27" x="-20.32" y="-2.54" drill="0.9"/>
<pad name="28" x="-20.32" y="-5.08" drill="0.9"/>
<pad name="29" x="-20.32" y="-7.62" drill="0.9"/>
<pad name="30" x="-20.32" y="-10.16" drill="0.9"/>
<pad name="31" x="20.32" y="-10.16" drill="0.9"/>
<pad name="32" x="20.32" y="-7.62" drill="0.9"/>
<pad name="33" x="20.32" y="-5.08" drill="0.9"/>
<pad name="34" x="20.32" y="-2.54" drill="0.9"/>
<pad name="35" x="20.32" y="0" drill="0.9"/>
<pad name="36" x="20.32" y="2.54" drill="0.9"/>
<pad name="37" x="20.32" y="5.08" drill="0.9"/>
<pad name="38" x="20.32" y="7.62" drill="0.9"/>
<pad name="39" x="20.32" y="10.16" drill="0.9"/>
<pad name="40" x="20.32" y="12.7" drill="0.9"/>
<text x="-18.034" y="17.399" size="1.778" layer="25" font="vector" ratio="10" rot="R180">&gt;NAME</text>
<text x="17.78" y="15.5448" size="1.778" layer="25" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-24.13" y1="-11.303" x2="-24.13" y2="13.843" width="0.1524" layer="25"/>
<text x="-24.13" y="13.97" size="1.27" layer="25" font="vector" rot="SR0">3.3</text>
<text x="-24.13" y="-12.7" size="1.27" layer="25" font="vector" rot="SR0">A7</text>
<text x="21.59" y="-12.7" size="1.27" layer="25" font="vector" rot="SR0">A2</text>
<text x="22.86" y="13.97" size="1.27" layer="25" font="vector" rot="SR0">G</text>
<text x="-20.32" y="13.97" size="1.27" layer="25" font="vector" rot="SR0">5</text>
<text x="-21.59" y="-12.7" size="1.27" layer="25" font="vector" rot="SR0">F1</text>
<text x="19.05" y="-12.7" size="1.27" layer="25" font="vector" rot="SR0">F4</text>
<text x="19.05" y="13.97" size="1.27" layer="25" font="vector" rot="SR0">F2</text>
<wire x1="19.05" y1="13.843" x2="24.13" y2="13.843" width="0.1524" layer="25"/>
<wire x1="19.05" y1="-11.303" x2="19.05" y2="13.843" width="0.1524" layer="25"/>
<wire x1="19.05" y1="-11.303" x2="24.13" y2="-11.303" width="0.1524" layer="25"/>
<wire x1="24.13" y1="-11.303" x2="24.13" y2="13.843" width="0.1524" layer="25"/>
<wire x1="-25.4" y1="-22.86" x2="25.4" y2="-22.86" width="0.3048" layer="25"/>
<wire x1="25.4" y1="-22.86" x2="25.4" y2="43.18" width="0.3048" layer="25"/>
<wire x1="25.4" y1="43.18" x2="-25.4" y2="43.18" width="0.3048" layer="25"/>
<wire x1="-25.4" y1="43.18" x2="-25.4" y2="-22.86" width="0.3048" layer="25"/>
</package>
<package name="DIL08">
<description>&lt;b&gt;Dual In Line&lt;/b&gt; 0.8mm holes, 2.54mm=100mil spacing</description>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8" diameter="1.6002" shape="square"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8" diameter="1.6002"/>
<pad name="5" x="3.81" y="3.81" drill="0.8" diameter="1.6002"/>
<pad name="6" x="1.27" y="3.81" drill="0.8" diameter="1.6002"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8" diameter="1.6002"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8" diameter="1.6002"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8" diameter="1.6002"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8" diameter="1.6002"/>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.715" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
</package>
<package name="KK-156-2">
<description>&lt;b&gt;KK 156 HEADER&lt;/b&gt;&lt;p&gt;
Source: http://www.molex.com/pdm_docs/sd/026604100_sd.pdf</description>
<wire x1="3.81" y1="4.95" x2="2.01" y2="4.95" width="0.2032" layer="21"/>
<wire x1="2.01" y1="4.95" x2="-1.985" y2="4.95" width="0.2032" layer="21"/>
<wire x1="-1.985" y1="4.95" x2="-3.785" y2="4.95" width="0.2032" layer="21"/>
<wire x1="-3.785" y1="4.95" x2="-3.785" y2="-4.825" width="0.2032" layer="21"/>
<wire x1="-3.785" y1="-4.825" x2="3.81" y2="-4.825" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-4.825" x2="3.81" y2="4.95" width="0.2032" layer="21"/>
<wire x1="-1.985" y1="2.525" x2="2.01" y2="2.525" width="0.2032" layer="21"/>
<wire x1="2.01" y1="2.525" x2="2.01" y2="4.95" width="0.2032" layer="21"/>
<wire x1="-1.985" y1="2.525" x2="-1.985" y2="4.95" width="0.2032" layer="21"/>
<pad name="1" x="-1.98" y="0" drill="1.7" diameter="2.1844" shape="long" rot="R90"/>
<pad name="2" x="1.98" y="0" drill="1.7" diameter="2.1844" shape="long" rot="R90"/>
<text x="-4.48" y="-4.445" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.75" y="-4.445" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="1X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1" shape="square" rot="R90"/>
<pad name="2" x="0" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="1X03/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-2.54" y="-3.81" drill="1" shape="square" rot="R90"/>
<pad name="2" x="0" y="-3.81" drill="1" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="1" shape="long" rot="R90"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
</package>
<package name="CAP200MILPOLARIZED">
<description>&lt;b&gt;POLARIZED CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm = 200mil, 0.8mm holes</description>
<wire x1="-2.8702" y1="1.3574" x2="2.8702" y2="1.3574" width="0.1524" layer="21" curve="-129.378377"/>
<wire x1="-2.8702" y1="-1.3574" x2="2.8702" y2="-1.3574" width="0.1524" layer="21" curve="129.378377"/>
<wire x1="-2.8702" y1="1.3574" x2="-2.8702" y2="-1.3574" width="0.1524" layer="51" curve="50.621623"/>
<wire x1="2.8702" y1="-1.3574" x2="2.8702" y2="1.3574" width="0.1524" layer="51" curve="50.621623"/>
<wire x1="-1.397" y1="0" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="0.635" x2="-1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.381" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="0.8" diameter="1.905"/>
<pad name="-" x="2.54" y="0" drill="0.8" diameter="1.905" shape="square"/>
<text x="3.048" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.048" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="21"/>
</package>
<package name="CAP100MIL">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm = 100mil, 0.8mm holes</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8"/>
<pad name="2" x="1.27" y="0" drill="0.8"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CAP200MIL">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm = 200mil, 0.8mm holes</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<pad name="2" x="2.54" y="0" drill="0.8"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="SO08">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt;&lt;br&gt;Also called SOIC&lt;br&gt;0.05in (1.27mm) between pins&lt;br&gt;
0.21in (5.4mm) center to center of pins&lt;br&gt;
0.27in (6.9mm) tip to tip</description>
<wire x1="-2.362" y1="-1.803" x2="2.362" y2="-1.803" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-1.803" x2="2.362" y2="1.803" width="0.1524" layer="21"/>
<wire x1="2.362" y1="1.803" x2="-2.362" y2="1.803" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="1.803" x2="-2.362" y2="-1.803" width="0.1524" layer="21"/>
<circle x="-1.8034" y="-0.9906" radius="0.3556" width="0.0508" layer="21"/>
<smd name="1" x="-1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="-1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="-0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<text x="4.0005" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.7305" y="-2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-2.8702" x2="-1.7272" y2="-1.8542" layer="51"/>
<rectangle x1="-0.8128" y1="-2.8702" x2="-0.4572" y2="-1.8542" layer="51"/>
<rectangle x1="0.4572" y1="-2.8702" x2="0.8128" y2="-1.8542" layer="51"/>
<rectangle x1="1.7272" y1="-2.8702" x2="2.0828" y2="-1.8542" layer="51"/>
<rectangle x1="-2.0828" y1="1.8542" x2="-1.7272" y2="2.8702" layer="51"/>
<rectangle x1="-0.8128" y1="1.8542" x2="-0.4572" y2="2.8702" layer="51"/>
<rectangle x1="0.4572" y1="1.8542" x2="0.8128" y2="2.8702" layer="51"/>
<rectangle x1="1.7272" y1="1.8542" x2="2.0828" y2="2.8702" layer="51"/>
</package>
<package name="1X01">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="TO92">
<description>&lt;b&gt;TO 92&lt;/b&gt; 0.8mm holes</description>
<wire x1="-2.0946" y1="-1.651" x2="-2.6549" y2="-0.254" width="0.127" layer="21" curve="-32.78104"/>
<wire x1="-2.6549" y1="-0.254" x2="-0.7863" y2="2.5485" width="0.127" layer="21" curve="-78.318477"/>
<wire x1="0.7863" y1="2.5484" x2="2.0945" y2="-1.651" width="0.127" layer="21" curve="-111.09954"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.2537" y1="-0.254" x2="-0.2863" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-2.6549" y1="-0.254" x2="-2.2537" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.2863" y1="-0.254" x2="0.2863" y2="-0.254" width="0.127" layer="21"/>
<wire x1="2.2537" y1="-0.254" x2="2.6549" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.2863" y1="-0.254" x2="2.2537" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.7863" y1="2.5485" x2="0.7863" y2="2.5485" width="0.127" layer="51" curve="-34.293591"/>
<pad name="3" x="1.27" y="0" drill="0.8"/>
<pad name="2" x="0" y="1.905" drill="0.8"/>
<pad name="1" x="-1.27" y="0" drill="0.8"/>
<text x="3.175" y="0.635" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="square" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1" shape="square" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
<package name="SOP65P640X120-17N">
<smd name="1" x="-2.921" y="2.286" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="2" x="-2.921" y="1.6256" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="3" x="-2.921" y="0.9652" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="4" x="-2.921" y="0.3302" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="5" x="-2.921" y="-0.3302" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="6" x="-2.921" y="-0.9652" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="7" x="-2.921" y="-1.6256" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="8" x="-2.921" y="-2.286" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="9" x="2.921" y="-2.286" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="10" x="2.921" y="-1.6256" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="11" x="2.921" y="-0.9652" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="12" x="2.921" y="-0.3302" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="13" x="2.921" y="0.3302" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="14" x="2.921" y="0.9652" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="15" x="2.921" y="1.6256" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="16" x="2.921" y="2.286" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="17" x="0" y="0" dx="2.9972" dy="2.9972" layer="1"/>
<wire x1="-2.2606" y1="2.1336" x2="-2.2606" y2="2.4384" width="0" layer="51"/>
<wire x1="-2.2606" y1="2.4384" x2="-3.302" y2="2.4384" width="0" layer="51"/>
<wire x1="-3.302" y1="2.4384" x2="-3.302" y2="2.1336" width="0" layer="51"/>
<wire x1="-3.302" y1="2.1336" x2="-2.2606" y2="2.1336" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.4732" x2="-2.2606" y2="1.778" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.778" x2="-3.302" y2="1.778" width="0" layer="51"/>
<wire x1="-3.302" y1="1.778" x2="-3.302" y2="1.4732" width="0" layer="51"/>
<wire x1="-3.302" y1="1.4732" x2="-2.2606" y2="1.4732" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.8128" x2="-2.2606" y2="1.1176" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.1176" x2="-3.302" y2="1.1176" width="0" layer="51"/>
<wire x1="-3.302" y1="1.1176" x2="-3.302" y2="0.8128" width="0" layer="51"/>
<wire x1="-3.302" y1="0.8128" x2="-2.2606" y2="0.8128" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.1778" x2="-2.2606" y2="0.4826" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.4826" x2="-3.302" y2="0.4826" width="0" layer="51"/>
<wire x1="-3.302" y1="0.4826" x2="-3.302" y2="0.1778" width="0" layer="51"/>
<wire x1="-3.302" y1="0.1778" x2="-2.2606" y2="0.1778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.4826" x2="-2.2606" y2="-0.1778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.1778" x2="-3.302" y2="-0.1778" width="0" layer="51"/>
<wire x1="-3.302" y1="-0.1778" x2="-3.302" y2="-0.4826" width="0" layer="51"/>
<wire x1="-3.302" y1="-0.4826" x2="-2.2606" y2="-0.4826" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.1176" x2="-2.2606" y2="-0.8128" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.8128" x2="-3.302" y2="-0.8128" width="0" layer="51"/>
<wire x1="-3.302" y1="-0.8128" x2="-3.302" y2="-1.1176" width="0" layer="51"/>
<wire x1="-3.302" y1="-1.1176" x2="-2.2606" y2="-1.1176" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.778" x2="-2.2606" y2="-1.4732" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.4732" x2="-3.302" y2="-1.4732" width="0" layer="51"/>
<wire x1="-3.302" y1="-1.4732" x2="-3.302" y2="-1.778" width="0" layer="51"/>
<wire x1="-3.302" y1="-1.778" x2="-2.2606" y2="-1.778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.4384" x2="-2.2606" y2="-2.1336" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.1336" x2="-3.302" y2="-2.1336" width="0" layer="51"/>
<wire x1="-3.302" y1="-2.1336" x2="-3.302" y2="-2.4384" width="0" layer="51"/>
<wire x1="-3.302" y1="-2.4384" x2="-2.2606" y2="-2.4384" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.1336" x2="2.2606" y2="-2.4384" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.4384" x2="3.302" y2="-2.4384" width="0" layer="51"/>
<wire x1="3.302" y1="-2.4384" x2="3.302" y2="-2.1336" width="0" layer="51"/>
<wire x1="3.302" y1="-2.1336" x2="2.2606" y2="-2.1336" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.4732" x2="2.2606" y2="-1.778" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.778" x2="3.302" y2="-1.778" width="0" layer="51"/>
<wire x1="3.302" y1="-1.778" x2="3.302" y2="-1.4732" width="0" layer="51"/>
<wire x1="3.302" y1="-1.4732" x2="2.2606" y2="-1.4732" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.8128" x2="2.2606" y2="-1.1176" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.1176" x2="3.302" y2="-1.1176" width="0" layer="51"/>
<wire x1="3.302" y1="-1.1176" x2="3.302" y2="-0.8128" width="0" layer="51"/>
<wire x1="3.302" y1="-0.8128" x2="2.2606" y2="-0.8128" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.1778" x2="2.2606" y2="-0.4826" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.4826" x2="3.302" y2="-0.4826" width="0" layer="51"/>
<wire x1="3.302" y1="-0.4826" x2="3.302" y2="-0.1778" width="0" layer="51"/>
<wire x1="3.302" y1="-0.1778" x2="2.2606" y2="-0.1778" width="0" layer="51"/>
<wire x1="2.2606" y1="0.4826" x2="2.2606" y2="0.1778" width="0" layer="51"/>
<wire x1="2.2606" y1="0.1778" x2="3.302" y2="0.1778" width="0" layer="51"/>
<wire x1="3.302" y1="0.1778" x2="3.302" y2="0.4826" width="0" layer="51"/>
<wire x1="3.302" y1="0.4826" x2="2.2606" y2="0.4826" width="0" layer="51"/>
<wire x1="2.2606" y1="1.1176" x2="2.2606" y2="0.8128" width="0" layer="51"/>
<wire x1="2.2606" y1="0.8128" x2="3.302" y2="0.8128" width="0" layer="51"/>
<wire x1="3.302" y1="0.8128" x2="3.302" y2="1.1176" width="0" layer="51"/>
<wire x1="3.302" y1="1.1176" x2="2.2606" y2="1.1176" width="0" layer="51"/>
<wire x1="2.2606" y1="1.778" x2="2.2606" y2="1.4732" width="0" layer="51"/>
<wire x1="2.2606" y1="1.4732" x2="3.302" y2="1.4732" width="0" layer="51"/>
<wire x1="3.302" y1="1.4732" x2="3.302" y2="1.778" width="0" layer="51"/>
<wire x1="3.302" y1="1.778" x2="2.2606" y2="1.778" width="0" layer="51"/>
<wire x1="2.2606" y1="2.4384" x2="2.2606" y2="2.1336" width="0" layer="51"/>
<wire x1="2.2606" y1="2.1336" x2="3.302" y2="2.1336" width="0" layer="51"/>
<wire x1="3.302" y1="2.1336" x2="3.302" y2="2.4384" width="0" layer="51"/>
<wire x1="3.302" y1="2.4384" x2="2.2606" y2="2.4384" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.54" x2="2.2606" y2="-2.54" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.54" x2="2.2606" y2="2.54" width="0" layer="51"/>
<wire x1="2.2606" y1="2.54" x2="0.3048" y2="2.54" width="0" layer="51"/>
<wire x1="0.3048" y1="2.54" x2="-0.3048" y2="2.54" width="0" layer="51"/>
<wire x1="-0.3048" y1="2.54" x2="-2.2606" y2="2.54" width="0" layer="51"/>
<wire x1="-2.2606" y1="2.54" x2="-2.2606" y2="-2.54" width="0" layer="51"/>
<wire x1="0.3048" y1="2.54" x2="-0.3048" y2="2.54" width="0" layer="51" curve="-180"/>
<text x="-3.7592" y="2.4892" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<wire x1="3.9624" y1="-1.6256" x2="5.0038" y2="-1.6256" width="0.1524" layer="21"/>
<wire x1="-1.8796" y1="-2.54" x2="1.8796" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.8796" y1="2.54" x2="0.3048" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.54" x2="-0.3048" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.54" x2="-1.8796" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.54" x2="-0.3048" y2="2.54" width="0.1524" layer="21" curve="-180"/>
<text x="-3.7592" y="2.4892" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="3.175" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-5.08" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
<package name="KK-100-2">
<description>&lt;b&gt;2-pin header&lt;/b&gt; solder connections for 18 gauge wire, holes are 0.065in, with 0.15in separation, 18 ga wire needs a hole at least 0.04030 in</description>
<pad name="P$1" x="0" y="0" drill="1.651" shape="square"/>
<pad name="P$2" x="3.81" y="0" drill="1.651"/>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="7.62" y="-2.54" size="1.27" layer="21" rot="R90">&gt;VALUE</text>
<wire x1="-0.3175" y1="1.905" x2="0.3175" y2="1.905" width="0.3048" layer="21"/>
<wire x1="3.4925" y1="1.905" x2="4.1275" y2="1.905" width="0.3048" layer="21"/>
<wire x1="3.81" y1="2.2225" x2="3.81" y2="1.5875" width="0.3048" layer="21"/>
</package>
<package name="TO220BV">
<description>&lt;b&gt;Molded Package&lt;/b&gt;&lt;p&gt;
grid 2.54 mm</description>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="G" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="D" x="0" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="S" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
<package name="TO220-4">
<description>&lt;b&gt;TO220&lt;/b&gt;&lt;br&gt;
with thermal pad</description>
<wire x1="-5.207" y1="-1.27" x2="5.207" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.207" y1="14.605" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-1.27" x2="5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="5.207" y1="11.176" x2="4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="4.318" y1="11.176" x2="4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="4.318" y1="12.7" x2="5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="5.207" y1="12.7" x2="5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-1.27" x2="-5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="11.176" x2="-4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="11.176" x2="-4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="12.7" x2="-5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="12.7" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.889" x2="5.08" y2="-0.889" width="0.127" layer="21"/>
<wire x1="5.08" y1="7.366" x2="5.08" y2="-0.889" width="0.0508" layer="21"/>
<wire x1="5.08" y1="7.366" x2="-5.08" y2="7.366" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-0.889" x2="-5.08" y2="7.366" width="0.0508" layer="21"/>
<circle x="0" y="11.176" radius="1.8034" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-6.35" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="0" y="-6.35" drill="1.1" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-6.35" drill="1.1" shape="long" rot="R90"/>
<text x="-4.572" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.572" y="2.032" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.175" y="0" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.635" y="0" size="1.27" layer="51" ratio="10">2</text>
<text x="1.905" y="0" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-4.064" layer="21"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-4.064" layer="21"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-4.064" layer="21"/>
<rectangle x1="-3.175" y1="-4.064" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="-0.635" y1="-4.064" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-4.064" x2="3.175" y2="-1.27" layer="21"/>
<rectangle x1="-2.921" y1="-6.604" x2="-2.159" y2="-4.699" layer="51"/>
<rectangle x1="-0.381" y1="-6.604" x2="0.381" y2="-4.699" layer="51"/>
<rectangle x1="2.159" y1="-6.604" x2="2.921" y2="-4.699" layer="51"/>
<pad name="4" x="0" y="11.176" drill="3.048" diameter="6.4516" shape="square"/>
<circle x="-4.064" y="0" radius="0.508" width="0.127" layer="21"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-4.445" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="TM4C123LAUNCHPAD">
<description>&lt;b&gt;TM4C123 LaunchPad&lt;/b&gt; has 40 pins</description>
<pin name="+3.3V" x="-35.56" y="10.16" length="short" direction="pwr"/>
<pin name="PB5" x="-35.56" y="7.62" length="short"/>
<pin name="PB0" x="-35.56" y="5.08" length="short"/>
<pin name="PB1" x="-35.56" y="2.54" length="short"/>
<pin name="PE4" x="-35.56" y="0" length="short"/>
<pin name="PA5" x="-35.56" y="-7.62" length="short"/>
<pin name="PA6" x="-35.56" y="-10.16" length="short"/>
<pin name="PA7" x="-35.56" y="-12.7" length="short"/>
<pin name="PA2" x="48.26" y="-12.7" length="short" rot="R180"/>
<pin name="PA3" x="48.26" y="-10.16" length="short" rot="R180"/>
<pin name="PA4" x="48.26" y="-7.62" length="short" rot="R180"/>
<pin name="PB6" x="48.26" y="-5.08" length="short" rot="R180"/>
<pin name="PB7" x="48.26" y="-2.54" length="short" rot="R180"/>
<pin name="RESET" x="48.26" y="0" length="short" direction="in" rot="R180"/>
<pin name="PF0" x="48.26" y="2.54" length="short" rot="R180"/>
<pin name="PE0" x="48.26" y="5.08" length="short" rot="R180"/>
<pin name="PB2" x="48.26" y="7.62" length="short" rot="R180"/>
<pin name="GND2" x="48.26" y="10.16" length="short" direction="pwr" rot="R180"/>
<pin name="+5V" x="-12.7" y="10.16" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="-12.7" y="7.62" length="short" direction="pwr" rot="R180"/>
<pin name="PD0" x="-12.7" y="5.08" length="short" rot="R180"/>
<pin name="PD1" x="-12.7" y="2.54" length="short" rot="R180"/>
<pin name="PE2" x="-12.7" y="-7.62" length="short" rot="R180"/>
<pin name="PE3" x="-12.7" y="-10.16" length="short" rot="R180"/>
<pin name="PF1" x="-12.7" y="-12.7" length="short" rot="R180"/>
<pin name="PF4" x="25.4" y="-12.7" length="short"/>
<pin name="PD7" x="25.4" y="-10.16" length="short"/>
<pin name="PD6" x="25.4" y="-7.62" length="short"/>
<pin name="PC7" x="25.4" y="-5.08" length="short"/>
<pin name="PC6" x="25.4" y="-2.54" length="short"/>
<pin name="PC5" x="25.4" y="0" length="short"/>
<pin name="PC4" x="25.4" y="2.54" length="short"/>
<pin name="PB3" x="25.4" y="5.08" length="short"/>
<pin name="PF3" x="25.4" y="7.62" length="short"/>
<pin name="PF2" x="25.4" y="10.16" length="short"/>
<pin name="PE5" x="-35.56" y="-2.54" length="short"/>
<pin name="PE1" x="-12.7" y="-5.08" length="short" rot="R180"/>
<pin name="PB4" x="-35.56" y="-5.08" length="short"/>
<pin name="PD3" x="-12.7" y="-2.54" length="short" rot="R180"/>
<pin name="PD2" x="-12.7" y="0" length="short" rot="R180"/>
<wire x1="-33.02" y1="15.24" x2="-33.02" y2="-15.24" width="0.2" layer="94"/>
<wire x1="-33.02" y1="-15.24" x2="-15.24" y2="-15.24" width="0.2" layer="94"/>
<text x="-30.48" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<text x="-30.48" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-15.24" y1="15.24" x2="-33.02" y2="15.24" width="0.2" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="15.24" width="0.2" layer="94"/>
<text x="-30.48" y="12.7" size="1.778" layer="94">J1</text>
<text x="-20.32" y="12.7" size="1.778" layer="94">J3</text>
<text x="30.48" y="12.7" size="1.778" layer="94">J4</text>
<text x="40.64" y="12.7" size="1.778" layer="94">J2</text>
<wire x1="27.94" y1="15.24" x2="27.94" y2="-15.24" width="0.2" layer="94"/>
<wire x1="45.72" y1="-15.24" x2="45.72" y2="15.24" width="0.2" layer="94"/>
<wire x1="45.72" y1="15.24" x2="27.94" y2="15.24" width="0.2" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="45.72" y2="-15.24" width="0.2" layer="94"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="MCP2551">
<description>&lt;b&gt;MCP2551&lt;/b&gt; CAN driver</description>
<wire x1="-17.78" y1="20.32" x2="-17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-10.16" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-17.78" y2="20.32" width="0.254" layer="94"/>
<text x="-3.81" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-14.224" y="12.7" size="1.27" layer="91">+V</text>
<text x="-13.716" y="-4.064" size="1.27" layer="91">-V</text>
<text x="-17.018" y="4.572" size="1.524" layer="94">TxD</text>
<text x="-17.018" y="-0.508" size="1.524" layer="94">Vref</text>
<pin name="RXD" x="-20.32" y="10.16" visible="pad" length="short" direction="in"/>
<pin name="CANH" x="-2.54" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="VSS" x="-12.7" y="-10.16" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VDD" x="-12.7" y="20.32" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="CANL" x="-2.54" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="TXD" x="-20.32" y="5.08" visible="pad" length="short" direction="out"/>
<pin name="VREF" x="-20.32" y="0" visible="pad" length="short" direction="pas"/>
<pin name="RS" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R90"/>
<text x="-17.018" y="9.652" size="1.524" layer="94">RxD</text>
<text x="-11.938" y="7.112" size="1.524" layer="94">CANH</text>
<text x="-10.668" y="-0.508" size="1.524" layer="94">Rs</text>
<text x="-11.938" y="3.302" size="1.524" layer="94">CANL</text>
</symbol>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="PINHD3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="CPOL-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-4.826" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="-2.54" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="+11V1">
<description>&lt;b&gt;11.1V &lt;/b&gt;</description>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+11.1V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="PINHD1">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="DRV8848">
<pin name="VM" x="-17.78" y="20.32" length="middle" direction="pwr"/>
<pin name="BIN2" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="BISEN" x="17.78" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="NSLEEP" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="AIN2" x="-17.78" y="10.16" length="middle" direction="in"/>
<pin name="BIN1" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="BOUT1" x="17.78" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="BOUT2" x="17.78" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="VINT" x="17.78" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="VREF" x="17.78" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="NFAULT" x="-17.78" y="-10.16" length="middle" direction="oc"/>
<pin name="EPAD" x="-17.78" y="-15.24" length="middle" direction="pas"/>
<pin name="AIN1" x="-17.78" y="15.24" length="middle" direction="in"/>
<pin name="GND" x="-17.78" y="-20.32" length="middle" direction="pas"/>
<pin name="AISEN" x="17.78" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="AOUT1" x="17.78" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="AOUT2" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<wire x1="-12.7" y1="25.4" x2="-12.7" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="12.7" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-25.4" x2="12.7" y2="25.4" width="0.4064" layer="94"/>
<wire x1="12.7" y1="25.4" x2="-12.7" y2="25.4" width="0.4064" layer="94"/>
<text x="-10.4394" y="26.8986" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-7.62" y="-28.6258" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="MFPD">
<wire x1="3.81" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="3.81" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="4.445" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.635" x2="3.175" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="3.175" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.762" x2="2.921" y2="1.016" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="4.699" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="-1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.5334" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.2352" y1="0" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.508" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0" x2="1.016" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0.508" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0.254" x2="2.032" y2="0" width="0.3048" layer="94"/>
<wire x1="2.032" y1="0" x2="1.143" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.143" y1="-0.254" x2="1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="1.143" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.524" y="-3.302" size="0.8128" layer="93">D</text>
<text x="1.524" y="2.54" size="0.8128" layer="93">S</text>
<text x="-2.54" y="-2.032" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="LM78XX-HOR">
<description>&lt;b&gt;Linear regulator&lt;/b&gt; mounted horizontally with ground shield; 7805</description>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-2.54" width="0.4064" layer="94"/>
<text x="5.08" y="-5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-1.778" size="1.524" layer="95">GND</text>
<text x="-4.445" y="1.905" size="1.524" layer="95">IN</text>
<text x="0.635" y="1.905" size="1.524" layer="95">OUT</text>
<pin name="IN" x="-7.62" y="2.54" visible="off" length="short" direction="in"/>
<pin name="GND" x="2.54" y="-5.08" visible="off" length="short" direction="pwr" rot="R90"/>
<pin name="OUT" x="7.62" y="2.54" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="SHIELD" x="-2.54" y="-5.08" visible="off" length="short" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;+3.3V power signal&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="RES200MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="RES300MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="RES400MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="RES_VERTICAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;Avago Technologies LED&lt;/b&gt;&lt;p&gt;
Green 2mA 5mm diffused,	HLMP-4740,	Digikey 516-1327-ND&lt;p&gt;
Red 1.6V 1mA 5mm diffused,	HLMP-D150,	Digikey 516-1323-ND&lt;p&gt;
Yellow 2mA 5mm diffused,	HLMP-4719,	Digikey 516-1326-ND&lt;p&gt;
Orange 10mA 5mm diffused	HLMP-D401,	Digikey 516-1330-ND&lt;p&gt;
Blue 10mA 5mm diffused</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EK-TM4C123GXL" prefix="IC">
<description>&lt;b&gt;Texas Instruments TM4C123 LaunchPad&lt;/b&gt;&lt;p&gt;EK-TM4C123GXL
&lt;a href="http://www.ti.com/tool/ek-tm4c123gxl"&gt;www.ti.com/tool/ek-tm4c123gxl&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TM4C123LAUNCHPAD" x="0" y="0"/>
</gates>
<devices>
<device name="P" package="TM4C123LAUNCHPAD">
<connects>
<connect gate="G$1" pin="+3.3V" pad="1"/>
<connect gate="G$1" pin="+5V" pad="21"/>
<connect gate="G$1" pin="GND" pad="22"/>
<connect gate="G$1" pin="GND2" pad="20"/>
<connect gate="G$1" pin="PA2" pad="11"/>
<connect gate="G$1" pin="PA3" pad="12"/>
<connect gate="G$1" pin="PA4" pad="13"/>
<connect gate="G$1" pin="PA5" pad="8"/>
<connect gate="G$1" pin="PA6" pad="9"/>
<connect gate="G$1" pin="PA7" pad="10"/>
<connect gate="G$1" pin="PB0" pad="3"/>
<connect gate="G$1" pin="PB1" pad="4"/>
<connect gate="G$1" pin="PB2" pad="19"/>
<connect gate="G$1" pin="PB3" pad="38"/>
<connect gate="G$1" pin="PB4" pad="7"/>
<connect gate="G$1" pin="PB5" pad="2"/>
<connect gate="G$1" pin="PB6" pad="14"/>
<connect gate="G$1" pin="PB7" pad="15"/>
<connect gate="G$1" pin="PC4" pad="37"/>
<connect gate="G$1" pin="PC5" pad="36"/>
<connect gate="G$1" pin="PC6" pad="35"/>
<connect gate="G$1" pin="PC7" pad="34"/>
<connect gate="G$1" pin="PD0" pad="23"/>
<connect gate="G$1" pin="PD1" pad="24"/>
<connect gate="G$1" pin="PD2" pad="25"/>
<connect gate="G$1" pin="PD3" pad="26"/>
<connect gate="G$1" pin="PD6" pad="33"/>
<connect gate="G$1" pin="PD7" pad="32"/>
<connect gate="G$1" pin="PE0" pad="18"/>
<connect gate="G$1" pin="PE1" pad="27"/>
<connect gate="G$1" pin="PE2" pad="28"/>
<connect gate="G$1" pin="PE3" pad="29"/>
<connect gate="G$1" pin="PE4" pad="5"/>
<connect gate="G$1" pin="PE5" pad="6"/>
<connect gate="G$1" pin="PF0" pad="17"/>
<connect gate="G$1" pin="PF1" pad="30"/>
<connect gate="G$1" pin="PF2" pad="40"/>
<connect gate="G$1" pin="PF3" pad="39"/>
<connect gate="G$1" pin="PF4" pad="31"/>
<connect gate="G$1" pin="RESET" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;+5.0V power signal&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP2551P">
<description>&lt;b&gt;MCP2551p&lt;/b&gt;&lt;br&gt;
CAN interface driver</description>
<gates>
<gate name="G$1" symbol="MCP2551" x="10.16" y="-5.08"/>
</gates>
<devices>
<device name="" package="DIL08">
<connects>
<connect gate="G$1" pin="CANH" pad="7"/>
<connect gate="G$1" pin="CANL" pad="6"/>
<connect gate="G$1" pin="RS" pad="8"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
<connect gate="G$1" pin="VREF" pad="5"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOIC" package="SO08">
<connects>
<connect gate="G$1" pin="CANH" pad="7"/>
<connect gate="G$1" pin="CANL" pad="6"/>
<connect gate="G$1" pin="RS" pad="8"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
<connect gate="G$1" pin="VREF" pad="5"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KK-156-2" prefix="X" uservalue="yes">
<description>&lt;b&gt;KK 156 HEADER&lt;/b&gt;&lt;p&gt;
Source: http://www.molex.com/pdm_docs/sd/026604100_sd.pdf</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="MV" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="KK-156-2">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_POLARIZED" prefix="C" uservalue="yes">
<description>&lt;B&gt;POLARIZED CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="CPOL-US" x="0" y="0"/>
</gates>
<devices>
<device name="E5-6" package="CAP200MILPOLARIZED">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="025-024X044" package="CAP100MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="CAP200MIL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X1" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PN2222" prefix="Q">
<description>&lt;b&gt;NPN Transistor&lt;/b&gt;</description>
<gates>
<gate name="G1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO92">
<connects>
<connect gate="G1" pin="B" pad="2"/>
<connect gate="G1" pin="C" pad="3"/>
<connect gate="G1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+11V1">
<description>&lt;b&gt;+11.1V battery&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+11V1" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DRV8848" prefix="U">
<description>DMOS FULL-BRIDGE MOTOR DRIVERS</description>
<gates>
<gate name="A" symbol="DRV8848" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P640X120-17N">
<connects>
<connect gate="A" pin="AIN1" pad="16"/>
<connect gate="A" pin="AIN2" pad="15"/>
<connect gate="A" pin="AISEN" pad="3"/>
<connect gate="A" pin="AOUT1" pad="2"/>
<connect gate="A" pin="AOUT2" pad="4"/>
<connect gate="A" pin="BIN1" pad="9"/>
<connect gate="A" pin="BIN2" pad="10"/>
<connect gate="A" pin="BISEN" pad="6"/>
<connect gate="A" pin="BOUT1" pad="7"/>
<connect gate="A" pin="BOUT2" pad="5"/>
<connect gate="A" pin="EPAD" pad="17"/>
<connect gate="A" pin="GND" pad="13"/>
<connect gate="A" pin="NFAULT" pad="8"/>
<connect gate="A" pin="NSLEEP" pad="1"/>
<connect gate="A" pin="VINT" pad="14"/>
<connect gate="A" pin="VM" pad="12"/>
<connect gate="A" pin="VREF" pad="11"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="DRV8848" constant="no"/>
<attribute name="PACKAGE" value="TSSOP-16" constant="no"/>
<attribute name="SUPPLIER" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KK-100-2">
<description>&lt;b&gt;2-pin header&lt;/b&gt; solder connections for 18 gauge wire, holes are 0.065in, with 0.15in separation, 18 ga wire needs a hole at least 0.04030 in</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="KK-100-2">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IRF9530" prefix="Q">
<description>&lt;b&gt;P-CHANNEL HEXFET POWER-MOS-FET&lt;/b&gt;&lt;p&gt;
Source: http://www.irf.com/product-info/datasheets/data/irf9530.pdf</description>
<gates>
<gate name="G$1" symbol="MFPD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO220BV">
<connects>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM78XX_HOR">
<description>&lt;b&gt;LM7805&lt;/b&gt; The LM78XX series of three-terminal positive regulators
is available in the TO-220 package and with several fixed
output voltages, making them useful in a wide range of
applications. Each type employs internal current limiting,
thermal shut-down, and safe operating area protection. If
adequate heat sinking is provided, they can deliver over
1 A output current. Although designed primarily as fixed-
voltage regulators, these devices can be used with exter-
nal components for adjustable voltages and currents.</description>
<gates>
<gate name="G$1" symbol="LM78XX-HOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO220-4">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
<connect gate="G$1" pin="SHIELD" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X02">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X2">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_BIG">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
</package>
<package name="JST-2-SMD-VERT">
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="2.54" y="-5.08" size="1.27" layer="25">&gt;Name</text>
<text x="2.24" y="3.48" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="R_SW_TH">
<wire x1="-1.651" y1="19.2532" x2="-1.651" y2="-1.3716" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-1.3716" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="19.2532" x2="13.589" y2="19.2532" width="0.2032" layer="21"/>
<wire x1="13.589" y1="19.2532" x2="13.589" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="13.589" y1="-2.2352" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.6002"/>
<pad name="P$2" x="0" y="16.9926" drill="1.6002"/>
<pad name="P$3" x="12.0904" y="15.494" drill="1.6002"/>
<pad name="P$4" x="12.0904" y="8.4582" drill="1.6002"/>
</package>
<package name="SCREWTERMINAL-5MM-2">
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="MOLEX-1X2_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
</package>
<package name="1X02_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-2-PTH">
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
</package>
<package name="1X02_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;H3&gt;JST-2-PTH-KIT&lt;/h3&gt;
2-Pin JST, through-hole connector&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.127" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST-2-PTH-VERT">
<wire x1="-2.95" y1="-2.25" x2="-2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="2.25" x2="2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="2.25" x2="2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-1.984" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="2.016" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="0.616" y="0.75" size="1.27" layer="51">+</text>
<text x="-1.384" y="0.75" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-SMD">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="0" size="0.4064" layer="27">&gt;Value</text>
<text x="-1.8415" y="-3.81" size="1.27" layer="21" font="vector" ratio="15" align="center-right">&gt;TOP_NEG</text>
<text x="-1.8415" y="-3.81" size="1.27" layer="22" font="vector" ratio="15" align="center-right">&gt;BTM_NEG</text>
<text x="1.8415" y="-3.81" size="1.27" layer="21" font="vector" ratio="15" rot="R180" align="center-right">&gt;TOP_POS</text>
<text x="1.8415" y="-3.81" size="1.27" layer="22" font="vector" ratio="15" rot="R180" align="center-right">&gt;BTM_POS</text>
</package>
</packages>
<symbols>
<symbol name="M02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M02" prefix="JP" uservalue="yes">
<description>Standard 2-pin 0.1" header. Use with &lt;br&gt;
- straight break away headers ( PRT-00116)&lt;br&gt;
- right angle break away headers (PRT-00553)&lt;br&gt;
- swiss pins (PRT-00743)&lt;br&gt;
- machine pins (PRT-00117)&lt;br&gt;
- female headers (PRT-00115)&lt;br&gt;&lt;br&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER" package="R_SW_TH">
<connects>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="2" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="STOREFRONT_ID" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-2-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-power">
<description>&lt;b&gt;Power Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TO220BV">
<description>&lt;b&gt;Molded Package&lt;/b&gt;&lt;p&gt;
grid 2.54 mm</description>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="G" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="D" x="0" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="S" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MFPD">
<wire x1="3.81" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="3.81" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="4.445" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.635" x2="3.175" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="3.175" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.762" x2="2.921" y2="1.016" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="4.699" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="-1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.5334" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.2352" y1="0" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.508" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0" x2="1.016" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0.508" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0.254" x2="2.032" y2="0" width="0.3048" layer="94"/>
<wire x1="2.032" y1="0" x2="1.143" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.143" y1="-0.254" x2="1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="1.143" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.524" y="-3.302" size="0.8128" layer="93">D</text>
<text x="1.524" y="2.54" size="0.8128" layer="93">S</text>
<text x="-2.54" y="-2.032" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="MFNS">
<wire x1="-1.1176" y1="2.413" x2="-1.1176" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.1176" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.5334" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="5.08" y2="1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.08" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="4.445" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.635" x2="5.715" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-0.635" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.715" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0.762" x2="5.969" y2="1.016" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="4.191" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0" x2="1.778" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0.254" x2="0.762" y2="0" width="0.3048" layer="94"/>
<wire x1="0.762" y1="0" x2="1.651" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.651" y1="-0.254" x2="1.651" y2="0" width="0.3048" layer="94"/>
<wire x1="1.651" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="7.62" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="2.54" size="0.8128" layer="93">D</text>
<text x="1.27" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="IRF9530" prefix="Q">
<description>&lt;b&gt;P-CHANNEL HEXFET POWER-MOS-FET&lt;/b&gt;&lt;p&gt;
Source: http://www.irf.com/product-info/datasheets/data/irf9530.pdf</description>
<gates>
<gate name="G$1" symbol="MFPD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO220BV">
<connects>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IRF540" prefix="Q">
<description>&lt;b&gt;HEXFET Power MOSFET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MFNS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO220BV">
<connects>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="SUPPLY9" library="EE445L" deviceset="GND" device=""/>
<part name="P+1" library="EE445L" deviceset="+5V" device=""/>
<part name="SUPPLY4" library="EE445L" deviceset="GND" device=""/>
<part name="X1" library="EE445L" deviceset="KK-156-2" device=""/>
<part name="X2" library="EE445L" deviceset="KK-156-2" device=""/>
<part name="SUPPLY6" library="EE445L" deviceset="GND" device=""/>
<part name="LAUNCHPAD" library="EE445L" deviceset="EK-TM4C123GXL" device="P"/>
<part name="SUPPLY8" library="EE445L" deviceset="GND" device=""/>
<part name="P+3" library="EE445L" deviceset="+5V" device=""/>
<part name="C10" library="EE445L" deviceset="CAPACITOR" device="025-024X044" value="0.1uF"/>
<part name="SUPPLY21" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY24" library="EE445L" deviceset="GND" device=""/>
<part name="P+5" library="EE445L" deviceset="+5V" device=""/>
<part name="5V_OUT" library="SparkFun-Connectors" deviceset="M02" device="3.5MM" value="+5V"/>
<part name="SUPPLY7" library="EE445L" deviceset="GND" device=""/>
<part name="R12" library="EE445L" deviceset="RESISTOR" device="0204/7" value="120"/>
<part name="CAN" library="SparkFun-Connectors" deviceset="M02" device="3.5MM" value="CAN"/>
<part name="SERVO_B" library="EE445L" deviceset="PINHD-1X3" device=""/>
<part name="SUPPLY25" library="EE445L" deviceset="GND" device=""/>
<part name="Q5" library="transistor-power" deviceset="IRF540" device=""/>
<part name="Q6" library="transistor-power" deviceset="IRF540" device=""/>
<part name="Q1" library="EE445L" deviceset="PN2222" device=""/>
<part name="Q2" library="EE445L" deviceset="PN2222" device=""/>
<part name="Q10" library="transistor-power" deviceset="IRF9530" device=""/>
<part name="Q9" library="transistor-power" deviceset="IRF9530" device=""/>
<part name="R11" library="EE445L" deviceset="RESISTOR" device="0204/7" value="10k"/>
<part name="R9" library="EE445L" deviceset="RESISTOR" device="0204/7" value="10k"/>
<part name="SUPPLY36" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY37" library="EE445L" deviceset="GND" device=""/>
<part name="R10" library="EE445L" deviceset="RESISTOR" device="0204/7" value="1k"/>
<part name="R8" library="EE445L" deviceset="RESISTOR" device="0204/7" value="1k"/>
<part name="C9" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="1uF"/>
<part name="SUPPLY38" library="EE445L" deviceset="GND" device=""/>
<part name="Q7" library="transistor-power" deviceset="IRF540" device=""/>
<part name="Q8" library="transistor-power" deviceset="IRF540" device=""/>
<part name="Q3" library="EE445L" deviceset="PN2222" device=""/>
<part name="Q4" library="EE445L" deviceset="PN2222" device=""/>
<part name="Q12" library="transistor-power" deviceset="IRF9530" device=""/>
<part name="Q11" library="EE445L" deviceset="IRF9530" device=""/>
<part name="R7" library="EE445L" deviceset="RESISTOR" device="0204/7" value="10k"/>
<part name="R5" library="EE445L" deviceset="RESISTOR" device="0204/7" value="10k"/>
<part name="SUPPLY39" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY40" library="EE445L" deviceset="GND" device=""/>
<part name="R6" library="EE445L" deviceset="RESISTOR" device="0204/7" value="1k"/>
<part name="R4" library="EE445L" deviceset="RESISTOR" device="0204/7" value="1k"/>
<part name="C8" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="1uF"/>
<part name="SUPPLY41" library="EE445L" deviceset="GND" device=""/>
<part name="LED1" library="EE445L" deviceset="LED" device="5MM" value="Green"/>
<part name="R2" library="EE445L" deviceset="RESISTOR" device="0204/7" value="2k"/>
<part name="SUPPLY28" library="EE445L" deviceset="GND" device=""/>
<part name="R3" library="EE445L" deviceset="RESISTOR" device="0204/7" value="680"/>
<part name="LED2" library="EE445L" deviceset="LED" device="5MM" value="Green"/>
<part name="SUPPLY29" library="EE445L" deviceset="GND" device=""/>
<part name="P+19" library="EE445L" deviceset="+5V" device=""/>
<part name="+5V" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="+11.1V" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="X3" library="EE445L" deviceset="KK-156-2" device=""/>
<part name="X4" library="EE445L" deviceset="KK-156-2" device=""/>
<part name="+3V1" library="EE445L" deviceset="+3V3" device=""/>
<part name="SERVO_A" library="EE445L" deviceset="PINHD-1X3" device=""/>
<part name="SUPPLY1" library="EE445L" deviceset="GND" device=""/>
<part name="+3.3V" library="EE445L" deviceset="PINHD-1X1" device="" value="+3.3V"/>
<part name="+3V2" library="EE445L" deviceset="+3V3" device=""/>
<part name="U$17" library="EE445L" deviceset="+11V1" device=""/>
<part name="U$18" library="EE445L" deviceset="+11V1" device=""/>
<part name="U$19" library="EE445L" deviceset="+11V1" device=""/>
<part name="U$20" library="EE445L" deviceset="+11V1" device=""/>
<part name="U$21" library="EE445L" deviceset="+11V1" device=""/>
<part name="JP2" library="EE445L" deviceset="PINHD-1X2" device=""/>
<part name="U1" library="EE445L" deviceset="DRV8848" device=""/>
<part name="SUPPLY2" library="EE445L" deviceset="GND" device=""/>
<part name="C7" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="2.2uF"/>
<part name="PC6" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="PC7" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="PC5" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="PC4" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="GND" library="EE445L" deviceset="PINHD-1X1" device="" value="GND"/>
<part name="GND1" library="EE445L" deviceset="PINHD-1X1" device="" value="GND"/>
<part name="R1" library="EE445L" deviceset="RESISTOR" device="0204/7" value="10k"/>
<part name="+3V3" library="EE445L" deviceset="+3V3" device=""/>
<part name="SUPPLY3" library="EE445L" deviceset="GND" device=""/>
<part name="FAULT" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="CANL" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="CANH" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="C11" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="10uF"/>
<part name="C12" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="10uF"/>
<part name="C6" library="EE445L" deviceset="CAPACITOR" device="C0805K" value="10uF"/>
<part name="C5" library="EE445L" deviceset="CAPACITOR" device="C0805K" value="0.1uF"/>
<part name="SERVOB" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="SERVOA" library="EE445L" deviceset="PINHD-1X1" device=""/>
<part name="U3" library="EE445L" deviceset="MCP2551P" device=""/>
<part name="J7" library="EE445L" deviceset="KK-100-2" device=""/>
<part name="BAT" library="EE445L" deviceset="PINHD-1X2" device=""/>
<part name="SUPPLY5" library="EE445L" deviceset="GND" device=""/>
<part name="SUPPLY10" library="EE445L" deviceset="GND" device=""/>
<part name="C1" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="4.7uF"/>
<part name="C2" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="1uF"/>
<part name="C3" library="EE445L" deviceset="CAPACITOR_POLARIZED" device="E5-6" value="4.7uF"/>
<part name="U2" library="EE445L" deviceset="LM78XX_HOR" device=""/>
<part name="U4" library="EE445L" deviceset="LM78XX_HOR" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-43.18" y="73.66" size="1.778" layer="97">Robot controller version 4
Jon Valvano
   / Steven Prickett 
    / Brent Wylie
Feb 18, 2016
EE445M/EE380L.6</text>
<frame x1="-152.4" y1="-76.2" x2="114.3" y2="120.65" columns="8" rows="5" layer="97"/>
<text x="-86.106" y="46.482" size="1.778" layer="97">Left motor</text>
<text x="-86.36" y="26.416" size="1.778" layer="97">Right motor</text>
<text x="69.85" y="-8.89" size="1.778" layer="97">CAN interface</text>
<text x="-31.75" y="111.76" size="5.08" layer="95" align="center">Motor Board</text>
<wire x1="-11.43" y1="50.546" x2="-11.43" y2="108.966" width="0.1524" layer="97"/>
<wire x1="-11.43" y1="108.966" x2="107.95" y2="108.966" width="0.1524" layer="97"/>
<wire x1="107.95" y1="108.966" x2="107.95" y2="50.546" width="0.1524" layer="97"/>
<wire x1="107.95" y1="50.546" x2="-11.43" y2="50.546" width="0.1524" layer="97"/>
<wire x1="-11.43" y1="108.966" x2="-11.43" y2="114.046" width="0.1524" layer="97"/>
<wire x1="-11.43" y1="114.046" x2="107.95" y2="114.046" width="0.1524" layer="97"/>
<wire x1="107.95" y1="114.046" x2="107.95" y2="108.966" width="0.1524" layer="97"/>
<text x="25.146" y="110.744" size="1.778" layer="97">Two +5V linear supplies</text>
<text x="-92.71" y="77.47" size="2.54" layer="97" rot="MR0">PB7 A+
PB6 A-
PB5 B+
PB4 B-</text>
<text x="53.34" y="-43.18" size="1.27" layer="97">vref is nc</text>
<text x="-55.372" y="70.358" size="1.778" layer="95" align="center">Steering Servo Header</text>
<wire x1="-146.05" y1="-69.85" x2="-146.05" y2="-10.16" width="0.1524" layer="97"/>
<wire x1="-146.05" y1="-10.16" x2="-52.07" y2="-10.16" width="0.1524" layer="97"/>
<wire x1="-52.07" y1="-10.16" x2="-52.07" y2="-69.85" width="0.1524" layer="97"/>
<wire x1="-52.07" y1="-69.85" x2="-146.05" y2="-69.85" width="0.1524" layer="97"/>
<wire x1="-146.05" y1="-10.16" x2="-146.05" y2="-6.35" width="0.1524" layer="97"/>
<wire x1="-146.05" y1="-6.35" x2="-52.07" y2="-6.35" width="0.1524" layer="97"/>
<wire x1="-52.07" y1="-6.35" x2="-52.07" y2="-10.16" width="0.1524" layer="97"/>
<wire x1="-50.8" y1="-69.85" x2="-50.8" y2="-10.16" width="0.1524" layer="97"/>
<wire x1="-50.8" y1="-10.16" x2="-50.8" y2="-6.35" width="0.1524" layer="97"/>
<wire x1="-50.8" y1="-6.35" x2="43.18" y2="-6.35" width="0.1524" layer="97"/>
<wire x1="43.18" y1="-6.35" x2="43.18" y2="-10.16" width="0.1524" layer="97"/>
<wire x1="43.18" y1="-10.16" x2="43.18" y2="-69.85" width="0.1524" layer="97"/>
<wire x1="43.18" y1="-69.85" x2="-50.8" y2="-69.85" width="0.1524" layer="97"/>
<wire x1="-50.8" y1="-10.16" x2="43.18" y2="-10.16" width="0.1524" layer="97"/>
<wire x1="69.85" y1="-3.81" x2="69.85" y2="44.45" width="0.1524" layer="97"/>
<wire x1="69.85" y1="44.45" x2="-10.795" y2="44.45" width="0.1524" layer="97"/>
<wire x1="-10.795" y1="44.45" x2="-10.795" y2="-3.81" width="0.1524" layer="97"/>
<wire x1="-10.795" y1="-3.81" x2="69.85" y2="-3.81" width="0.1524" layer="97"/>
<wire x1="-10.795" y1="44.45" x2="-10.795" y2="48.26" width="0.1524" layer="97"/>
<wire x1="-10.795" y1="48.26" x2="69.85" y2="48.26" width="0.1524" layer="97"/>
<wire x1="69.85" y1="48.26" x2="69.85" y2="44.45" width="0.1524" layer="97"/>
<text x="21.336" y="45.466" size="1.778" layer="97">Power Indicator LEDs</text>
<wire x1="-146.05" y1="-3.81" x2="-71.12" y2="-3.81" width="0.1524" layer="97"/>
<wire x1="-71.12" y1="-3.81" x2="-71.12" y2="68.58" width="0.1524" layer="97"/>
<wire x1="-71.12" y1="68.58" x2="-146.05" y2="68.58" width="0.1524" layer="97"/>
<wire x1="-146.05" y1="68.58" x2="-146.05" y2="-3.81" width="0.1524" layer="97"/>
<wire x1="-146.05" y1="68.58" x2="-146.05" y2="72.39" width="0.1524" layer="97"/>
<wire x1="-146.05" y1="72.39" x2="-71.12" y2="72.39" width="0.1524" layer="97"/>
<wire x1="-71.12" y1="72.39" x2="-71.12" y2="68.58" width="0.1524" layer="97"/>
<text x="-99.06" y="-8.382" size="1.778" layer="97" align="center">Discrete H-Bridge A</text>
<text x="-4.064" y="-8.636" size="1.778" layer="97" align="center">Discrete H-Bridge B</text>
<text x="-111.252" y="70.358" size="1.778" layer="97" align="center">DRV8848 Dual Brushed DC Motor Driver</text>
<wire x1="-68.58" y1="-3.81" x2="-14.224" y2="-3.81" width="0.1524" layer="97"/>
<wire x1="-14.224" y1="-3.81" x2="-14.224" y2="68.58" width="0.1524" layer="97"/>
<wire x1="-14.224" y1="68.58" x2="-68.58" y2="68.58" width="0.1524" layer="97"/>
<wire x1="-68.58" y1="68.58" x2="-68.58" y2="-3.81" width="0.1524" layer="97"/>
<wire x1="-68.58" y1="68.58" x2="-68.58" y2="72.39" width="0.1524" layer="97"/>
<wire x1="-68.58" y1="72.39" x2="-14.224" y2="72.39" width="0.1524" layer="97"/>
<wire x1="-14.224" y1="72.39" x2="-14.224" y2="68.58" width="0.1524" layer="97"/>
<wire x1="46.99" y1="-69.85" x2="46.99" y2="-10.16" width="0.1524" layer="95"/>
<wire x1="46.99" y1="-10.16" x2="46.99" y2="-6.35" width="0.1524" layer="95"/>
<wire x1="46.99" y1="-6.35" x2="107.95" y2="-6.35" width="0.1524" layer="95"/>
<wire x1="107.95" y1="-6.35" x2="107.95" y2="-10.16" width="0.1524" layer="95"/>
<wire x1="107.95" y1="-10.16" x2="107.95" y2="-69.85" width="0.1524" layer="95"/>
<wire x1="107.95" y1="-69.85" x2="46.99" y2="-69.85" width="0.1524" layer="95"/>
<wire x1="46.99" y1="-10.16" x2="107.95" y2="-10.16" width="0.1524" layer="95"/>
<wire x1="107.442" y1="-3.81" x2="107.442" y2="44.45" width="0.1524" layer="97"/>
<wire x1="107.442" y1="44.45" x2="73.152" y2="44.45" width="0.1524" layer="97"/>
<wire x1="73.152" y1="44.45" x2="73.152" y2="-3.81" width="0.1524" layer="97"/>
<wire x1="73.152" y1="-3.81" x2="107.442" y2="-3.81" width="0.1524" layer="97"/>
<wire x1="73.152" y1="44.45" x2="73.152" y2="48.26" width="0.1524" layer="97"/>
<wire x1="73.152" y1="48.26" x2="107.442" y2="48.26" width="0.1524" layer="97"/>
<wire x1="107.442" y1="48.26" x2="107.442" y2="44.45" width="0.1524" layer="97"/>
<text x="75.438" y="45.466" size="1.778" layer="97">Profiling</text>
<text x="-132.08" y="-60.96" size="1.778" layer="97">Q3</text>
<text x="-68.58" y="-60.96" size="1.778" layer="97">Q4</text>
<text x="-38.1" y="-60.96" size="1.778" layer="97">Q1</text>
<text x="27.94" y="-60.96" size="1.778" layer="97">Q2</text>
<text x="-109.22" y="-58.42" size="1.778" layer="97">N-channel could be
IRF522
IRF530
IRF540</text>
<text x="-12.7" y="-58.42" size="1.778" layer="97">N-channel could be
IRF522
IRF530
IRF540</text>
<text x="-142.24" y="-20.32" size="1.778" layer="97">P-channel could be
IRF9530
IRF9540</text>
<text x="-45.72" y="-20.32" size="1.778" layer="97">P-channel could be
IRF9530
IRF9540</text>
<text x="-10.795" y="59.69" size="1.778" layer="97">Deans </text>
<text x="-24.13" y="33.02" size="2.54" layer="97" rot="MR0">PD1 ServoB
PD0 ServoA</text>
<text x="102.87" y="-67.31" size="2.54" layer="97" rot="MR0">PE4 CAN RxD
PE5 CAN TxD</text>
<text x="-114.3" y="-2.54" size="1.778" layer="97">If you build the DRV8848 circuit
do not include the discrete H-bridges</text>
<text x="-91.44" y="-17.78" size="1.778" layer="97">If you build the discrete H-bridges
do not include the DRV8848 circuit</text>
<text x="38.1" y="99.06" size="1.778" layer="97">LM7805 1A</text>
<text x="27.94" y="68.58" size="1.778" layer="97">LM7805 1A</text>
<text x="-66.04" y="38.1" size="1.778" layer="97">10V</text>
<text x="66.04" y="53.34" size="1.778" layer="97">Both U2 and U4 need heat sinks</text>
<text x="-106.68" y="-30.48" size="1.778" layer="97">35V</text>
<text x="-10.16" y="-30.48" size="1.778" layer="97">35V</text>
<text x="-66.04" y="2.54" size="1.778" layer="97">10V</text>
<text x="17.78" y="83.82" size="1.778" layer="97">35V</text>
<text x="-129.54" y="63.5" size="1.778" layer="91">DO NOT BUILD DRV8848 CIRCUIT</text>
<text x="63.5" y="60.96" size="1.778" layer="97">16V</text>
<text x="63.5" y="81.28" size="1.778" layer="97">16V</text>
</plain>
<instances>
<instance part="SUPPLY9" gate="GND" x="-90.17" y="101.6" smashed="yes">
<attribute name="VALUE" x="-96.901" y="105.029" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="1" x="68.58" y="-15.24" smashed="yes">
<attribute name="VALUE" x="67.31" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="68.58" y="-60.96"/>
<instance part="X1" gate="-1" x="-78.105" y="41.91"/>
<instance part="X1" gate="-2" x="-78.105" y="36.83"/>
<instance part="X2" gate="-2" x="-78.105" y="16.51"/>
<instance part="X2" gate="-1" x="-78.105" y="21.59"/>
<instance part="SUPPLY6" gate="GND" x="-90.805" y="6.985" smashed="yes">
<attribute name="VALUE" x="-89.535" y="8.636" size="1.778" layer="96"/>
</instance>
<instance part="LAUNCHPAD" gate="G$1" x="-99.06" y="96.52" smashed="yes">
<attribute name="NAME" x="-129.54" y="113.03" size="1.778" layer="95"/>
<attribute name="VALUE" x="-129.54" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="-35.56" y="104.14" smashed="yes">
<attribute name="VALUE" x="-45.085" y="103.251" size="1.778" layer="96"/>
</instance>
<instance part="P+3" gate="1" x="-100.33" y="111.76" smashed="yes">
<attribute name="VALUE" x="-102.362" y="112.268" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="75.184" y="-20.32" rot="R90"/>
<instance part="SUPPLY21" gate="GND" x="80.264" y="-24.13" smashed="yes">
<attribute name="VALUE" x="74.041" y="-27.305" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY24" gate="GND" x="45.72" y="55.88" smashed="yes">
<attribute name="VALUE" x="50.165" y="53.721" size="1.778" layer="96"/>
</instance>
<instance part="P+5" gate="1" x="82.55" y="103.886" smashed="yes">
<attribute name="VALUE" x="81.28" y="103.886" size="1.778" layer="96"/>
</instance>
<instance part="5V_OUT" gate="G$1" x="97.79" y="95.25" smashed="yes" rot="MR0">
<attribute name="NAME" x="101.6" y="91.44" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="89.154" y="91.186" smashed="yes">
<attribute name="VALUE" x="91.567" y="89.027" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R12" gate="G$1" x="87.63" y="-38.1" rot="R90"/>
<instance part="CAN" gate="G$1" x="99.06" y="-39.37" smashed="yes">
<attribute name="NAME" x="101.854" y="-31.75" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="SERVO_B" gate="A" x="-41.91" y="48.26"/>
<instance part="SUPPLY25" gate="GND" x="-57.15" y="35.56" smashed="yes">
<attribute name="VALUE" x="-56.007" y="37.211" size="1.778" layer="96"/>
</instance>
<instance part="Q5" gate="G$1" x="-21.59" y="-52.07" smashed="yes">
<attribute name="NAME" x="-22.86" y="-57.785" size="1.778" layer="95"/>
<attribute name="VALUE" x="-24.13" y="-53.34" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="Q6" gate="G$1" x="13.97" y="-52.07" smashed="yes" rot="MR0">
<attribute name="NAME" x="10.16" y="-57.15" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="17.78" y="-46.355" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="Q1" gate="G1" x="-34.29" y="-52.07" smashed="yes">
<attribute name="VALUE" x="-41.91" y="-57.15" size="1.778" layer="96"/>
</instance>
<instance part="Q2" gate="G1" x="26.67" y="-52.07" smashed="yes" rot="MR0">
<attribute name="VALUE" x="34.29" y="-57.15" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="Q10" gate="G$1" x="13.97" y="-33.02" smashed="yes" rot="MR0">
<attribute name="NAME" x="9.525" y="-29.21" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="17.78" y="-25.4" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="Q9" gate="G$1" x="-21.59" y="-33.02" smashed="yes">
<attribute name="NAME" x="-17.145" y="-28.575" size="1.778" layer="95"/>
<attribute name="VALUE" x="-23.495" y="-34.925" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R11" gate="G$1" x="24.13" y="-29.21" smashed="yes" rot="R90">
<attribute name="NAME" x="22.6314" y="-33.02" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="27.432" y="-33.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R9" gate="G$1" x="-31.75" y="-29.21" smashed="yes" rot="R90">
<attribute name="NAME" x="-33.2486" y="-33.02" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-28.448" y="-33.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY36" gate="GND" x="17.78" y="-64.77" smashed="yes">
<attribute name="VALUE" x="22.225" y="-66.929" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY37" gate="GND" x="-25.4" y="-64.77" smashed="yes">
<attribute name="VALUE" x="-20.955" y="-66.929" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="31.75" y="-44.45" smashed="yes" rot="R270">
<attribute name="NAME" x="33.2486" y="-40.64" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="28.448" y="-40.64" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R8" gate="G$1" x="-39.37" y="-44.45" smashed="yes" rot="R90">
<attribute name="NAME" x="-40.8686" y="-48.26" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-36.068" y="-48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="-3.81" y="-25.4"/>
<instance part="SUPPLY38" gate="GND" x="-3.81" y="-34.29" smashed="yes">
<attribute name="VALUE" x="-6.477" y="-38.227" size="1.778" layer="96"/>
</instance>
<instance part="Q7" gate="G$1" x="-116.84" y="-52.07" smashed="yes">
<attribute name="NAME" x="-111.76" y="-48.26" size="1.778" layer="95"/>
<attribute name="VALUE" x="-119.38" y="-53.34" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="Q8" gate="G$1" x="-81.28" y="-52.07" smashed="yes" rot="MR0">
<attribute name="NAME" x="-85.725" y="-57.15" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-77.47" y="-46.355" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="Q3" gate="G1" x="-129.54" y="-52.07" smashed="yes">
<attribute name="VALUE" x="-137.16" y="-57.15" size="1.778" layer="96"/>
</instance>
<instance part="Q4" gate="G1" x="-68.58" y="-52.07" smashed="yes" rot="MR0">
<attribute name="VALUE" x="-60.96" y="-57.15" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="Q12" gate="G$1" x="-81.28" y="-33.02" smashed="yes" rot="MR0">
<attribute name="NAME" x="-86.36" y="-30.48" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-77.47" y="-25.4" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="Q11" gate="G$1" x="-116.84" y="-33.02" smashed="yes">
<attribute name="NAME" x="-111.76" y="-30.48" size="1.778" layer="95"/>
<attribute name="VALUE" x="-118.745" y="-34.29" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R7" gate="G$1" x="-71.12" y="-29.21" smashed="yes" rot="R90">
<attribute name="NAME" x="-72.6186" y="-33.02" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-67.818" y="-33.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R5" gate="G$1" x="-127" y="-29.21" smashed="yes" rot="R90">
<attribute name="NAME" x="-128.4986" y="-33.02" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-123.698" y="-33.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY39" gate="GND" x="-77.47" y="-64.77" smashed="yes">
<attribute name="VALUE" x="-73.025" y="-66.929" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY40" gate="GND" x="-120.65" y="-64.77" smashed="yes">
<attribute name="VALUE" x="-116.205" y="-66.929" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="-63.5" y="-44.45" smashed="yes" rot="R270">
<attribute name="NAME" x="-62.0014" y="-40.64" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-66.802" y="-40.64" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R4" gate="G$1" x="-134.62" y="-44.45" smashed="yes" rot="R90">
<attribute name="NAME" x="-136.1186" y="-48.26" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-131.318" y="-48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="-99.06" y="-25.4"/>
<instance part="SUPPLY41" gate="GND" x="-99.06" y="-34.29" smashed="yes">
<attribute name="VALUE" x="-101.727" y="-38.227" size="1.778" layer="96"/>
</instance>
<instance part="LED1" gate="G$1" x="3.175" y="14.605"/>
<instance part="R2" gate="G$1" x="3.175" y="23.495" rot="R90"/>
<instance part="SUPPLY28" gate="GND" x="3.175" y="5.715"/>
<instance part="R3" gate="G$1" x="26.67" y="23.495" rot="R90"/>
<instance part="LED2" gate="G$1" x="26.67" y="14.605"/>
<instance part="SUPPLY29" gate="GND" x="26.67" y="5.715"/>
<instance part="P+19" gate="1" x="26.67" y="36.195" smashed="yes">
<attribute name="VALUE" x="24.003" y="33.528" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+5V" gate="G$1" x="31.75" y="32.385" smashed="yes" rot="R90">
<attribute name="NAME" x="31.115" y="33.655" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="36.83" y="26.035" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+11.1V" gate="G$1" x="8.255" y="32.385" smashed="yes" rot="R90">
<attribute name="NAME" x="7.62" y="33.655" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.335" y="26.035" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X3" gate="-1" x="-104.14" y="-43.18"/>
<instance part="X3" gate="-2" x="-88.9" y="-43.18" rot="R180"/>
<instance part="X4" gate="-1" x="-12.7" y="-43.18"/>
<instance part="X4" gate="-2" x="6.35" y="-43.18" rot="R180"/>
<instance part="+3V1" gate="G$1" x="-137.16" y="113.03"/>
<instance part="SERVO_A" gate="A" x="-41.91" y="12.7"/>
<instance part="SUPPLY1" gate="GND" x="-57.15" y="0" smashed="yes">
<attribute name="VALUE" x="-56.515" y="1.651" size="1.778" layer="96"/>
</instance>
<instance part="+3.3V" gate="G$1" x="53.34" y="36.83" rot="R90"/>
<instance part="+3V2" gate="G$1" x="63.5" y="39.37"/>
<instance part="U$17" gate="G$1" x="2.54" y="101.6"/>
<instance part="U$18" gate="G$1" x="3.175" y="36.195"/>
<instance part="U$19" gate="G$1" x="-142.9004" y="65.405" smashed="yes">
<attribute name="VALUE" x="-140.6144" y="64.643" size="1.778" layer="96"/>
</instance>
<instance part="U$20" gate="G$1" x="-99.06" y="-16.51"/>
<instance part="U$21" gate="G$1" x="-3.81" y="-16.51"/>
<instance part="JP2" gate="G$1" x="78.994" y="94.234" smashed="yes" rot="R180">
<attribute name="NAME" x="75.438" y="95.885" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="85.344" y="99.314" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U1" gate="A" x="-110.49" y="31.75" smashed="yes">
<attribute name="NAME" x="-120.9294" y="58.6486" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="-115.57" y="53.9242" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="-84.074" y="50.546" smashed="yes">
<attribute name="VALUE" x="-82.677" y="51.689" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="-84.074" y="60.452"/>
<instance part="PC6" gate="G$1" x="80.01" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="91.44" y="29.845" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="86.36" y="33.02" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PC7" gate="G$1" x="80.01" y="40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="91.44" y="42.545" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="86.36" y="45.72" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PC5" gate="G$1" x="80.01" y="15.24" smashed="yes" rot="R180">
<attribute name="NAME" x="91.44" y="17.145" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="86.36" y="20.32" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PC4" gate="G$1" x="80.01" y="2.54" smashed="yes" rot="R180">
<attribute name="NAME" x="91.44" y="4.445" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="86.36" y="7.62" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND" gate="G$1" x="52.07" y="24.13" smashed="yes" rot="R180">
<attribute name="NAME" x="63.5" y="26.035" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="57.15" y="29.21" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND1" gate="G$1" x="52.07" y="11.43" smashed="yes" rot="R180">
<attribute name="NAME" x="58.42" y="8.255" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="55.88" y="16.51" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R1" gate="G$1" x="-142.24" y="8.89" smashed="yes" rot="R90">
<attribute name="NAME" x="-143.7386" y="5.08" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-138.684" y="6.35" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V3" gate="G$1" x="-142.24" y="17.78" smashed="yes">
<attribute name="VALUE" x="-138.43" y="12.954" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="62.23" y="3.81" smashed="yes">
<attribute name="VALUE" x="62.865" y="-0.889" size="1.778" layer="96"/>
</instance>
<instance part="FAULT" gate="G$1" x="-121.92" y="0" smashed="yes">
<attribute name="NAME" x="-135.89" y="-2.413" size="1.778" layer="95"/>
<attribute name="VALUE" x="-128.27" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="CANL" gate="G$1" x="87.63" y="-55.88" smashed="yes" rot="R270">
<attribute name="NAME" x="85.09" y="-59.817" size="1.778" layer="95"/>
<attribute name="VALUE" x="82.55" y="-49.53" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="CANH" gate="G$1" x="87.63" y="-20.828" smashed="yes" rot="R90">
<attribute name="NAME" x="90.678" y="-17.399" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="92.71" y="-27.178" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="-63.5" y="48.26"/>
<instance part="C12" gate="G$1" x="-63.5" y="12.7"/>
<instance part="C6" gate="G$1" x="-132.08" y="59.69" smashed="yes">
<attribute name="NAME" x="-129.921" y="60.706" size="1.778" layer="95"/>
<attribute name="VALUE" x="-131.064" y="55.499" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="-139.065" y="60.325" smashed="yes">
<attribute name="NAME" x="-138.049" y="60.96" size="1.778" layer="95"/>
<attribute name="VALUE" x="-136.398" y="52.705" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SERVOB" gate="G$1" x="-25.4" y="58.42" smashed="yes">
<attribute name="NAME" x="-43.18" y="59.69" size="1.778" layer="95"/>
<attribute name="VALUE" x="-31.75" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="SERVOA" gate="G$1" x="-25.4" y="22.86" smashed="yes">
<attribute name="NAME" x="-43.18" y="24.13" size="1.778" layer="95"/>
<attribute name="VALUE" x="-31.75" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="81.28" y="-43.18" smashed="yes">
<attribute name="NAME" x="77.47" y="-31.75" size="1.778" layer="95"/>
<attribute name="VALUE" x="54.61" y="-58.42" size="1.778" layer="96"/>
</instance>
<instance part="J7" gate="G$1" x="-7.62" y="67.31" smashed="yes" rot="R180">
<attribute name="NAME" x="-1.27" y="61.595" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="0" y="72.39" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="BAT" gate="G$1" x="-8.255" y="81.915" smashed="yes" rot="R180">
<attribute name="NAME" x="-4.445" y="86.995" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-1.905" y="86.995" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="-90.805" y="28.575" smashed="yes">
<attribute name="VALUE" x="-91.44" y="32.766" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY10" gate="GND" x="-132.08" y="6.985" smashed="yes">
<attribute name="VALUE" x="-128.905" y="7.366" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="68.58" y="67.31"/>
<instance part="C2" gate="G$1" x="25.4" y="87.63"/>
<instance part="C3" gate="G$1" x="68.58" y="87.63"/>
<instance part="U2" gate="G$1" x="45.72" y="71.12"/>
<instance part="U4" gate="G$1" x="45.72" y="91.44"/>
</instances>
<busses>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="+3.3V"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="-134.62" y1="106.68" x2="-137.16" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="106.68" x2="-137.16" y2="110.49" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3.3V" gate="G$1" pin="1"/>
<wire x1="63.5" y1="34.29" x2="53.34" y2="34.29" width="0.1524" layer="91"/>
<wire x1="63.5" y1="36.83" x2="63.5" y2="34.29" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="15.24" x2="-142.24" y2="13.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<pinref part="LAUNCHPAD" gate="G$1" pin="GND"/>
<wire x1="-111.76" y1="104.14" x2="-90.17" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="68.58" y1="-58.42" x2="68.58" y2="-55.88" width="0.1524" layer="91"/>
<junction x="68.58" y="-55.88"/>
<wire x1="68.58" y1="-55.88" x2="73.66" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-48.26" x2="73.66" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-53.34" x2="68.58" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="RS"/>
<pinref part="U3" gate="G$1" pin="VSS"/>
</segment>
<segment>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<pinref part="LAUNCHPAD" gate="G$1" pin="GND2"/>
<wire x1="-50.8" y1="106.68" x2="-35.56" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<wire x1="80.264" y1="-20.32" x2="80.264" y2="-21.59" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
<pinref part="U2" gate="G$1" pin="SHIELD"/>
<wire x1="43.18" y1="63.5" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="48.26" y1="66.04" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
<wire x1="48.26" y1="63.5" x2="45.72" y2="63.5" width="0.1524" layer="91"/>
<wire x1="45.72" y1="63.5" x2="43.18" y2="63.5" width="0.1524" layer="91"/>
<junction x="45.72" y="63.5"/>
<wire x1="45.72" y1="63.5" x2="45.72" y2="58.42" width="0.1524" layer="91"/>
<label x="31.496" y="60.452" size="1.778" layer="95"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="64.77" x2="2.54" y2="64.77" width="0.1524" layer="91"/>
<wire x1="-5.715" y1="79.375" x2="2.54" y2="79.375" width="0.1524" layer="91"/>
<wire x1="2.54" y1="79.375" x2="2.54" y2="78.74" width="0.1524" layer="91"/>
<pinref part="BAT" gate="G$1" pin="1"/>
<wire x1="2.54" y1="78.74" x2="2.54" y2="64.77" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="SHIELD"/>
<wire x1="43.18" y1="86.36" x2="43.18" y2="83.82" width="0.1524" layer="91"/>
<wire x1="43.18" y1="83.82" x2="45.72" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="45.72" y1="83.82" x2="48.26" y2="83.82" width="0.1524" layer="91"/>
<wire x1="48.26" y1="83.82" x2="48.26" y2="86.36" width="0.1524" layer="91"/>
<wire x1="45.72" y1="83.82" x2="45.72" y2="78.74" width="0.1524" layer="91"/>
<junction x="45.72" y="83.82"/>
<wire x1="45.72" y1="78.74" x2="25.4" y2="78.74" width="0.1524" layer="91"/>
<junction x="2.54" y="78.74"/>
<pinref part="C2" gate="G$1" pin="-"/>
<wire x1="25.4" y1="78.74" x2="2.54" y2="78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="82.55" x2="25.4" y2="78.74" width="0.1524" layer="91"/>
<junction x="25.4" y="78.74"/>
<pinref part="C3" gate="G$1" pin="-"/>
<wire x1="45.72" y1="78.74" x2="68.58" y2="78.74" width="0.1524" layer="91"/>
<wire x1="68.58" y1="78.74" x2="68.58" y2="82.55" width="0.1524" layer="91"/>
<junction x="45.72" y="78.74"/>
<wire x1="2.54" y1="64.77" x2="2.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="2.54" y="64.77"/>
<wire x1="2.54" y1="58.42" x2="45.72" y2="58.42" width="0.1524" layer="91"/>
<junction x="45.72" y="58.42"/>
<pinref part="C1" gate="G$1" pin="-"/>
<wire x1="68.58" y1="58.42" x2="68.58" y2="62.23" width="0.1524" layer="91"/>
<wire x1="45.72" y1="58.42" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="5V_OUT" gate="G$1" pin="1"/>
<wire x1="89.154" y1="95.25" x2="90.17" y2="95.25" width="0.1524" layer="91"/>
<wire x1="89.154" y1="95.25" x2="89.154" y2="93.726" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SERVO_B" gate="A" pin="3"/>
<wire x1="-44.45" y1="45.72" x2="-57.15" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-57.15" y1="45.72" x2="-57.15" y2="40.64" width="0.1524" layer="91"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<pinref part="C11" gate="G$1" pin="-"/>
<wire x1="-57.15" y1="40.64" x2="-57.15" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="43.18" x2="-63.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="40.64" x2="-57.15" y2="40.64" width="0.1524" layer="91"/>
<junction x="-57.15" y="40.64"/>
</segment>
<segment>
<pinref part="Q1" gate="G1" pin="E"/>
<wire x1="-31.75" y1="-57.15" x2="-31.75" y2="-59.69" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="S"/>
<wire x1="-31.75" y1="-59.69" x2="-25.4" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-59.69" x2="-19.05" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="-19.05" y1="-59.69" x2="-19.05" y2="-57.15" width="0.1524" layer="91"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<wire x1="-25.4" y1="-62.23" x2="-25.4" y2="-59.69" width="0.1524" layer="91"/>
<junction x="-25.4" y="-59.69"/>
</segment>
<segment>
<pinref part="Q6" gate="G$1" pin="S"/>
<pinref part="Q2" gate="G1" pin="E"/>
<wire x1="11.43" y1="-57.15" x2="11.43" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-59.69" x2="17.78" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-59.69" x2="24.13" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="24.13" y1="-59.69" x2="24.13" y2="-57.15" width="0.1524" layer="91"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<wire x1="17.78" y1="-62.23" x2="17.78" y2="-59.69" width="0.1524" layer="91"/>
<junction x="17.78" y="-59.69"/>
</segment>
<segment>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<pinref part="C9" gate="G$1" pin="-"/>
<wire x1="-3.81" y1="-31.75" x2="-3.81" y2="-30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q3" gate="G1" pin="E"/>
<wire x1="-127" y1="-57.15" x2="-127" y2="-59.69" width="0.1524" layer="91"/>
<pinref part="Q7" gate="G$1" pin="S"/>
<wire x1="-127" y1="-59.69" x2="-120.65" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="-120.65" y1="-59.69" x2="-114.3" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-59.69" x2="-114.3" y2="-57.15" width="0.1524" layer="91"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
<wire x1="-120.65" y1="-62.23" x2="-120.65" y2="-59.69" width="0.1524" layer="91"/>
<junction x="-120.65" y="-59.69"/>
</segment>
<segment>
<pinref part="Q8" gate="G$1" pin="S"/>
<pinref part="Q4" gate="G1" pin="E"/>
<wire x1="-83.82" y1="-57.15" x2="-83.82" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="-59.69" x2="-77.47" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="-77.47" y1="-59.69" x2="-71.12" y2="-59.69" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-59.69" x2="-71.12" y2="-57.15" width="0.1524" layer="91"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
<wire x1="-77.47" y1="-62.23" x2="-77.47" y2="-59.69" width="0.1524" layer="91"/>
<junction x="-77.47" y="-59.69"/>
</segment>
<segment>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
<pinref part="C8" gate="G$1" pin="-"/>
<wire x1="-99.06" y1="-31.75" x2="-99.06" y2="-30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="3.175" y1="9.525" x2="3.175" y2="8.255" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="26.67" y1="8.255" x2="26.67" y2="9.525" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SERVO_A" gate="A" pin="3"/>
<wire x1="-44.45" y1="10.16" x2="-57.15" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-57.15" y1="10.16" x2="-57.15" y2="5.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<pinref part="C12" gate="G$1" pin="-"/>
<wire x1="-57.15" y1="5.08" x2="-57.15" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="7.62" x2="-63.5" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="5.08" x2="-57.15" y2="5.08" width="0.1524" layer="91"/>
<junction x="-57.15" y="5.08"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<pinref part="C7" gate="G$1" pin="-"/>
<wire x1="-84.074" y1="55.372" x2="-84.074" y2="53.086" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND" gate="G$1" pin="1"/>
<wire x1="54.61" y1="24.13" x2="54.61" y2="19.05" width="0.1524" layer="91"/>
<pinref part="GND1" gate="G$1" pin="1"/>
<wire x1="54.61" y1="11.43" x2="62.23" y2="11.43" width="0.1524" layer="91"/>
<wire x1="62.23" y1="11.43" x2="62.23" y2="19.05" width="0.1524" layer="91"/>
<wire x1="62.23" y1="19.05" x2="54.61" y2="19.05" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="62.23" y1="6.35" x2="62.23" y2="11.43" width="0.1524" layer="91"/>
<junction x="62.23" y="11.43"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="GND"/>
<wire x1="-128.27" y1="11.43" x2="-132.08" y2="11.43" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="EPAD"/>
<wire x1="-128.27" y1="16.51" x2="-132.08" y2="16.51" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="16.51" x2="-132.08" y2="11.43" width="0.1524" layer="91"/>
<junction x="-132.08" y="16.51"/>
<wire x1="-132.08" y1="50.8" x2="-132.08" y2="16.51" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="-139.065" y1="55.245" x2="-139.065" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-139.065" y1="50.8" x2="-132.08" y2="50.8" width="0.1524" layer="91"/>
<junction x="-132.08" y="50.8"/>
<wire x1="-132.08" y1="54.61" x2="-132.08" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="-132.08" y1="9.525" x2="-132.08" y2="11.43" width="0.1524" layer="91"/>
<junction x="-132.08" y="11.43"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="AISEN"/>
<wire x1="-92.71" y1="31.75" x2="-90.805" y2="31.75" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="-90.805" y1="31.115" x2="-90.805" y2="31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="-90.805" y1="9.525" x2="-90.805" y2="11.43" width="0.1524" layer="91"/>
<wire x1="-90.805" y1="11.43" x2="-92.71" y2="11.43" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="BISEN"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+1" gate="1" pin="+5V"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="68.58" y1="-20.32" x2="68.58" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="72.644" y1="-20.32" x2="68.58" y2="-20.32" width="0.1524" layer="91"/>
<junction x="68.58" y="-20.32"/>
<pinref part="U3" gate="G$1" pin="VDD"/>
<wire x1="68.58" y1="-22.86" x2="68.58" y2="-20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="+5V"/>
<wire x1="-111.76" y1="106.68" x2="-100.33" y2="106.68" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="-100.33" y1="106.68" x2="-100.33" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="5V_OUT" gate="G$1" pin="2"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="82.55" y1="94.234" x2="82.55" y2="97.79" width="0.1524" layer="91"/>
<wire x1="82.55" y1="97.79" x2="82.55" y2="101.346" width="0.1524" layer="91"/>
<wire x1="90.17" y1="97.79" x2="82.55" y2="97.79" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="81.534" y1="94.234" x2="82.55" y2="94.234" width="0.1524" layer="91"/>
<label x="77.216" y="99.568" size="1.778" layer="95"/>
<junction x="82.55" y="97.79"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="P+19" gate="1" pin="+5V"/>
<wire x1="26.67" y1="28.575" x2="26.67" y2="29.845" width="0.1524" layer="91"/>
<pinref part="+5V" gate="G$1" pin="1"/>
<wire x1="26.67" y1="29.845" x2="26.67" y2="33.655" width="0.1524" layer="91"/>
<wire x1="31.75" y1="29.845" x2="26.67" y2="29.845" width="0.1524" layer="91"/>
<junction x="26.67" y="29.845"/>
</segment>
</net>
<net name="+11.1V" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="+11.1V"/>
<wire x1="2.54" y1="101.6" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
<junction x="2.54" y="93.98"/>
<wire x1="38.1" y1="93.98" x2="25.4" y2="93.98" width="0.1524" layer="91"/>
<wire x1="25.4" y1="93.98" x2="5.08" y2="93.98" width="0.1524" layer="91"/>
<wire x1="5.08" y1="93.98" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
<junction x="5.08" y="93.98"/>
<label x="11.43" y="95.25" size="1.778" layer="95"/>
<wire x1="5.08" y1="67.31" x2="5.08" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="5.08" y1="73.66" x2="5.08" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="67.31" x2="5.08" y2="67.31" width="0.1524" layer="91"/>
<pinref part="BAT" gate="G$1" pin="2"/>
<wire x1="-5.715" y1="81.915" x2="2.54" y2="81.915" width="0.1524" layer="91"/>
<wire x1="2.54" y1="81.915" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="IN"/>
<pinref part="C2" gate="G$1" pin="+"/>
<wire x1="25.4" y1="90.17" x2="25.4" y2="93.98" width="0.1524" layer="91"/>
<junction x="25.4" y="93.98"/>
<pinref part="U2" gate="G$1" pin="IN"/>
<wire x1="38.1" y1="73.66" x2="5.08" y2="73.66" width="0.1524" layer="91"/>
<junction x="5.08" y="73.66"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="+11.1V"/>
<wire x1="-132.08" y1="63.5" x2="-139.065" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-139.065" y1="63.5" x2="-142.875" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-128.27" y1="52.07" x2="-142.875" y2="52.07" width="0.1524" layer="91"/>
<wire x1="-142.875" y1="52.07" x2="-142.875" y2="63.5" width="0.1524" layer="91"/>
<junction x="-142.875" y="63.5"/>
<wire x1="-142.875" y1="63.5" x2="-142.9004" y2="65.405" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VM"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-132.08" y1="62.23" x2="-132.08" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-139.065" y1="62.865" x2="-139.065" y2="63.5" width="0.1524" layer="91"/>
<junction x="-139.065" y="63.5"/>
</segment>
<segment>
<pinref part="Q9" gate="G$1" pin="S"/>
<wire x1="-19.05" y1="-27.94" x2="-19.05" y2="-21.59" width="0.1524" layer="91"/>
<pinref part="Q10" gate="G$1" pin="S"/>
<wire x1="-19.05" y1="-21.59" x2="-3.81" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-21.59" x2="11.43" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-21.59" x2="11.43" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="-31.75" y1="-24.13" x2="-31.75" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-31.75" y1="-21.59" x2="-19.05" y2="-21.59" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="11.43" y1="-21.59" x2="24.13" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="24.13" y1="-21.59" x2="24.13" y2="-24.13" width="0.1524" layer="91"/>
<junction x="-19.05" y="-21.59"/>
<junction x="11.43" y="-21.59"/>
<pinref part="C9" gate="G$1" pin="+"/>
<wire x1="-3.81" y1="-22.86" x2="-3.81" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-21.59" x2="-3.81" y2="-16.51" width="0.1524" layer="91"/>
<junction x="-3.81" y="-21.59"/>
<pinref part="U$21" gate="G$1" pin="+11.1V"/>
</segment>
<segment>
<pinref part="Q11" gate="G$1" pin="S"/>
<wire x1="-114.3" y1="-27.94" x2="-114.3" y2="-21.59" width="0.1524" layer="91"/>
<pinref part="Q12" gate="G$1" pin="S"/>
<wire x1="-114.3" y1="-21.59" x2="-99.06" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-21.59" x2="-83.82" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="-21.59" x2="-83.82" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-127" y1="-24.13" x2="-127" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-127" y1="-21.59" x2="-114.3" y2="-21.59" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-83.82" y1="-21.59" x2="-71.12" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-21.59" x2="-71.12" y2="-24.13" width="0.1524" layer="91"/>
<junction x="-114.3" y="-21.59"/>
<junction x="-83.82" y="-21.59"/>
<pinref part="C8" gate="G$1" pin="+"/>
<wire x1="-99.06" y1="-22.86" x2="-99.06" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-21.59" x2="-99.06" y2="-16.51" width="0.1524" layer="91"/>
<junction x="-99.06" y="-21.59"/>
<pinref part="U$20" gate="G$1" pin="+11.1V"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="3.175" y1="28.575" x2="3.175" y2="29.845" width="0.1524" layer="91"/>
<pinref part="+11.1V" gate="G$1" pin="1"/>
<wire x1="3.175" y1="29.845" x2="3.175" y2="36.195" width="0.1524" layer="91"/>
<wire x1="8.255" y1="29.845" x2="3.175" y2="29.845" width="0.1524" layer="91"/>
<junction x="3.175" y="29.845"/>
<pinref part="U$18" gate="G$1" pin="+11.1V"/>
</segment>
</net>
<net name="M_PB7" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB7"/>
<wire x1="-50.8" y1="93.98" x2="-48.26" y2="93.98" width="0.1524" layer="91"/>
<label x="-48.26" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-128.27" y1="46.99" x2="-136.398" y2="46.99" width="0.1524" layer="91"/>
<label x="-136.398" y="46.99" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="AIN1"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-134.62" y1="-39.37" x2="-134.62" y2="-36.83" width="0.1524" layer="91"/>
<label x="-134.62" y="-36.83" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="M_PB6" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB6"/>
<wire x1="-50.8" y1="91.44" x2="-48.26" y2="91.44" width="0.1524" layer="91"/>
<label x="-48.26" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-128.27" y1="41.91" x2="-136.398" y2="41.91" width="0.1524" layer="91"/>
<label x="-136.398" y="41.91" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="AIN2"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="-39.37" x2="-63.5" y2="-36.83" width="0.1524" layer="91"/>
<label x="-63.5" y="-36.83" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="M_PB4" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB4"/>
<wire x1="-134.62" y1="91.44" x2="-137.16" y2="91.44" width="0.1524" layer="91"/>
<label x="-137.16" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-128.27" y1="31.75" x2="-136.398" y2="31.75" width="0.1524" layer="91"/>
<label x="-136.398" y="31.75" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="BIN2"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="31.75" y1="-39.37" x2="31.75" y2="-36.83" width="0.1524" layer="91"/>
<label x="31.75" y="-36.83" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="M_PB5" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB5"/>
<wire x1="-134.62" y1="104.14" x2="-137.16" y2="104.14" width="0.1524" layer="91"/>
<label x="-137.16" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-128.27" y1="36.83" x2="-136.652" y2="36.83" width="0.1524" layer="91"/>
<label x="-136.652" y="36.83" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="BIN1"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-39.37" y1="-39.37" x2="-39.37" y2="-36.83" width="0.1524" layer="91"/>
<label x="-39.37" y="-36.83" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="M_PE4" class="0">
<segment>
<label x="-137.16" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="LAUNCHPAD" gate="G$1" pin="PE4"/>
<wire x1="-134.62" y1="96.52" x2="-137.16" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="57.15" y="-33.02" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U3" gate="G$1" pin="RXD"/>
<wire x1="60.96" y1="-33.02" x2="57.15" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="M_PE5" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PE5"/>
<wire x1="-134.62" y1="93.98" x2="-137.16" y2="93.98" width="0.1524" layer="91"/>
<label x="-137.16" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="57.15" y="-38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="60.96" y1="-38.1" x2="57.15" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="TXD"/>
</segment>
</net>
<net name="M_PD0" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PD0"/>
<wire x1="-111.76" y1="101.6" x2="-109.22" y2="101.6" width="0.1524" layer="91"/>
<label x="-109.22" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SERVO_A" gate="A" pin="1"/>
<wire x1="-44.45" y1="15.24" x2="-50.8" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="15.24" x2="-50.8" y2="22.86" width="0.1524" layer="91"/>
<label x="-50.8" y="24.13" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="SERVOA" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="22.86" x2="-50.8" y2="24.13" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="22.86" x2="-50.8" y2="22.86" width="0.1524" layer="91"/>
<junction x="-50.8" y="22.86"/>
</segment>
</net>
<net name="HB_GATE_B+" class="0">
<segment>
<pinref part="Q1" gate="G1" pin="C"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-31.75" y1="-46.99" x2="-31.75" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-31.75" y1="-43.18" x2="-31.75" y2="-34.29" width="0.1524" layer="91"/>
<junction x="-31.75" y="-43.18"/>
<pinref part="Q9" gate="G$1" pin="G"/>
<wire x1="-26.67" y1="-43.18" x2="-31.75" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="-35.56" x2="-26.67" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-26.67" y="-43.18"/>
<pinref part="Q5" gate="G$1" pin="G"/>
<wire x1="-24.13" y1="-54.61" x2="-26.67" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="-54.61" x2="-26.67" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HB_GATE_B-" class="0">
<segment>
<pinref part="Q2" gate="G1" pin="C"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="24.13" y1="-46.99" x2="24.13" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="24.13" y1="-43.18" x2="24.13" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="24.13" y1="-43.18" x2="19.05" y2="-43.18" width="0.1524" layer="91"/>
<junction x="24.13" y="-43.18"/>
<wire x1="19.05" y1="-43.18" x2="19.05" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="Q10" gate="G$1" pin="G"/>
<junction x="19.05" y="-43.18"/>
<pinref part="Q6" gate="G$1" pin="G"/>
<wire x1="16.51" y1="-54.61" x2="19.05" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-54.61" x2="19.05" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HB_BASE_B-" class="0">
<segment>
<pinref part="Q2" gate="G1" pin="B"/>
<wire x1="29.21" y1="-52.07" x2="31.75" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="31.75" y1="-52.07" x2="31.75" y2="-49.53" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="HB_BASE_B+" class="0">
<segment>
<pinref part="Q1" gate="G1" pin="B"/>
<wire x1="-36.83" y1="-52.07" x2="-39.37" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-39.37" y1="-52.07" x2="-39.37" y2="-49.53" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="HB_GATE_A+" class="0">
<segment>
<pinref part="Q3" gate="G1" pin="C"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-127" y1="-46.99" x2="-127" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-127" y1="-43.18" x2="-127" y2="-34.29" width="0.1524" layer="91"/>
<junction x="-127" y="-43.18"/>
<pinref part="Q11" gate="G$1" pin="G"/>
<wire x1="-121.92" y1="-43.18" x2="-127" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="-35.56" x2="-121.92" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-121.92" y="-43.18"/>
<pinref part="Q7" gate="G$1" pin="G"/>
<wire x1="-119.38" y1="-54.61" x2="-121.92" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="-54.61" x2="-121.92" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HB_GATE_A-" class="0">
<segment>
<pinref part="Q4" gate="G1" pin="C"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="-46.99" x2="-71.12" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-43.18" x2="-71.12" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-43.18" x2="-76.2" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-71.12" y="-43.18"/>
<wire x1="-76.2" y1="-43.18" x2="-76.2" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="Q12" gate="G$1" pin="G"/>
<junction x="-76.2" y="-43.18"/>
<pinref part="Q8" gate="G$1" pin="G"/>
<wire x1="-78.74" y1="-54.61" x2="-76.2" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="-54.61" x2="-76.2" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HB_BASE_A-" class="0">
<segment>
<pinref part="Q4" gate="G1" pin="B"/>
<wire x1="-66.04" y1="-52.07" x2="-63.5" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-52.07" x2="-63.5" y2="-49.53" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="HB_BASE_A+" class="0">
<segment>
<pinref part="Q3" gate="G1" pin="B"/>
<wire x1="-132.08" y1="-52.07" x2="-134.62" y2="-52.07" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="-52.07" x2="-134.62" y2="-49.53" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DRV_MOTOR_A-" class="0">
<segment>
<pinref part="U1" gate="A" pin="AOUT2"/>
<wire x1="-92.71" y1="36.83" x2="-80.645" y2="36.83" width="0.1524" layer="91"/>
<pinref part="X1" gate="-2" pin="S"/>
<label x="-94.615" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="DRV_MOTOR_B+" class="0">
<segment>
<pinref part="U1" gate="A" pin="BOUT1"/>
<pinref part="X2" gate="-1" pin="S"/>
<wire x1="-92.71" y1="21.59" x2="-80.645" y2="21.59" width="0.1524" layer="91"/>
<label x="-93.345" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="VLED1" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="LED1" gate="G$1" pin="A"/>
<wire x1="3.175" y1="17.145" x2="3.175" y2="18.415" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VLED3" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="26.67" y1="17.145" x2="26.67" y2="18.415" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CANL" class="0">
<segment>
<pinref part="CAN" gate="G$1" pin="1"/>
<wire x1="91.44" y1="-39.37" x2="106.68" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-43.18" x2="91.44" y2="-39.37" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="87.63" y1="-43.18" x2="91.44" y2="-43.18" width="0.1524" layer="91"/>
<junction x="87.63" y="-43.18"/>
<wire x1="83.82" y1="-40.64" x2="83.82" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-43.18" x2="87.63" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="CANL" gate="G$1" pin="1"/>
<wire x1="87.63" y1="-53.34" x2="87.63" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-40.64" x2="83.82" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="CANL"/>
</segment>
</net>
<net name="CANH" class="0">
<segment>
<pinref part="CAN" gate="G$1" pin="2"/>
<wire x1="91.44" y1="-36.83" x2="106.68" y2="-36.83" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-33.02" x2="91.44" y2="-36.83" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="87.63" y1="-33.02" x2="91.44" y2="-33.02" width="0.1524" layer="91"/>
<junction x="87.63" y="-33.02"/>
<wire x1="83.82" y1="-33.02" x2="87.63" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-35.56" x2="83.82" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="CANH" gate="G$1" pin="1"/>
<wire x1="87.63" y1="-23.368" x2="87.63" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-35.56" x2="83.82" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="CANH"/>
</segment>
</net>
<net name="HB_MOTOR_B+" class="0">
<segment>
<pinref part="Q9" gate="G$1" pin="D"/>
<wire x1="-19.05" y1="-43.18" x2="-19.05" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-19.05" y1="-43.18" x2="-15.24" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-19.05" y="-43.18"/>
<junction x="-19.05" y="-43.18"/>
<pinref part="Q5" gate="G$1" pin="D"/>
<wire x1="-19.05" y1="-46.99" x2="-19.05" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="X4" gate="-1" pin="S"/>
<label x="-17.78" y="-41.91" size="1.778" layer="95"/>
</segment>
</net>
<net name="HB_MOTOR_B-" class="0">
<segment>
<pinref part="Q10" gate="G$1" pin="D"/>
<wire x1="11.43" y1="-43.18" x2="11.43" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-43.18" x2="8.89" y2="-43.18" width="0.1524" layer="91"/>
<junction x="11.43" y="-43.18"/>
<pinref part="Q6" gate="G$1" pin="D"/>
<wire x1="11.43" y1="-46.99" x2="11.43" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="X4" gate="-2" pin="S"/>
<label x="10.16" y="-44.45" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="HB_MOTOR_A-" class="0">
<segment>
<pinref part="X3" gate="-2" pin="S"/>
<pinref part="Q12" gate="G$1" pin="D"/>
<wire x1="-83.82" y1="-43.18" x2="-83.82" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="-43.18" x2="-86.36" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-83.82" y="-43.18"/>
<pinref part="Q8" gate="G$1" pin="D"/>
<wire x1="-83.82" y1="-46.99" x2="-83.82" y2="-43.18" width="0.1524" layer="91"/>
<label x="-86.36" y="-45.72" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="HB_MOTOR_A+" class="0">
<segment>
<pinref part="X3" gate="-1" pin="S"/>
<pinref part="Q11" gate="G$1" pin="D"/>
<wire x1="-114.3" y1="-43.18" x2="-114.3" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-43.18" x2="-106.68" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-114.3" y="-43.18"/>
<junction x="-114.3" y="-43.18"/>
<pinref part="Q7" gate="G$1" pin="D"/>
<wire x1="-114.3" y1="-46.99" x2="-114.3" y2="-43.18" width="0.1524" layer="91"/>
<label x="-113.03" y="-41.91" size="1.778" layer="95"/>
</segment>
</net>
<net name="DRV_MOTOR_B-" class="0">
<segment>
<pinref part="U1" gate="A" pin="BOUT2"/>
<pinref part="X2" gate="-2" pin="S"/>
<wire x1="-92.71" y1="16.51" x2="-80.645" y2="16.51" width="0.1524" layer="91"/>
<label x="-93.345" y="17.145" size="1.778" layer="95"/>
</segment>
</net>
<net name="DRV_MOTOR_A+" class="0">
<segment>
<pinref part="U1" gate="A" pin="AOUT1"/>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="-92.71" y1="41.91" x2="-80.645" y2="41.91" width="0.1524" layer="91"/>
<label x="-93.98" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="M_PD1" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PD1"/>
<wire x1="-111.76" y1="99.06" x2="-109.22" y2="99.06" width="0.1524" layer="91"/>
<label x="-109.22" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SERVO_B" gate="A" pin="1"/>
<wire x1="-44.45" y1="50.8" x2="-50.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="50.8" x2="-50.8" y2="58.42" width="0.1524" layer="91"/>
<label x="-50.8" y="59.69" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="SERVOB" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="58.42" x2="-50.8" y2="59.69" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="58.42" x2="-50.8" y2="58.42" width="0.1524" layer="91"/>
<junction x="-50.8" y="58.42"/>
</segment>
</net>
<net name="BAT5" class="0">
<segment>
<wire x1="53.34" y1="73.66" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<wire x1="68.58" y1="73.66" x2="82.55" y2="73.66" width="0.1524" layer="91"/>
<wire x1="82.55" y1="91.694" x2="82.55" y2="73.66" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="81.534" y1="91.694" x2="82.55" y2="91.694" width="0.1524" layer="91"/>
<label x="73.152" y="77.978" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="OUT"/>
<pinref part="C1" gate="G$1" pin="+"/>
<wire x1="68.58" y1="69.85" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<junction x="68.58" y="73.66"/>
</segment>
</net>
<net name="VINT" class="0">
<segment>
<wire x1="-92.71" y1="52.07" x2="-89.916" y2="52.07" width="0.1524" layer="91"/>
<wire x1="-89.916" y1="52.07" x2="-89.916" y2="46.99" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VREF"/>
<wire x1="-89.916" y1="46.99" x2="-92.71" y2="46.99" width="0.1524" layer="91"/>
<wire x1="-89.916" y1="52.07" x2="-89.916" y2="66.802" width="0.1524" layer="91"/>
<wire x1="-89.916" y1="66.802" x2="-84.074" y2="66.802" width="0.1524" layer="91"/>
<wire x1="-84.074" y1="66.802" x2="-84.074" y2="62.992" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VINT"/>
<pinref part="C7" gate="G$1" pin="+"/>
<junction x="-89.916" y="52.07"/>
<label x="-96.266" y="60.706" size="1.778" layer="95"/>
</segment>
</net>
<net name="M_PC4" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PC4"/>
<wire x1="-73.66" y1="99.06" x2="-78.994" y2="99.06" width="0.1524" layer="91"/>
<label x="-79.248" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PC4" gate="G$1" pin="1"/>
<wire x1="82.55" y1="2.54" x2="93.98" y2="2.54" width="0.1524" layer="91"/>
<label x="93.98" y="2.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="M_PC5" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PC5"/>
<wire x1="-73.66" y1="96.52" x2="-78.994" y2="96.52" width="0.1524" layer="91"/>
<label x="-79.248" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="82.55" y1="15.24" x2="93.98" y2="15.24" width="0.1524" layer="91"/>
<label x="93.98" y="15.24" size="1.27" layer="95" xref="yes"/>
<pinref part="PC5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="M_PC6" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PC6"/>
<wire x1="-73.66" y1="93.98" x2="-78.74" y2="93.98" width="0.1524" layer="91"/>
<label x="-79.248" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PC6" gate="G$1" pin="1"/>
<wire x1="82.55" y1="27.94" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<label x="93.98" y="27.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="M_PC7" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PC7"/>
<wire x1="-73.66" y1="91.44" x2="-78.994" y2="91.44" width="0.1524" layer="91"/>
<label x="-79.248" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PC7" gate="G$1" pin="1"/>
<wire x1="82.55" y1="40.64" x2="93.98" y2="40.64" width="0.1524" layer="91"/>
<label x="93.98" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="M_PB0" class="0">
<segment>
<pinref part="LAUNCHPAD" gate="G$1" pin="PB0"/>
<wire x1="-134.62" y1="101.6" x2="-137.16" y2="101.6" width="0.1524" layer="91"/>
<label x="-137.16" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="NFAULT"/>
<wire x1="-128.27" y1="21.59" x2="-135.255" y2="21.59" width="0.1524" layer="91"/>
<label x="-136.652" y="21.59" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-135.255" y1="21.59" x2="-136.398" y2="21.59" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="3.81" x2="-142.24" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="2.54" x2="-135.255" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-135.255" y1="2.54" x2="-135.255" y2="21.59" width="0.1524" layer="91"/>
<junction x="-135.255" y="21.59"/>
<wire x1="-124.46" y1="0" x2="-135.255" y2="0" width="0.1524" layer="91"/>
<wire x1="-135.255" y1="0" x2="-135.255" y2="2.54" width="0.1524" layer="91"/>
<junction x="-135.255" y="2.54"/>
<pinref part="FAULT" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SERVO5" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="OUT"/>
<wire x1="53.34" y1="93.98" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<label x="59.69" y="95.25" size="1.778" layer="95"/>
<pinref part="C3" gate="G$1" pin="+"/>
<wire x1="68.58" y1="90.17" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="68.58" y1="93.98" x2="68.58" y2="101.6" width="0.1524" layer="91"/>
<junction x="68.58" y="93.98"/>
</segment>
<segment>
<wire x1="-44.45" y1="48.26" x2="-55.88" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="48.26" x2="-55.88" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="+"/>
<wire x1="-63.5" y1="50.8" x2="-63.5" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="53.34" x2="-55.88" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SERVO_B" gate="A" pin="2"/>
<label x="-63.5" y="54.61" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-55.88" y1="12.7" x2="-55.88" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="+"/>
<wire x1="-55.88" y1="17.78" x2="-63.5" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="17.78" x2="-63.5" y2="15.24" width="0.1524" layer="91"/>
<pinref part="SERVO_A" gate="A" pin="2"/>
<wire x1="-44.45" y1="12.7" x2="-55.88" y2="12.7" width="0.1524" layer="91"/>
<label x="-63.5" y="19.05" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="NSLEEP"/>
<wire x1="-128.27" y1="26.67" x2="-143.51" y2="26.67" width="0.1524" layer="91"/>
<label x="-143.51" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
