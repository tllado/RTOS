// Controller.h
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 26th, 2016

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define speedMin 60
#define speedMax 100
// Gains here are scaled up x1000 for increased resolution
#define KpSide 2000 // default = 2000
#define KiSide 0    // default = 0
#define KdSide 2000 // default = 2000
#define KpFrnt 2000 // default = 2000
#define KiFrnt 0    // default = 0
#define KdFrnt 500  // default = 0
#define damp   0	// damping factor, scaled up x1000

#define REVERSE_DIST_MIN 20  // minimum value sensor should read before leaving reverse state

 extern uint8_t RunController;  // determines if the simple controller should run


////////////////////////////////////////////////////////////////////////////////
// simpleController()
// Runs simple PID controllers to balance pairs of symmetrical IR sensors.
// Input: new sensor data
// Output: motor speed and servo position commands

void simpleController(void);
