// Controller.c
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 26th, 2016

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "Controller.h"
#include "IR.h"
#include "Lab7_Motor.h"
#include "Motors.h"
#include "Profiler.h"

uint8_t RunController;  // determines if the simple controller should run


////////////////////////////////////////////////////////////////////////////////
// Median()
// Very basic median filter.
// Input: three integers
// Output: Single integer, median of input values

int Median(int n1, int n2, int n3) {
    int n0 = 0;
    if(n1 > n2) {
        n0 = n1;
        n1 = n2;
        n2 = n0;
    }
    if(n2 > n3) {
        n0 = n2;
        n2 = n3;
        n3 = n0;
    }
    if(n1 > n2) {
        n0 = n1;
        n1 = n2;
        n2 = n0;
    }
    return n2;
}

////////////////////////////////////////////////////////////////////////////////
// calcError()
// Calculates P/I/D error values for symmetric pairs of IR sensors.
// Input: none, acts on global variables
// Output: none, acts on global variables

int IR_E_EP, IR_E_EI, IR_E_ED, IR_E_E1, IR_E_E2, IR_E_E3, IR_E_Prev,
    IR_F_EP, IR_F_EI, IR_F_ED, IR_F_E1, IR_F_E2, IR_F_E3, IR_F_Prev,
    IR_O_EP, IR_O_EI, IR_O_ED, IR_O_E1, IR_O_E2, IR_O_E3, IR_O_Prev,
    IR_S_EP, IR_S_EI, IR_S_ED, IR_S_E1, IR_S_E2, IR_S_E3, IR_S_Prev;
    

void calcError(void) {
    // Center IR Sensors
    IR_E_EP = (int)IR5_Dist - (int)IR6_Dist;    // calc P Error
    
    IR_E_EI += IR_E_EP;                         // calc I Error
    
    IR_E_E3 = IR_E_E2;                          // calc D Error
    IR_E_E2 = IR_E_E1;
    IR_E_E1 = IR_E_EP - IR_E_Prev;
    IR_E_ED = Median(IR_E_E1, IR_E_E2, IR_E_E3);
    IR_E_Prev = IR_E_EP;

    // Front IR Sensors
    IR_F_EP = (int)IR4_Dist - (int)IR7_Dist;    // calc P Error
    
    IR_F_EI += IR_F_EP;                         // calc I Error
    
    IR_F_E3 = IR_F_E2;                          // calc D Error
    IR_F_E2 = IR_F_E1;
    IR_F_E1 = IR_F_EP - IR_F_Prev;
    IR_F_ED = Median(IR_F_E1, IR_F_E2, IR_F_E3);
    IR_F_Prev = IR_F_EP;
 
    // Corner IR Sensors
    IR_O_EP = (int)IR0_Dist  - (int)IR3_Dist;    // calc P Error
 
    IR_O_EI += IR_O_EP;                         // calc I Error

    IR_O_E3 = IR_O_E2;                          // calc D Error
    IR_O_E2 = IR_O_E1;
    IR_O_E1 = IR_O_EP - IR_O_Prev;
    IR_O_ED = Median(IR_O_E1, IR_O_E2, IR_O_E3);
    IR_O_Prev = IR_O_EP;

    // Side IR Sensors
    IR_S_EP = (int)IR1_Dist - (int)IR2_Dist;    // calc P Error
   
    IR_S_EI += IR_S_EP;                         // calc I Error

    IR_S_E3 = IR_S_E2;                          // calc D Error
    IR_S_E2 = IR_S_E1;
    IR_S_E1 = IR_S_EP - IR_S_Prev;
    IR_S_ED = Median(IR_S_E1, IR_S_E2, IR_S_E3);
    IR_S_Prev = IR_S_EP;
}

////////////////////////////////////////////////////////////////////////////////
// PID()
// Standard PID controller. Uses global variables and combines the results of
//  two PID calculations.
// Input: K and EDor values for IRS and IRF
// Output: Combined control value for both IRS andIRF

int PID() {
    int ctrlCntr = (KP_E*IR_E_EP + KI_E*IR_E_EI + KD_E*IR_E_ED)/1000;
    int ctrlFrnt = (KP_F*IR_F_EP + KI_F*IR_F_EI + KD_F*IR_F_ED)/1000;
    int ctrlCrnr = (KP_O*IR_O_EP + KI_O*IR_O_EI + KD_O*IR_O_ED)/1000;
    int ctrlSide = (KP_S*IR_S_EP + KI_S*IR_S_EI + KD_S*IR_S_ED)/1000;

    return ctrlCntr + ctrlFrnt + ctrlCrnr + ctrlSide;
}

////////////////////////////////////////////////////////////////////////////////
// simpleController()
// Runs simple PID controllers to balance pairs of symmetrical IR sensors.
// Input: new sensor data
// Output: motor speed and servo position commands

void simpleController(void) {
	if(RunController){
    calcError();
    int direction = PID();
    int speed = SPEED_MAX - (SPEED_MAX - SPEED_MIN)*abs(direction)/127;
    motorUpdate(speed, direction);
	}
}
