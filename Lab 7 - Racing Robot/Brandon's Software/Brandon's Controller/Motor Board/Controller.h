// Controller.h
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 20th, 2016



//******************** Constants *********************************************
#define KP_TURN_R_SERVO -50            // Kp for when servo is turning right
#define KP_TURN_L_SERVO 50             // Kp for when servo is turning left

#define KP_SLIDE_R_SERVO -10          // Kp for when servo is turning right
#define KP_SLIDE_L_SERVO 10           // Kp for when servo is turning left

#define KP_OBSTACLE_R_SERVO -59       // Kp for when servo has obstacle on right
#define KP_OBSTACLE_L_SERVO 59        // Kp for when servo has obstacle on left

#define IR0_DIST_MAX 41                // maximum value that IR0 should read in normal operations

#define IR1_DIST_MIN 20                // minimum value that IR1 should read in normal operations before needing to turn right
#define IR1_DIST_MAX 50                // maximum value that IR1 should read in normal operations before needing to turn left

#define IR2_DIST_MIN 20                // minimum value that IR2 should read in normal operations before needing to turn left
#define IR2_DIST_MAX 50                // maximum value that IR2 should read in normal operations before needing to turn right

#define IR3_DIST_MAX 41                // maximum value that IR3 should read in normal operations

#define REVERSE_CLEAR_DIST 15          // minimum value respective IR should read while reversing, before leaveing a reverse state         

#define PING1_STRAIGHT 50              // robot should move straight if ping1 reads greater than this

#define STRAIGHT_DUTY 60               // duty cycle for motors in straight state
#define TURN_RIGHT_DUTY 30             // duty cycle for motors in turn right state
#define TURN_LEFT_DUTY 30              // duty cycle for motors in turn left state
#define SLIDE_RIGHT_DUTY 45            // duty cycle for motors in slide right state
#define SLIDE_LEFT_DUTY 45             // duty cycle for motors in slide left state
#define OBSTACLE_RIGHT_DUTY 50         // duty cycle for motors in obstacle right state
#define OBSTACLE_LEFT_DUTY 50          // duty cycle for motors in obstacle left state
#define REVERSE_DUTY 30                // duty cycle for motors in reverse states

// *************************************************************************


// ************* Enums ****************************************************
typedef enum{                 // statemachine enums for various states
	ST_STRAIGHT,
	ST_TURN_RIGHT,
	ST_TURN_LEFT,
	ST_SLIDE_RIGHT,
	ST_SLIDE_LEFT,
	ST_OBSTACLE_RIGHT,
	ST_OBSTACLE_LEFT,
	ST_REVERSE_RIGHT,
	ST_REVERSE_LEFT,
}StateType;

typedef enum{                 // motor controller enums for various direction
  DIR_FORWARD,
	DIR_REVERSE,
}DirType;
// *************************************************************************


// ************* Externs ****************************************************
extern StateType State_Servo;                            // state machine for servo
// *************************************************************************


//******************** Prototypes *********************************************

// ******** Motor_Controller ************
// Adjust the duty cycles for both motors
// Inputs: dutyPercentA - Percent of duty you wish to apply to motor A (0-100)
//         directionA - Direction you wish to spin motor A
//         dutyPercentB - Percent of duty you wish to apply to motor B (0-100)
//         directionB - Direction you wish to spin motor B
// Output: none
void Motor_Controller (uint8_t dutyPercentA, DirType directionA, uint8_t dutyPercentB, DirType directionB);
	
//******** Servo_Controller **************
// Calculates the output that should be applied to servo based on 
// measured, desired, and kp values.
// inputs:  none
// outputs: none
void Servo_Controller(int32_t measured, int32_t desired, int32_t kp);

//******** Servo_StateMachine_CW **************
// Perioidic background thread.
// Controls servo using PID assuming the robot moves around 
// the track in a clockwise manner.
// inputs:  none
// outputs: none
void Servo_StateMachine_CW(void);

//******** Check_Straight **************
// Checks if robot needs to enter go straight state
// inputs:  none
// outputs: true - needs to enter go straight state, 
//          false - stay in same state
uint8_t Check_Straight(void);


//******** Check_Turn_Right **************
// Checks if robot needs to enter turn right state
// inputs:  none
// outputs: true - needs to enter turn right state, 
//          false - stay in same state
uint8_t Check_Turn_Right(void);

//******** Check_Turn_Left **************
// Checks if robot needs to enter turn left state
// inputs:  none
// outputs: true - needs to enter turn left state, 
//          false - stay in same state
uint8_t Check_Turn_Left(void);

//******** Check_Slide_Right **************
// Checks if robot needs to enter slide right state
// inputs:  none
// outputs: true - needs to enter slide right state, 
//          false - stay in same state
uint8_t Check_Slide_Right(void);

//******** Check_Slide_Left **************
// Checks if robot needs to enter slide left state
// inputs:  none
// outputs: true - needs to enter slide left state, 
//          false - stay in same state
uint8_t Check_Slide_Left(void);

//******** Check_Obstacle_Right **************
// Checks if robot needs to enter obstacle right state
// inputs:  none
// outputs: true - needs to enter obstacle right state, 
//          false - stay in same state
uint8_t Check_Obstacle_Right(void);

//******** Check_Obstacle_Left **************
// Checks if robot needs to enter obstacle left state
// inputs:  none
// outputs: true - needs to enter obstacle left state, 
//          false - stay in same state
uint8_t Check_Obstacle_Left(void);

//******** Check_Reverse_Right **************
// Checks if robot needs to enter reverse right state
// inputs:  none
// outputs: true - needs to enter reverse right state, 
//          false - stay in same state
uint8_t Check_Reverse_Right(void);

//******** Check_Reverse_Left **************
// Checks if robot needs to enter reverse left state
// inputs:  none
// outputs: true - needs to enter reverse left state, 
//          false - stay in same state
uint8_t Check_Reverse_Left(void);

//******** Check_Reverse_Right_Clear **************
// Checks if robot needs to leave reverse right state
// inputs:  none
// outputs: true - needs to leave reverse right state, 
//          false - stay in same state
uint8_t Check_Reverse_Right_Clear(void);

//******** Check_Reverse_Left_Clear **************
// Checks if robot needs to leave reverse left state
// inputs:  none
// outputs: true - needs to leave reverse left state, 
//          false - stay in same state
uint8_t Check_Reverse_Left_Clear(void);
