// Ping.c
// Runs on TM4C123
// uses ping PB2, PB4, PB6, PC5, for falling edge triggered interrupts 
// to capture the pulse width for sonar Ping))) sensors.  Note, Port C handler 
// is defined in Buttons.c
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 4th, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "Ping.h"
#include "ST7735.h"
#include "OS.h"
#include "Interrupts.h"
#include "SysTick.h"
#include "Lab7_Sensor.h"
                           
//******************** Globals ***********************************************
volatile uint32_t StartTime0;          // Timer count on rising edge for Ping #0.
volatile uint32_t StartTime1;          // Timer count on rising edge for Ping #1.
volatile uint32_t StartTime2;          // Timer count on rising edge for Ping #2.
volatile uint32_t StartTime3;          // Timer count on rising edge for Ping #3.

volatile uint32_t PulseTime0;          // Time of Ping #0's pulse (.1ms)
volatile uint32_t PulseTime1;          // Time of Ping #1's pulse (.1ms)
volatile uint32_t PulseTime2;          // Time of Ping #2's pulse (.1ms)
volatile uint32_t PulseTime3;          // Time of Ping #3's pulse (.1ms)

volatile uint32_t Ping0_Dist;          // calibrated ping0 sensor data
volatile uint32_t Ping1_Dist;          // calibrated ping1 sensor data
volatile uint32_t Ping2_Dist;          // calibrated ping2 sensor data
volatile uint32_t Ping3_Dist;          // calibrated ping3 sensor data

const uint8_t PingLookup[SIZE_OF_LOOKUP] = {     // lookup table for ping sensors
	4,5,6,7,8,9,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,31,
	32,33,34,35,36,37,38,39,40,41,42,44,45,46,47,48,49,50,51,52,53,54,55,56,58,59,
	60,61,62,63,64,65,66,67,68,69,71,72,73,74,75,76,77,78,79,80,81,82,83,85
};
//****************************************************************************


//******** Ping_Init *************** 
// Initializes ping's signal pins  PB2, PB4, PB6, PC5, for falling edge 
// triggered interrupts.  Also adds the sensors' 4 foreground threads.
// Max pulse measurement is (2^24-1)*12.5ns = 209.7151ms (using OS_Time())
// Min pulse measurement determined by time to run ISR, which is about 1us
// inputs:  none
// outputs: none
void Ping_Init(void){
	// PB2, PB4, PB6
  SYSCTL_RCGCGPIO_R |= 0x02;             // activate clock for port B      
	while((SYSCTL_PRGPIO_R&0x02) == 0){};  // allow time for clock to stabilize	
  GPIO_PORTB_DIR_R &= ~0x54;             // make PB2, PB4, PB6 inputs
  GPIO_PORTB_DEN_R |= 0x54;              // enable digital I/O on PB2, PB4, PB6
  GPIO_PORTB_AMSEL_R &= ~0x54;           // disable analog functionality on PB2, PB4, PB6
	GPIO_PORTB_PDR_R |= 0x54;              // enable weak pull-down on PB2, PB4, PB6
  GPIO_PORTB_IS_R &= ~0x54;              // PB2, PB4, PB6 is edge-sensitive 
  GPIO_PORTB_IBE_R &= ~0x54;             // PB2, PB4, PB6 is not both edges 
  GPIO_PORTB_IEV_R &= ~0x54;             // PB2, PB4, PB6 falling edge events 
  GPIO_PORTB_ICR_R = 0x54;               // clear flags 2, 4, 6
  GPIO_PORTB_IM_R |= 0x54;               // arm interrupt on PB2, PB4, PB6
  NVIC_PRI0_R &= 0xFFFF00FF;             // priority 1
	NVIC_PRI0_R |= 0x00002000;             // priority 1
  NVIC_EN0_R = NVIC_EN0_INT1;            // enable interrupt 1 in NVIC
		
	// PC5
  SYSCTL_RCGCGPIO_R |= 0x04;             // activate clock for port C 
  while((SYSCTL_PRGPIO_R&0x04) == 0){};  // allow time for clock to stabilize	
  GPIO_PORTC_DIR_R &= ~0x20;             // make PF4 an input
  GPIO_PORTC_DEN_R |= 0x20;              // enable digital I/O on PC5
  GPIO_PORTC_AMSEL_R &= ~0x20;           // disable analog functionality on PC5
	GPIO_PORTC_PDR_R |= 0x20;              // enable weak pull-down on PC5
  GPIO_PORTC_IS_R &= ~0x20;              // PC5 is edge-sensitive 
  GPIO_PORTC_IBE_R &= ~0x20;             // PC5 is not both edges 
  GPIO_PORTC_IEV_R &= ~0x20;             // PC5 falling edge event
  GPIO_PORTC_ICR_R = 0x20;               // clear flag 5
  GPIO_PORTC_IM_R |= 0x20;               // arm interrupt on PC5
  NVIC_PRI0_R &= 0xFF00FFFF;             // priority 1
	NVIC_PRI0_R |= 0x00200000;             // priority 1
  NVIC_EN0_R = NVIC_EN0_INT2;            // enable interrupt 2 in NVIC	
		
	// add Ping))) sensor's foreground threads
	OS_AddThread(&Ping0, 128, 1);                    // Sends a pulse to the ping sensor #0, 10 times a second
	OS_AddThread(&Ping1, 128, 1);                    // Sends a pulse to the ping sensor #1, 10 times a second
	OS_AddThread(&Ping2, 128, 1);                    // Sends a pulse to the ping sensor #2, 10 times a second
	OS_AddThread(&Ping3, 128, 1);                    // Sends a pulse to the ping sensor #3, 10 times a second
}

// ******** GPIOPortB_Handler **************
// Captures pulse width on PB2, PB4, PB6.
void GPIOPortB_Handler(void){
	// PB2 triggered (ping3)
	if(GPIO_PORTB_RIS_R&0x04){
		GPIO_PORTB_ICR_R = 0x04;                                            // acknowledge flag2
		int32_t status = StartCritical();

		// capture time on falling edge
		uint32_t endTime = OS_Time();
		PulseTime3 = OS_TimeDifference(StartTime3,endTime)/8000;            // how long the pulse was high (.1ms units)
		
		// set pin back in default state for next signal pulse
		GPIO_PORTB_PDR_R |= 0x04;                                           // enable weak pull-down on PB2
		EndCritical(status);
	}
	
	// PB4 triggered (ping1)
	if(GPIO_PORTB_RIS_R&0x10){
		GPIO_PORTB_ICR_R = 0x10;                                            // acknowledge flag4
		int32_t status = StartCritical();

		// capture time on falling edge
		uint32_t endTime = OS_Time();
		PulseTime1 = OS_TimeDifference(StartTime1,endTime)/8000;            // how long the pulse was high (.1ms units)
		
		// set pin back in default state for next signal pulse
		GPIO_PORTB_PDR_R |= 0x10;                                           // enable weak pull-down on PB4
		EndCritical(status);
	}

	// PB6 triggered (ping0)
	if(GPIO_PORTB_RIS_R&0x40){
		GPIO_PORTB_ICR_R = 0x40;                                            // acknowledge flag6
		int32_t status = StartCritical();

		// capture time on falling edge
		uint32_t endTime = OS_Time();
		PulseTime0 = OS_TimeDifference(StartTime0,endTime)/8000;            // how long the pulse was high (.1ms units)
		
		// set pin back in default state for next signal pulse
		GPIO_PORTB_PDR_R |= 0x40;                                           // enable weak pull-down on PB6
		EndCritical(status);
	}
}


//******** SendPulse *************** 
// Issue a 40kHz sound pulse by sending a short 
// logical high burst ~(5us) to the ping sensor.
// Called from PulsePing().
// inputs:  pingID - the ID of the ping sensor you wish to send a pulse to.
// outputs: none
void SendPulse(uint8_t pingID){
	int32_t status = StartCritical();
	switch (pingID){
    // ping sensor 0
		case(0):{
			NVIC_DIS0_R = NVIC_DIS0_INT1;       // disable interrupt 1 in NVIC
			
			// set PB6 as a GPIO output
			GPIO_PORTB_DIR_R |= 0x40;           // make PB6 output
		
			// create 5us pulse
			PB6 = 0x40;
			SysTick_Wait(400);                  // 5us / 12.5ns = 400
			PB6 = 0x00;	
			break;
		}
		// ping sensor 1
	  case(1):{
			NVIC_DIS0_R = NVIC_DIS0_INT1;       // disable interrupt 1 in NVIC
			
			// set PB4 as a GPIO output
			GPIO_PORTB_DIR_R |= 0x10;           // make PB4 output
		
			// create 5us pulse
			PB4 = 0x10;
			SysTick_Wait(400);                  // 5us / 12.5ns = 400
			PB4 = 0x00;	
			break;
		}
		
		// ping sensor 2
		case(2):{
			NVIC_DIS0_R = NVIC_DIS0_INT2;      // disable interrupt 2 in NVIC
			
			// set PF4 as a GPIO output
			GPIO_PORTC_DIR_R |= 0x20;           // make PC5 output
		
			// create 5us pulse
			PC5 = 0x20;
			SysTick_Wait(400);                  // 5us / 12.5ns = 400
			PC5 = 0x00;	
			break;
		}
		
		// ping sensor 3
		case(3):{
			NVIC_DIS0_R = NVIC_DIS0_INT1;       // disable interrupt 1 in NVIC
			
			// set PB2 as a GPIO output
			GPIO_PORTB_DIR_R |= 0x04;           // make PB2 output
		
			// create 5us pulse
			PB2 = 0x04;
			SysTick_Wait(400);                  // 5us / 12.5ns = 400
			PB2 = 0x00;	
			break;
		}
	}
	EndCritical(status);
}


//******** ConfigForCapture *************** 
// Configure pins for input capture with a pull up resistor
// Called from Ping#().
// inputs:  pingID - the ID of the ping sensor you wish configure for input capture
// outputs: none
void ConfigForCapture(uint8_t pingID){
	int32_t status = StartCritical();
	switch (pingID){
		// ping sensor 0
	  case(0):{
			GPIO_PORTB_DIR_R &= ~0x40;             // make PB6 input
			GPIO_PORTB_PUR_R |= 0x40;              // enable weak pull-up on PB6
			GPIO_PORTB_ICR_R = 0x40;               // clear flag6
			NVIC_EN0_R = NVIC_EN0_INT1;            // re-enable interrupt 1 in NVIC since SendPule() disabled it
			StartTime0 = OS_Time();                // set startTime for measuring Ping pulse
			break;
		}
		
    // ping sensor 1
	  case(1):{
			GPIO_PORTB_DIR_R &= ~0x10;             // make PB4 input
			GPIO_PORTB_PUR_R |= 0x10;              // enable weak pull-up on PB4
			GPIO_PORTB_ICR_R = 0x10;               // clear flag4
			NVIC_EN0_R = NVIC_EN0_INT1;            // re-enable interrupt 1 in NVIC since SendPule() disabled it
			StartTime1 = OS_Time();                // set startTime for measuring Ping pulse
			break;
		}
		
		// ping sensor 2
		case(2):{
			GPIO_PORTC_DIR_R &= ~0x20;             // make PC5 input
			GPIO_PORTC_PUR_R |= 0x20;              // enable weak pull-up on PC5
			GPIO_PORTC_ICR_R = 0x20;               // clear flag5
			NVIC_EN0_R = NVIC_EN0_INT2;            // re-enable interrupt 2 in NVIC since SendPule() disabled it
			StartTime2 = OS_Time();                // set startTime for measuring Ping pulse
			break;
		}
		
		// ping sensor 3
		case(3):{
			GPIO_PORTB_DIR_R &= ~0x04;             // make PB2 input
			GPIO_PORTB_PUR_R |= 0x04;              // enable weak pull-up on PB2
			GPIO_PORTB_ICR_R = 0x04;               // clear flag2
			NVIC_EN0_R = NVIC_EN0_INT1;            // re-enable interrupt 1 in NVIC since SendPule() disabled it
			StartTime3 = OS_Time();                // set startTime for measuring Ping pulse
			break;
		}
	}
	EndCritical(status);
}

//******** Ping0 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping0(void){
	uint8_t pingID = 0;
	// send pulses 10 times a second
	while(1){
		SendPulse(pingID);         // send a single pulse to the Ping sensor
		SysTick_Wait(60000);       // wait for holdoff time = 750 us. 
		ConfigForCapture(pingID);  // configure pin to capture pulse width
		
		// sleep ~100ms to accomplish 10Hz pinging.  Slighty less so it can offset with other ping threads
		OS_Sleep(98);   
	}
}


//******** Ping1 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping1(void){
	uint8_t pingID = 1;
	// send pulses 10 times a second
	while(1){
		SendPulse(pingID);         // send a single pulse to the Ping sensor
		SysTick_Wait(60000);       // wait for holdoff time = 750 us. 
		ConfigForCapture(pingID);  // configure pin to capture pulse width
		
		// sleep ~100ms to accomplish 10Hz pinging.  Slighty less so it can offset with other ping threads
		OS_Sleep(99);   
	}
}


//******** Ping2 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping2(void){
	uint8_t pingID = 2;
	// send pulses 10 times a second
	while(1){
		SendPulse(pingID);         // send a single pulse to the Ping sensor
		SysTick_Wait(60000);       // wait for holdoff time = 750 us. 
		ConfigForCapture(pingID);  // configure pin to capture pulse width
		
		// sleep ~100ms to accomplish 10Hz pinging.  Slighty less so it can offset with other ping threads
		OS_Sleep(100);   
	}
}


//******** Ping3 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping3(void){
	uint8_t pingID = 3;
	// send pulses 10 times a second
	while(1){
		SendPulse(pingID);         // send a single pulse to the Ping sensor
		SysTick_Wait(60000);       // wait for holdoff time = 750 us. 
		ConfigForCapture(pingID);  // configure pin to capture pulse width
		
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(101);   
	}
}


//******** PingDisplay *************** 
// Foreground thread.
// Display ping sensor readings 10 times a second
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void PingDisplay(void){
	while(1){                                          // never dies
		// calculate distance using formula from lecture 9 notes
		uint32_t ping0_measured = DIST(PulseTime0);
		uint32_t ping1_measured = DIST(PulseTime1);
		uint32_t ping2_measured = DIST(PulseTime2);
		uint32_t ping3_measured = DIST(PulseTime3);
		
		// calibration with lookup table
		// Ping0
		if(ping0_measured <= SIZE_OF_LOOKUP-1){
		  Ping0_Dist = PingLookup[ping0_measured];
		}			
    else{   
      Ping0_Dist = PingLookup[SIZE_OF_LOOKUP-1];   // ceiling
		}	
		
		// Ping1
		if(ping1_measured <= SIZE_OF_LOOKUP-1){
		  Ping1_Dist = PingLookup[ping1_measured];
		}			
    else{
      Ping1_Dist = PingLookup[SIZE_OF_LOOKUP-1];   // ceiling
		}				
		
		// Ping2
		if(ping2_measured <= SIZE_OF_LOOKUP-1){
		  Ping2_Dist = PingLookup[ping2_measured];
		}			
    else{
      Ping2_Dist = PingLookup[SIZE_OF_LOOKUP-1];   // ceiling
		}	
		
		// Ping3
		if(ping3_measured <= SIZE_OF_LOOKUP-1){
		  Ping3_Dist = PingLookup[ping3_measured];
		}			
    else{
      Ping3_Dist = PingLookup[SIZE_OF_LOOKUP-1];   // ceiling
		}	
			
		// Display results to ST7735
		// Ping0
		if(Ping0_Dist >= PingLookup[SIZE_OF_LOOKUP-1]){       // unmeasureable distance ~ >= 100cm
			ST7735_SetTextColor(ST7735_RED);        
		}
		else if(Ping0_Dist < 10){                                  
      ST7735_SetTextColor(ST7735_BLUE);			
		}
		else{                                                 // 10cm < dist < 100cm 
      ST7735_SetTextColor(ST7735_YELLOW);			
		}
		ST7735_Message(0,0,"Ping0 dist(cm) = ",Ping0_Dist);   // display distance  	
		
		// Ping1
		if(Ping1_Dist >= PingLookup[SIZE_OF_LOOKUP-1]){       // unmeasureable distance ~ >= 100cm
			ST7735_SetTextColor(ST7735_RED);        
		}
		else if(Ping1_Dist < 10){                                  
      ST7735_SetTextColor(ST7735_BLUE);			
		}
		else{                                                 // 10cm < dist < 100cm 
      ST7735_SetTextColor(ST7735_YELLOW);			
		}
		ST7735_Message(0,2,"Ping1 dist(cm) = ",Ping1_Dist);   // display distance  	
		
		// Ping2
		if(Ping2_Dist >= PingLookup[SIZE_OF_LOOKUP-1]){       // unmeasureable distance ~ >= 100cm
			ST7735_SetTextColor(ST7735_RED);        
		}
		else if(Ping2_Dist < 10){                                  
      ST7735_SetTextColor(ST7735_BLUE);			
		}
		else{                                                 // 10cm < dist < 100cm 
      ST7735_SetTextColor(ST7735_YELLOW);			
		}
		ST7735_Message(0,4,"Ping2 dist(cm) = ",Ping2_Dist);   // display distance  	
		
		// Ping3
		if(Ping3_Dist >= PingLookup[SIZE_OF_LOOKUP-1]){       // unmeasureable distance ~ >= 100cm
			ST7735_SetTextColor(ST7735_RED);        
		}
		else if(Ping3_Dist < 10){                                  
      ST7735_SetTextColor(ST7735_BLUE);			
		}
		else{                                                 // 10cm < dist < 100cm 
      ST7735_SetTextColor(ST7735_YELLOW);			
		}
		ST7735_Message(0,6,"Ping3 dist(cm) = ",Ping3_Dist);   // display distance  
		
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);  
	}
}


//******** PingAccuracy *************** 
// Foreground thread.
// Initalizes sensor, collects data, and displays results. 
// Used to determine accuracy of sensors
// inputs:  none
// outputs: none
uint8_t PingTestNum = 0;                // current iteration of test
uint32_t PingBuff[5];               // buffer to store data points
uint8_t PingID = 0;                 // ID of ping sensor you are testing. Ping 1 thru 3.
void PingAccuracy(void){
	PingTestNum++;
	// send pulses 10 times a second
	for(int i = 0; i<5 ;i++){
		SendPulse(PingID);                          // send a single pulse to the Ping sensor
		SysTick_Wait(60000);                        // wait for holdoff time = 750 us.  
		ConfigForCapture(PingID);                   // configure pin to capture pulse width
	
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100); 
		uint8_t pulseTime;
		switch (PingID){
			// ping sensor 0 (J9Y header)
			case(0):{
				pulseTime = PulseTime0;
				break;
			}
			// ping sensor 1 (J10Y header)
			case(1):{
				pulseTime = PulseTime1;
				break;
			}
			// ping sensor 2 (J12Y header)
			case(2):{
				pulseTime = PulseTime2;
				break;
			}
			// ping sensor 3 (J11Y header)
			case(3):{
				pulseTime = PulseTime3;
				break;
			}
		}
		int32_t status = StartCritical();
		if(DIST(pulseTime) < 100){                   // distance in cm
			PingBuff[i] = pulseTime;                   // store to buffer for display later
			EndCritical(status);
		}
		else{
			i--;                                       // erroneous data. retry data point
			EndCritical(status);
		}
	}			
	ST7735_FillScreen(0);            // set screen to black
	ST7735_Message(0,0,"Test # ", PingTestNum);
	for(int i = 0; i<5; i++){
		uint32_t dist = C_AIR * (PingBuff[i]>>1);    // distance in cm
		ST7735_Message(0,i+1,"Dist(cm) = ", dist);   // display results
	}
	PingDebounce = 0;
	OS_Kill();
}
