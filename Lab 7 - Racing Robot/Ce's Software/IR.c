// IR.c
// Runs on TM4C123
// Background thread using Timer1A to execute the function that getting the
// data from pre-initialized ADC, to implement a SW trigger ADC
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 8th, 2016

#include <stdint.h>
#include "Interrupts.h"
#include "SysTick.h"
#include "ADC.h"
#include "ST7735.h"
#include "OS.h"
#include "IR.h"

//******************** Globals ***********************************************
uint8_t IR_IRToDisplay;        // determines which IR sensor's data to display in IRDisplay()
long x0[64],y0[64];         // input and output arrays for FFT IR 0
long x1[64],y1[64];         // input and output arrays for FFT IR 1
long x2[64],y2[64];         // input and output arrays for FFT IR 2
long x3[64],y3[64];         // input and output arrays for FFT IR 3
unsigned long volatile IR_distance0, IR_distance1, IR_distance2, IR_distance3;
//****************************************************************************

//********************Prototypes********************************************
void cr4_fft_64_stm32(void *pssOUT, void *pssIN, unsigned short Nbin);   // FFT in cr4_fft_64_stm32.s, STMicroelectronics
//**************************************************************************


//private global
uint32_t DataLost = 0;
unsigned long DCcomponent0, DCcomponent1, DCcomponent2, DCcomponent3;


//private function
unsigned long IRCompute(unsigned long DCcomponent);


//******** IR_Init_HW ***************
// Timer 0A, HW ADC with the corresponding sampling frequency
void IR_Init_HW(void){
	OS_AddThread(&Consumer1,128,1);		//set the consumer to use the fifo in ADC
	OS_AddThread(&Consumer2,128,1);		//set the consumer to use the fifo2 in ADC
	OS_AddThread(&Consumer3,128,1);		//set the consumer to use the fifo in ADC
	OS_AddThread(&Consumer4,128,1);		//set the consumer to use the fifo2 in ADC
	OS_AddPeriodicThread(&IR_Compute, TIME_100MS, 1); // 10Hz for the computation update of the distance
}

void IR_Compute(void){
	IR_distance0 = IRCompute(DCcomponent0);
	IR_distance1 = IRCompute(DCcomponent1);
	IR_distance2 = IRCompute(DCcomponent2);
	IR_distance3 = IRCompute(DCcomponent3);
}


//*************** Timer trigger ADC for IR Sensor ***************
// HARDWARE timer-triggered ADC sampling at 400Hz
// Producer runs as PART of ADC ISR(ISR run producer)
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
// Display thread updates LCD with measurement
//

//******** Producer1 *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer1(unsigned long data){
	if(OS_Fifo1_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}


//******** Producer2 *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer2(unsigned long data){
	if(OS_Fifo2_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}


//******** Producer3 *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer3(unsigned long data){
	if(OS_Fifo3_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}
//******** Producer4 *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer4(unsigned long data){
	if(OS_Fifo4_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}

//******** Consumer1 *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer1(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data0;
	#ifdef	IR_64_10
		ADC_InitHWTrigger1(IR_0_INIT, IR_FREQUENCY_64_10, &Producer1);		//enable HW ADC for IR sensor 1
	#endif
	#ifdef	IR_32_10
		ADC_InitHWTrigger1(IR_0_INIT, IR_FREQUENCY_32_10, &Producer1);		//enable HW ADC for IR sensor 1
	#endif
  while(1){
		#ifdef	IR_64_10
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 1
      data0 = OS_Fifo1_Get();    // get from producer
      x0[t] = data0;             // real part is 0 to 4095, imaginary part is 0
    }
		#endif
		#ifdef	IR_32_10
    for(t = 0; t < 32; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 1
      data0 = OS_Fifo1_Get();    // get from producer
      x0[t] = data0;             // real part is 0 to 4095, imaginary part is 0
			x0[t + 32] = data0;             // for function call
    }
		#endif
    cr4_fft_64_stm32(y0,x0,64);  // complex FFT of last 64 ADC values
    DCcomponent0 = y0[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** Consumer2 *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer2(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data1;
	#ifdef	IR_64_10
	ADC_InitHWTrigger2(IR_1_INIT, IR_FREQUENCY_64_10, &Producer2);		//enable HW ADC for IR sensor 1
  #endif
	#ifdef	IR_32_10
	ADC_InitHWTrigger2(IR_1_INIT, IR_FREQUENCY_32_10, &Producer2);		//enable HW ADC for IR sensor 1
	#endif
	while(1){
		#ifdef	IR_64_10
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data1 = OS_Fifo2_Get();    // get from producer
      x1[t] = data1;             // real part is 0 to 4095, imaginary part is 0
    }
		#endif
		#ifdef	IR_32_10
		for(t = 0; t < 32; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data1 = OS_Fifo2_Get();    // get from producer
      x1[t] = data1;             // real part is 0 to 4095, imaginary part is 0
			x1[t + 32] = data1;             // real part is 0 to 4095, imaginary part is 0
    }
		#endif
    cr4_fft_64_stm32(y1,x1,64);  // complex FFT of last 64 ADC values
    DCcomponent1 = y1[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** Consumer3 *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer3(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data2;
	#ifdef	IR_64_10
	ADC_InitHWTrigger3(IR_2_INIT, IR_FREQUENCY_64_10, &Producer3);		//enable HW ADC for IR sensor 1
	#endif
	#ifdef	IR_32_10
	ADC_InitHWTrigger3(IR_2_INIT, IR_FREQUENCY_32_10, &Producer3);		//enable HW ADC for IR sensor 1
	#endif
  while(1){
		#ifdef	IR_64_10
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data2 = OS_Fifo3_Get();    // get from producer
      x2[t] = data2;             // real part is 0 to 4095, imaginary part is 0
    }
		#endif
		#ifdef	IR_32_10
		for(t = 0; t < 32; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data2 = OS_Fifo3_Get();    // get from producer
      x2[t] = data2;             // real part is 0 to 4095, imaginary part is 0
			x2[t + 32] = data2;             // real part is 0 to 4095, imaginary part is 0
    }
		#endif
    cr4_fft_64_stm32(y2,x2,64);  // complex FFT of last 64 ADC values
    DCcomponent2 = y2[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** Consumer4 *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer4(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data3;
	#ifdef	IR_64_10
	ADC_InitHWTrigger4(IR_3_INIT, IR_FREQUENCY_64_10, &Producer4);		//enable HW ADC for IR sensor 1
  #endif
	#ifdef	IR_32_10
	ADC_InitHWTrigger4(IR_3_INIT, IR_FREQUENCY_32_10, &Producer4);		//enable HW ADC for IR sensor 1
	#endif
	while(1){
		#ifdef	IR_64_10
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data3 = OS_Fifo4_Get();    // get from producer
      x3[t] = data3;             // real part is 0 to 4095, imaginary part is 0
    }
		#endif
		#ifdef	IR_32_10
    for(t = 0; t < 32; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data3 = OS_Fifo4_Get();    // get from producer
      x3[t] = data3;             // real part is 0 to 4095, imaginary part is 0
			x3[t + 32] = data3;             // real part is 0 to 4095, imaginary part is 0
    }
		#endif
    cr4_fft_64_stm32(y3,x3,64);  // complex FFT of last 64 ADC values
    DCcomponent3 = y3[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** IRCompute *************** 
// Function to calculate the distance according to the voltage and DCcomponent
unsigned long IRCompute(unsigned long DCcomponent){
	unsigned long voltage = 3000*DCcomponent/4095;               // calibrate your device so voltage is in mV	
	unsigned long distance;
	if(DCcomponent >= 3688){
		//smaller than 6cm and just say 6 cm
		distance = 6;
	}else if(DCcomponent >= 3420){
		//6-7cm, 1/7
		distance = 8244/(voltage-2505+1178);
		if(distance < 6){
			distance = 6;
		}else if(distance > 7){
			distance = 7;
		}
	}else if(DCcomponent >= 2596){
		//7-10cm, 1/10
		distance = 14082/(voltage-1901+1408);
		if(distance < 7){
			distance = 7;
		}else if(distance > 10){
			distance = 10;
		}
	}else if(DCcomponent >= 1880){
		//10-13cm, 1/13
		distance = 22725/(voltage-1377+1748);
		if(distance < 10){
			distance = 10;
		}else if(distance > 13){
			distance = 13;
		}
	}else if(DCcomponent >= 1564){
		//13-18cm, 1/18
		distance = 10832/(voltage-1146+602);
		if(distance < 13){
			distance = 13;
		}else if(distance > 18){
			distance = 18;
		}
	}else if(DCcomponent >= 1280){
		//18-23cm, 1/23
		distance = 17223/(voltage-938+749);
		if(distance < 18){
			distance = 18;
		}else if(distance > 23){
			distance = 23;
		}
	}else if(DCcomponent >= 972){
		//23-28cm, 1/28
		distance = 29055/(voltage-712+1038);
		if(distance < 23){
			distance = 23;
		}else if(distance > 28){
			distance = 28;
		}
	}else if(DCcomponent >= 884){
		//28-33cm, 1/33
		distance = 11911/(voltage-647+361);
		if(distance < 28){
			distance = 28;
		}else if(distance > 33){
			distance = 33;
		}
	}else if(DCcomponent >= 765){
		//33-38cm, 1/38
		distance = 21859/(voltage-560+575);
		if(distance < 33){
			distance = 33;
		}else if(distance > 38){
			distance = 38;
		}
	}else if(DCcomponent >= 668){
		//38-43cm, 1/43
		distance = 23217/(voltage-489+540);
		if(distance < 38){
			distance = 38;
		}else if(distance > 43){
			distance = 43;
		}
	}else if(DCcomponent >= 572){
		//43-48cm, 1/48
		distance = 29025/(voltage-419+605);
		if(distance < 43){
			distance = 43;
		}else if(distance > 48){
			distance = 48;
		}
	}else if(DCcomponent >= 520){
		//48-53cm, 1/53
		distance = 19378/(voltage-381+366);
		if(distance < 48){
			distance = 48;
		}else if(distance > 53){
			distance = 53;
		}
	}else if(DCcomponent >= 445){
		//53-58cm, 1/58
		distance = 33772/(voltage-326+582);
		if(distance < 53){
			distance = 53;
		}else if(distance > 58){
			distance = 58;
		}
	}else if(DCcomponent >= 376){
		//58-63cm, 1/63
		distance = 36933/(voltage-275+586);
		if(distance < 58){
			distance = 58;
		}else if(distance > 63){
			distance = 63;
		}
	}else if(DCcomponent >= 348){
		//63-68cm, 1/68
		distance = 17571/(voltage-255+258);
		if(distance < 63){
			distance = 63;
		}else if(distance > 68){
			distance = 68;
		}
	}else{
		//bigger than 68
		distance = 68;
	}
	return distance;
}

//******** IRDisplay *************** 
// Foreground thread.
// Display IR sensor with the timer interval according to the ADC sampling rate for IR sensor
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void IRDisplay(void){
	while(1){
		//Display IR
		// IR0
		if(IR_distance0 >= 68){    
      ST7735_SetTextColor(ST7735_RED);
		}
		else if(IR_distance0 < 10){                                     
			ST7735_SetTextColor(ST7735_BLUE);		
		}
		else{
			ST7735_SetTextColor(ST7735_YELLOW);	
		}
		ST7735_Message(1,0,"IR0 dist(cm) = ",IR_distance0);      // display distance  
		
		// IR1
		if(IR_distance1 >= 50){    
      ST7735_SetTextColor(ST7735_RED);
		}
		else if(IR_distance1 < 10){                                     
			ST7735_SetTextColor(ST7735_BLUE);		
		}
		else{
			ST7735_SetTextColor(ST7735_YELLOW);	
		}
		ST7735_Message(1,2,"IR1 dist(cm) = ",IR_distance1);      // display distance  
		
		// IR2
		if(IR_distance2 >= 50){    
      ST7735_SetTextColor(ST7735_RED);
		}
		else if(IR_distance2 < 10){                                     
			ST7735_SetTextColor(ST7735_BLUE);		
		}
		else{
			ST7735_SetTextColor(ST7735_YELLOW);	
		}
		ST7735_Message(1,4,"IR2 dist(cm) = ",IR_distance2);      // display distance
		
		// IR3
		if(IR_distance3 >= 50){    
      ST7735_SetTextColor(ST7735_RED);
		}
		else if(IR_distance3 < 10){			
			ST7735_SetTextColor(ST7735_BLUE);		
		}
		else{
			ST7735_SetTextColor(ST7735_YELLOW);	
		}
		ST7735_Message(1,6,"IR3 dist(cm) = ",IR_distance3);      // display distance  
		
		//sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);  
	}
}


//******** DAS *************** 
// BACKGROUND thread, calculates 60Hz notch filter
// runs 2000 times/sec
// samples channel 4, PD3,
// inputs:  none
// outputs: none

void Consumer_SW(void){
	int t;
	unsigned long data2;
  for(t = 0; t < 32; t++){   // collect 64 ADC samples, 2.5m per ADC data
		//sensor 2
    data2 = ADC_In();           // channel set when calling ADC_Init = OS_Fifo_Get();    // get from producer
    x2[t] = data2;             // real part is 0 to 4095, imaginary part is 0
		x2[32+t] = data2;             // real part is 0 to 4095, imaginary part is 0
  }
  cr4_fft_64_stm32(y2,x2,64);  // complex FFT of last 64 ADC values
  DCcomponent2 = y2[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero		
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
}


//******** IR_Init_SW *************** 
void IR_Init_SW(void){
	ADC_InitSWTrigger(IR_0_INIT);  // sequencer 2, channel 1, PE2, sampling in DAS(), set it REALLY quick!
	//The period should be 100MS, 10Hz
	//SHOULD BE 640Hz, so there will be 10Hz FFT calculation
	//what about 320Hz, and do the 32FFT?
	OS_AddPeriodicThread(&Consumer_SW, TIME_100MS, 1); // 2 kHz real time sampling of PD3, DAS will go to get it in 2K Hz
}


//******** IRAccuracy *************** 
// Foreground thread.
// Initalizes sensor, collects data, and displays results. 
// Used to determine accuracy of sensors
// inputs:  none
// outputs: none
uint8_t IRDebounce = 0;               // only add one thread when button is pressed
uint8_t IRTestNum = 0;                // current iteration of test
uint32_t IRBuff[5];               // buffer to store data points
uint8_t IRID = 1;                 // ID of ping sensor you are testing. Ping 1 thru 3.
unsigned long tempDCcomponent;
unsigned int voltage, distance;
void IRAccuracy(void){
	int32_t status = StartCritical();
	if(!IRDebounce){
		IRDebounce = 1;
		IRTestNum++;
		EndCritical(status);
	  // send pulses 10 times a second
	  for(int i = 0; i<5 ;i++){
			OS_Sleep(100); 
			switch (IRID){
				// ping sensor 1 (J10Y header)
				case(1):{
					tempDCcomponent = DCcomponent1;
					break;
	    	}
				// ping sensor 2 (J12Y header)
			  case(2):{
					//pulseTime = PulseTime2;
					break;
	    	}
				// ping sensor 3 (J11Y header)
				case(3):{
          //pulseTime = PulseTime3;
					break;
	    	}
			}
			int32_t status = StartCritical();
			voltage = 3000*tempDCcomponent/4095;               // calibrate your device so voltage is in mV
			distance = 10000/(((voltage - 100)*10000/26500) + 125);
			if(distance < 80){          // distance in cm
			  IRBuff[i] = distance;                   // store to buffer for display later
				EndCritical(status);
			}
			else{
				i--;                                       // erroneous data. retry data point
				EndCritical(status);
			}
		}
		ST7735_FillScreen(0);            // set screen to black
		ST7735_Message(0,0,"Test # ", IRTestNum);
		for(int i = 0; i<5; i++){
			uint32_t dist = (IRBuff[i]);    // distance in cm
		  ST7735_Message(0,i+1,"Dist(cm) = ", dist);   // display results
		}
		IRDebounce = 0;
	}
	EndCritical(status);
	OS_Kill();
}
