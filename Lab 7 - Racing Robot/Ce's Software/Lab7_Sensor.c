// Lab7_Sensor.c
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016


// ************ ST7735's Interface*********************************************
// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground
// ****************************************************************************


//************ Timer Resources *************************************************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer4A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 0
//  Timer5A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = 4
//******************************************************************************

//************ OS's Backgroud Threads ******************************************
//  OS_Aging - priority = 7 (only added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 3
//******************************************************************************

//************ Interrupts ******************************************************
//  PB2 - EdgeTriggered Interrupts for Ping.c; priority = 1
//  PB4 - EdgeTriggered Interrupts for Ping.c; priority = 1
//  PB6 - EdgeTriggered Interrupts for Ping.c; priority = 1
//  PC5 - EdgeTriggered Interrupts for Ping.c; priority = 1
//******************************************************************************

//******************** Libraries ***********************************************
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab7_Sensor.h"
#include "OS.h"
#include "UART.h"
#include "ST7735.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"
#include "Interrupts.h"
#include "ADC.h"
#include "Ping.h"
#include "SysTick.h"
#include "CAN_4C123\can0.h"
#include "Buttons.h"
#include "IR.h"
//**************************************************************************


//******************** Globals ***********************************************
uint32_t IdleCount;           // counts how many iterations IdleTask() is ran.
uint32_t RcvCount=0;          // the number of times CanReceive() is called
uint8_t sequenceNum=0;        // incremented every time CanSend() is called. Patches into sent data
uint8_t PingDebounce = 0;         // only add one ButtonWorkPing() thread when button is pressed
uint8_t FirstRun;             // boolean that only adds race threads once when button is pressed. 
//****************************************************************************


//******** IdleTask  *************** 
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
		PF1 ^= 0x02;        // debugging profiler 
    IdleCount++;        // debugging 
		WaitForInterrupt(); // low-power mode
  }
}


//******** Interpreter **************
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}


//******** CANProducer_Ping0 **************
// Foreground thread.
// Sends sensor data over CAN.
// inputs:  none
// outputs: none
void CANProducer(void){ 
	uint8_t frame[4];                          // stores the CAN frame's message in four 8bit chuncks
	uint32_t id;                               // stores the CAN frame's sender ID
	uint32_t input;                            // stores the 32bit message that you wish to send
	
	while(1){
		// Ping0
		id = PING_0;
	  input = Ping0_Dist;
	  frame[0] = input;                         // Byte 0
	  frame[1] = input>>8;                      // Byte 1
	  frame[2] = input>>16;                     // Byte 2
	  frame[3] = (id<<4) + ((input>>24)&0x0F);  // Byte 3 (4 bits though plus id)
	  CAN0_SendData(frame);                     // send 4bits id + 28 bits data	
		OS_Sleep(50);  		                      
		
		id = PING_1;
		// Ping1
		input = Ping1_Dist;
	  frame[0] = input;                         // Byte 0
	  frame[1] = input>>8;                      // Byte 1
	  frame[2] = input>>16;                     // Byte 2
	  frame[3] = (id<<4) + ((input>>24)&0x0F);  // Byte 3 (4 bits though plus id)
	  CAN0_SendData(frame);                     // send 4bits id + 28 bits data	
		OS_Sleep(50);  		     

		// Ping2
		id = PING_2;
		input = Ping2_Dist;
	  frame[0] = input;                         // Byte 0
	  frame[1] = input>>8;                      // Byte 1
	  frame[2] = input>>16;                     // Byte 2
	  frame[3] = (id<<4) + ((input>>24)&0x0F);  // Byte 3 (4 bits though plus id)
	  CAN0_SendData(frame);                     // send 4bits id + 28 bits data	
		OS_Sleep(50); 

    // Ping3
		id = PING_3;
		input = Ping3_Dist;
	  frame[0] = input;                         // Byte 0
	  frame[1] = input>>8;                      // Byte 1
	  frame[2] = input>>16;                     // Byte 2
	  frame[3] = (id<<4) + ((input>>24)&0x0F);  // Byte 3 (4 bits though plus id)
	  CAN0_SendData(frame);                     // send 4bits id + 28 bits data	
		OS_Sleep(50);  		 
	}
	// runs forever, never kills
}


//******** ButtonWorkPing *************** 
// Aperiodic Background thread.
// Creates a thread that initalizes sensor and 
// displays results when pressed. 
// inputs:  none
// outputs: none
void ButtonWorkPing(void){
	int32_t status = StartCritical();
	if(!PingDebounce){
		PingDebounce = 1;
		EndCritical(status);
	  OS_AddThread(&PingAccuracy,128,2);
	}
	else{
		EndCritical(status);
	}
}


//******** ButtonWorkRace *************** 
// Aperiodic Background thread.
// Begins race when pressed.
// Can only add threads once.
// inputs:  none
// outputs: none
void ButtonWorkRace(void){
	if(FirstRun){
		FirstRun = 0;                                    // Should only initalize threads once.
		
		// add threads needed for race
		OS_AddThread(&PingDisplay, 128, 6);              // Display ping results
		OS_AddThread(&IRDisplay, 128, 6);								 // display IR results
		OS_AddThread(&CANProducer, 128, 1);              // send sensor information to motor board
		
		// send start signal to motor board
	  uint8_t frame[4];                                // stores CAN frame's data that will be sent
		uint32_t id = START;                             // id for start race command
		frame[3] = (id<<4);                              //Byte 3 (4 bits id)
		CAN0_SendData(frame);                            //send 4 bit start id
		
		// enable Timer5A so that robot stops after 180 seconds
	  NVIC_EN2_R = 1<<(92-(32*2));                     // Enable IRQ 92 in NVIC
    TIMER5_CTL_R = 0x00000001;                       // Enable timer5A
	}
}


int main(void){  
	// intialize globals
	IdleCount = 0;                                     // number of times IdleTask() is ran
	FirstRun = 1;                                      // prevents ButtonWorkRace() from running more than once
                
  // initalize modules	
	OS_Init();                                         // initialize OS, disable interrupts
	PortF_Init();                                      // initialize Port F profiling
  CAN0_Open();                                       // initalize CAN0
	Ping_Init();                                       // initialize 4 Ping))) sensors pins and threads
	IR_Init_HW();											                 // initialize 4 IR sensors by enabling the HW ADC, producer threads, and consumer threads
	//IR_Init_SW();
	
	// add button tasks
	OS_AddButtonTask(&ButtonWorkRace, PF4_TASK, 4);    // start race when button is pressed
//	OS_AddButtonTask(&ButtonWorkPing, PF0_TASK, 4);    // ping senser accuracy thread
	
  // add initial foreground threads
  OS_AddThread(&Interpreter, 128, 2);                // adds command line interpreter over UART
  OS_AddThread(&IdleTask, 128, 7);                   // runs when nothing useful to do

	// finished initialization
  OS_Launch(TIMESLICE);                              // doesn't return, interrupts enabled in here
	return 0;                                          // this never executes
}
