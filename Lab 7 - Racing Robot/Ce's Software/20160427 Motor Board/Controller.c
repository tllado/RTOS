// Controller.c
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 26th, 2016

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "Controller.h"
#include "Lab7_Motor.h"
#include "Motors.h"
#include "Profiler.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

 int IRSide_ErrP, IRSide_ErrI, IRSide_ErrD, IRSide_ErD1, IRSide_ErD2,          \
     IRSide_ErD3, IRSide_Prev, IRFrnt_ErrP, IRFrnt_ErrI, IRFrnt_ErrD,          \
     IRFrnt_ErD1, IRFrnt_ErD2, IRFrnt_ErD3, IRFrnt_Prev;
 
 uint8_t RunController;  // determines if the simple controller should run

////////////////////////////////////////////////////////////////////////////////
// Median()
// Very basic median filter.
// Input: three integers
// Output: Single integer, median of input values

 int Median(int n1, int n2, int n3) {
     int n0 = 0;
     if(n1 > n2) {
         n0 = n1;
         n1 = n2;
         n2 = n0;
     }
     if(n2 > n3) {
         n0 = n2;
         n2 = n3;
         n3 = n0;
     }
     if(n1 > n2) {
         n0 = n1;
         n1 = n2;
         n2 = n0;
     }
     return n2;
 }

////////////////////////////////////////////////////////////////////////////////
// calcError()
// Calculates P/I/D error values for symmetric pairs of IR sensors.
// Input: old error values and new sensor values for IRSide and IRFrnt
// Output: update error values for IRSide and IRFrnt

 void calcError(void) {
     // Side IR Sensors
     IRSide_ErrP = (int)IR0_Data - (int)IR3_Data;   // calc P error
    
     IRSide_ErrI += IRSide_ErrP;                 // calc I error

     IRSide_ErD3 = IRSide_ErD2;                  // calc D error
     IRSide_ErD2 = IRSide_ErD1;
     IRSide_ErD1 = IRSide_ErrP - IRSide_Prev;
     IRSide_ErrD = Median(IRSide_ErD1, IRSide_ErD2, IRSide_ErD3);
     IRSide_Prev = IRSide_ErrP;

     // Front IR Sensors
     IRFrnt_ErrP = (int)IR1_Data - (int)IR2_Data;   // calc P error
    
     IRFrnt_ErrI += IRFrnt_ErrP;                 // calc I error
    
     IRFrnt_ErD3 = IRFrnt_ErD2;                  // calc D error
     IRFrnt_ErD2 = IRFrnt_ErD1;
     IRFrnt_ErD1 = IRFrnt_ErrP - IRFrnt_Prev;
     IRFrnt_ErrD = Median(IRFrnt_ErD1, IRFrnt_ErD2, IRFrnt_ErD3);
     IRFrnt_Prev = IRFrnt_ErrP;
 }

////////////////////////////////////////////////////////////////////////////////
// PID()
// Very basic PID controller. Uses global variables and combines the results of
//  two PID calculations.
// Input: K and Error values for IRSide and IRFrnt
// Output: Combined control value for both IRSide andIRFrnt

 int PID() {
     int ctrlSide = KpSide*IRSide_ErrP/1000  \
                  + KiSide*IRSide_ErrI/1000  \
                  + KdSide*IRSide_ErrD/1000;
     int ctrlFrnt = KdFrnt*IRFrnt_ErrP/1000  \
                  + KiFrnt*IRFrnt_ErrI/1000  \
                  + KdFrnt*IRFrnt_ErrD/1000;
     return ctrlFrnt + ctrlSide;
 }

////////////////////////////////////////////////////////////////////////////////
// damper()
// implements simulated damping for servo control
// input: desired servo value
// output: dampened servo value
int damper(int current) {
    static int previous;

    previous = previous + (current - previous)*(1000 - damp)/1000;
    
    return previous;
}

////////////////////////////////////////////////////////////////////////////////
// simpleController()
// Runs simple PID controllers to balance pairs of symmetrical IR sensors.
// Input: new sensor data
// Output: motor speed and servo position commands

void simpleController(void) {
	if(RunController){
    calcError();
    int direction = damper(PID());
    int speed = speedMax - (speedMax - speedMin)*abs(direction)/127;
    motorUpdate(speed, direction);
	}
}
