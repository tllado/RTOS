// Lab7_Motor.h
// Runs on TM4C123
// Real Time Operating System for Lab 7's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016

////////////////////////////////////////////////////////////////////////////////
// Debug mode?
//#define DEBUG

// CAN frame ids
#define PING_0   0
#define PING_1   1
#define PING_2   2
#define PING_3   3
#define IR_1     4
#define IR_2     5
#define IR_3     6
#define IR_4     7
#define BUMPER_0 8
#define BUMPER_1 9
#define START    10
//**************************************************************************


//******************** Externs *********************************************
extern uint32_t IR0_Data;        // IR0 data sent from sensor board
extern uint32_t IR1_Data;        // IR1 data sent from sensor board
extern uint32_t IR2_Data;        // IR2 data sent from sensor board
extern uint32_t IR3_Data;        // IR3 data sent from sensor board
extern uint32_t Ping1_Data;      // ping1 data sent from sensor board
//extern uint32_t Bumper0_Data;      // bumper0 data sent from sensor board
//extern uint32_t Bumper1_Data;      // bumper1 data sent from sensor board
//**************************************************************************


//********************Prototypes********************************************

//******** CANConsumer **************
// Foreground thread.
// Retrieves sensor data from CAN FIFO.
// inputs:  none
// outputs: none
void CANConsumer(void);

//******** ServoController **************
// Perioidic background thread.
// Controls servo using PID
// inputs:  none
// outputs: none
void ServoController(void);

//**************************************************************************


