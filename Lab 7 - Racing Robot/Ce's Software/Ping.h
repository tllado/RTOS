// Ping.h
// Runs on TM4C123
// uses ping PB2, PB4, PB6, PC5, for falling edge triggered interrupts 
// to capture the pulse width for sonar Ping))) sensors.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 4th, 2016


//********************Constants*********************************************  
#define PB2                     (*((volatile unsigned long *)0x40005010))
#define PB4                     (*((volatile unsigned long *)0x40005040))
#define PB6                     (*((volatile unsigned long *)0x40005100))
#define PC5                     (*((volatile unsigned long *)0x40006080))

#define NVIC_EN0_INT1           1<<1        // Interrupt 1 enable
#define NVIC_EN0_INT2           1<<2        // Interrupt 2 enable
#define NVIC_DIS0_INT1          1<<1        // Interrupt 1 disable
#define NVIC_DIS0_INT2          1<<2        // Interrupt 2 disable

#define TIMER_TAILR_TAILRL_M    0x0000FFFF  // GPTM TimerA Interval Load Register Low
#define SIZE_OF_LOOKUP          76
#define TEMP_CELSIUS            25
#define C_AIR                   (3315+(6*TEMP_CELSIUS))/1000   // speed of air in sound (cm/.1ms)  
//**************************************************************************


//********************Macros************************************************ 
#define DIST(x) ((C_AIR * x)>>1)	          // derive distance from pulsewidth
//**************************************************************************


//********************Extern************************************************ 
extern volatile uint32_t StartTime2;        // Timer count on rising edge for Ping #2.
extern volatile uint32_t PulseTime2;        // Time of Ping #2's pulse (.1ms)

extern volatile uint32_t Ping0_Dist;          // calibrated ping0 sensor data
extern volatile uint32_t Ping1_Dist;          // calibrated ping1 sensor data
extern volatile uint32_t Ping2_Dist;          // calibrated ping2 sensor data
extern volatile uint32_t Ping3_Dist;          // calibrated ping3 sensor data
//**************************************************************************


//********************Prototypes********************************************


//******** Ping_Init *************** 
// Initializes ping's signal pins  PB2, PB4, PB6, PC5, for falling edge 
// triggered interrupts.
// Max pulse measurement is (2^24-1)*12.5ns = 209.7151ms (using OS_Time())
// Min pulse measurement determined by time to run ISR, which is about 1us
// inputs:  none
// outputs: none
void Ping_Init(void);

//******** SendPulse *************** 
// Issue a 40kHz sound pulse by sending a short 
// logical high burst ~(5us) to the ping sensor.
// Called from PulsePing().
// inputs:  pingID - the ID of the ping sensor you wish to send a pulse to
// outputs: none
void SendPulse(uint8_t pingID);

//******** ConfigForCapture *************** 
// Configure pins for input capture with a pull up resistor
// Called from PulsePing().
// inputs:  pingID - the ID of the ping sensor you wish configure for input capture
// outputs: none
void ConfigForCapture(uint8_t pingID);

//******** Ping0 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping0(void);

//******** Ping1 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping1(void);

//******** Ping2 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping2(void);

//******** Ping3 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping3(void);

//******** PingDisplay *************** 
// Foreground thread.
// Display ping sensor readings 10 times a second
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void PingDisplay(void);

//******** PingAccuracy *************** 
// Foreground thread.
// Initalizes sensor, collects data, and displays results. 
// inputs:  none
// outputs: none
void PingAccuracy(void);
