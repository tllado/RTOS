// Lab7_Sensor.h
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016


//**********************Compilter Directives********************************
  
// *************************************************************************

//********************Constants*********************************************
#define ADC_SAMPLE_RATE 400          // producer/consumer sampling rate
#define PWM_FREQ        40000        // PWM frequency. 40000 = 500Hz

// CAN frame ids
#define PING_0   0
#define PING_1   1
#define PING_2   2
#define PING_3   3
#define IR_1     4
#define IR_2     5
#define IR_3     6
#define IR_4     7
#define BUMPER_0 8
#define BUMPER_1 9
#define START    10
//**************************************************************************

//********************Externs***********************************************
extern uint8_t PingDebounce;               // only add one thread when button is pressed
extern uint8_t FirstRun;               // boolean that only adds race threads once when button is pressed. 
//**************************************************************************


//********************Prototypes********************************************

//**************************************************************************


