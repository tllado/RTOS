// Lab7_Sensor.c
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016


// ************ ST7735's Interface*********************************************
// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground
// ****************************************************************************


//************ Timer Resources *************************************************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - Ping1() ; priority = 1
//  Timer4A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 0
//  Timer5A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = 4
//******************************************************************************


//************ OS's Backgroud Threads ******************************************
//  PingCompute - priority = 2
//  CANProducer - priority = 3
//  OS_Aging - priority = 7 (only added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 1

//******************************************************************************


//******************** Libraries ***********************************************
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab7_Sensor.h"
#include "OS.h"
#include "UART.h"
#include "ST7735.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"
#include "Interrupts.h"
#include "ADC.h"
#include "Ping.h"
#include "SysTick.h"
#include "CAN_4C123\can0.h"
#include "Buttons.h"
#include "IR.h"
//**************************************************************************


//******************** Globals ***********************************************
uint32_t IdleCount;           // counts how many iterations IdleTask() is ran.
uint8_t FirstRun;             // boolean that only adds race threads once when button is pressed.
uint8_t CurrentState_Data;    // the current state of the robot is at from the motor board.
uint8_t StartFrame[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};  // start frame sent from sensor board to motor board
//****************************************************************************


//******** IdleTask  *************** 
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
		PF1 ^= 0x02;        // debugging profiler 
    IdleCount++;        // debugging 
		WaitForInterrupt(); // low-power mode
  }
}


//******** Interpreter **************
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}


//******** CANProducer **************
// Periodic background thread.
// Sends sensor data over CAN.
// inputs:  none
// outputs: none
void CANProducer(void){ 
	uint8_t frame[8];                   // stores the CAN frame's message in four 8bit chuncks
	frame[0] = IR0_Dist;                // Byte 0
	frame[1] = IR1_Dist;                // Byte 1
	frame[2] = IR2_Dist;                // Byte 2
	frame[3] = IR3_Dist;                // Byte 3
	frame[4] = Ping1_Dist;              // Byte 4
//	frame[5] = Bumper0_Data;            // Byte 5
//	frame[6] = Bumper1_Data;            // Byte 6
//	frame[7] = StartRace;               // Byte 7
	
	CAN0_SendData(frame);               // send 64 bits of sensor data		                      
}

//******** CANConsumer **************
// Periodic background thread.
// receives motor data over CAN.
// inputs:  none
// outputs: none
void CANConsumer(void){
  while(1){
		if(CAN0PutPt!=CAN0GetPt){                       // if not empty, then get from FIFO
			do{                                           
				// retrieve all CAN messages in FIFO
				CurrentState_Data = CAN0_Fifo_Get();        // Byte 0 of message
				CAN0_Fifo_Get();                            // Byte 1 of message
				CAN0_Fifo_Get();                            // Byte 2 of message
				CAN0_Fifo_Get();                            // Byte 3 of message
				CAN0_Fifo_Get();                            // Byte 4 of message
				CAN0_Fifo_Get();                            // Byte 5 of message
        CAN0_Fifo_Get();                            // Byte 6 of message
        CAN0_Fifo_Get();                            // Byte 7 of message
				// print result
				printf("CurrentState = %u\n\r", CurrentState_Data);
			}
			while(CAN0PutPt!=CAN0GetPt);
		}
	}
	// runs forever, does not get killed	                      
}

//******** ButtonWorkRace *************** 
// Aperiodic Background thread.
// Begins race when pressed.
// Can only add threads once.
// inputs:  none
// outputs: none
void ButtonWorkRace(void){
	if(FirstRun){
		FirstRun = 0;                                      // Should only initalize threads once.
		
		// add threads needed for race
		OS_AddThread(&PingDisplay, 128, 6);                // Display ping results
		OS_AddThread(&IRDisplay, 128, 6);								   // display IR results
		OS_AddPeriodicThread(&CANProducer, TIME_100MS, 3); // send sensor information to motor board
		OS_AddPeriodicThread(&CANConsumer, TIME_100MS, 3); // receive motor information from motor board
		
		// send start signal to motor board
		CAN0_SendData(StartFrame);                         //send 64 bit start id
		
		// enable Timer5A interrupts so that robot stops after 180 seconds
	  NVIC_EN2_R = 1<<(92-(32*2));                       // Enable IRQ 92 in NVIC

	}
}


int main(void){  
	// intialize globals
	IdleCount = 0;                                     // number of times IdleTask() is ran
	FirstRun = 1;                                      // prevents ButtonWorkRace() from running more than once
                
  // initalize modules	
	OS_Init();                                         // initialize OS, disable interrupts
	PortF_Init();                                      // initialize Port F profiling
  CAN0_Open();                                       // initalize CAN0
	Ping_Init();                                       // initialize 4 Ping))) sensors pins and threads
  IR_Init_HW();											                 // initialize 4 IR sensors by enabling the HW ADC, producer threads, and consumer threads
	
	// add button tasks
	OS_AddButtonTask(&ButtonWorkRace, PF4_TASK, 4);    // start race when button is pressed
	
  // add initial foreground threads
  OS_AddThread(&Interpreter, 128, 2);                // adds command line interpreter over UART
  OS_AddThread(&IdleTask, 128, 7);                   // runs when nothing useful to do

	// finished initialization
  OS_Launch(TIMESLICE);                              // doesn't return, interrupts enabled in here
	return 0;                                          // this never executes
}
