// IR.c
// Runs on TM4C123
// Background thread using Timer1A to execute the function that getting the
// data from pre-initialized ADC, to implement a SW trigger ADC
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 8th, 2016

#include <stdint.h>
#include "Interrupts.h"
#include "SysTick.h"
#include "ADC.h"
#include "ST7735.h"
#include "OS.h"
#include "IR.h"

//******************** Globals ***********************************************
uint8_t IR_IRToDisplay;        // determines which IR sensor's data to display in IRDisplay()
long x0[64],y0[64];         // input and output arrays for FFT IR 0
long x1[64],y1[64];         // input and output arrays for FFT IR 1
long x2[64],y2[64];         // input and output arrays for FFT IR 2
long x3[64],y3[64];         // input and output arrays for FFT IR 3
unsigned long volatile IR0_Dist, IR1_Dist, IR2_Dist, IR3_Dist;
//****************************************************************************

//********************Prototypes********************************************
void cr4_fft_64_stm32(void *pssOUT, void *pssIN, unsigned short Nbin);   // FFT in cr4_fft_64_stm32.s, STMicroelectronics
//**************************************************************************


//private global
uint32_t DataLost = 0;
unsigned long DCcomponent0, DCcomponent1, DCcomponent2, DCcomponent3;


//******** IR_Init_HW ***************
// Timer 0A, HW ADC with the corresponding sampling frequency
void IR_Init_HW(void){
	OS_AddThread(&Consumer1,128,1);		//set the consumer to use the fifo in ADC
	OS_AddThread(&Consumer2,128,1);		//set the consumer to use the fifo2 in ADC
	OS_AddThread(&Consumer3,128,1);		//set the consumer to use the fifo in ADC
	OS_AddThread(&Consumer4,128,1);		//set the consumer to use the fifo2 in ADC
}


//*************** Timer trigger ADC for IR Sensor ***************
// HARDWARE timer-triggered ADC sampling at 400Hz
// Producer runs as PART of ADC ISR(ISR run producer)
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
// Display thread updates LCD with measurement
//

//******** Producer1 *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer1(unsigned long data){
	if(OS_Fifo1_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}


//******** Producer2 *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer2(unsigned long data){
	if(OS_Fifo2_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}


//******** Producer3 *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer3(unsigned long data){
	if(OS_Fifo3_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}
//******** Producer4 *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, START by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is READY,(one data ready, send to consumer)
// Your ADC ISR CALLs this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer4(unsigned long data){
	if(OS_Fifo4_Put(data) == 0){ // send to consumer
		DataLost++;
	} 
}

//******** Consumer1 *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer1(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data0;
	ADC_InitHWTrigger1(IR_0_INIT, IR_FREQUENCY, &Producer1);		//enable HW ADC for IR sensor 1
	//ADC_InitHWTrigger2(IR_2, IR_FREQUENCY, &Producer2);		//enable HW ADC for IR sensor 2
  while(1){
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 1
      data0 = OS_Fifo1_Get();    // get from producer
      x0[t] = data0;             // real part is 0 to 4095, imaginary part is 0
    }
    cr4_fft_64_stm32(y0,x0,64);  // complex FFT of last 64 ADC values
    DCcomponent0 = y0[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** Consumer2 *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer2(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data1;
	ADC_InitHWTrigger2(IR_1_INIT, IR_FREQUENCY, &Producer2);		//enable HW ADC for IR sensor 1
  while(1){
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data1 = OS_Fifo2_Get();    // get from producer
      x1[t] = data1;             // real part is 0 to 4095, imaginary part is 0
    }
    cr4_fft_64_stm32(y1,x1,64);  // complex FFT of last 64 ADC values
    DCcomponent1 = y1[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** Consumer3 *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer3(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data2;
	ADC_InitHWTrigger3(IR_2_INIT, IR_FREQUENCY, &Producer3);		//enable HW ADC for IR sensor 1
  while(1){
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data2 = OS_Fifo3_Get();    // get from producer
      x2[t] = data2;             // real part is 0 to 4095, imaginary part is 0
    }
    cr4_fft_64_stm32(y2,x2,64);  // complex FFT of last 64 ADC values
    DCcomponent2 = y2[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** Consumer4 *************** 
// FOREground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT!!!
// 1000 = 1ms, 1ms*64 = 64ms(15.625 Hz)
// 2000 = 0.5ms, 0.5ms*64 = 32ms(31.25 Hz)
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox, after calculate FFT
void Consumer4(void){
	//OS_AddThread(&Display,128,0);
	int t = 0;
	unsigned long data3;
	ADC_InitHWTrigger4(IR_3_INIT, IR_FREQUENCY, &Producer4);		//enable HW ADC for IR sensor 1
  while(1){
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data3 = OS_Fifo4_Get();    // get from producer
      x3[t] = data3;             // real part is 0 to 4095, imaginary part is 0
    }
    cr4_fft_64_stm32(y3,x3,64);  // complex FFT of last 64 ADC values
    DCcomponent3 = y3[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** IRCompute *************** 
// Function to calculate the distance according to the voltage and DCcomponent
unsigned long IRCompute(unsigned long DCcomponent){
	unsigned long voltage = 3000*DCcomponent/4095;               // calibrate your device so voltage is in mV	
	unsigned long distance;
	if(DCcomponent >= 3726){
		//smaller than 7cm and just say 7 cm
		distance = 7;
	}else if(DCcomponent >= 2900){
		//7-10cm, 1/10, 605
		distance = 14117/(voltage-2124+1411);
	}else if(DCcomponent >= 1952){
		//10-15cm,1/15, 694
		distance = 20820/(voltage-1430+1388);
	}else if(DCcomponent >= 1660){
		//15-18cm,1/18, 214
		distance = 19260/(voltage-1216+1070);
	}else if(DCcomponent >= 1520){
		//18-20cm,1/20, 103
		distance = 18540/(voltage-1113+927);
	}else if(DCcomponent >= 1336){
		//20-23cm,1/23, 134
		distance = 20547/(voltage-979+893);
	}else if(DCcomponent >= 1212){
		//23-25cm,1/25, 91
		distance = 26163/(voltage-888+1047);
	}else if(DCcomponent >= 1117){
		//25-28cm,1/28, 70
		distance = 16333/(voltage-818+583);
	}else if(DCcomponent >= 1000){
		//28-30cm,1/30, 86
		distance = 36120/(voltage-732+1204);
	}else if(DCcomponent >= 932){
		//30-33cm,1/33, 49
		distance = 16170/(voltage-683+490);
	}else if(DCcomponent >= 890){
		//33-35cm,1/35, 31
		distance = 17902/(voltage-652+511);
	}else if(DCcomponent >= 812){
		//35-38cm,1/38, 57
		distance = 25270/(voltage-595+665);
	}else if(DCcomponent >= 765){
		//38-40cm,1/40, 35
		distance = 26600/(voltage-560+665);
	}else if(DCcomponent >= 692){
		//40-45cm,1/45, 53
		distance = 18720/(voltage-507+416);
	}else if(DCcomponent >= 621){
		//45-50cm,1/50, 52
		distance = 22500/(voltage-455+450);
	}else if(DCcomponent >= 572){
		//50-55cm,1/55, 36
		distance = 19800/(voltage-419+360);
	}else if(DCcomponent >= 494){
		//55-60cm,1/60, 57
		distance = 37620/(voltage-362+627);
	}else if(DCcomponent >= 423){
		//60-70cm,1/70, 52
		distance = 21840/(voltage-310+312);
	}else if(DCcomponent >= 396){
		//70-80cm,1/80, 20
		distance = 11200/(voltage-290+140);
	}else if(DCcomponent >= 300){
		//80-100cm,1/100, 70
		distance = 28000/(voltage-220+280);
	}else{
		//bigger than 100
		distance = 100;
	}	
	return distance;
}

//******** IRDisplay *************** 
// Foreground thread.
// Display IR sensor with the timer interval according to the ADC sampling rate for IR sensor
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void IRDisplay(void){
	while(1){
		//display the IR
		// IR0
		IR0_Dist = IRCompute(DCcomponent0);
		if(IR0_Dist >= 50){    
      ST7735_SetTextColor(ST7735_RED);
		}
		else if(IR0_Dist < 10){                                     
			ST7735_SetTextColor(ST7735_BLUE);		
		}
		else{
			ST7735_SetTextColor(ST7735_YELLOW);	
		}
		ST7735_Message(1,0,"IR0 dist(cm) = ",IR0_Dist);      // display distance 
		
		// IR1
		IR1_Dist = IRCompute(DCcomponent1);
		if(IR1_Dist >= 50){    
      ST7735_SetTextColor(ST7735_RED);
		}
		else if(IR1_Dist < 10){                                     
			ST7735_SetTextColor(ST7735_BLUE);		
		}
		else{
			ST7735_SetTextColor(ST7735_YELLOW);	
		}
		ST7735_Message(1,2,"IR1 dist(cm) = ",IR1_Dist);      // display distance 
		
		// IR2
		IR2_Dist = IRCompute(DCcomponent2);
		if(IR2_Dist >= 50){    
      ST7735_SetTextColor(ST7735_RED);
		}
		else if(IR2_Dist < 10){                                     
			ST7735_SetTextColor(ST7735_BLUE);		
		}
		else{
			ST7735_SetTextColor(ST7735_YELLOW);	
		}
		ST7735_Message(1,4,"IR2 dist(cm) = ",IR2_Dist);      // display distance
		
		// IR3
		IR3_Dist = IRCompute(DCcomponent3);
		if(IR3_Dist >= 50){    
      ST7735_SetTextColor(ST7735_RED);
		}
		else if(IR3_Dist < 10){			
			ST7735_SetTextColor(ST7735_BLUE);		
		}
		else{
			ST7735_SetTextColor(ST7735_YELLOW);	
		}
		ST7735_Message(1,6,"IR3 dist(cm) = ",IR3_Dist);      // display distance  
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);  
	}
}


//******** DAS *************** 
// BACKGROUND thread, calculates 60Hz notch filter
// runs 2000 times/sec
// samples channel 4, PD3,
// inputs:  none
// outputs: none
void Consumer_SW(void){
	int t;
	unsigned long data2;
  while(1){
    for(t = 0; t < 64; t++){   // collect 64 ADC samples, 2.5m per ADC data
			//sensor 2
      data2 = ADC_In();           // channel set when calling ADC_Init = OS_Fifo_Get();    // get from producer
      x2[t] = data2;             // real part is 0 to 4095, imaginary part is 0
    }
    cr4_fft_64_stm32(y2,x2,64);  // complex FFT of last 64 ADC values
    DCcomponent2 = y2[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero		
    //OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
	}
}


//******** IR_Init_SW *************** 
void IR_Init_SW(void){
	ADC_InitSWTrigger(IR_2_INIT);  // sequencer 2, channel 1, PE2, sampling in DAS(), set it REALLY quick!
	OS_AddPeriodicThread(&Consumer_SW,IR_FREQUENCY,1); // 2 kHz real time sampling of PD3, DAS will go to get it in 2K Hz
}


//******** IRAccuracy *************** 
// Foreground thread.
// Initalizes sensor, collects data, and displays results. 
// Used to determine accuracy of sensors
// inputs:  none
// outputs: none
uint8_t IRDebounce = 0;               // only add one thread when button is pressed
uint8_t IRTestNum = 0;                // current iteration of test
uint32_t IRBuff[5];               // buffer to store data points
uint8_t IRID = 1;                 // ID of ping sensor you are testing. Ping 1 thru 3.
unsigned long tempDCcomponent;
unsigned int voltage, distance;
void IRAccuracy(void){
	int32_t status = StartCritical();
	if(!IRDebounce){
		IRDebounce = 1;
		IRTestNum++;
		EndCritical(status);
	  // send pulses 10 times a second
	  for(int i = 0; i<5 ;i++){
			OS_Sleep(100); 
			switch (IRID){
				// ping sensor 1 (J10Y header)
				case(1):{
					tempDCcomponent = DCcomponent1;
					break;
	    	}
				// ping sensor 2 (J12Y header)
			  case(2):{
					//pulseTime = PulseTime2;
					break;
	    	}
				// ping sensor 3 (J11Y header)
				case(3):{
          //pulseTime = PulseTime3;
					break;
	    	}
			}
			int32_t status = StartCritical();
			voltage = 3000*tempDCcomponent/4095;               // calibrate your device so voltage is in mV
			distance = 10000/(((voltage - 100)*10000/26500) + 125);
			if(distance < 80){          // distance in cm
			  IRBuff[i] = distance;                   // store to buffer for display later
				EndCritical(status);
			}
			else{
				i--;                                       // erroneous data. retry data point
				EndCritical(status);
			}
		}
		ST7735_FillScreen(0);            // set screen to black
		ST7735_Message(0,0,"Test # ", IRTestNum);
		for(int i = 0; i<5; i++){
			uint32_t dist = (IRBuff[i]);    // distance in cm
		  ST7735_Message(0,i+1,"Dist(cm) = ", dist);   // display results
		}
		IRDebounce = 0;
	}
	EndCritical(status);
	OS_Kill();
}
