// Controller.c
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 20th, 2016

#include <stdint.h>
#include "Controller.h"
#include "Lab7_Motor.h"
#include "PWM.h"
#include "Profiler.h"

//******************** Globals ***********************************************
StateType State_Servo;                            // state machine for servo

int32_t Xstar_Servo = 42;                         // desired value
int32_t X_Servo;                                  // measured value from sensors
int32_t E_Servo;                                  // error between desired and actual
int32_t P_Servo;                                  // proportional term
int32_t I_Servo;                                  // integral term
int32_t U_Servo;                                  // duty cycle output to servo
// ***************************************************************************


//******** ServoController_CW **************
// Perioidic background thread.
// Controls servo using PID assuming the robot move around 
// the track in a clockwise manner.
// inputs:  none
// outputs: none
void ServoController_CW(void){
	uint32_t frontIRs = IR0_Data + IR1_Data;                          // sum of IR0 and IR1 sensor measurments. Helps determine state of robot.
  switch(State_Servo){
		///////////////////////////////////
		//     STATE = MOVE STRAIGHT     //
		///////////////////////////////////
		case ST_STRAIGHT: {
			
			// check if servo needs to turn
      if(frontIRs > IR_NORMAL){
				State_Servo = ST_TURN;
				PF1 = 0x00;
				break;
			}
			
      PF1 = 0x02;
			// move straight
		  PWM1A_Duty(SERVO_CENTER);
			PF1 = 0x00;
		  break;
		}
		
		///////////////////////////////////
		//         STATE = TURN          //
		///////////////////////////////////	
		case ST_TURN: {
			// check if robot needs to move straight
			if(Ping1_Data > PING1_STRAIGHT){
				State_Servo = ST_STRAIGHT;
				break;
			}
			
			// check if robot needs to turn left
			if(IR0_Data > IR0_DIST){
				PF2 = 0x04;
				E_Servo = IR0_Data - IR0_DIST;                // error between desired and actual
				
				// calculate PID terms
	      P_Servo = KP_L_SERVO * E_Servo;             
      	U_Servo = P_Servo + SERVO_CENTER;  
				PF2 = 0x00;
				
			}
			
			// check if robot needs to turn right
			else if(IR1_Data > IR1_DIST){
				PF3 = 0x08;
				// calculate error
				E_Servo = IR1_DIST - IR1_Data;                // error between desired and actual
				
				// calculate PID terms
	      P_Servo = KP_R_SERVO * E_Servo;             
      	U_Servo = P_Servo + SERVO_CENTER;  
				PF3 = 0x00;
			}
			
			// constrain outputs
     	if(U_Servo > SERVO_LEFT){
     		U_Servo = SERVO_LEFT;
     	}
     	if(U_Servo < SERVO_RIGHT){
     		U_Servo = SERVO_RIGHT;
     	}
				
			// apply new output
			PWM1A_Duty(U_Servo);
			
		  break;
		}
		
		///////////////////////////////////
		//         STATE = REVERSE       //
		///////////////////////////////////	
		case ST_REVERSE: {
			
			
			
			
			break;
		}
		
    ///////////////////////////////////
		//        STATE = OBSTACLE       //
		///////////////////////////////////	
		case ST_OBSTACLE: {
			
		  break;
		}
	}
}


//void ServoController(void){
//		// calculate error
//	X_Servo = IR2_Data;
//	E_Servo = Xstar_Servo - X_Servo;                // error between desired and actual
//	
//	// calculate PID terms
//	P_Servo = (KP_N_SERVO*E_Servo)/KP_D_SERVO;      // Kp = KP_N_SERVO / KP_D_SERVO
//	U_Servo = P_Servo+SERVO_CENTER;                 // PI controller
//	
//	// constrain outputs
//	if(U_Servo > SERVO_LEFT){
//		U_Servo = SERVO_LEFT;
//	}
//	if(U_Servo < SERVO_RIGHT){
//		U_Servo = SERVO_RIGHT;
//	}
//	
//	PWM1A_Duty(U_Servo);
//	// apply new output
//}
