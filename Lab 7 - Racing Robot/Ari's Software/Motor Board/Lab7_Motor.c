// Lab7_Motor.c
// Runs on TM4C123
// Real Time Operating System for Lab 7's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016


//************ Timer Resources *************************************************
//  SysTick - OS_Launch() ; priority = 7
//  Timer4A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 0
//  Timer5A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//******************************************************************************


//************ OS's Backgroud Threads ******************************************
//  OS_Aging - priority = 7 (only added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 1
//******************************************************************************


//******************** Libraries ***********************************************
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab7_Motor.h"
#include "OS.h"
#include "UART.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"
#include "Interrupts.h"
#include "PWM.h"
#include "CAN_4C123\can0.h"
#include "Buttons.h"
#include "Controller.h"
//**************************************************************************


//******************** Globals ***********************************************
uint8_t FirstRun;         // binary global that allows the Start thread to only be added once 
uint32_t IdleCount;       // counts how many iterations IdleTask() is ran.
uint32_t IR0_Data;        // IR0 data sent from sensor board
uint32_t IR1_Data;        // IR1 data sent from sensor board
uint32_t IR2_Data;        // IR2 data sent from sensor board
uint32_t IR3_Data;        // IR3 data sent from sensor board
uint32_t Ping1_Data;      // ping1 data sent from sensor board
uint32_t Bumper0_Data;    // Bumper0 data sent from sensor board
uint32_t Bumper1_Data;    // Bumper0 data sent from sensor board
uint32_t StartRace;       // Start race signal sent from sensor board
//****************************************************************************


//******** IdleTask  *************** 
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
		PF1 ^= 0x02;        // debugging profiler 
    IdleCount++;        // debugging 
		WaitForInterrupt(); // low-power mode
  }
}


//******** Interpreter **************
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}

//******** CANProducer **************
// Periodic background thread.
// Sends  motor data over CAN.
// inputs:  none
// outputs: none
void CANProducer(void){ 
	uint8_t frame[8];                   // stores the CAN frame's message in four 8bit chuncks
	frame[0] = State_Servo;               // Byte 0
//	frame[1] = IR1_Dist;                // Byte 1
//	frame[2] = IR2_Dist;                // Byte 2
//	frame[3] = IR3_Dist;                // Byte 3
//	frame[4] = Ping1_Dist;              // Byte 4
//	frame[5] = Bumper0_Data;            // Byte 5
//	frame[6] = Bumper1_Data;            // Byte 6
//	frame[7] = StartRace;               // Byte 7
	
	CAN0_SendData(frame);               // send 64 bits of sensor data		                      
}

//******** CANConsumer **************
// Foreground thread.
// Retrieves sensor data from CAN FIFO.
// inputs:  none
// outputs: none
void CANConsumer(void){ 
  while(1){
		if(CAN0PutPt!=CAN0GetPt){                       // if not empty, then get from FIFO
			do{                                           
				// retrieve all CAN messages in FIFO
				IR0_Data = CAN0_Fifo_Get();               // Byte 0 of message
				IR1_Data = CAN0_Fifo_Get();               // Byte 1 of message
				IR2_Data = CAN0_Fifo_Get();               // Byte 2 of message
				IR3_Data = CAN0_Fifo_Get();               // Byte 3 of message
				Ping1_Data = CAN0_Fifo_Get();             // Byte 4 of message
				Bumper0_Data = CAN0_Fifo_Get();           // Byte 5 of message
        Bumper1_Data = CAN0_Fifo_Get();           // Byte 6 of message
        StartRace = CAN0_Fifo_Get();              // Byte 7 of message
				
				// print results
				printf("IR0(cm)= %u\n\r", IR0_Data);
				printf("IR1(cm)= %u\n\r", IR1_Data);
				printf("IR2(cm)= %u\n\r", IR2_Data);
				printf("IR3(cm)= %u\n\r", IR3_Data);
				printf("Ping1(cm)= %u\n\r", Ping1_Data);
				printf("Bumper0 = %u\n\r", Bumper0_Data);
				printf("Bumper1 = %u\n\r", Bumper1_Data);
				printf("StartRace = %u\n\r", StartRace);
							
				// Start Race
				if(FirstRun == 0 && StartRace){
					FirstRun = 1;
					OS_AddPeriodicThread(&ServoController_CW,TIME_100MS, 1);   // add controler for servo
					
					// enable Timer5A interrupts so that robot stops after 180 seconds
					NVIC_EN2_R = 1<<(92-(32*2));                            // Enable IRQ 92 in NVIC
					
					// start motor
					uint8_t duty_percent = 25;
					uint16_t duty = (PWM_MOTOR_FREQ*duty_percent)/100;
					PWM0B_Duty(0);
					PWM01A_Duty(0);
					PWM0A_Duty(duty);
					PWM01B_Duty(duty);
					break;
				}
			}
			while(CAN0PutPt!=CAN0GetPt);
		}
	}
	// runs forever, does not get killed
}  


int main(void){  
	// intialize globals
	IdleCount = 0;
	FirstRun = 0;
	State_Servo = ST_STRAIGHT;                      // servo should initially be centered
         
  // initalize modules	
	OS_Init();                                      // initialize OS, disable interrupts
	PortF_Init();                                   // initialize Port F profiling
	CAN0_Open();                                    // initalize CAN0
  PWM_Init();                                     // initalize all PWMs used for motors and servos
	
  // create initial foreground threads
  OS_AddThread(&Interpreter, 128, 2);             // add command line interpreter
 // OS_AddThread(&IdleTask, 128, 7);                // runs when nothing useful to do
	OS_AddThread(&CANProducer, 128, 1);             // collects sensor data from CAN fifo
	OS_AddThread(&CANConsumer, 128, 1);             // collects sensor data from CAN fifo

  OS_Launch(TIMESLICE);                           // doesn't return, interrupts enabled in here
  return 0;                                       // this never executes
}
