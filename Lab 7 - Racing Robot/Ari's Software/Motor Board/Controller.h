// Controller.h
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 20th, 2016



//******************** Constants *********************************************
#define KP_N_SERVO 120                 // Kp's numerator for servo. Kp = KP_N_SERVO / KP_D_SERVO
#define KP_D_SERVO 1                   // Kp's denominator for servo. Kp = KP_N_SERVO / KP_D_SERVO

#define KP_R_SERVO 53                  // Kp for when servo is turning right
#define KP_L_SERVO 35                  // Kp for when servo is turning left

#define IR0_DIST 28                    // standard value that IR0 should read in normal operations
#define IR1_DIST 53                    // standard value that IR1 should read in normal operations
#define IR_NORMAL IR0_DIST + IR1_DIST  // max value that the sum of IR0 and IR1 should read during normal operations
#define IR_OBSTACLE IR_NORMAL - 20     // max value that sum of IR0 and IR1 will read in the event of an obstacle

#define PING1_STRAIGHT 60              // robot should move straight if ping1 reads greater than this
// *************************************************************************


// ************* Enums ****************************************************
typedef enum{
	ST_STRAIGHT,
	ST_TURN,
	ST_REVERSE,
	ST_OBSTACLE,
}StateType;

// *************************************************************************


// ************* Externs ****************************************************
extern StateType State_Servo;                            // state machine for servo
// *************************************************************************


//******************** Prototypes *********************************************

//******** ServoController_CW **************
// Perioidic background thread.
// Controls servo using PID assuming the robot move around 
// the track in a clockwise manner.
// inputs:  none
// outputs: none
void ServoController_CW(void);

