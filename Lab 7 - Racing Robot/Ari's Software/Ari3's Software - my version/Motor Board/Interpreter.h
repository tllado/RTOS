// Interpreter.h
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// March 20th, 2016

#define CMD_LEN 10                           // maximum number of letters allowed for command names.
#define NUM_OF(x) (sizeof(x)/sizeof(x[0]))   // counts the # of available commands.


// ************* Stucts ****************************************************
typedef struct{
  const char *name;
  void (*func)(void);
	const char *tag;
}cmdTable;
// **************************************************************************


//------------Interpreter_Init-------
// Initialize interpreter
void Interpreter_Init(void);

//------------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void);

//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void);

//------------cmdMotor-----------------
// Sends commands to the motor
void cmdMotor(void);

//------------cmdServo-----------------
// Control the servo for steering. 0 degrees is far right, 
// 30 degrees is center and 60 degrees is far left.
void cmdServo(void);

//------------cmdCAN-----------------
// Control CAN.
void cmdCAN(void);

