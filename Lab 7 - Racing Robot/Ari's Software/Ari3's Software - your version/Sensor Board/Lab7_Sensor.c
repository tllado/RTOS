// Lab7_Sensor.c
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016


// ************ ST7735's Interface*********************************************
// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground
// ****************************************************************************


//************ Timer Resources *************************************************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - Ping1() ; priority = 2
//  Timer4A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 1
//  Timer5A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = 0
//******************************************************************************


//************ OS's Backgroud Threads ******************************************
//  PingCompute - priority = 2
//  CANProducer - priority = 3
//  OS_Aging - priority = 7 (only added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 1
//******************************************************************************


//******************** Libraries ***********************************************
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab7_Sensor.h"
#include "OS.h"
#include "UART.h"
#include "ST7735.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"
#include "Interrupts.h"
#include "ADC.h"
#include "Ping.h"
#include "SysTick.h"
#include "CAN_4C123\can0.h"
#include "Buttons.h"
#include "IR.h"
#include "Timer5.h"
//**************************************************************************


//******************** Globals ***********************************************
uint32_t IdleCount;           // counts how many iterations IdleTask() is ran.
uint8_t FirstRun;             // boolean that only adds race threads once when button is pressed. 
uint8_t CurrentState_Data;    // the current state of the robot, sent from the motor board.

uint8_t StartFrame[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF};  // start frame sent from sensor board to motor board
//****************************************************************************


//******** IdleTask  *************** 
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
		PF1 ^= 0x02;        // debugging profiler 
    IdleCount++;        // debugging 
		WaitForInterrupt(); // low-power mode
  }
}


//******** Interpreter **************
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}


//******** CANProducer **************
// Periodic background thread.
// Sends sensor data over CAN.
// inputs:  none
// outputs: none
void CANProducer(void){ 
	uint8_t frame[8];                   // stores the CAN frame's message in four 8bit chuncks
	frame[0] = IR0_Dist;                // Byte 0
	frame[1] = IR1_Dist;                // Byte 1
	frame[2] = IR2_Dist;                // Byte 2
	frame[3] = IR3_Dist;                // Byte 3
	frame[4] = Ping1_Dist;              // Byte 4
	if(GPIO_PORTC_DATA_R & 0x40){       // Byte 5 from Bumper0 (PC6)
	  frame[5] = true;
	}
  else{
    frame[5] = false;
  }
	if(GPIO_PORTC_DATA_R & 0x80){       // Byte 6 from Bumper1 (PC7)
	  frame[6] = true;
	}
  else{
    frame[6] = false;
  }
//	frame[7] = StartRace;               // Byte 7
	
	CAN0_SendData(frame);               // send 64 bits of sensor data	

}


//******** CANConsumer **************
// Foreground thread.
// Receives messages from motor board over CAN.
// inputs:  none
// outputs: none
void CANConsumer(void){
  while(1){
		if(CAN0PutPt!=CAN0GetPt){                       // if not empty, then get from FIFO
			do{                                           
				// retrieve all CAN messages in FIFO
				CurrentState_Data = CAN0_Fifo_Get();        // Byte 0 of message
				CAN0_Fifo_Get();                            // Byte 1 of message
				CAN0_Fifo_Get();                            // Byte 2 of message
				CAN0_Fifo_Get();                            // Byte 3 of message
				CAN0_Fifo_Get();                            // Byte 4 of message
				CAN0_Fifo_Get();                            // Byte 5 of message
        CAN0_Fifo_Get();                            // Byte 6 of message
        CAN0_Fifo_Get();                            // Byte 7 of message
			}
			while(CAN0PutPt!=CAN0GetPt);
		}
	}
	// runs forever, does not get killed	                      
}


//******** StateDisplay *************** 
// Foreground thread.
// Display robot's state 10 times a second to the ST7735 
// inputs:  none
// outputs: none
void StateDisplay (void){
	while(1){
		ST7735_SetTextColor(ST7735_YELLOW);
		ST7735_Message(0,0,"State:             ",0);        
		switch(CurrentState_Data){
			case ST_STRAIGHT:{
				ST7735_Message(0,1,"STRAIGHT           ",0);   
				break;
			}
			case ST_TURN_RIGHT:{
				ST7735_Message(0,1,"TURN_RIGHT         ",0);    
				break;
			}
			case ST_TURN_LEFT:{
				ST7735_Message(0,1,"TURN_LEFT          ",0);   
				break;
			}	
			case ST_SLIDE_RIGHT:{
				ST7735_Message(0,1,"SLIDE_RIGHT        ",0);   
				break;
			}	
			case ST_SLIDE_LEFT:{
				ST7735_Message(0,1,"SLIDE_LEFT         ",0);   
				break;
			}	
			case ST_OBSTACLE_RIGHT:{
				ST7735_Message(0,1,"OBSTACLE_RIGHT     ",0);   
				break;
			}	
			case ST_OBSTACLE_LEFT:{
				ST7735_Message(0,1,"OBSTACLE_LEFT      ",0);   
				break;
			}	
			case ST_REVERSE_RIGHT:{
				ST7735_Message(0,1,"REVERSE_RIGHT      ",0);   
				break;
			}	
			case ST_REVERSE_LEFT:{
				ST7735_Message(0,1,"REVERSE_LEFT       ",0);   
				break;
			}	
		}
			
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);  
	}
}


//******** ButtonWorkRace *************** 
// Aperiodic Background thread.
// Begins race when pressed.
// Can only add threads once.
// inputs:  none
// outputs: none
void ButtonWorkRace(void){
	if(FirstRun){
		FirstRun = 0;                                      // Should only initalize threads once.
		
		// add threads for debugging
		OS_AddThread(&PingDisplay, 128, 6);                // display ping results
		OS_AddThread(&IRDisplay, 128, 6);								   // display IR results
		OS_AddThread(&StateDisplay, 128, 6);						   // display state of robot
		OS_AddThread(&CANConsumer, 128, 6);                // receive motor information from motor board

		// add threads needed for race
		OS_AddPeriodicThread(&CANProducer, TIME_100MS, 3); // send sensor information to motor board
		BUMPER0_Init();                                    // bumper 0 initialization
	  BUMPER1_Init();                                    // bumper 1 initialization

		// send start signal to motor board
		CAN0_SendData(StartFrame);                         //send 64 bit start id
		
		// enable Timer5A and its interrupts so that robot stops after 180 seconds
	  Timer5A_Enable();
	}
}


int main(void){  
	// intialize globals
	IdleCount = 0;                                     // number of times IdleTask() is ran
	FirstRun = 1;                                      // prevents ButtonWorkRace() from running more than once
                
  // initalize modules	
	OS_Init();                                         // initialize OS, disable interrupts
	PortF_Init();                                      // initialize Port F profiling
  CAN0_Open();                                       // initalize CAN0
	Ping_Init();                                       // initialize 4 Ping))) sensors pins and threads
  IR_Init_HW();											                 // initialize 4 IR sensors by enabling the HW ADC, producer threads, and consumer threads
	
	// add button tasks
	OS_AddButtonTask(&ButtonWorkRace, PF4_TASK, 4);    // start race when button is pressed
	
  // add initial foreground threads
  OS_AddThread(&Interpreter, 128, 2);                // adds command line interpreter over UART
  OS_AddThread(&IdleTask, 128, 7);                   // runs when nothing useful to do

	// finished initialization
  OS_Launch(TIMESLICE);                              // doesn't return, interrupts enabled in here
	return 0;                                          // this never executes
}
