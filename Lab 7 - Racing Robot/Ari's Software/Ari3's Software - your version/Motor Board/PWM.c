// PWM.c
// Provides PWM outputs on select microcontoller pins
// Runs on TM4C123
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 3rd, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PWM.h"


// The following table provides sample parameter
// inputs for the functions in this module.
// ---------------------------
// Freq(Hz)    Duty      duty%
// ---------------------------
// 40000       30000		 75%
// 40000       10000     25%
// 40000        4000     10%
//  4000        2000     50%
//  1000         900     90%
//  1000         100     10%
// ---------------------------


//******************** Globals ***********************************************
uint16_t DutyPWM0A;       // duty cycle for PWM0A
uint16_t DutyPWM0B;       // duty cycle for PWM0B
uint16_t DutyPWM01A;      // duty cycle for PWM01A
uint16_t DutyPWM01B;      // duty cycle for PWM01B
uint16_t DutyPWM1A;       // duty cycle for PWM1A
uint16_t DutyPWM0;        // duty cycle for motor 1
uint16_t DutyPWM01;       // duty cycle for motor 2
uint16_t DutyPWM1A;       // duty cycle for servo A
//****************************************************************************


// ******** PWM_Init ************
// Initialize all PWM pins and their duty cycles used in this project.
// Inputs: none
// Output: none
void PWM_Init(void){
	DutyPWM0A = 0;                                  // start motor A- at 0 duty cycle
	DutyPWM0B = 0;                                  // start motor A+ at 0 duty cycle
	DutyPWM01A = 0;                                 // start motor B- at 0 duty cycle
	DutyPWM01B = 0;  	                              // start motor B+ at 0 duty cycle
	DutyPWM1A = SERVO_CENTER;                       // start servo in middle    
	PWM0A_Init(PWM_MOTOR_FREQ, DutyPWM0A);          // initialize PWM0A  (motorA)
  PWM0B_Init(PWM_MOTOR_FREQ, DutyPWM0B);          // initialize PWM0B  (motorA)
	PWM01A_Init(PWM_MOTOR_FREQ, DutyPWM01A);        // initialize PWM01A (motorB)
  PWM01B_Init(PWM_MOTOR_FREQ, DutyPWM01B);        // initialize PWM01B (motorB)
	PWM1A_Init(PWM_SERVO_FREQ, DutyPWM1A);          // initialize PWM1A  (servo)
}


// ******** PWM0A_Init ************
// Initialize PWM0A (M0PWM0 on PB6).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM1
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB6/M0PWM0
void PWM0A_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= 0x01;             // 1) activate PWM0
  SYSCTL_RCGCGPIO_R |= 0x02;            // 2) activate port B
  while((SYSCTL_PRGPIO_R&0x02) == 0){}; // allow time to finish activating
  GPIO_PORTB_AFSEL_R |= 0x40;           // enable alt funct on PB6
  GPIO_PORTB_PCTL_R |= (GPIO_PORTB_PCTL_R&0xF0FFFFFF)|0x04000000;  // configure PB6 as M0PWM0
  GPIO_PORTB_AMSEL_R &= ~0x40;          // disable analog functionality on PB6
  GPIO_PORTB_DEN_R |= 0x40;             // enable digital I/O on PB6
  SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV; // 3) use PWM divider
  SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M; //    clear PWM divider field
  SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_2;  //    configure for /2 divider
  PWM0_0_CTL_R = 0;                     // 4) re-loading down-counting mode
  PWM0_0_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);  // PB6 goes low on LOAD, high on CMPA down
  PWM0_0_LOAD_R = period - 1;           // 5) cycles needed to count down to 0
  PWM0_0_CMPA_R = duty - 1;             // 6) count value when output rises
  PWM0_0_CTL_R |= 0x00000001;           // 7) start PWM0
  PWM0_ENABLE_R |= 0x00000001;          // enable PB6/M0PWM0
}


// ******** PWM0A_Duty ************
// Adjust the duty cycle of PWM signal on PB6
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM0A_Duty(uint16_t duty){
  PWM0_0_CMPA_R = duty - 1;             // 6) count value when output rises
}


// ******** PWM0B_Init ************
// Initialize PWM0B (M0PWM1 on PB7).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM0
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB7/M0PWM1
void PWM0B_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= 0x01;             // 1) activate PWM0
  SYSCTL_RCGCGPIO_R |= 0x02;            // 2) activate port B
  while((SYSCTL_PRGPIO_R&0x02) == 0){}; // allow time to finish activating
  GPIO_PORTB_AFSEL_R |= 0x80;           // enable alt funct on PB7
  GPIO_PORTB_PCTL_R |= (GPIO_PORTB_PCTL_R&0x0FFFFFFF)|0x40000000;  // configure PB7 as M0PWM1
  GPIO_PORTB_AMSEL_R &= ~0x80;          // disable analog functionality on PB7
  GPIO_PORTB_DEN_R |= 0x80;             // enable digital I/O on PB7
  SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV; // 3) use PWM divider
  SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M; //    clear PWM divider field
  SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_2;  //    configure for /2 divider
  PWM0_0_CTL_R = 0;                     // 4) re-loading down-counting mode
  PWM0_0_GENB_R = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO); // PB7 goes low on LOAD, high on CMPB down
  PWM0_0_LOAD_R = period - 1;           // 5) cycles needed to count down to 0
  PWM0_0_CMPB_R = duty - 1;             // 6) count value when output rises
  PWM0_0_CTL_R |= 0x00000001;           // 7) start PWM0
  PWM0_ENABLE_R |= 0x00000002;          // enable PB7/M0PWM1
}


// ******** PWM0B_Duty ************
// Adjust the duty cycle of PWM signal on PB7
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM0B_Duty(uint16_t duty){
  PWM0_0_CMPB_R = duty - 1;             // 6) count value when output rises
}

// ******** PWM01A_Init ************
// Initialize PWM01A (M0PWM2 on PB4).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM1
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB6/M0PWM0
void PWM01A_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= 0x01;             // 1) activate PWM0
  SYSCTL_RCGCGPIO_R |= 0x02;            // 2) activate port B
  while((SYSCTL_PRGPIO_R&0x02) == 0){}; // allow time to finish activating
  GPIO_PORTB_AFSEL_R |= 0x10;           // enable alt funct on PB4
  GPIO_PORTB_PCTL_R |= (GPIO_PORTB_PCTL_R&0xFFF0FFFF)|0x00040000;  // configure PB4 as M0PWM2
  GPIO_PORTB_AMSEL_R &= ~0x10;          // disable analog functionality on PB4
  GPIO_PORTB_DEN_R |= 0x10;             // enable digital I/O on PB4
  SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV; // 3) use PWM divider
  SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M; //    clear PWM divider field
  SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_2;  //    configure for /2 divider
  PWM0_1_CTL_R = 0;                     // 4) re-loading down-counting mode
  PWM0_1_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);  // PB4 goes low on LOAD, high on CMPA down
  PWM0_1_LOAD_R = period - 1;           // 5) cycles needed to count down to 0
  PWM0_1_CMPA_R = duty - 1;             // 6) count value when output rises
  PWM0_1_CTL_R |= 0x00000001;           // 7) start PWM0
  PWM0_ENABLE_R |= 0x00000004;          // enable PB4/M0PWM2
}


// ******** PWM01A_Duty ************
// Adjust the duty cycle of PWM signal on PB4
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM01A_Duty(uint16_t duty){
  PWM0_1_CMPA_R = duty - 1;             // 6) count value when output rises
}


// ******** PWM01B_Init ************
// Initialize PWM01B (M0PWM3 on PB5).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM0
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB5/M0PWM3
void PWM01B_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= 0x01;             // 1) activate PWM0
  SYSCTL_RCGCGPIO_R |= 0x02;            // 2) activate port B
  while((SYSCTL_PRGPIO_R&0x02) == 0){}; // allow time to finish activating
  GPIO_PORTB_AFSEL_R |= 0x20;           // enable alt funct on PB5
  GPIO_PORTB_PCTL_R |= (GPIO_PORTB_PCTL_R&0xFF0FFFFF)|0x00400000;  // configure PB5 as M0PWM3
  GPIO_PORTB_AMSEL_R &= ~0x20;          // disable analog functionality on PB5
  GPIO_PORTB_DEN_R |= 0x20;             // enable digital I/O on PB5
  SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV; // 3) use PWM divider
  SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M; //    clear PWM divider field
  SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_2;  //    configure for /2 divider
  PWM0_1_CTL_R = 0;                     // 4) re-loading down-counting mode
  PWM0_1_GENB_R = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO); // PB5 goes low on LOAD, high on CMPB down
  PWM0_1_LOAD_R = period - 1;           // 5) cycles needed to count down to 0
  PWM0_1_CMPB_R = duty - 1;             // 6) count value when output rises
  PWM0_1_CTL_R |= 0x00000001;           // 7) start PWM0
  PWM0_ENABLE_R |= 0x00000008;          // enable PB5/M0PWM3
}


// ******** PWM01B_Duty ************
// Adjust the duty cycle of PWM signal on PB5
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM01B_Duty(uint16_t duty){
  PWM0_1_CMPB_R = duty - 1;             // 6) count value when output rises
}

// ******** PWM1A_Init ************
// Initialize PWM1A (M1PWM0 on PD0).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM1
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PB6/M0PWM0
void PWM1A_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= 0x02;             // 1) activate PWM1
  SYSCTL_RCGCGPIO_R |= 0x08;            // 2) activate port D
  while((SYSCTL_PRGPIO_R&0x08) == 0){}; // allow time to finish activating
  GPIO_PORTD_AFSEL_R |= 0x01;           // enable alt funct on PD0
  GPIO_PORTD_PCTL_R |= (GPIO_PORTB_PCTL_R&0xFFFFFFF0)|0x00000005;  // configure PD0 as M1PWM0
  GPIO_PORTD_AMSEL_R &= ~0x01;          // disable analog functionality on PD0
  GPIO_PORTD_DEN_R |= 0x01;             // enable digital I/O on PD0
  SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV; // 3) use PWM divider
  SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M; //    clear PWM divider field
  SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_32;  //    configure for /32 divider
  PWM1_0_CTL_R = 0;                     // 4) re-loading down-counting mode
  PWM1_0_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);  // PD0 goes low on LOAD, high on CMPA down
  PWM1_0_LOAD_R = period - 1;           // 5) cycles needed to count down to 0
  PWM1_0_CMPA_R = duty - 1;             // 6) count value when output rises
  PWM1_0_CTL_R |= 0x00000001;           // 7) start PWM0
  PWM1_ENABLE_R |= 0x00000001;          // enable PD0/M1PWM0
}


// ******** PWM1A_Duty ************
// Adjust the duty cycle of PWM signal on PD0
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM1A_Duty(uint16_t duty){
  PWM1_0_CMPA_R = duty - 1;             // 6) count value when output rises
}


// ******** PWM1B_Init ************
// Initialize PWM1B (M1PWM1 on PD1).
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2 
//                = 80 MHz/2 = 40 MHz (in this example)
// Inputs: period - number of PWM clock cycles in one period (3<=period), must be same as M0PWM0
//         duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: PWM signal on PD1/M1PWM1
void PWM1B_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= 0x02;             // 1) activate PWM1
  SYSCTL_RCGCGPIO_R |= 0x08;            // 2) activate port D
  while((SYSCTL_PRGPIO_R&0x08) == 0){}; // allow time to finish activating
  GPIO_PORTD_AFSEL_R |= 0x02;           // enable alt funct on PD1
  GPIO_PORTD_PCTL_R |= (GPIO_PORTB_PCTL_R&0xFFFFFF0F)|0x00000050;  // configure PD1 as M1PWM1
  GPIO_PORTD_AMSEL_R &= ~0x02;          // disable analog functionality on PD1
  GPIO_PORTD_DEN_R |= 0x02;             // enable digital I/O on PD1
  SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV; // 3) use PWM divider
  SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M; //    clear PWM divider field
  SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_32;  //    configure for /32 divider
  PWM1_0_CTL_R = 0;                     // 4) re-loading down-counting mode
  PWM1_0_GENB_R = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO); // PD1 goes low on LOAD, high on CMPB down
  PWM1_0_LOAD_R = period - 1;           // 5) cycles needed to count down to 0
  PWM1_0_CMPB_R = duty - 1;             // 6) count value when output rises
  PWM1_0_CTL_R |= 0x00000001;           // 7) start PWM1
  PWM1_ENABLE_R |= 0x00000002;          // enable PD1/M1PWM1
}


// ******** PWM1B_Duty ************
// Adjust the duty cycle of PWM signal on PD1
// Inputs: duty - number of PWM clock cycles output is high  (2<=duty<=period-1)
// Output: none
void PWM1B_Duty(uint16_t duty){
  PWM1_0_CMPB_R = duty - 1;             // 6) count value when output rises
}
