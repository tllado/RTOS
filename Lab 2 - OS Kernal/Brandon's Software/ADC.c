// ADC.c
// Runs on TM4C123 
// Provides analog-to-digital functionality. 
// Brandon Boesch
// Ce Wei
// January 23, 2016

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to Arm Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015

 Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

// Analog channel inputs[0:11] on TM4C123:
// AIN0  - PE3 
// AIN1  - PE2 
// AIN2  - PE1 
// AIN3  - PE0 
// AIN4  - PD3 
// AIN5  - PD2 
// AIN6  - PD1 
// AIN7  - PD0 
// AIN8  - PE5
// AIN9  - PE4 
// AIN10 - PB4 
// AIN11 - PB5 

#include <stdint.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab2.h"
#include "Interrupts.h"
#include "ADC.h"
#include "Profiler.h"

//***********Global Variables******************
void (*UserTask)(unsigned long data);       // user function called by HWTriggered ADC sampling

//*********************************************


// *************** ADC_HWTrigger ********************
// Initializes a channel on ADC0 for timer0 sampling. 
// Samples are passed onto a user defined function
// The parameters below are modified:
// Timer0A: initialized and enabled
// Mode: 16-bit, down counting
// One-shot or periodic: periodic
// Interval value: programmable using 16-bit period
// Sample time is busPeriod*period
// Max sample rate: <=125,000 samples/second
// Sequencer 0 priority: 1st (lowest)
// Sequencer 1 priority: 2nd
// Sequencer 2 priority: 3rd
// Sequencer 3 priority: 4th (highest)
// SS3 triggering event: Timer0A
// SS3 1st sample source: programmable using variable 'channelNum' [0:11]
// SS3 interrupts: initialized and enabled.

// Inputs: channelNum - ADC channel input [0:11].
//         freq - sample frequency.
//         task - function pointer that accepts the collected ADC values.
// Output: none
void ADC_InitHWTrigger(uint8_t channelNum, uint32_t freq, void(*task)(unsigned long data)){
	uint32_t prescale = 80;         // prescale and period used to determine freq. Currently set for f=400Hz. See pg 108 of textbook
	uint32_t period = 1000000/freq; // prescale and period used to determine freq. Currently set for f=400Hz.  See pg 108 of textbook
	UserTask = task;                // user function
	long sr = StartCritical();
	channelInit(channelNum);        // initalize ADC channels
  SYSCTL_RCGCADC_R |= 0x01;       // activate ADC0 
  SYSCTL_RCGCTIMER_R |= 0x01;     // activate timer0 
	while((SYSCTL_PRADC_R&0x01) == 0){}; // allow time for clock to stabilize
	while((SYSCTL_PRTIMER_R&0x01) == 0){}; // allow time for clock to stabilize
  TIMER0_CTL_R = 0x00000000;      // disable timer0A during setup
  TIMER0_CTL_R |= 0x00000020;     // enable timer0A trigger to ADC
  TIMER0_CFG_R = 0x4;             // configure for 16-bit timer mode
  TIMER0_TAMR_R = 0x00000002;     // configure for periodic mode, default down-count settings
  TIMER0_TAPR_R = prescale-1;     // prescale value for trigger
  TIMER0_TAILR_R = period-1;      // start value for trigger
  TIMER0_IMR_R = 0x00000000;      // disable all interrupts
  TIMER0_CTL_R |= 0x00000001;     // enable timer0A 32-b, periodic, no interrupts
  ADC0_PC_R = 0x01;               // configure for 125K samples/sec
  ADC0_SSPRI_R = 0x3210;          // Sequencer 3 = lowest priority, 0 = highest priority
  ADC0_ACTSS_R &= ~0x08;          // disable sample sequencer 3
  ADC0_EMUX_R = (ADC0_EMUX_R&0xFFFF0FFF)+0x5000; // timer trigger event
  ADC0_SSMUX3_R = channelNum;
  ADC0_SSCTL3_R = 0x06;           // set flag and end                       
  ADC0_IM_R |= 0x08;              // enable SS3 interrupts
  ADC0_ACTSS_R |= 0x08;           // enable sample sequencer 3
  NVIC_PRI4_R = (NVIC_PRI4_R&0xFFFF00FF)|0x00004000; //priority 2
  NVIC_EN0_R = 1<<17;           // enable interrupt 17 in NVIC
  EndCritical(sr);
}

void ADC0Seq3_Handler(void){
  ADC0_ISC_R = 0x08;          // acknowledge ADC sequence 3 completion
  UserTask(ADC0_SSFIFO3_R);   // 12-bit result pushed out to user task
}

// enable ADC seq3 interrupts
void ADC_Seq3Enable(void){
	NVIC_EN0_R = 1<<17;           // enable IRQ 17 in NVIC (ADC sequencer 3)
}

// disable ADC seq3 interrupts
void ADC_Seq3Disable(void){
	NVIC_DIS0_R = 1<<17;          // disable IRQ 17 in NVIC (ADC sequencer 3)
}


// *************** ADC_InitSWTrigger ********************
// This initialization function sets up the ADC according to the
// following parameters. 
// Max sample rate: <=125,000 samples/second
// Sequencer 0 priority: 1st (lowest)
// Sequencer 1 priority: 2nd
// Sequencer 2 priority: 3rd
// Sequencer 3 priority: 4th (highest)
// SS2 triggering event: software trigger
// SS2 sample source: Ain0-11
// SS2 interrupts: enabled but not promoted to controller

// Inputs: channelNum - ADC channel input [0:11].
// Output: none
void ADC_InitSWTrigger(uint8_t channelNum){
	long sr = StartCritical();
	SYSCTL_RCGCADC_R |= 0x01;       // activate ADC0 
	channelInit(channelNum);        // initalize ADC channels
	ADC0_PC_R &= ~0xF;              // clear max sample rate field
  ADC0_PC_R |= 0x1;               // configure for 125K samples/sec
  ADC0_SSPRI_R = 0x3210;          // Sequencer 3 = lowest priority, 0 = highest priority
	ADC0_ACTSS_R &= ~0x0004;        // disable sample sequencer 2
  ADC0_EMUX_R &= ~0x0F00;         // seq2 is software trigger
	ADC0_SSMUX2_R = channelNum;     // set channels for SS2
  ADC0_SSCTL2_R = 0x0060;         // no TS0 D0 IE0 END0 TS1 D1, yes IE1 END1
  ADC0_IM_R &= ~0x0004;           // disable SS2 interrupts
  ADC0_ACTSS_R |= 0x0004;         // enable sample sequencer 2
	EndCritical(sr);
}

//------------ADC_In------------
// Busy-wait Analog to digital conversion
// Input: none
// Output: 12-bit result of ADC conversions
// Samples ADC port designated in ADC_InitSWTrigger()
// 125k max sampling
// software trigger, busy-wait sampling
// Inputs: None.
// Output: Value sampled by ADC
uint16_t ADC_In(void){ 
	uint16_t data;                   // 0) return variable
  ADC0_PSSI_R = 0x0004;            // 1) initiate SS2
  while((ADC0_RIS_R&0x04)==0){};   // 2) wait for conversion done
  data = ADC0_SSFIFO2_R&0xFFF;     // 3) read result
  ADC0_ISC_R = 0x0004;             // 4) acknowledge completion
	return data;
}

// *************** channelInit ********************
// Initializes which ADC channel will be used.
// Inputs: channelNum - ADC channel input [0:11].
// Output: none
void channelInit(uint8_t channelNum){
	  // **** GPIO pin initialization ****
  switch(channelNum){             // 1) activate clock
    case 0:
    case 1:
    case 2:
    case 3:
    case 8:
    case 9:                       //    these are on GPIO_PORTE
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R4;
      while((SYSCTL_PRGPIO_R&0x10) == 0){}; // allow time for clock to stabilize 		
			break;
    case 4:
    case 5:
    case 6:
    case 7:                       //    these are on GPIO_PORTD
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R3; 
		  while((SYSCTL_PRGPIO_R&0x08) == 0){}; // allow time for clock to stabilize 
		  break;
    case 10:
    case 11:                      //    these are on GPIO_PORTB
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R1; 
		  while((SYSCTL_PRGPIO_R&0x02) == 0){}; // allow time for clock to stabilize 
		  break;
		default: return;              //    0 to 11 are valid channels on the TM4C123
  }

  switch(channelNum){
    case 0:                       //      Ain0 is on PE3
      GPIO_PORTE_DIR_R &= ~0x08;  // 3.0) make PE3 input
      GPIO_PORTE_AFSEL_R |= 0x08; // 4.0) enable alternate function on PE3
      GPIO_PORTE_DEN_R &= ~0x08;  // 5.0) disable digital I/O on PE3
      GPIO_PORTE_AMSEL_R |= 0x08; // 6.0) enable analog functionality on PE3
      break;
    case 1:                       //      Ain1 is on PE2
      GPIO_PORTE_DIR_R &= ~0x04;  // 3.1) make PE2 input
      GPIO_PORTE_AFSEL_R |= 0x04; // 4.1) enable alternate function on PE2
      GPIO_PORTE_DEN_R &= ~0x04;  // 5.1) disable digital I/O on PE2
      GPIO_PORTE_AMSEL_R |= 0x04; // 6.1) enable analog functionality on PE2
      break;
    case 2:                       //      Ain2 is on PE1
      GPIO_PORTE_DIR_R &= ~0x02;  // 3.2) make PE1 input
      GPIO_PORTE_AFSEL_R |= 0x02; // 4.2) enable alternate function on PE1
      GPIO_PORTE_DEN_R &= ~0x02;  // 5.2) disable digital I/O on PE1
      GPIO_PORTE_AMSEL_R |= 0x02; // 6.2) enable analog functionality on PE1
      break;
    case 3:                       //      Ain3 is on PE0
      GPIO_PORTE_DIR_R &= ~0x01;  // 3.3) make PE0 input
      GPIO_PORTE_AFSEL_R |= 0x01; // 4.3) enable alternate function on PE0
      GPIO_PORTE_DEN_R &= ~0x01;  // 5.3) disable digital I/O on PE0
      GPIO_PORTE_AMSEL_R |= 0x01; // 6.3) enable analog functionality on PE0
      break;
    case 4:                       //      Ain4 is on PD3
      GPIO_PORTD_DIR_R &= ~0x08;  // 3.4) make PD3 input
      GPIO_PORTD_AFSEL_R |= 0x08; // 4.4) enable alternate function on PD3
      GPIO_PORTD_DEN_R &= ~0x08;  // 5.4) disable digital I/O on PD3
      GPIO_PORTD_AMSEL_R |= 0x08; // 6.4) enable analog functionality on PD3
      break;
    case 5:                       //      Ain5 is on PD2
      GPIO_PORTD_DIR_R &= ~0x04;  // 3.5) make PD2 input
      GPIO_PORTD_AFSEL_R |= 0x04; // 4.5) enable alternate function on PD2
      GPIO_PORTD_DEN_R &= ~0x04;  // 5.5) disable digital I/O on PD2
      GPIO_PORTD_AMSEL_R |= 0x04; // 6.5) enable analog functionality on PD2
      break;
    case 6:                       //      Ain6 is on PD1
      GPIO_PORTD_DIR_R &= ~0x02;  // 3.6) make PD1 input
      GPIO_PORTD_AFSEL_R |= 0x02; // 4.6) enable alternate function on PD1
      GPIO_PORTD_DEN_R &= ~0x02;  // 5.6) disable digital I/O on PD1
      GPIO_PORTD_AMSEL_R |= 0x02; // 6.6) enable analog functionality on PD1
      break;
    case 7:                       //      Ain7 is on PD0
      GPIO_PORTD_DIR_R &= ~0x01;  // 3.7) make PD0 input
      GPIO_PORTD_AFSEL_R |= 0x01; // 4.7) enable alternate function on PD0
      GPIO_PORTD_DEN_R &= ~0x01;  // 5.7) disable digital I/O on PD0
      GPIO_PORTD_AMSEL_R |= 0x01; // 6.7) enable analog functionality on PD0
      break;
    case 8:                       //      Ain8 is on PE5
      GPIO_PORTE_DIR_R &= ~0x20;  // 3.8) make PE5 input
      GPIO_PORTE_AFSEL_R |= 0x20; // 4.8) enable alternate function on PE5
      GPIO_PORTE_DEN_R &= ~0x20;  // 5.8) disable digital I/O on PE5
      GPIO_PORTE_AMSEL_R |= 0x20; // 6.8) enable analog functionality on PE5
      break;
    case 9:                       //      Ain9 is on PE4
      GPIO_PORTE_DIR_R &= ~0x10;  // 3.9) make PE4 input
      GPIO_PORTE_AFSEL_R |= 0x10; // 4.9) enable alternate function on PE4
      GPIO_PORTE_DEN_R &= ~0x10;  // 5.9) disable digital I/O on PE4
      GPIO_PORTE_AMSEL_R |= 0x10; // 6.9) enable analog functionality on PE4
      break;
    case 10:                      //       Ain10 is on PB4
      GPIO_PORTB_DIR_R &= ~0x10;  // 3.10) make PB4 input
      GPIO_PORTB_AFSEL_R |= 0x10; // 4.10) enable alternate function on PB4
      GPIO_PORTB_DEN_R &= ~0x10;  // 5.10) disable digital I/O on PB4
      GPIO_PORTB_AMSEL_R |= 0x10; // 6.10) enable analog functionality on PB4
      break;
    case 11:                      //       Ain11 is on PB5
      GPIO_PORTB_DIR_R &= ~0x20;  // 3.11) make PB5 input
      GPIO_PORTB_AFSEL_R |= 0x20; // 4.11) enable alternate function on PB5
      GPIO_PORTB_DEN_R &= ~0x20;  // 5.11) disable digital I/O on PB5
      GPIO_PORTB_AMSEL_R |= 0x20; // 6.11) enable analog functionality on PB5
      break;
  }
}

