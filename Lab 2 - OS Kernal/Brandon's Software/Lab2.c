// Lab2.c
// Runs on LM4F120/TM4C123
// Real Time Operating System for Labs 2 and 3
// Lab2 Part 1: Testmain1 and Testmain2
// Lab2 Part 2: Testmain3 Testmain4  and main
// Lab3: Testmain5 Testmain6, Testmain7, and main (with SW2)

// Jonathan W. Valvano 1/31/14, valvano@mail.utexas.edu
// EE445M/EE380L.6 
// You may use, edit, run or distribute this file 
// You are free to change the syntax/organization of this file

// LED outputs to logic analyzer for OS profile 
// PF1 is preemptive thread switch
// PF2 is periodic task, samples PD3
// PF3 is SW1 task (touch PF4 button)

// Button inputs
// PF0 is SW2 task (Lab3)
// PF4 is SW1 button input

// Analog inputs
// PD3 Ain3 sampled at 2k, sequencer 2, by DAS software start in ISR
// PD2 Ain5 sampled at 250Hz, sequencer 3, by Producer, timer tigger

//************Timer Resources*******************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - OS_AddPeriodicThread() ; priority = variable
//  Timer2A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//  Timer3A - OS_Sleep() ; priority = 4
//**********************************************

#include <string.h> 
#include <stdint.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab2.h"
#include "ST7735.h"
#include "UART.h"
#include "OS.h"
#include "ADC.h"
#include "Profiler.h"
#include "Interpreter.h"

//********************Globals***********************************************
long x[64],y[64];                   // input and output arrays for FFT
unsigned long PIDWork;              // current number of PID calculations finished
unsigned long FilterWork;           // number of digital filter calculations finished
unsigned long NumSamples;           // incremented every ADC sample, in Producer
unsigned long DataLost;             // data sent by Producer, but not received by Consumer
long MaxJitter;                     // largest time jitter between interrupts in usec
unsigned long const JitterSize=JITTERSIZE;
unsigned long JitterHistogram[JITTERSIZE]={0,};
unsigned long Count1;               // number of times thread1 loops
unsigned long Count2;               // number of times thread2 loops
unsigned long Count3;               // number of times thread3 loops
unsigned long Count4;               // number of times thread4 loops
unsigned long Count5;               // number of times thread5 loops
unsigned long BackGroundCnt;        // number of times background thread is called (Debug mode)
//**************************************************************************


//------------------Task 1--------------------------------
// 2 kHz sampling ADC channel 1, using software start trigger
// background thread executed at 2 kHz
// 60-Hz notch high-Q, IIR filter, assuming fs=2000 Hz
// y(n) = (256x(n) -503x(n-1) + 256x(n-2) + 498y(n-1)-251y(n-2))/256 (2k sampling)
// y(n) = (256x(n) -476x(n-1) + 256x(n-2) + 471y(n-1)-251y(n-2))/256 (1k sampling)
long Filter(long data){
static long x[6]; // this MACQ needs twice
static long y[6];
static unsigned long n=3;   // 3, 4, or 5
  n++;
  if(n==6) n=3;     
  x[n] = x[n-3] = data;  // two copies of new data
  y[n] = (256*(x[n]+x[n-2])-503*x[n-1]+498*y[n-1]-251*y[n-2]+128)/256;
  y[n-3] = y[n];         // two copies of filter outputs too
  return y[n];
} 

//******** DAS *************** 
// background thread, calculates 60Hz notch filter
// runs 2000 times/sec
// samples channel 4, PD3,
// inputs:  none
// outputs: none
unsigned long DASoutput;
void DAS(void){ 
  unsigned long input;  
  unsigned static long LastTime;  // time at previous ADC sample
  unsigned long thisTime;         // time at current ADC sample
  long jitter;                    // time between measured and expected, in us
  if(NumSamples < RUNLENGTH){   // finite time run
    #ifdef PROFILER		
    PE0 ^= 0x01;
    #endif		
    input = ADC_In();           // channel set when calling ADC_InitSWTrigger
    #ifdef PROFILER		
    PE0 ^= 0x01;
    #endif		
    thisTime = OS_Time();       // current time, 12.5 ns
    DASoutput = Filter(input);
    FilterWork++;        // calculation finished
    if(FilterWork>1){    // ignore timing of first interrupt
      unsigned long diff = OS_TimeDifference(LastTime,thisTime);
      if(diff>PERIOD){
        jitter = (diff-PERIOD+4)/8;  // in 0.1 usec
      }
			else{
        jitter = (PERIOD-diff+4)/8;  // in 0.1 usec
      }
      if(jitter > MaxJitter){
        MaxJitter = jitter; // in usec
      }       // jitter should be 0
      if(jitter >= JitterSize){
        jitter = JITTERSIZE-1;
      }
      JitterHistogram[jitter]++; 
    }
    LastTime = thisTime;
    #ifdef PROFILER		
    PE0 ^= 0x01;
    #endif		
  }
}
//--------------end of Task 1-----------------------------

//------------------Task 2--------------------------------
// background thread executes with SW1 button
// one foreground task created with button push
// foreground treads run for 2 sec and die
// ***********ButtonWork*************
void ButtonWork(void){
unsigned long myId = OS_Id(); 
  #ifdef PROFILER	
  PE1 ^= 0x02;
  #endif
  ST7735_Message(1,0,"NumCreated =",NumCreated); 
  #ifdef PROFILER	
  PE1 ^= 0x02;
  #endif
  OS_Sleep(50);     // set this to sleep for 50msec
  ST7735_Message(1,1,"PIDWork     =",PIDWork);
  ST7735_Message(1,2,"DataLost    =",DataLost);
  ST7735_Message(1,3,"Jitter 0.1us=",MaxJitter);
  #ifdef PROFILER	
  PE1 ^= 0x02;
  #endif
  OS_Kill();  // done, OS does not return from a Kill
} 

//************SW1Push*************
// Called when SW1 Button pushed
// Adds another foreground task
// background threads execute once and return
void SW1Push(void){
  if(OS_MsTime() > 20){ // debounce
    OS_AddThread(&ButtonWork,100,4);
    OS_ClearMsTime();  // at least 20ms between touches
  }
}
//************SW2Push*************
// Called when SW2 Button pushed, Lab 3 only
// Adds another foreground task
// background threads execute once and return
void SW2Push(void){
  if(OS_MsTime() > 20){ // debounce
    OS_AddThread(&ButtonWork,100,4);
    OS_ClearMsTime();  // at least 20ms between touches
  }
}
//--------------end of Task 2-----------------------------

//------------------Task 3--------------------------------
// hardware timer-triggered ADC sampling at 400Hz
// Producer runs as part of ADC ISR
// Producer uses fifo to transmit 400 samples/sec to Consumer
// every 64 samples, Consumer calculates FFT
// every 2.5ms*64 = 160 ms (6.25 Hz), consumer sends data to Display via mailbox
// Display thread updates LCD with measurement

//******** Producer *************** 
// The Producer in this lab will be called from your ADC ISR
// A timer runs at 400Hz, started by your ADC_Collect
// The timer triggers the ADC, creating the 400Hz sampling
// Your ADC ISR runs when ADC data is ready
// Your ADC ISR calls this function with a 12-bit sample 
// sends data to the consumer, runs periodically at 400Hz
// inputs:  none
// outputs: none
void Producer(unsigned long data){  
  if(NumSamples < RUNLENGTH){   // finite time run
    NumSamples++;               // number of samples
    if(OS_Fifo_Put(data) == 0){ // send to consumer
      DataLost++;
    } 
  } 
}

//******** Consumer *************** 
// foreground thread, accepts data from producer
// calculates FFT, sends DC component to Display
// inputs:  none
// outputs: none
void Consumer(void){ 
unsigned long data,DCcomponent;   // 12-bit raw ADC sample, 0 to 4095
unsigned long t;                  // time in 2.5 ms
unsigned long myId = OS_Id(); 
	
  ADC_InitHWTrigger(5, FS, &Producer); // start ADC sampling, channel 5, PD2, 400 Hz
  OS_AddThread(&Display,128,0); 
  while(NumSamples < RUNLENGTH){ 
		#ifdef PROFILER	
    PE2 = 0x04;
		#endif
    for(t = 0; t < 64; t++){   // collect 64 ADC samples
      data = OS_Fifo_Get();    // get from producer
      x[t] = data;             // real part is 0 to 4095, imaginary part is 0
    }
		#ifdef PROFILER	
    PE2 = 0x00;
		#endif
    cr4_fft_64_stm32(y,x,64);  // complex FFT of last 64 ADC values
    DCcomponent = y[0]&0xFFFF; // Real part at frequency 0, imaginary part should be zero
		OS_MailBox_Send(DCcomponent); // called every 2.5ms*64 = 160ms
  }
	ADC_Seq3Disable();     // Turn off ADC seq3 
  OS_Kill();  // done
}
//******** Display *************** 
// foreground thread, accepts data from consumer
// displays calculated results on the LCD
// inputs:  none                            
// outputs: none
void Display(void){ 
unsigned long data,voltage;
  ST7735_Message(0,1,"Run length = ",(RUNLENGTH)/FS);   // top half used for Display
  while(NumSamples < RUNLENGTH) { 
    data = OS_MailBox_Recv();
    voltage = 3000*data/4095;               // calibrate your device so voltage is in mV
		#ifdef PROFILER	
    PE3 = 0x08;
		#endif
    ST7735_Message(0,2,"v(mV) =",voltage);  
		#ifdef PROFILER
    PE3 = 0x00;
		#endif
  } 
  OS_Kill();  // done
} 

//--------------end of Task 3-----------------------------

//------------------Task 4--------------------------------
// foreground thread that runs without waiting or sleeping
// it executes a digital controller 
//******** PID *************** 
// foreground thread, runs a PID controller
// never blocks, never sleeps, never dies
// inputs:  none
// outputs: none
short IntTerm;     // accumulated error, RPM-sec
short PrevError;   // previous error, RPM
short Coeff[3];    // PID coefficients
short Actuator;
void PID(void){ 
  short err;  // speed error, range -100 to 100 RPM
  unsigned long myId = OS_Id(); 
  PIDWork = 0;
  IntTerm = 0;
  PrevError = 0;
  Coeff[0] = 384;   // 1.5 = 384/256 proportional coefficient
  Coeff[1] = 128;   // 0.5 = 128/256 integral coefficient
  Coeff[2] = 64;    // 0.25 = 64/256 derivative coefficient*
  while(NumSamples < RUNLENGTH) { 
    for(err = -1000; err <= 1000; err++){    // made-up data
      Actuator = PID_stm32(err,Coeff)/256;
    }
    PIDWork++;        // calculation finished
  }
  for(;;){ }          // done
}
//--------------end of Task 4-----------------------------

//------------------Task 5--------------------------------
// UART background ISR performs serial input/output
// Two software fifos are used to pass I/O data to foreground
// The interpreter runs as a foreground thread
// The UART driver should call OS_Wait(&RxDataAvailable) when foreground tries to receive
// The UART ISR should call OS_Signal(&RxDataAvailable) when it receives data from Rx
// Similarly, the transmit channel waits on a semaphore in the foreground
// and the UART ISR signals this semaphore (TxRoomLeft) when getting data from fifo
// Modify your intepreter from Lab 1, adding commands to help debug 
// Interpreter is a foreground thread, accepts input from serial port, outputs to serial port

// add the following commands, leave other commands, if they make sense
// 1) print performance measures 
//    time-jitter, number of data points lost, number of calculations performed
//    i.e., NumSamples, NumCreated, MaxJitter, DataLost, FilterWork, PIDwork
// 2) print debugging parameters 
//    i.e., x[], y[] 
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
 while(1){
		Interpreter_Parse();
	}
}    
//--------------end of Task 5-----------------------------


#ifdef MAIN
	
//*******************final user main DEMONTRATE THIS TO TA**********
int main(void){ 
  OS_Init();           // initialize, disable interrupts
  PortE_Init();
	ST7735_InitR(INITR_REDTAB);       // initialize ST7735 LCD
	ST7735_DrawFastHLine(0, 79, 127, ST7735_WHITE);  // draw divider for two devices
	OS_InitSemaphore(&LCDFree,1);
  DataLost = 0;        // lost data between producer and consumer
  NumSamples = 0;
  MaxJitter = 0;       // in 1us units

//********initialize communication channels
  OS_MailBox_Init();
  OS_Fifo_Init(128);    // *note* 4 is not big enough

//*******attach background tasks************************
	//task 1
  ADC_InitSWTrigger(4);  // sequencer 2, channel 4, PD3, sampling in DAS()
  OS_AddPeriodicThread(&DAS,PERIOD,1); // 2 kHz real time sampling of PD3
	
	//task 2
	OS_AddSW1Task(&SW1Push,2);
//  OS_AddSW2Task(&SW2Push,2);  // add this line in Lab 3

//********create initial foreground threads************
  //task 3
	OS_AddThread(&Consumer,128,1); 
	
  //task 4
  OS_AddThread(&PID,128,3);  // Lab 3, make this lowest priority

  //task 5
  OS_AddThread(&Interpreter,128,2); 
	
  OS_Launch(TIME_2MS); // doesn't return, interrupts enabled in here
  return 0;            // this never executes
}

#endif

#ifdef TESTMAIN1

//*******************First TEST**********
// This is the simplest configuration, test this first, (Lab 2 part 1)
// Count1 Count2 Count3 should be equal or off by one at all times.
// run this with: 
// no UART interrupts
// no SYSTICK interrupts
// no timer interrupts
// no switch interrupts
// no ADC serial port or LCD output
// no calls to semaphores
void Thread1(void){
  Count1 = 0;          
  for(;;){
    PE0 ^= 0x01;       // heartbeat
    Count1++;
    OS_Suspend();      // cooperative multitasking
  }
}
void Thread2(void){
  Count2 = 0;          
  for(;;){
    PE1 ^= 0x02;       // heartbeat
    Count2++;
    OS_Suspend();      // cooperative multitasking
  }
}
void Thread3(void){
  Count3 = 0;          
  for(;;){
    PE2 ^= 0x04;       // heartbeat
    Count3++;
    OS_Suspend();      // cooperative multitasking
  }
}

int main(void){       // Testmain1
  OS_Init();          // initialize, disable interrupts
  PortE_Init();       // profile user threads
  OS_AddThread(&Thread1,128,1); 
  OS_AddThread(&Thread2,128,2); 
  OS_AddThread(&Thread3,128,3); 
  OS_Launch(TIME_2MS); // doesn't return, interrupts enabled in here
  return 0;            // this never executes
}

#endif

#ifdef TESTMAIN2

//*******************Second TEST**********
// After the First test runs, test this (Lab 2 part 1)
// Count1 Count2 Count3 should be equal on average.
// Counts are larger than testmain1.
// run this with: 
// no UART interrupts
// SYSTICK interrupts, with or without period established by OS_Launch
// no timer interrupts
// no switch interrupts
// no ADC serial port or LCD output
// no calls to semaphores
void Thread1b(void){
  Count1 = 0;          
  for(;;){
    PE0 ^= 0x01;       // heartbeat
    Count1++;
  }
}
void Thread2b(void){
  Count2 = 0;          
  for(;;){
    PE1 ^= 0x02;       // heartbeat
    Count2++;
  }
}
void Thread3b(void){
  Count3 = 0;          
  for(;;){
    PE2 ^= 0x04;       // heartbeat
    Count3++;
  }
}

int main(void){        // Testmain2
  OS_Init();           // initialize, disable interrupts
  PortE_Init();        // profile user threads
  OS_AddThread(&Thread1b,128,1); 
  OS_AddThread(&Thread2b,128,2); 
  OS_AddThread(&Thread3b,128,3); 
  OS_Launch(TIME_2MS); // doesn't return, interrupts enabled in here
  return 0;            // this never executes
}

#endif

#ifdef TESTMAIN3

//*******************Third TEST**********
// Once the Second test runs, test this (Lab 2 part 2)
// run this with: 
// no UART1 interrupts
// SYSTICK interrupts, with or without period established by OS_Launch
// Timer interrupts, with or without period established by OS_AddPeriodicThread
// PortF GPIO interrupts, active low
// no ADC serial port or LCD output
// tests the spinlock semaphores, tests Sleep and Kill
int Lost;
void BackgroundThread1c(void){   // called at 1000 Hz
  Count1++;
  OS_Signal(&Readyc);
}

void Thread5c(void){
  for(;;){
    OS_Wait(&Readyc);
    Count5++;   // Count2 + Count5 should equal Count1 
    Lost = Count1-Count5-Count2;
  }
}

void Thread2c(void){
  OS_InitSemaphore(&Readyc,0);
  Count1 = 0;    // number of times signal is called      
  Count2 = 0;    
  Count5 = 0;    // Count2 + Count5 should equal Count1  
  OS_AddThread(&Thread5c,128,3); 
  OS_AddPeriodicThread(&BackgroundThread1c,TIME_1MS,0); 
  for(;;){
    OS_Wait(&Readyc);
    Count2++;   // Count2 + Count5 should equal Count1
  }
}

void Thread3c(void){
  Count3 = 0;          
  for(;;){
    Count3++;
  }
}

void Thread4c(void){ int i;
  for(i=0;i<64;i++){
    Count4++;
    OS_Sleep(10);
  }
  OS_Kill();
  Count4 = 0;
}

void BackgroundThread5c(void){   // called when Select button pushed
  OS_AddThread(&Thread4c,128,3); 
}
      
int main(void){   // Testmain3
  Count4 = 0;       
  OS_Init();           // initialize, disable interrupts

// Count2 + Count5 should equal Count1
  OS_AddSW1Task(&BackgroundThread5c,2);
  OS_AddThread(&Thread2c,128,2); 
  OS_AddThread(&Thread3c,128,3); 
  OS_AddThread(&Thread4c,128,3); 
  OS_Launch(TIME_2MS); // doesn't return, interrupts enabled in here
  return 0;            // this never executes
}

#endif

#ifdef TESTMAIN4

//*******************Fourth TEST**********
// Once the Third test runs, run this example (Lab 2 part 2)
// Count1 should exactly equal Count2
// Count3 should be very large
// Count4 increases by 640 every time select is pressed
// NumCreated increase by 1 every time select is pressed

// run this with: 
// no UART interrupts
// SYSTICK interrupts, with or without period established by OS_Launch
// Timer interrupts, with or without period established by OS_AddPeriodicThread
// Select switch interrupts, active low
// no ADC serial port or LCD output
// tests the spinlock semaphores, tests Sleep and Kill
void BackgroundThread1d(void){   // called at 1000 Hz
static int i=0;
  i++;
  if(i==50){
    i = 0;         //every 50 ms
    Count1++;
    OS_bSignal(&Readyd);
  }
}
void Thread2d(void){
  OS_InitSemaphore(&Readyd,0);
  Count1 = 0;          
  Count2 = 0;          
  for(;;){
    OS_bWait(&Readyd);
    Count2++;     
  }
}
void Thread3d(void){
  Count3 = 0;          
  for(;;){
    Count3++;
  }
}
void Thread4d(void){ int i;
  for(i=0;i<640;i++){
    Count4++;
    OS_Sleep(1);
  }
  OS_Kill();
}
void BackgroundThread5d(void){   // called when Select button pushed
  OS_AddThread(&Thread4d,128,3); 
}
int main(void){   // Testmain4
  Count4 = 0;          
  OS_Init();           // initialize, disable interrupts
  OS_AddPeriodicThread(&BackgroundThread1d,PERIOD,0); 
  OS_AddSW1Task(&BackgroundThread5d,2);
  OS_AddThread(&Thread2d,128,2); 
  OS_AddThread(&Thread3d,128,3); 
  OS_AddThread(&Thread4d,128,3); 
  OS_Launch(TIME_2MS); // doesn't return, interrupts enabled in here
  return 0;            // this never executes
}

#endif


#ifdef TESTMAIN7

//******************* Lab 3 Measurement of context switch time**********
// Run this to measure the time it takes to perform a task switch
// UART0 not needed 
// SYSTICK interrupts, period established by OS_Launch
// first timer not needed
// second timer not needed
// SW1 not needed, 
// SW2 not needed
// logic analyzer on PF1 for systick interrupt (in your OS)
//                on PE0 to measure context switch time
void Thread8(void){   // only thread running
  while(1){
    PE0 ^= 0x01;      // debugging profile  
  }
}
int main(void){       // Testmain7
  PortE_Init();
  OS_Init();          // initialize, disable interrupts
  OS_AddThread(&Thread8,128,2); 
  OS_Launch(TIME_1MS/10); // 100us, doesn't return, interrupts enabled in here
  return 0;               // this never executes
}

#endif


#ifdef DEBUG1
//******************* Personal testing space************************  
void Thread1(void){  
	static uint32_t lastTime1;    // used for captureing last ittertaion's time
  uint32_t thisTime1;           // used for captureing current ittertaion's time
  bool timeReady1 = false;
	OS_Sleep(5000);
	while(1){
		PE0 ^= 0x01;                // debugging profile  
		thisTime1 = OS_Time();      // current time, 12.5 ns units
		if(timeReady1){             // do not run unless Thread1 has been called more than once
			unsigned long diff1 = OS_TimeDifference(lastTime1,thisTime1);
			ST7735_Message(0,0,"diff1 = ",diff1); 
			ST7735_Message(0,1,"count1 = ",Count1); 
			ST7735_Message(0,2,"thread count = ",NumCreated);
			OS_Kill();
		}
		lastTime1 = OS_Time();
		timeReady1 = true;
		Count1++;
  }
}
    
void Thread2(void){
  static uint32_t lastTime2;    // used for captureing last ittertaion's time
  uint32_t thisTime2;           // used for captureing current ittertaion's time	
	bool timeReady2 = false;
	OS_Sleep(7000);
  while(1){
		PE1 ^= 0x02;                // debugging profile 
    thisTime2 = OS_Time();      // current time, 12.5 ns units		
		if(timeReady2){             // do not run unless Thread1 has been called more than once
			unsigned long diff2 = OS_TimeDifference(lastTime2,thisTime2);
			ST7735_Message(0,4,"diff2 = ",diff2); 
			ST7735_Message(0,5,"count2 = ",Count2); 
			ST7735_Message(0,6,"thread count = ",NumCreated);
			OS_Kill();
		}
		lastTime2 = OS_Time();
		timeReady2 = true;
		Count2++;
  }
}

void Thread3(void){
  static uint32_t lastTime3;    // used for captureing last ittertaion's time
  uint32_t thisTime3;           // used for captureing current ittertaion's time	
	bool timeReady3 = false;
	OS_Sleep(10000);
  while(1){
		PE2 ^= 0x04;                // debugging profile 
    thisTime3 = OS_Time();      // current time, 12.5 ns units		
		if(timeReady3){             // do not run unless Thread1 has been called more than once
			unsigned long diff3 = OS_TimeDifference(lastTime3,thisTime3);
			ST7735_Message(1,0,"diff3 = ",diff3); 
			ST7735_Message(1,1,"count3 = ",Count3); 
			ST7735_Message(1,2,"thread count = ",NumCreated);
			OS_Kill();
		}
		lastTime3 = OS_Time();
		timeReady3 = true;
		Count3++;
  }
}

void Thread4(void){
  static uint32_t lastTime4;    // used for captureing last ittertaion's time
  uint32_t thisTime4;           // used for captureing current ittertaion's time	
	bool timeReady4 = false;
//	OS_Sleep(15000);
  while(1){
		PE3 ^= 0x08;                // debugging profile 
    thisTime4 = OS_Time();      // current time, 12.5 ns units		
		if(timeReady4){             // do not run unless Thread1 has been called more than once
			unsigned long diff4 = OS_TimeDifference(lastTime4,thisTime4);
			ST7735_Message(1,4,"diff4 = ",diff4); 
			ST7735_Message(1,5,"count4 = ",Count4); 
			ST7735_Message(1,6,"thread count = ",NumCreated);
//			OS_Kill();
		}
		lastTime4 = OS_Time();
		timeReady4 = true;
		Count4++;
  }
}

void BackgroundThread(void){
  BackGroundCnt++;
}

int main(void){                     // Debuging main
  PortE_Init();                  	
  ST7735_InitR(INITR_REDTAB);       // initialize ST7735 LCD
	ST7735_DrawFastHLine(0, 79, 127, ST7735_WHITE);  // draw divider for two devices
  OS_Init();                        // initialize, disable interrupts

	OS_InitSemaphore(&LCDFree,1);
	Count1 = 0;
	Count2 = 0;
	Count3 = 0;
	Count4 = 0;
	BackGroundCnt = 0;
	OS_AddSW1Task(&BackgroundThread,2);
  OS_AddThread(&Thread1,128,2); 
	OS_AddThread(&Thread2,128,2); 
  OS_AddThread(&Thread3,128,2); 
	OS_AddThread(&Thread4,128,2); 
	
	OS_Launch(TIME_2MS);    // doesn't return, interrupts enabled in here
  return 0;               // this never executes
}

#endif
