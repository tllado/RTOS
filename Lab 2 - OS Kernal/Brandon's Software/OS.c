// OS.c
// Runs on TM4C123 
// Create and manage threads.
// Brandon Boesch
// Ce Wei
// January 25, 2016

#include <stdint.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab2.h"
#include "Interrupts.h"
#include "OS.h"
#include "PLL.h"
#include "Timer2.h"
#include "Timer3.h"
#include "buttons.h"


void (*PeriodicTask)(void);   // user function

// *****************Globals**************************
unsigned long NumCreated;           // number of foreground threads created
TCBType tcbs[MAX_NUMTHREADS];       // Thread Control Blocks (TCB)
TCBType *RunPt;                     // pointer to running TCB
int32_t Stacks[MAX_NUMTHREADS][STACKSIZE];
static uint32_t Fifo[FIFOSIZE];
volatile uint32_t *PutPt;
volatile uint32_t *GetPt;
volatile uint32_t MailBoxData;      // data in mailbox that is passed from producer to consumer
volatile uint32_t MsTime;           // Current time(in ms) since reset
// **************************************************

// *****************Global Semephores****************
volatile Sema4Type Readyc;          // set in background
volatile Sema4Type Readyd;          // set in background
volatile Sema4Type LCDFree;         // ST7735 binary semephore. 0=busy, 1=free
volatile Sema4Type CurrentSize;     // signaled in OS_Fifo_Put. Signifies data is in fifo
volatile Sema4Type FifoMutex;       // signaled in OS_Fifo_Put and OS_Fifo_Get. Allows mutual exclusion of R-W-M access of fifo
volatile Sema4Type DataValid;       // signaled in OS_MailBox_Send. 0 = no data available, 1 = data available.
volatile Sema4Type BoxFree;         // signaled in OS_MailBox_Recv. 0 = mail box not available, 1 = mailbox available
volatile Sema4Type RxDataAvailable; // signaled in UART's copyHardwareToSoftware. 0 = Rx Fifo is empty, else = data in Rx Fifo
volatile Sema4Type TxRoomLeft;      // signaled in UART's copySoftwareToHardware. 0 = Tx Fifo is full, else = room available in Tx Fifo
// **************************************************


//******** OS_AddPeriodicThread *************** 
// Activate TIMER1 interrupts to add a background periodic task.
// typically this function receives the highest priority
// Inputs: pointer to a void/void background function
//         period given in system time units (12.5ns)
//         priority 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// You are free to select the time resolution for this function
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal  OS_bSignal	 OS_AddThread
// This task does not have a Thread ID
// In lab 2, this command will be called 0 or 1 times
// In lab 2, the priority field can be ignored
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddPeriodicThread(void(*task)(void), unsigned long period, unsigned long priority){
	volatile uint32_t delay;
	long sr = StartCritical();
  SYSCTL_RCGCTIMER_R |= 0x02;   // 0) activate TIMER1
	delay = SYSCTL_RCGCTIMER_R;   // allow time to finish activating
  PeriodicTask = task;          // user function
  TIMER1_CTL_R = 0x00000000;    // 1) disable TIMER1A during setup
  TIMER1_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER1_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER1_TAILR_R = period-1;    // 4) reload value
  TIMER1_TAPR_R = 0;            // 5) bus clock resolution
  TIMER1_ICR_R = 0x00000001;    // 6) clear TIMER1A timeout flag
  TIMER1_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|(priority << 13); // 8) priority
  NVIC_EN0_R = 1<<21;           // 9) enable IRQ 21 in NVIC
  TIMER1_CTL_R = 0x00000001;    // 10) enable TIMER1A
	EndCritical(sr);
	return 1;
}


// ***************** Timer1A_Handler ******************
void Timer1A_Handler(void){
	// PF1 = 0x02;                     // turn on debugging LED
  TIMER1_ICR_R = TIMER_ICR_TATOCINT; // acknowledge TIMER1A timeout
  (*PeriodicTask)();                 // execute user task
	// PF1 = 0x00;	                   // turn off debugging LED
}


// ***************** Timer1A_Enable ******************
void Timer1A_Enable(void){
	NVIC_EN0_R = 1<<21;                // enable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000001;         // enable TIMER1A
}


// ***************** Timer1A_Disable ******************
void Timer1A_Disable(void){
	NVIC_DIS0_R = 1<<21;               // disable IRQ 21 in NVIC
	TIMER1_CTL_R = 0x00000000;         // disable TIMER1A
}


//*********************************************************************************
//*********************************************************************************
//*********************************************************************************

// ******** OS_Init ************
// initialize operating system, disable interrupts until OS_Launch
// initialize OS controlled I/O: serial, ADC, systick, LaunchPad I/O and timers 
// input:  none
// output: none
void OS_Init(void){
  DisableInterrupts();
	PLL_Init(Bus80MHz);         // set processor clock to 80 MHz 
	TCBInit();                  // initialize TCBs
	NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
  NVIC_SYS_PRI3_R =(NVIC_SYS_PRI3_R&0x00FFFFFF)|0xE0000000; // priority 7
	Timer2_Init(0xFFFFFFFF);    // intialize timer2 with max 32bit period. Used for OS_Time()
	Timer3_Init(80000);         // interrupt every millisecond.  Used for OS_Sleep();
}


//******** TCBInit *************** 
// Initialize all the TCBs. Called within OS_Init.
// Inputs: none
// Outputs: none
void TCBInit(void){
	NumCreated = 0 ;            // active thread count
	for(int i = 0; i < MAX_NUMTHREADS; i++){
		tcbs[i].id = i;           // set unique id for each tcb
		tcbs[i].alive = false;    // all tcbs are initially inactive
		tcbs[i].sleepCnt = 0;     // all tcbs initially have a sleepcnt of 0
	}
}


//***********SetInitialStack************
// Make the TCB looks like be suspend from previous states
// R15 - PC will be set in OS_AddThread
// R13 - user stack will not be changed
void SetInitialStack(int i){
  tcbs[i].sp = &Stacks[i][STACKSIZE-16]; // thread stack pointer														 
  Stacks[i][STACKSIZE-1] = 0x01000000;   // thumb bit, PSR, last one in the array and lowest in the memory							 
	Stacks[i][STACKSIZE-3] = 0x14141414;   // R14, LR
  Stacks[i][STACKSIZE-4] = 0x12121212;   // R12
  Stacks[i][STACKSIZE-5] = 0x03030303;   // R3
  Stacks[i][STACKSIZE-6] = 0x02020202;   // R2
  Stacks[i][STACKSIZE-7] = 0x01010101;   // R1
  Stacks[i][STACKSIZE-8] = 0x00000000;   // R0
  Stacks[i][STACKSIZE-9] = 0x11111111;   // R11
  Stacks[i][STACKSIZE-10] = 0x10101010;  // R10
  Stacks[i][STACKSIZE-11] = 0x09090909;  // R9
  Stacks[i][STACKSIZE-12] = 0x08080808;  // R8
  Stacks[i][STACKSIZE-13] = 0x07070707;  // R7
  Stacks[i][STACKSIZE-14] = 0x06060606;  // R6
  Stacks[i][STACKSIZE-15] = 0x05050505;  // R5
  Stacks[i][STACKSIZE-16] = 0x04040404;  // R4
}


//******** OS_AddThread *************** 
// add a foregound thread to the scheduler. Increments NumCreated.
// Inputs: pointer to a void/void foreground task
//         number of bytes allocated for its stack
//         priority, 0 is highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// stack size must be divisable by 8 (aligned to double word boundary)
// In Lab 2, you can ignore both the stackSize and priority fields
// In Lab 3, you can ignore the stackSize fields
int OS_AddThread(void(*task)(void), unsigned long stackSize, unsigned long priority){
	int32_t status = StartCritical();
	// create first thread
	if(NumCreated == 0){
		tcbs[0].alive = true;
		tcbs[0].next = &tcbs[0]; // cycle back to self since the only TCB alive
		SetInitialStack(0); 
		Stacks[0][STACKSIZE-2] = (int32_t)(task); // set PC Register
		RunPt = &tcbs[0];        // thread 0 will run first
	}
	// trying to create more threads then allocated
	else if(NumCreated >= MAX_NUMTHREADS){ 
		EndCritical(status);
		return 0;                // failure
	}
	// create new thread
	else{
		int i;                                // i holds the new tcb index
		for(i = 0; i < MAX_NUMTHREADS; i++){  // find dead TCB slot to bring alive
		  if(tcbs[i].alive == false){
				tcbs[i].alive = true;             // bring TCB to life
				break;                          
			}
		}
		if(i == 0){                         // need to find thread that will point to this new thread. Look on far side of TCB list                 
			int j;                            // j holds the wrap around value for tcbs[i]
			for(j = MAX_NUMTHREADS-1; j >= 0; j--){  // search from max value and count down until an alive TCB is found
			  if(tcbs[j].alive == true){
					tcbs[i].next = tcbs[j].next;  // new thread's 'next' pointer connects to next alive thread
					tcbs[j].next = &tcbs[i];      // break old thread's 'next' pointer and connect to new thread
					break;
				}
			}
		}
		else{
		  tcbs[i].next = tcbs[i-1].next;  // new thread's 'next' pointer connects to next alive thread
		  tcbs[i-1].next = &tcbs[i];	    // break old thread's 'next' pointer and connect to new thread
		}
		SetInitialStack(i);
		tcbs[i].sleepCnt = 0;                     // set sleepCnt to zero for new thread
		Stacks[i][STACKSIZE-2] = (int32_t)(task); // set PC Register
	}
	NumCreated++;                   // add to alive thread count
	EndCritical(status);
	return 1;
}

	
//******** OS_Launch *************** 
// start the scheduler, enable interrupts
// Inputs: number of 12.5ns clock cycles for each time slice
//         you may select the units of this parameter
// Outputs: none (does not return)
// In Lab 2, you can ignore the theTimeSlice field
// In Lab 3, you should implement the user-defined TimeSlice field
// It is ok to limit the range of theTimeSlice to match the 24-bit SysTick
void OS_Launch(unsigned long theTimeSlice){
  NVIC_ST_RELOAD_R = theTimeSlice - 1; // reload value
  NVIC_ST_CTRL_R = 0x00000007; // enable, core clock and interrupt arm
  StartOS();                   // start on the first task
}


// ******** OS_Suspend ************
// suspend execution of currently running thread
// scheduler will choose another thread to execute
// Can be used to implement cooperative multitasking 
// Same function as OS_Sleep(0)
// input:  none
// output: none
void OS_Suspend(void){
	NVIC_ST_CURRENT_R = 0;          // clear the counter
  NVIC_INT_CTRL_R = 0x04000000;		// manually triggers SysTick. SysTick handler performs thread switch
}	


// ******** OS_InitSemaphore ************
// Initialize semaphore. A value >= 1 means semaphor is free for use 
// input:  pointer to a semaphore
// output: none
void OS_InitSemaphore(volatile Sema4Type *semaPt, long value){
  semaPt->Value = value;
}
	

// ******** OS_Wait ************
// decrement semaphore 
// Lab2 spinlock
// Lab3 block if less than zero
// input:  pointer to a counting semaphore
// output: none
void OS_Wait(volatile Sema4Type *semaPt){
  DisableInterrupts();
	while(semaPt->Value <= 0){
		EnableInterrupts();
		OS_Suspend();          // run thread switcher
		DisableInterrupts();
	}
	semaPt->Value = semaPt->Value - 1;
	EnableInterrupts();
} 
		

// ******** OS_Signal ************
// increment semaphore 
// Lab2 spinlock
// Lab3 wakeup blocked thread if appropriate 
// input:  pointer to a counting semaphore
// output: none
void OS_Signal(volatile Sema4Type *semaPt){
	int32_t status = StartCritical();
	semaPt->Value = semaPt->Value + 1;
	EndCritical(status);
}
	

// ******** OS_bWait ************
// Lab2 spinlock, set to 0
// Lab3 block if less than zero
// input:  pointer to a binary semaphore
// output: none
void OS_bWait(volatile Sema4Type *semaPt){
  DisableInterrupts();
	while(semaPt->Value == 0){
		EnableInterrupts();
		OS_Suspend();          // run thread switcher
		DisableInterrupts();
	}
	semaPt->Value = 0;
	EnableInterrupts();
}
	

// ******** OS_bSignal ************
// Lab2 spinlock, set to 1
// Lab3 wakeup blocked thread if appropriate 
// input:  pointer to a binary semaphore
// output: none
void OS_bSignal(volatile Sema4Type *semaPt){
	int32_t status = StartCritical();
	semaPt->Value = 1;
	EndCritical(status);
}


// ******** OS_Sleep ************
// place this thread into a dormant state. Having all active threads 
// sleeping will break this implementation.
// input:  number of msec to sleep
// output: none
// You are free to select the time resolution for this function
// OS_Sleep(0) implements cooperative multitasking
void OS_Sleep(unsigned long sleepTime){
  RunPt->sleepCnt = sleepTime;
	OS_Suspend();
}


// ******** OS_Kill ************
// Kill the currently running thread, release its TCB and stack. 
// Decrements NumCreated.
// input:  none
// output: none
void OS_Kill(void){
	int32_t status = StartCritical();
	// determine if the thread you are killing is the first living thread in the list
	int i = 0;
	bool firstTCB = true;      // assume the thread you are killing is the first alive in the list
	do{                        // check the life status of all TCBs in the list before the one you are killing
		if(OS_Id() == 0) firstTCB = true;          // TCB[0] has to be the first in the list. breaks out of while loop
		else if(tcbs[i].alive) firstTCB = false;   // if a TCB is alive earlier in the list, then the one you are killing is not first
		i++;
	} while(i < OS_Id()); 
	
	// removing first thread in list
	if(firstTCB){     
		for(int j = MAX_NUMTHREADS-1; j >= 0; j--){  // search from max value and count down until an alive TCB is found
		  if(tcbs[j].alive == true){
				tcbs[j].next = tcbs[OS_Id()].next;   // redirect previous thread's 'next' pointer to the thread after this killed thread
				break;
			}
		}
	}
	// removing thread from list other than the first
	else{
		i--;       // have index for TCB immedietly before previous
		while(1){  // continue looking for previous living TCB until found 
      if(tcbs[i].alive){
				tcbs[i].next = tcbs[OS_Id()].next; // redirect previous thread's 'next' pointer to the thread after this killed thread
				break;
			}
			i--;
		}			
	}
	tcbs[OS_Id()].alive = false;
	NumCreated--;
	OS_Suspend();
	EndCritical(status);
}


//******** OS_Id *************** 
// returns the thread ID for the currently running thread
// Inputs: none
// Outputs: Thread ID, number greater than zero 
unsigned long OS_Id(void){
  return RunPt->id;
}


//******** OS_AddSW1Task *************** 
// add a background task to run whenever the SW1 (PF4) button is pushed
// Inputs: pointer to a void/void background function
//         priority 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal  OS_bSignal	 OS_AddThread
// This task does not have a Thread ID
// In labs 2 and 3, this command will be called 0 or 1 times
// In lab 2, the priority field can be ignored
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddSW1Task(void(*task)(void), unsigned long priority){
	if(NumCreated == MAX_NUMTHREADS) return 0;  // cannot make new task since max # of threads is already active
	SW1_Init(*task, priority);
  return 1;
}


//******** OS_AddSW2Task *************** 
// add a background task to run whenever the SW2 (PF0) button is pushed
// Inputs: pointer to a void/void background function
//         priority 0 is highest, 5 is lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed user task will run to completion and return
// This task can not spin block loop sleep or kill
// This task can call issue OS_Signal, it can call OS_AddThread
// This task does not have a Thread ID
// In lab 2, this function can be ignored
// In lab 3, this command will be called will be called 0 or 1 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddSW2Task(void(*task)(void), unsigned long priority){return 1;}


// ******** OS_Time ************
// Return the system time. Uses Timer2A 
// Inputs:  none
// Outputs: time in 12.5ns units, 0 to 4294967295
// The time resolution should be less than or equal to 1us, and the precision 32 bits.
// It is ok to change the resolution and precision of this function as long as 
// this function and OS_TimeDifference have the same resolution and precision. 
unsigned long OS_Time(void){
  return TIMER2_TAR_R;         // Timer2A is used to return time
}


// ******** OS_TimeDifference ************
// Calculates difference between two times
// Inputs:  two times measured with OS_Time
// Outputs: time difference in 12.5ns units 
// The time resolution should be less than or equal to 1us, and the precision at least 12 bits.
// It is ok to change the resolution and precision of this function as long as 
// this function and OS_Time have the same resolution and precision 
unsigned long OS_TimeDifference(unsigned long start, unsigned long stop){
  return start-stop;           
}


// ******** OS_ClearMsTime ************
// sets the system time to zero (Timer2A)
// Inputs:  none
// Outputs: none
void OS_ClearMsTime(void){
	long sr = StartCritical();
  MsTime = 0;
	EndCritical(sr);
}
	
	
// ******** OS_MsTime ************
// Reads the current time (Timer2A) in ms units (assuming 80MHz clock)
// Inputs:  none
// Outputs: time in ms units since last clear
// You are free to select the time resolution for this function
// It is ok to make the resolution to match the first call to OS_AddPeriodicThread
unsigned long OS_MsTime(void){
	long sr = StartCritical();
	MsTime = (0xFFFFFFFF - TIMER2_TAR_R)/80000;
	EndCritical(sr);
  return MsTime;
}


// ******** OS_Fifo_Init ************
// Initialize the Fifo to be empty
// Inputs: size
// Outputs: none 
// In Lab 2, you can ignore the size field
// In Lab 3, you should implement the user-defined fifo size
// In Lab 3, you can put whatever restrictions you want on size
//    e.g., 4 to 64 elements
//    e.g., must be a power of 2,4,8,16,32,64,128
void OS_Fifo_Init(unsigned long size){
  PutPt = &Fifo[0];
	GetPt = &Fifo[0];
	OS_InitSemaphore(&CurrentSize, 0);  // Nothing in fifo initially
	OS_InitSemaphore(&FifoMutex, 1);    // Fifo is available initially
}
	
	
// ******** OS_Fifo_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting.
// This function cannot enable/disable intterupts 
// since it will be called from interrupts.
// Inputs:  data
// Outputs: true - when data is properly saved,
//          false - if data not saved, because fifo was full
int OS_Fifo_Put(unsigned long data){
  volatile uint32_t *nextPutPt;
	nextPutPt = PutPt + 1;
	if(nextPutPt == &Fifo[FIFOSIZE]){
		nextPutPt = &Fifo[0];		// wrap around
	}
	if(nextPutPt == GetPt){
		return 0;  // cannot put since fifo is full. Results in lost data
	}
	else{
		*(PutPt) = data;
		PutPt = nextPutPt;
		OS_Signal(&CurrentSize);
	}
  return 1;    // data put in fifo successfully
}


// ******** OS_Fifo_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will spin/block if empty
// Inputs:  none
// Outputs: data 
unsigned long OS_Fifo_Get(void){
	int32_t returnValue;    // holds the value poped off of fifo.
	OS_Wait(&CurrentSize);  // spin/block for data
	OS_bWait(&FifoMutex);   // only one thread can access the fifo at any given time
	returnValue = *GetPt;   // pop element off fifo
	GetPt++;
	if(GetPt == &Fifo[FIFOSIZE]){
		GetPt = &Fifo[0];     // wrap around
	}
	OS_bSignal(&FifoMutex); 	//release the fifo
  return returnValue;
}


// ******** OS_Fifo_Size ************
// Check the status of the Fifo
// Inputs: none
// Outputs: returns the number of elements in the Fifo
//          greater than zero if a call to OS_Fifo_Get will return right away
//          zero or less than zero if the Fifo is empty 
//          zero or less than zero if a call to OS_Fifo_Get will spin or block
long OS_Fifo_Size(void){
  return CurrentSize.Value;
}


// ******** OS_MailBox_Init ************
// Initialize communication channel
// Inputs:  none
// Outputs: none
void OS_MailBox_Init(void){
  OS_InitSemaphore(&DataValid, 0);  // Nothing in mailbox initially
	OS_InitSemaphore(&BoxFree, 1);    // Mailbox is available initially
}
	
	
// ******** OS_MailBox_Send ************
// enter mail into the MailBox
// Inputs:  data to be sent
// Outputs: none
// This function will be called from a foreground thread
// It will spin/block if the MailBox contains data not yet received 
void OS_MailBox_Send(unsigned long data){
  OS_bWait(&BoxFree);
	MailBoxData = data;
	OS_bSignal(&DataValid);
}
	
	
// ******** OS_MailBox_Recv ************
// remove mail from the MailBox
// Inputs:  none
// Outputs: data received
// This function will be called from a foreground thread
// It will spin/block if the MailBox is empty 	
unsigned long OS_MailBox_Recv(void){
  OS_bWait(&DataValid);
	uint32_t returnData = MailBoxData;
	OS_bSignal(&BoxFree);
  return returnData;
}
