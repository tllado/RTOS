// Profiler.c
// Runs on TM4C123 
// Provides digital output pins that can be attached 
// to a scope to create a profile of the system.
// Brandon Boesch
// Ce Wei
// January 31, 2016

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Profiler.h"

//------------PortE_Init------------
// Initialize PortE pins PE0-3 as outputs
// Input: none
// Output: none
void PortE_Init(void){ 
  SYSCTL_RCGCGPIO_R |= 0x10;         // activate port E
	while((SYSCTL_PRGPIO_R&0x10) == 0){}; // allow time for clock to stabilize        
  GPIO_PORTE_DIR_R |= 0x0F;          // make PE3-0 output heartbeats
  GPIO_PORTE_AFSEL_R &= ~0x0F;       // disable alt funct on PE3-0
  GPIO_PORTE_DEN_R |= 0x0F;          // enable digital I/O on PE3-0
  GPIO_PORTE_PCTL_R = ~0x0000FFFF;
  GPIO_PORTE_AMSEL_R &= ~0x0F;;      // disable analog functionality on PE
}
