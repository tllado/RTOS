// Profiler.h
// Runs on TM4C123 
// Provides digital output pins that can be attached to a scope to 
// create a profile of the system.
// Brandon Boesch
// Ce Wei
// January 31, 2016

#define PE0  (*((volatile unsigned long *)0x40024004))
#define PE1  (*((volatile unsigned long *)0x40024008))
#define PE2  (*((volatile unsigned long *)0x40024010))
#define PE3  (*((volatile unsigned long *)0x40024020))
	
//------------PortE_Init------------
// Initialize PortE pins PE0-3 as outputs
// Input: none
// Output: none
void PortE_Init(void);
