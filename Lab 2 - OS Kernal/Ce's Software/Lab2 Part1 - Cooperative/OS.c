// os.c for Lab1
// Ce Wei
 //1st - include
#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "OS.h"
#include "Interrupts.h"

 //2nd - extern PUBLIC variable and functions used in this c file
void DisableInterrupts(void); //Disable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void PLL_Init(uint32_t freq); 
void StartOS(void);						//in OSasm

 //3rd - define
#define NUMTHREADS  10       // maximum number of threads
#define STACKSIZE   100      // number of 32-bit words in stack

 //4th - struct, union, enum
struct TCB{
	int32_t *sp;       // pointer to stack (valid for threads not running
  struct TCB *next;  // linked-list pointer to next TCB block
};
typedef struct TCB TCBType;

 //5th - global variable and constants, might be public or private
TCBType tcbs[NUMTHREADS];
TCBType *RunPt;
int32_t Stacks[NUMTHREADS][STACKSIZE];
static uint32_t ThreadCounter;
static uint32_t FirstThread;
static uint32_t NextThreadPosition;
static uint32_t OS_SystemTime;
static uint32_t Period;

	//syntax of function pointer, define a function pointer
void (*PeriodicTask)(void);   // user function 

 //6th - private functions(static), only define and prototype
void SetInitialStack(int i);	
 //7th - public functions define IS IN header file!
 
 //8th - all functions implementation


void SetInitialStack(int i){
  tcbs[i].sp = &Stacks[i][STACKSIZE-16]; // thread stack pointer
																				 // MAKE the TCB looks like be suspend from previous states
  Stacks[i][STACKSIZE-1] = 0x01000000;   // thumb bit, PSR, last one in the array and lowest in the memory
																				 // The PC R15 will be set out of this FUNCTION
																				 // The R13 user stack will not be changed???
	Stacks[i][STACKSIZE-3] = 0x14141414;   // R14, LR
  Stacks[i][STACKSIZE-4] = 0x12121212;   // R12
  Stacks[i][STACKSIZE-5] = 0x03030303;   // R3
  Stacks[i][STACKSIZE-6] = 0x02020202;   // R2
  Stacks[i][STACKSIZE-7] = 0x01010101;   // R1
  Stacks[i][STACKSIZE-8] = 0x00000000;   // R0
  Stacks[i][STACKSIZE-9] = 0x11111111;   // R11
  Stacks[i][STACKSIZE-10] = 0x10101010;  // R10
  Stacks[i][STACKSIZE-11] = 0x09090909;  // R9
  Stacks[i][STACKSIZE-12] = 0x08080808;  // R8
  Stacks[i][STACKSIZE-13] = 0x07070707;  // R7
  Stacks[i][STACKSIZE-14] = 0x06060606;  // R6
  Stacks[i][STACKSIZE-15] = 0x05050505;  // R5
  Stacks[i][STACKSIZE-16] = 0x04040404;  // R4
}

// ******** OS_Init ************
// initialize operating system, disable interrupts until OS_Launch
// initialize OS controlled I/O: serial, ADC, systick, LaunchPad I/O and timers 
// input:  none
// output: none
//set up the systick timer, BUT not let it run in periodic mode
//disable interrupt
//OS_Launch will enable the core interrupt
void OS_Init(void){
	DisableInterrupts();								// disable interrupt during initialization
	PLL_Init(Bus80MHz);                 // set processor clock to 80 MHz
	//actually unnecessary for cooperative
  NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
  NVIC_SYS_PRI3_R =(NVIC_SYS_PRI3_R&0x00FFFFFF)|0xE0000000; // priority 7, least priority
	ThreadCounter = 0;					//reset counter to 0, no threads at beginning
	FirstThread = 0;						//start thread index is 0, ALTHOUGH it has not been added yet
	NextThreadPosition = 0;			//next thread will be added to the position 0
}



// ******** OS_InitSemaphore ************
// initialize semaphore 
// input:  pointer to a semaphore
// output: none
/*
void OS_InitSemaphore(Sema4Type *semaPt, long value){


}
*/

// ******** OS_Wait ************
// decrement semaphore 
// Lab2 spinlock
// Lab3 block if less than zero
// input:  pointer to a counting semaphore
// output: none
/*
void OS_Wait(Sema4Type *semaPt){


}
*/

// ******** OS_Signal ************
// increment semaphore 
// Lab2 spinlock
// Lab3 wakeup blocked thread if appropriate 
// input:  pointer to a counting semaphore
// output: none
/*
void OS_Signal(Sema4Type *semaPt){


}
*/

// ******** OS_bWait ************
// Lab2 spinlock, set to 0
// Lab3 block if less than zero
// input:  pointer to a binary semaphore
// output: none
/*
void OS_bWait(Sema4Type *semaPt){


}
*/

// ******** OS_bSignal ************
// Lab2 spinlock, set to 1
// Lab3 wakeup blocked thread if appropriate 
// input:  pointer to a binary semaphore
// output: none
/*
void OS_bSignal(Sema4Type *semaPt){


}
*/



//******** OS_AddThread *************** 
// add a FOREgound thread to the scheduler
// Inputs: pointer to a void/void foreground task
//         number of BYTES allocated for its stack
//         priority, 0 is highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// STACK SIZE must be divisable by 8 (aligned to double word boundary)
// In Lab 2, you can IGNORE both the stackSize and priority fields
// In Lab 3, you can IGNORE the stackSize fields
int OS_AddThread(void(*task)(void), 
   unsigned long stackSize, unsigned long priority){ int32_t status;
	status = StartCritical();
	// add the threads to the TCB arrays
	if(ThreadCounter == 0){
		//no threads now
		tcbs[0].next = &tcbs[0]; // 0 points to 0, self cycle
		SetInitialStack(0); Stacks[0][STACKSIZE-2] = (int32_t)(task); // initial for thread 0 and its PC
		RunPt = &tcbs[0];       // thread 0 will run first
		ThreadCounter++;				//should be 1
		NextThreadPosition++;		//should be 1, next will be added to the index 1
														//FirstThread still be 0, it only changed when it is removed
		EndCritical(status);
		return 1;               // successful
	}else{
		//there are some threads now, link them
		if(ThreadCounter >= NUMTHREADS){
			//TOO much threads than the max possible value
			EndCritical(status);
			return 0;
		}
		//still could hold, should add the new one in the cycle, break the cycle first(last point to first cycle)
	  tcbs[NextThreadPosition - 1].next = &tcbs[NextThreadPosition]; // break old cycle, last point to next
		tcbs[NextThreadPosition].next = &tcbs[FirstThread]; // next to the initial
		//set next one 
		SetInitialStack(NextThreadPosition); Stacks[NextThreadPosition][STACKSIZE-2] = (int32_t)(task); // initial for thread x and its PC
		ThreadCounter++;				//increament the counter of threads
		NextThreadPosition++;		//increament of the next position
														//FirstThread still be 0, it only changed when it is removed
		EndCritical(status);
		return 1;               // successful		
	}
	//EndCritical(status);
	//return 1;
}




//******** OS_Id *************** 
// returns the thread ID for the currently running thread
// Inputs: none
// Outputs: Thread ID, number greater than zero 
/*
unsigned long OS_Id(void){


}
*/


//******** OS_AddPeriodicThread *************** 
// add a BACKground periodic task
// typically this function receives the highest priority
// Inputs: pointer to a void/void BACKGROUND function
//         period given in system time units (12.5ns)--80Mhz frequency
//         priority 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// You are free to select the time resolution for this function
// It is assumed that the user task will run to completion and return
// This task can NOT spin, block, loop, sleep, or kill
// This task can call OS_Signal  OS_bSignal	 OS_AddThread
// This task does NOT have a Thread ID
// In lab 2, this command will be called 0 or 1 times
// In lab 2, the priority field can be ignored
// In lab 3, this command will be called 0 1 or 2 times
// In lab 3, there will be UP TO FOUR background threads, and this priority field 
//           determines the relative priority of these four threads
int OS_AddPeriodicThread(void(*task)(void), 
   unsigned long period, unsigned long priority){
	 //initialized the timer 1A and pass the function pointer to the initialization
  SYSCTL_RCGCTIMER_R |= 0x02;   // 0) activate TIMER1
//set the function pointer to the parameter function pointer
  PeriodicTask = task;          // user function
  TIMER1_CTL_R = 0x00000000;    // 1) disable TIMER1A during setup
  TIMER1_CFG_R = 0x00000000;    // 2) configure for 32-bit mode
  TIMER1_TAMR_R = 0x00000002;   // 3) configure for periodic mode, default down-count settings
  TIMER1_TAILR_R = period-1;    // 4) reload value
  TIMER1_TAPR_R = 0;            // 5) bus clock resolution
  TIMER1_ICR_R = 0x00000001;    // 6) clear TIMER1A timeout flag
  TIMER1_IMR_R = 0x00000001;    // 7) arm timeout interrupt
  NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|0x00008000; // 8) priority 4
// interrupts enabled in the main program after all devices initialized
// vector number 37, interrupt number 21
  NVIC_EN0_R = 1<<21;           // 9) enable IRQ 21 in NVIC  (TIMER 1A)
  TIMER1_CTL_R = 0x00000001;    // 10) enable TIMER1A	 
	//return 1 anyway in lab 1
	OS_SystemTime = 0;
	Period = period;
	return 1;
}
// The TIMER1A handler for the background task
void Timer1A_Handler(void){
	OS_SystemTime += Period;
  TIMER1_ICR_R = TIMER_ICR_TATOCINT;// acknowledge TIMER1A timeout
//The 1A_handler could only getthe periodictask in this file, it could not get the task
//the task is used to assign the function 
  (*PeriodicTask)();                // execute user task
}
	 


//******** OS_AddSW1Task *************** 
// add a background task to run whenever the SW1 (PF4) button is pushed
// Inputs: pointer to a void/void background function
//         priority 0 is the highest, 5 is the lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed that the user task will run to completion and return
// This task can not spin, block, loop, sleep, or kill
// This task can call OS_Signal  OS_bSignal	 OS_AddThread
// This task does not have a Thread ID
// In labs 2 and 3, this command will be called 0 or 1 times
// In lab 2, the priority field can be ignored
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
/*
int OS_AddSW1Task(void(*task)(void), unsigned long priority){


}
*/

//******** OS_AddSW2Task *************** 
// add a background task to run whenever the SW2 (PF0) button is pushed
// Inputs: pointer to a void/void background function
//         priority 0 is highest, 5 is lowest
// Outputs: 1 if successful, 0 if this thread can not be added
// It is assumed user task will run to completion and return
// This task can not spin block loop sleep or kill
// This task can call issue OS_Signal, it can call OS_AddThread
// This task does not have a Thread ID
// In lab 2, this function can be ignored
// In lab 3, this command will be called will be called 0 or 1 times
// In lab 3, there will be up to four background threads, and this priority field 
//           determines the relative priority of these four threads
/*
int OS_AddSW2Task(void(*task)(void), unsigned long priority){


}
*/

// ******** OS_Sleep ************
// place this thread into a dormant state
// input:  number of msec to sleep
// output: none
// You are free to select the time resolution for this function
// OS_Sleep(0) implements cooperative multitasking
/*
void OS_Sleep(unsigned long sleepTime){


}
*/

// ******** OS_Kill ************
// kill the currently running thread, release its TCB and stack
// input:  none
// output: none
/*
void OS_Kill(void){


}
*/



// ******** OS_Suspend ************
// suspend execution of currently running thread
// scheduler will CHOOSE NEXT THREAD to execute
// Can be used to implement cooperative multitasking 
// SAME FUNCTION as OS_Sleep(0)
// input:  none
// output: none
//trigger the systick handler but using the INT register
void OS_Suspend(void){
	NVIC_ST_CURRENT_R = 0;					//clear counter, also clear the COUNT bit in CTRL R, NOT trigger
	NVIC_INT_CTRL_R = 0x04000000;		//trigger SysTick by using the INT
}



// ******** OS_Fifo_Init ************
// Initialize the Fifo to be empty
// Inputs: size
// Outputs: none 
// In Lab 2, you can ignore the size field
// In Lab 3, you should implement the user-defined fifo size
// In Lab 3, you can put whatever restrictions you want on size
//    e.g., 4 to 64 elements
//    e.g., must be a power of 2,4,8,16,32,64,128
/*
void OS_Fifo_Init(unsigned long size){


}
*/

// ******** OS_Fifo_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting 
// Inputs:  data
// Outputs: true if data is properly saved,
//          false if data not saved, because it was full
// Since this is called by interrupt handlers 
//  this function can not disable or enable interrupts
/*
int OS_Fifo_Put(unsigned long data){


}
*/

// ******** OS_Fifo_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will spin/block if empty
// Inputs:  none
// Outputs: data 
/*
unsigned long OS_Fifo_Get(void){


}
*/

// ******** OS_Fifo_Size ************
// Check the status of the Fifo
// Inputs: none
// Outputs: returns the number of elements in the Fifo
//          greater than zero if a call to OS_Fifo_Get will return right away
//          zero or less than zero if the Fifo is empty 
//          zero or less than zero if a call to OS_Fifo_Get will spin or block
/*
long OS_Fifo_Size(void){


}
*/

// ******** OS_MailBox_Init ************
// Initialize communication channel
// Inputs:  none
// Outputs: none
/*
void OS_MailBox_Init(void){


}
*/

// ******** OS_MailBox_Send ************
// enter mail into the MailBox
// Inputs:  data to be sent
// Outputs: none
// This function will be called from a foreground thread
// It will spin/block if the MailBox contains data not yet received 
/*
void OS_MailBox_Send(unsigned long data){


}
*/

// ******** OS_MailBox_Recv ************
// remove mail from the MailBox
// Inputs:  none
// Outputs: data received
// This function will be called from a foreground thread
// It will spin/block if the MailBox is empty 
/*
unsigned long OS_MailBox_Recv(void){


}
*/

// ******** OS_Time ************
// return the system time 
// Inputs:  none
// Outputs: time in 12.5ns units, 0 to 4294967295
// The time resolution should be less than or equal to 1us, and the precision 32 bits
// It is ok to change the resolution and precision of this function as long as 
//   this function and OS_TimeDifference have the same resolution and precision 
/*
unsigned long OS_Time(void){


}
*/



// ******** OS_TimeDifference ************
// Calculates difference between two times
// Inputs:  two times measured with OS_Time
// Outputs: time difference in 12.5ns units 
// The time resolution should be less than or equal to 1us, and the precision at least 12 bits
// It is ok to change the resolution and precision of this function as long as 
//   this function and OS_Time have the same resolution and precision 
/*
unsigned long OS_TimeDifference(unsigned long start, unsigned long stop){


}
*/


//The periodic function should increase the global counter???
// ******** OS_ClearMsTime ************
// sets the system time to zero (from Lab 1)
// Inputs:  none
// Outputs: none
// You are free to change how this works
//set the global 32-bit counter to zero
void OS_ClearMsTime(void){
	OS_SystemTime = 0;
}

//simply return the global counter???
// ******** OS_MsTime ************
// reads the current time in msec (from Lab 1)
// Inputs:  none
// Outputs: time in ms units
// You are free to select the time resolution for this function
// It is ok to make the resolution to match the first call to OS_AddPeriodicThread
//return the global 32-bit counter(system time)
unsigned long OS_MsTime(void){
	return OS_SystemTime;
}


//******** OS_Launch *************** 
// start the SCHEDULER, ENABLE interrupts
// Inputs: number of 12.5ns clock cycles for each time slice
//         you may select the units of this parameter
// Outputs: none (does not return)
// In Lab 2, you can ignore the theTimeSlice field
// In Lab 3, you should implement the user-defined TimeSlice field
// It is ok to limit the range of theTimeSlice to match the 24-bit SysTick
void OS_Launch(unsigned long theTimeSlice){
																					//for cooperative, the systick will not genearte interrupt
																					//just start OS, the trigger function in the OS_suspend will handle
  //NVIC_ST_RELOAD_R = theTimeSlice - 1; // reload value
  //NVIC_ST_CTRL_R = 0x00000007; // enable, core clock and interrupt arm
  StartOS();                   // start on the first task
}
