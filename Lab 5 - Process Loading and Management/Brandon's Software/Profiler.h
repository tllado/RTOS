// Profiler.h
// Runs on TM4C123 
// Provides digital output pins on PortF that can be  
// attached to a scope to create a profile of the system.
// Brandon Boesch
// Ce Wei
// March 19th, 2016


// ******************** Constants ******************************************
#define PF1  (*((volatile unsigned long *)0x40025008))
#define PF2  (*((volatile unsigned long *)0x40025010))
#define PF3  (*((volatile unsigned long *)0x40025020))
//**************************************************************************


//********************Prototypes********************************************

//******** PortF_Init **************
// Initalize port F for profiling pins
// inputs:  none
// outputs: none
void PortF_Init(void);
