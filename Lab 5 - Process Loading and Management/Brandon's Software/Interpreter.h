// Interpreter.h
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// March 20th, 2016

#define CMD_LEN 10                           // maximum number of letters allowed for command names.
#define NUM_OF(x) (sizeof(x)/sizeof(x[0]))   // counts the # of available commands.
#define MAX_FILE_NAME 13

// ************* Stucts ****************************************************
typedef struct{
  const char *name;
  void (*func)(void);
	const char *tag;
}cmdTable;
// **************************************************************************


//------------Interpreter_Init-------
// Initialize interpreter
void Interpreter_Init(void);

//------------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void);

//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void);

//------------cmdMount-----------------
// Mount the SD card
void cmdMount(void);

//------------cmdRead-----------------
// Reads the contents of a file on the SD card.
void cmdRead(void);

//------------cmdHeap-----------------
// Heap manager
void cmdHeap(void);
	
//------------cmdELF-----------------
// Load an ELF file
void cmdELF(void);

//------------cmdMultELF-----------------
// Load multiple ELF files at once
void cmdMultELF(void);
