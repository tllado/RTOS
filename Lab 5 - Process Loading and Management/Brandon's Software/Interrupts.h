// Interrupts.h
// Runs on TM4C123 
// Provides interrupt functions' delarations. 
// Brandon Boesch
// Ce Wei
// January 23, 2016

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode
