// Lab5.c
// Runs on TM4C123
// Real Time Operating System for Lab 5
// Brandon Boesch
// Ce Wei
// March 29th, 2016


// ************ ST7735's microSD card interface*******************
// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground
// **************************************************************


//************ Timer Resources *******************
//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - OS_AddPeriodicThread(), Os_AddPeriodicThread() ; priority = 0
//  Timer2A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(), OS_MsTime() ; priority = none(no interrupts)
//  Timer3A - OS_Sleep() ; priority = 3
//  Timer5A - diskio.c ; priority = 2
//************************************************


#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "Lab5.h"
#include "OS.h"
#include "UART.h"
#include "ST7735.h"
#include "Profiler.h"
#include "Interpreter.h"
#include "Retarget.h"
#include "Interrupts.h"
#include "ff.h"
#include "Heap.h"
#include "elfloader-master/loader.h"


//******************** Globals ***********************************************
uint32_t IdleCount;       // counts how many iterations IdleTask() is ran.
//**************************************************************************


//******** IdleTask  *************** 
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none
void IdleTask(void){ 
  while(1) { 
		PF1 ^= 0x02;        // debugging profiler 
    IdleCount++;        // debugging 
		WaitForInterrupt(); // low-power mode
  }
}


//******** Interpreter **************
// Foreground thread 
// Accepts input from serial port, outputs to serial port
// inputs:  none
// outputs: none
void Interpreter(void){   
	Interpreter_Init();
  while(1){
		Interpreter_Parse();
	}
}

//******** InitialThreads **************
// Inital foreground threads created by parent process
// inputs:  none
// outputs: none
void InitialThreads(void){
	OS_AddThread(&Interpreter, 128, 2, RunProcess); 
  OS_AddThread(&IdleTask, 128, 7, RunProcess);       // runs when nothing useful to do
	OS_Kill();
}


int main(void){  
	// intialize globals
	IdleCount = 0;
	
  // initalize modules	
	OS_Init();                             // initialize OS, disable interrupts
	PortF_Init();                          // initialize Port F profiling

	// add parent process which creates initial foreground threads.  See InitalThread().
	OS_AddProcess(&InitialThreads,0,0,128,0); // adds parent process(process id = 0)
 
  OS_Launch(TIMESLICE);                  // doesn't return, interrupts enabled in here
  return 0;                              // this never executes
}
