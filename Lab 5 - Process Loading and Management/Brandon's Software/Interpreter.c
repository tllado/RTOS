// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ce Wei
// March 20th, 2016

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "Interpreter.h"
#include "UART.h"
#include "Lab5.h"
#include "ff.h"
#include "OS.h"
#include "Heap.h"
#include "elfloader-master/loader.h"
#include "ST7735.h"

static FATFS g_sFatFs;   // used for mounting SD Card


// *************Global Variables*****************************

cmdTable Commands[] = {        // CMD_LEN defines max command name length
	{"help",     cmdHelp,        "Displays all available commands."},
	{"mount",    cmdMount,       "Mount the SD card"},
	{"read",     cmdRead,        "Reads the contents of a file on the SD card."},
  {"heap",     cmdHeap,        "Heap manager."},
  {"elf",      cmdELF,         "Load an ELF file."},
	{"mult",     cmdMultELF,     "Load multiple ELF files at once."},
};

char cmd[CMD_LEN+1];   // string to store command line inputs. +1 for null.
// *********************************************************************


void Interpreter_Init(void){
	printf("\n\n\r******************************************************\n\r");
	printf("                  Welcome to bOS.\n\r");
	printf("         Type \"help\" for a list of commands.\n\r");
	printf("******************************************************\n\r");
}


//---------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void){
	printf("\n\r--Enter Command--> ");
	UART_InStringNL(cmd, CMD_LEN);  
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(cmd,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
	printf("Command not recognized.\n\r");
}

	
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
	printf("Below is a list of availble commands:\n\r");
	// output formating and display command name
	for(int i = 0; i < NUM_OF(Commands); i++){
		if(i+1 <10) printf("    ");                   
		else if(i+1 < 100) printf("   ");     
		printf("%d",i+1);                             
		printf(") ");                         
		// display command name
		printf("%s",(char*)Commands[i].name);  
    // output formating		
		for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
		  printf("-");                             
		}
		// display command tag
    printf("%s\n\r",(char*)Commands[i].tag);     
	}
}


//------------cmdMount-----------------
// Mount the SD card
void cmdMount(void){
	OS_bWait(&LCDFree);                   // capture resource
	FRESULT MountFresult = f_mount(&g_sFatFs, "", 0);
	OS_bSignal(&LCDFree);                 // release resource
	if(MountFresult){
    printf("\n\rMount error");
    while(1){};
  }
	else{
    printf("\n\rMount successful\n\r");
	}
}


//------------cmdRead-----------------
// Reads the contents of a file on the SD card.
void cmdRead(void){
	UINT successfulreads;
	FIL Handle;
	uint8_t buff, x, y;
  printf("Enter name of file to read (12 characters max): ");
	char fileName[MAX_FILE_NAME];            // string to store command line inputs.
	UART_InStringNL(fileName, MAX_FILE_NAME); 
	
	// open file for reading and read entirety of file
	FRESULT Fresult = f_open(&Handle, fileName, FA_READ);
	if(Fresult == FR_OK){
		printf("\n\r-----------------\n\r");                             // add divider; 
		Fresult = f_read(&Handle, &buff, 1, &successfulreads);
		// get a character in 'buff' and the number of successful reads in 'successfulreads'
		x = 0;                              // start in the first column
    y = 10;                             // start in the second row
	//	while((Fresult == FR_OK) && (successfulreads == 1) && (y <= 130)){
    while((Fresult == FR_OK) && (successfulreads == 1) && (y <= 255)){
      if(buff == '\n'){
        x = 0;                          // go to the first column (this seems implied)
        y = y + 10;                     // go to the next row
      } 
			else if(buff == '\r'){
        x = 0;                          // go to the first column
      } 
			else{                             // the character is printable, so print it
        printf("%c", buff);
        x = x + 6;                      // go to the next column
        if(x > 122){                    // reached the right edge of the screen
          x = 0;                        // go to the first column
          y = y + 10;                   // go to the next row
        }
      }
			Fresult = f_read(&Handle, &buff, 1, &successfulreads);
	  }
		// close the file
    Fresult = f_close(&Handle);
	  printf("\n\r-----------------\n\n\r");                           // add divider
		printf("End of file.\n\r"); 
	}
}


//------------cmdHeap-----------------
// Heap manager
void cmdHeap(void){
	static HeapPtrType HeapPointers[MAX_NUM_PROCESS];
	
  printf("Choose a command for the heap:\n\r");
	printf("    (1) Initialize - Initalize the heap to a clean state\n\r");
	printf("    (2) Directory - Display the available pointers\n\r");
	printf("    (3) Malloc - Allocate memory\n\r");
	printf("    (4) Free - Release memory back to the heap\n\r");
	printf("--Enter Command #--> ");
	uint32_t num = UART_InUDec();
	
	switch (num){
		case(1): {
			Heap_Init();
			printf("\n\rHeap initialization complete:\n\r");
			break;
		}
		
	  case(2): {
			printf("\n\rBelow are the available pointers:\n\r");
			for(int i = 0; i < MAX_NUM_PROCESS; i++){
				if(HeapPointers[i].alive){
					printf("    %s\n\r", HeapPointers[i].name);
				}
			}
			printf("Finished displaying available pointers.\n\r");
			break;
		}
		case(3): {
			// find available HeapPointer
      int i;
		  for(i = 0; i < MAX_NUM_PROCESS; i++){
				if(HeapPointers[i].alive == 0){
					break;
				}
			}
			// if(no more HeapPointer are available), then error
			if(i == MAX_NUM_PROCESS){
				printf("\n\rNo more pointers available for heap");
			}
			// allocate new pointer
			else{
				// bring pointer alive
				HeapPointers[i].alive = 1;
				
				// give pointer a name
				printf("\n\rEnter the name of the new heap pointer (12 characters max): ");
				char ptrName[MAX_PTR_NAME];            // string to store command line inputs.
				UART_InStringNL(ptrName, MAX_PTR_NAME); 
				strcpy(HeapPointers[i].name, ptrName);
				
				// assign the size of the memory block
				printf("\n\rHow large do you want the memory allocation to be (in bytes): ");
        uint32_t num = UART_InUDec();
			  HeapPointers[i].size = num;
				
				// call Malloc and assign pointer to HeapPointers
				int8_t *ptr = (int8_t*) Heap_Malloc(4 * sizeof(int8_t));
				HeapPointers[i].ptr = ptr;
				
				printf("\n\rNew pointer sucessfully allocaed in memory.\n\r");
				break;
			}
		}
		case(4): {
			printf("\n\rEnter the name of the pointer to free(12 characters max): ");
			char ptrName[MAX_PTR_NAME];            // string to store command line inputs.
		  UART_InStringNL(ptrName, MAX_PTR_NAME); 
			// find pointer in HeapPointers
			int i;
			for(i = 0; i < MAX_NUM_PROCESS; i++){
				if(!strcmp(HeapPointers[i].name, ptrName) && HeapPointers[i].alive){
					break;  // pointer found
				}
			}
		  // if(pointer not found), then error
			if(i == MAX_NUM_PROCESS){
				printf("\n\rThere are no pointers with that name.\n\r");
			}
			else{
			  Heap_Free(ptrName);
				HeapPointers[i].alive = 0;
				printf("\n\rPointer has been freed.\n\r");
			}
			break;
		}
		default: {
			printf("\n\rImproper selection\n\r"); return;
		}
	}		
}


//------------cmdELF-----------------
// Load an ELF file
void cmdELF(void){
	// automatically mount SD card
	OS_bWait(&LCDFree);                   // capture resource
	FRESULT MountFresult = f_mount(&g_sFatFs, "", 0);
	OS_bSignal(&LCDFree);                 // release resource
	if(MountFresult){
    printf("\n\rMount error");
    while(1){};
  }
	
	// choose ELF file to load
	printf("Enter name of ELF file to load (12 characters max): ");
	char fileName[MAX_FILE_NAME];            // string to store command line inputs.
	UART_InStringNL(fileName, MAX_FILE_NAME); 
	
	// load ELF file
	static const ELFSymbol_t symtab[] = {
    { "ST7735_Message", ST7735_Message }
  };
	
	ELFEnv_t env = { symtab, 1 };
	int result = exec_elf(fileName, &env);
  if (result == -1){
		printf("\n\rError loading ELF file\n\r");
  }
	else{
		printf("\n\rELF file loaded sucessfully\n\r");
	}
}

//------------cmdMultELF-----------------
// Load multiple ELF files at once
void cmdMultELF(void){

	// choose ELF file to load
	printf("Enter name of ELF file to load (12 characters max): ");
	char fileName[MAX_FILE_NAME];            // string to store command line inputs.
	UART_InStringNL(fileName, MAX_FILE_NAME); 
	
	printf("Enter how many times would you like to open the file: ");
	uint32_t num = UART_InUDec();
	printf("\n\n\r");
	
	// load ELF file
	static const ELFSymbol_t symtab[] = {
    { "ST7735_Message", ST7735_Message }
  };
	ELFEnv_t env = { symtab, 1 };
	
	int i = 1;
	while(num){
		// mount SD card in between every call to exec_elf
	  OS_bWait(&LCDFree);                   // capture resource
	  FRESULT MountFresult = f_mount(&g_sFatFs, "", 0);
	  OS_bSignal(&LCDFree);                 // release resource
	  if(MountFresult){
      printf("\n\rMount error");
      while(1){};
    }
		
		// load elf file
		int result = exec_elf(fileName, &env);
    if (result == -1){
	  	printf("\n\rError loading ELF file, attempt # %u\n\r", i);
    }
	  else{
		  printf("\n\rELF file loaded sucessfully, attempt # %u\n\r", i);
	  }
		num--;
		i++;
	}
	printf("Multiple loading of elf file complete.\n\r");
}

