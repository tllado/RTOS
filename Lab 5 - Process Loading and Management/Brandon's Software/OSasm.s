;/*****************************************************************************/
; OSasm.s: low-level OS commands, written in assembly                       */
; Runs on LM4F120/TM4C123
; A very simple real time operating system with minimal features.
; Daniel Valvano
; January 29, 2015
;
; This example accompanies the book
;  "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
;  ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015
;
;  Programs 4.4 through 4.12, section 4.2
;
;Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
;    You may use, edit, run or distribute this file
;    as long as the above copyright notice remains
; THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
; OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
; VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
; OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
; For more information about my classes, my research, and my books, see
; http://users.ece.utexas.edu/~valvano/
; */


PE3          EQU 0x40024020   ; access PE3 for profiling
	
	    AREA DATA
Last    SPACE 4               ; allocate a global of 4 bytes. Last time on Timer2
	
	
        AREA |.text|, CODE, READONLY, ALIGN=2
        THUMB
        REQUIRE8
        PRESERVE8

        EXTERN  RunPt              ; currently running thread
		EXTERN  RunProcess         ; currently running process
		EXTERN  PriorityLists      ; array of different priority's linked lists	
		EXTERN  MaxNumPri          ; Maximum number of priority levels	
		EXTERN  Scheduler          ; passed in from OS.c. Determines which scheduler to run (1 = Priority, 0 = RoundRobin)	
			
		EXPORT  StartOS_ASM
        EXPORT  SysTick_Handler
		EXPORT  OS_Wait_ASM
		EXPORT	OS_Signal_ASM
		EXPORT  SVC_Handler
			
	    EXTERN  OS_ProcessId
		EXTERN	OS_Id
        EXTERN  OS_Sleep
		EXTERN	OS_Kill
		EXTERN	OS_Time
		EXTERN	OS_AddThread



;------SysTick_Handler----------
;Determines which scheduler should be ran, RoundRobin or Priority
SysTick_Handler                ; 1)    saves R0-R3,R12,LR,PC,PSR, prevents interrupts, 
	CPSID   I                  ; 1.1)  prevent interrupt during switch
	PUSH    {R4-R11}           ; 1.2)  save remaining regs r4-11								
	LDR     R0, =Scheduler     ; 1.3)  R0 = pointer to Scheduler variable					   
	LDR     R0, [R0]           ; 1.4)  R0 = Scheduler variable
    CMP     R0, #0             ; 1.5)  check if Scheduler variable = 0
    BEQ     RoundRobin         ; 1.6)  if R0 = 0, use RoundRobin scheduler
    B       Priority	       ; 1.7)  else, use Priority scheduler
	
	
;------Priority----------
; A priority schedular that performs a context switch on the thread
; with the highest tempPriority.  If multiple threads have the same tempPriority
; then a round robin algorithm is used to schedule the next thread.
; Assumes all priority levels have their own linked list of active threads.
; This implementation uses aging to prevent starvation of threads.					  
Priority                       ; 1)    Priority Scheduler. Initializes registers for use, increment old RunPt's RunCnt.
	LDR     R0, =RunPt         ; 1.1)  R0= pointer to RunPt, old thread
    LDR     R1, [R0]           ; 1.2)  R1 = RunPt
    LDR     R6, [R1, #20]      ; 1.3)  R6 = RunPt->RunCnt
	ADD     R6, #1             ; 1.4)  R6 = RunCnt+1
	STR     R6, [R1, #20]      ; 1.5)  RunPt->RunCnt = RunPt->RunCnt + 1
	LDR     R2, [R1, #16]      ; 1.6)  R2 = RunPt->tempPriority
	LDR     R3, =PriorityLists ; 1.7)  R3 = pointer to PriorityLists
	MOV     R4, #0             ; 1.8)  R4 = 0. Counter for cycling through priority levels. Starts at highest level
	LDR     R5, [R3,R4,LSL #3] ; 1.8)  R5 = PriorityLists[R4].Value.  LSL #3 = size of element in PriorityList       
	
CheckList                      ; 2)    Find new RunPt by scanning though all priority lists 
                               ;       until a thread is found. If current list's priority == RunPt->tempPriority 
							   ;       priority, then use RoundRobin.
	CMP     R5, #0 			   ; 2.1)  determine how many threads are in current priority list.
	BEQ     NextList           ; 2.2)  if current priority level has zero threads, move to next list	
	CMP     R4, R2             ; 2.3)  check if(current list's priority == RunPt->tempPriority)
	BEQ     SameList           ; 2.4)  if (current list's priority == RunPt->tempPriority), then use RoundRobin on same list
    B       Update             ; 2.5)  else, move RunPt to this priority level

NextList                       ; 3)    Move to next priority level, and find its priority value
    ADD     R4, #1             ; 3.1)  else, move to next priority level
	LDR     R5, [R3,R4,LSL #3] ; 3.2)  R5 = PriorityLists[R4].Value.  LSL #3 = size of element in PriorityList       
    B       CheckList          ; 3.3)  check if this priority list has any elements to choose
	
SameList                       ; 4)    Round robin scheduling on current priority list	
	STR     SP, [R1]           ; 4.1)  Save SP into TCB
    LDR     R1, [R1, #4]       ; 4.2)  R1 = RunPt->next
    STR     R1, [R0]           ; 4.3)  RunPt = R1
    LDR     SP, [R1]           ; 4.4)  new thread SP; SP = RunPt->sp;
	B       Finish             ; 4.5)  finalize context switch
	
Update                         ; 5)    Sets RunPt to first active thread in priority level stored in R4
    STR     SP, [R1]           ; 5.1)  Save SP into TCB
	LSL     R4, #3             ; 5.2)  find offset for priority list based of priority level, R4      
	ADD     R4, #4             ; 5.3)  create offset to list
	LDR     R1, [R3, R4]       ; 5.4)  R1 = PriorityLists[RunPt->tempPriority].list
	STR     R1, [R0]           ; 5.5)  RunPt = R1
    LDR     SP, [R1]           ; 5.6)  new thread SP; SP = RunPt->sp;

Finish                         ; 6)    Updates RunProcess. Restore reg R4-11, reenable interrupts, and BX LR
	LDR     R5, [R1, #28]      ; 6.1)  R5 = RunPt->ProcesssId for new thread
	LDR     R6, =RunProcess    ; 6.2)  R6 = pointer to RunProcess
	STR     R5, [R6]           ; 6.3)  RunProcess = RunPt->ProcessId
	POP     {R4-R11}           ; 6.4)  restore regs r4-11
    CPSIE   I                  ; 6.5)  tasks run with interrupts enabled
    BX      LR                 ; 6.6)  restore R0-R3,R12,LR,PC,PSR
	
	

;------RoundRobin----------
; A roundrobin schedular that performs a context switch on the next active thread
; Assumes all priority levels have their own linked list of active threads.
; This implementation does not use aging since starving is not an issue.

; if(RunPt->next == PriorityLists[RunPt->tempPriority].list){	
;   int testP = RunPt->tempPriority + 1;
;   while(PriorityLists[testP].Value == 0){
;     testP++;
;     testP % 8;
;   }
;   RunPt = PriorityLists[testP].list;
; }
; else {
;   RunPt = RunPt->next;
; }
;	                 
RoundRobin                     ; 1)    initializes registers for use
	LDR     R0, =RunPt         ; 1.1)  R0= pointer to RunPt, old thread
    LDR     R1, [R0]           ; 1.2)  R1 = RunPt
	LDR     R2, [R1, #16]      ; 1.3)  R2 = RunPt->tempPriority
	LDR     R3, =PriorityLists ; 1.4)  R3 = pointer to PriorityLists
	LDR     R4, [R1, #4]       ; 1.5)  R4 = RunPt->next
	LSL     R5, R2, #3         ; 1.6)  LSL #3 = size of element in PriorityList
	ADD     R5, R3             ; 1.7)  R5 = &PriorityLists[RunPt->tempPriority]
	LDR     R6, [R5, #4]       ; 1.8)  R6 = PriorityLists[RunPt->tempPriority].list
	LDR     R7, =MaxNumPri     ; 1.9)  R7 = pointer to MaxNumPri
	LDR     R7, [R7]           ; 1.10) R7 = MaxNumPri

CheckList2                     ; 2)    Find new RunPt. If at the end of current priority list scan
                               ;       through other priority lists until an active thread is found.
							   ;       Else just use next pointer.
	CMP     R4, R6             ; 2.1)  RunPt->next == PriorityLists[RunPt->tempPriority].list
    BEQ     NextList2	       ; 2.2)  If true, you have reached the end of this priority level's list.  Move to a new list
    B       SameList2          ; 2.3)  Else perform roundrobin on current list

NextList2                      ; 3  )  Cycle through Priority lists until an active TCB is found. 
                               ;       Once found update RunPt and stack.
    ADD     R2, #1             ; 3.1)  R2 = RunPt->tempPriority + 1
	CMP     R2, R7             ; 3.2)  R3 == MaxNumPri
	IT      EQ                 ; 3.3)  If true, R2 = 0. Start looking at the top of priority list
	MOVEQ   R2, #0             ; 3.4)  ----------see previous instruction comment-------------
	LDR     R8, [R3,R2,LSL #3] ; 3.5)  R8 = PriorityLists[R2].Value.  LSL #3 = size of element in PriorityList 
    CMP     R8, #0             ; 3.6)  PriorityLists[R2].Value == 0
	BEQ     NextList2          ; 3.7)  If true, try for next priority level's list
	                           ; 3.8)  else, RunPt = PriorityLists[R2].list
	LSL     R5, R2, #3         ; 3.8)  LSL #3 = size of element in PriorityList
	ADD     R5, R3             ; 3.9)  R5 = &PriorityLists[RunPt->tempPriority]
	LDR     R6, [R5, #4]       ; 3.10) R6 = PriorityLists[RunPt->tempPriority].list
    STR     SP, [R1]           ; 3.11) Save SP into TCB
	STR     R6, [R0]           ; 3.12) RunPt = RunPt->next
	LDR     SP, [R6]           ; 3.13) new thread SP; SP = RunPt->sp;
	B       Finish2            ; 3.14) finalize context switch
	
SameList2                      ; 4)    Round robin scheduling on current priority list	
    STR     SP, [R1]           ; 4.1)  Save SP into TCB
    STR     R4, [R0]           ; 4.3)  RunPt = RunPt->next
	LDR     SP, [R4]           ; 4.4)  new thread SP; SP = RunPt->sp;

Finish2                        ; 5)    Update RunProcess. Restore reg R4-11, reenable interrupts, and BX LR
    LDR     R5, [R1, #28]      ; 5.1)  R5 = RunPt->ProcesssId for new thread
	LDR     R6, =RunProcess    ; 5.2)  R6 = pointer to RunProcess
	STR     R5, [R6]           ; 5.3)  RunProcess = RunPt->ProcessId
	POP     {R4-R11}           ; 5.4)  restore regs R4-11
    CPSIE   I                  ; 5.5)  tasks run with interrupts enabled
    BX      LR                 ; 5.6)  restore R0-R3,R12,LR,PC,PSR



;------SysTick_Handler2----------
; A roundrobin schedular that performs a context switch. Assumes all 
; active threads are in one linked list.
SysTick_Handler2               ; 1)   Saves R0-R3,R12,LR,PC,PSR
    CPSID   I                  ; 2)   Prevent interrupt during switch
    PUSH    {R4-R11}           ; 3)   Save remaining regs r4-11
    ;LDR     R4, =PE3           ; 4.1) Toggle PE3 twice. R4 = &PE3
    ;LDR     R5, [R4]           ; 4.2) R5 = PE3
    ;EOR     R5, #0x08		   ; 4.3) R5 ^= 0x8
	;STR     R5, [R4]           ; 4.4) PE3 ^= 0x8
	;EOR     R5, #0x08          ; 4.5) R5 ^= 0x8
	;STR     R5, [R4]           ; 4.6) PE3 ^= 0x8
    LDR     R0, =RunPt         ; 5.1) R0=pointer to RunPt, old thread
    LDR     R1, [R0]           ; 5.2) R1 = RunPt
    STR     SP, [R1]           ; 6)   Save SP into TCB
    LDR     R1, [R1, #4]       ; 7.1) R1 = RunPt->next
    STR     R1, [R0]           ; 7.2) RunPt = R1
    LDR     SP, [R1]           ; 8)   new thread SP; SP = RunPt->sp;
	;EOR     R5, #0x08		    ; 9.1) Toggle PE3. R5 ^= 0x8
	;STR     R5, [R4]           ; 9.2) PE3 ^= 0x8
    POP     {R4-R11}           ; 10)  restore regs r4-11
    CPSIE   I                  ; 11)  tasks run with interrupts enabled
    BX      LR                 ; 12)  restore R0-R3,R12,LR,PC,PSR



StartOS_ASM
    LDR     R0, =RunPt         ; currently running thread
    LDR     R2, [R0]           ; R2 = value of RunPt
    LDR     SP, [R2]           ; new thread SP; SP = RunPt->stackPointer;
    POP     {R4-R11}           ; restore regs r4-11
    POP     {R0-R3}            ; restore regs r0-3
    POP     {R12}
    POP     {LR}               ; discard LR from initial stack
    POP     {LR}               ; start location
    POP     {R1}               ; discard PSR
    CPSIE   I                  ; Enable interrupts at processor level
    BX      LR                 ; start first thread


OS_Wait_ASM
	LDREX	R1, [R0]		;Counter
	SUBS	R1, #1			;Counter - 1
	ITT		PL				;OK if >= 0
	STREXPL	R2, R1, [R0]	;try update
	CMPPL	R2, #0			;succeed?
	BNE		OS_Wait_ASM		;no, try again
	BX		LR				
	
	
OS_Signal_ASM
	LDREX	R1, [R0]		;Counter
	ADD		R1, #1			;Counter + 1
	STREX	R2, R1, [R0]	;try update
	CMP		R2, #0			;succeed?
	BNE		OS_Signal_ASM	;try again
	BX		LR


SVC_Handler                 ; saves R0-R3,R12,LR,PC,PSR
    LDR    R12, [SP, #24]   ; Return address
	LDRH   R12, [R12, #-2]  ; Location of SVC instruction that triggered exception. Instruction is 2 bytes
	BIC    R12, #0xFF00     ; Extract SVC ID in R12
	LDM    SP, {R0-R3}      ; Copy any function paramters
	PUSH   {LR}             ; store LR
                             
    CMP	   R12, #0          ; begin switch statement
    BEQ    ProcessId
    CMP	   R12, #1
    BEQ    Id
	CMP	   R12, #2
    BEQ    Kill
	CMP	   R12, #3
    BEQ    Sleep
	CMP	   R12, #4
    BEQ    Time
	CMP	   R12, #5
    BEQ    AddThread


ProcessId
    BL     OS_ProcessId
	B      CaseEnd
Id
	BL     OS_Id
	B      CaseEnd
Kill
	BL     OS_Kill
	B      CaseEnd
Sleep
	BL     OS_Sleep
	B      CaseEnd	
Time
	BL     OS_Time
	B      CaseEnd
AddThread
	BL     OS_AddThread
	B      CaseEnd
	            
CaseEnd
    POP    {LR}             ; Retrive original LR
	STR    R0, [SP]         ; Store return value
	BX     LR               ; Return from exception
	
    ALIGN
    END

